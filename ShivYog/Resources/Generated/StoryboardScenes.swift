// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length implicit_return

// MARK: - Storyboard Scenes

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
internal enum StoryboardScene {
  internal enum Authentication: StoryboardType {
    internal static let storyboardName = "Authentication"

    internal static let initialScene = InitialSceneType<UIKit.UINavigationController>(storyboard: Authentication.self)

    internal static let createPasswordVC = SceneType<ShivYog.CreatePasswordVC>(storyboard: Authentication.self, identifier: "CreatePasswordVC")

    internal static let forgotPasswordVC = SceneType<ShivYog.ForgotPasswordVC>(storyboard: Authentication.self, identifier: "ForgotPasswordVC")

    internal static let loggedInDevicesVC = SceneType<ShivYog.LoggedInDevicesVC>(storyboard: Authentication.self, identifier: "LoggedInDevicesVC")

    internal static let navLogin = SceneType<UIKit.UINavigationController>(storyboard: Authentication.self, identifier: "NavLogin")

    internal static let noNetworkVC = SceneType<ShivYog.NoNetworkVC>(storyboard: Authentication.self, identifier: "NoNetworkVC")

    internal static let signInInitialVC = SceneType<ShivYog.SignInInitialVC>(storyboard: Authentication.self, identifier: "SignInInitialVC")

    internal static let signInVC = SceneType<ShivYog.SignInVC>(storyboard: Authentication.self, identifier: "SignInVC")

    internal static let successVC = SceneType<ShivYog.SuccessVC>(storyboard: Authentication.self, identifier: "SuccessVC")

    internal static let verificationVC = SceneType<ShivYog.VerificationVC>(storyboard: Authentication.self, identifier: "VerificationVC")

    internal static let walkThroughVC = SceneType<ShivYog.WalkThroughVC>(storyboard: Authentication.self, identifier: "WalkThroughVC")
  }
  internal enum Categories: StoryboardType {
    internal static let storyboardName = "Categories"

    internal static let initialScene = InitialSceneType<UIKit.UINavigationController>(storyboard: Categories.self)

    internal static let categoriesVC = SceneType<ShivYog.CategoriesVC>(storyboard: Categories.self, identifier: "CategoriesVC")

    internal static let selectCategoryVC = SceneType<ShivYog.SelectCategoryVC>(storyboard: Categories.self, identifier: "SelectCategoryVC")
  }
  internal enum Home: StoryboardType {
    internal static let storyboardName = "Home"

    internal static let initialScene = InitialSceneType<UIKit.UINavigationController>(storyboard: Home.self)

    internal static let audioPlayerVC = SceneType<ShivYog.AudioPlayerVC>(storyboard: Home.self, identifier: "AudioPlayerVC")

    internal static let homeVC = SceneType<ShivYog.HomeVC>(storyboard: Home.self, identifier: "HomeVC")

    internal static let notificationsVC = SceneType<ShivYog.NotificationsVC>(storyboard: Home.self, identifier: "NotificationsVC")

    internal static let pdfViewerVC = SceneType<ShivYog.PDFViewerVC>(storyboard: Home.self, identifier: "PDFViewerVC")

    internal static let productItemsVC = SceneType<ShivYog.ProductItemsVC>(storyboard: Home.self, identifier: "ProductItemsVC")

    internal static let productListingVC = SceneType<ShivYog.ProductListingVC>(storyboard: Home.self, identifier: "ProductListingVC")

    internal static let searchVC = SceneType<ShivYog.SearchVC>(storyboard: Home.self, identifier: "SearchVC")

    internal static let tabVC = SceneType<ShivYog.TabVC>(storyboard: Home.self, identifier: "TabVC")

    internal static let videoPlayerVC = SceneType<ShivYog.VideoPlayerVC>(storyboard: Home.self, identifier: "VideoPlayerVC")
  }
  internal enum LaunchScreen: StoryboardType {
    internal static let storyboardName = "LaunchScreen"

    internal static let initialScene = InitialSceneType<UIKit.UIViewController>(storyboard: LaunchScreen.self)
  }
  internal enum MySpace: StoryboardType {
    internal static let storyboardName = "MySpace"

    internal static let initialScene = InitialSceneType<UIKit.UINavigationController>(storyboard: MySpace.self)

    internal static let createPlaylistVC = SceneType<ShivYog.CreatePlaylistVC>(storyboard: MySpace.self, identifier: "CreatePlaylistVC")

    internal static let downloadOptionsVC = SceneType<ShivYog.DownloadOptionsVC>(storyboard: MySpace.self, identifier: "DownloadOptionsVC")

    internal static let myJourneyVC = SceneType<ShivYog.MyJourneyVC>(storyboard: MySpace.self, identifier: "MyJourneyVC")

    internal static let mySpaceVC = SceneType<ShivYog.MySpaceVC>(storyboard: MySpace.self, identifier: "MySpaceVC")

    internal static let playlistOptionsVC = SceneType<ShivYog.PlaylistOptionsVC>(storyboard: MySpace.self, identifier: "PlaylistOptionsVC")

    internal static let playlistVC = SceneType<ShivYog.PlaylistVC>(storyboard: MySpace.self, identifier: "PlaylistVC")

    internal static let selectTimeVC = SceneType<ShivYog.SelectTimeVC>(storyboard: MySpace.self, identifier: "SelectTimeVC")
  }
  internal enum Profile: StoryboardType {
    internal static let storyboardName = "Profile"

    internal static let initialScene = InitialSceneType<UIKit.UINavigationController>(storyboard: Profile.self)

    internal static let changeEmailNameNumberVC = SceneType<ShivYog.ChangeEmailNameNumberVC>(storyboard: Profile.self, identifier: "ChangeEmailNameNumberVC")

    internal static let changePasswordVC = SceneType<ShivYog.ChangePasswordVC>(storyboard: Profile.self, identifier: "ChangePasswordVC")

    internal static let contactUsVC = SceneType<ShivYog.ContactUsVC>(storyboard: Profile.self, identifier: "ContactUsVC")

    internal static let inAppPurchaseVC = SceneType<ShivYog.InAppPurchaseVC>(storyboard: Profile.self, identifier: "InAppPurchaseVC")

    internal static let paymentsVC = SceneType<ShivYog.PaymentsVC>(storyboard: Profile.self, identifier: "PaymentsVC")

    internal static let preferencesVC = SceneType<ShivYog.PreferencesVC>(storyboard: Profile.self, identifier: "PreferencesVC")

    internal static let profileVC = SceneType<ShivYog.ProfileVC>(storyboard: Profile.self, identifier: "ProfileVC")

    internal static let redeemGiftVC = SceneType<ShivYog.RedeemGiftVC>(storyboard: Profile.self, identifier: "RedeemGiftVC")

    internal static let rewardVC = SceneType<ShivYog.RewardVC>(storyboard: Profile.self, identifier: "RewardVC")

    internal static let sendGiftVC = SceneType<ShivYog.SendGiftVC>(storyboard: Profile.self, identifier: "SendGiftVC")

    internal static let webContentVC = SceneType<ShivYog.WebContentVC>(storyboard: Profile.self, identifier: "WebContentVC")
  }
  internal enum Store: StoryboardType {
    internal static let storyboardName = "Store"

    internal static let initialScene = InitialSceneType<UIKit.UINavigationController>(storyboard: Store.self)

    internal static let myStoreVC = SceneType<ShivYog.MyStoreVC>(storyboard: Store.self, identifier: "MyStoreVC")

    internal static let paymentResponseVC = SceneType<ShivYog.PaymentResponseVC>(storyboard: Store.self, identifier: "PaymentResponseVC")

    internal static let productDescriptionVC = SceneType<ShivYog.ProductDescriptionVC>(storyboard: Store.self, identifier: "ProductDescriptionVC")
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

// MARK: - Implementation Details

internal protocol StoryboardType {
  static var storyboardName: String { get }
}

internal extension StoryboardType {
  static var storyboard: UIStoryboard {
    let name = self.storyboardName
    return UIStoryboard(name: name, bundle: BundleToken.bundle)
  }
}

internal struct SceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type
  internal let identifier: String

  internal func instantiate() -> T {
    let identifier = self.identifier
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }

  @available(iOS 13.0, tvOS 13.0, *)
  internal func instantiate(creator block: @escaping (NSCoder) -> T?) -> T {
    return storyboard.storyboard.instantiateViewController(identifier: identifier, creator: block)
  }
}

internal struct InitialSceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type

  internal func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }

  @available(iOS 13.0, tvOS 13.0, *)
  internal func instantiate(creator block: @escaping (NSCoder) -> T?) -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController(creator: block) else {
      fatalError("Storyboard \(storyboard.storyboardName) does not have an initial scene.")
    }
    return controller
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
