// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ColorAsset.Color", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetColorTypeAlias = ColorAsset.Color
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name



internal extension UIImage {
  static let backArrowWhite = ImageAsset(name: "BackArrowWhite")
  static let downArrowBlack = ImageAsset(name: "DownArrowBlack")
  static let downArrowWhite = ImageAsset(name: "DownArrowWhite")
  static let eyeBlackHidden = ImageAsset(name: "EyeBlackHidden")
  static let eyeBlackShow = ImageAsset(name: "EyeBlackShow")
  static let eyeHidden = ImageAsset(name: "EyeHidden")
  static let eyeShow = ImageAsset(name: "EyeShow")
  static let gradient = ImageAsset(name: "Gradient")
  static let logoutDevices = ImageAsset(name: "LogoutDevices")
  static let mailBlack = ImageAsset(name: "MailBlack")
  static let mobile = ImageAsset(name: "Mobile")
  static let mobileBlack = ImageAsset(name: "MobileBlack")
  static let otpBG = ImageAsset(name: "OtpBG")
  static let selectedPage = ImageAsset(name: "SelectedPage")
  static let signInBG = ImageAsset(name: "SignInBG")
  static let signInLine = ImageAsset(name: "SignInLine")
  static let signInLogo = ImageAsset(name: "SignInLogo")
  static let signinImage = ImageAsset(name: "SigninImage")
  static let splash = ImageAsset(name: "Splash")
  static let success = ImageAsset(name: "Success")
  static let successBG = ImageAsset(name: "SuccessBG")
  static let topBG = ImageAsset(name: "TopBG")
  static let unselectedPage = ImageAsset(name: "UnselectedPage")
  static let walkBG = ImageAsset(name: "WalkBG")
  static let walkOne = ImageAsset(name: "WalkOne")
  static let walkThree = ImageAsset(name: "WalkThree")
  static let walkTwo = ImageAsset(name: "WalkTwo")
  static let bannerOne = ImageAsset(name: "BannerOne")
  static let filterWhite = ImageAsset(name: "FilterWhite")
  static let freeTag = ImageAsset(name: "FreeTag")
  static let unlock = ImageAsset(name: "Unlock")
  static let demoFour = ImageAsset(name: "DemoFour")
  static let demoOne = ImageAsset(name: "DemoOne")
  static let demoThree = ImageAsset(name: "DemoThree")
  static let demoTwo = ImageAsset(name: "DemoTwo")
  static let descriptionOne = ImageAsset(name: "DescriptionOne")
  static let descriptionTwo = ImageAsset(name: "DescriptionTwo")
  static let addToPlaylist = ImageAsset(name: "AddToPlaylist")
  static let cancel = ImageAsset(name: "Cancel")
  static let cancelwithBG = ImageAsset(name: "CancelwithBG")
  static let categoriesSelected = ImageAsset(name: "CategoriesSelected")
  static let categoriesUnSelected = ImageAsset(name: "CategoriesUnSelected")
  static let deleteBlack = ImageAsset(name: "DeleteBlack")
  static let download = ImageAsset(name: "Download")
  static let edit = ImageAsset(name: "Edit")
  static let error = ImageAsset(name: "Error")
  static let favourite = ImageAsset(name: "Favourite")
  static let favouriteHome = ImageAsset(name: "FavouriteHome")
  static let favouriteLite = ImageAsset(name: "FavouriteLite")
  static let favouriteUnSelected = ImageAsset(name: "FavouriteUnSelected")
  static let favouriteUnSelectedLite = ImageAsset(name: "FavouriteUnSelectedLite")
  static let favouriteUnselectedHome = ImageAsset(name: "FavouriteUnselectedHome")
  static let filter = ImageAsset(name: "Filter")
  static let homeSelected = ImageAsset(name: "HomeSelected")
  static let homeUnSelected = ImageAsset(name: "HomeUnSelected")
  static let more = ImageAsset(name: "More")
  static let moreWhite = ImageAsset(name: "MoreWhite")
  static let mySpaceSelected = ImageAsset(name: "MySpaceSelected")
  static let mySpaceUnSelected = ImageAsset(name: "MySpaceUnSelected")
  static let notification = ImageAsset(name: "Notification")
  static let notificationWithBG = ImageAsset(name: "NotificationWithBG")
  static let pdfWhite = ImageAsset(name: "PDFWhite")
  static let pdfWhiteSmall = ImageAsset(name: "PDFWhiteSmall")
  static let playWhite = ImageAsset(name: "PlayWhite")
  static let premium = ImageAsset(name: "Premium")
  static let profileSelected = ImageAsset(name: "ProfileSelected")
  static let profileUnSelected = ImageAsset(name: "ProfileUnSelected")
  static let searchBlack = ImageAsset(name: "SearchBlack")
  static let searchFull = ImageAsset(name: "SearchFull")
  static let searchGray = ImageAsset(name: "SearchGray")
  static let searchPlay = ImageAsset(name: "SearchPlay")
  static let shareBlack = ImageAsset(name: "ShareBlack")
  static let storeSelected = ImageAsset(name: "StoreSelected")
  static let storeUnSelected = ImageAsset(name: "StoreUnSelected")
  static let successGreen = ImageAsset(name: "SuccessGreen")
  static let tabBG = ImageAsset(name: "TabBG")
  static let bronzeBadge = ImageAsset(name: "BronzeBadge")
  static let bronzeStatus = ImageAsset(name: "BronzeStatus")
  static let cancelWhite = ImageAsset(name: "CancelWhite")
  static let downloads = ImageAsset(name: "Downloads")
  static let favourites = ImageAsset(name: "Favourites")
  static let goldBadge = ImageAsset(name: "GoldBadge")
  static let goldStatus = ImageAsset(name: "GoldStatus")
  static let leftArrow = ImageAsset(name: "LeftArrow")
  static let myProducts = ImageAsset(name: "MyProducts")
  static let noStatus = ImageAsset(name: "NoStatus")
  static let platinumBadge = ImageAsset(name: "PlatinumBadge")
  static let playlistBG = ImageAsset(name: "PlaylistBG")
  static let playlists = ImageAsset(name: "Playlists")
  static let rightArrow = ImageAsset(name: "RightArrow")
  static let searchWhite = ImageAsset(name: "SearchWhite")
  static let silverBadge = ImageAsset(name: "SilverBadge")
  static let silverStatus = ImageAsset(name: "SilverStatus")
  static let starBadge = ImageAsset(name: "StarBadge")
  static let notificationDemoOne = ImageAsset(name: "NotificationDemoOne")
  static let otpBG1 = ImageAsset(name: "OtpBG1")
  static let backward = ImageAsset(name: "Backward")
  static let cancelWhiteMiniPlayer = ImageAsset(name: "CancelWhiteMiniPlayer")
  static let downloadWhite = ImageAsset(name: "DownloadWhite")
  static let fastBackward = ImageAsset(name: "FastBackward")
  static let fastFordward = ImageAsset(name: "FastFordward")
  static let forward = ImageAsset(name: "Forward")
  static let handleThumb = ImageAsset(name: "HandleThumb")
  static let noRepeat = ImageAsset(name: "NoRepeat")
  static let pauseRoundBlack = ImageAsset(name: "PauseRoundBlack")
  static let pauseWhite = ImageAsset(name: "PauseWhite")
  static let pauseWhiteMiniPlayer = ImageAsset(name: "PauseWhiteMiniPlayer")
  static let playBlack = ImageAsset(name: "PlayBlack")
  static let playRoundBlack = ImageAsset(name: "PlayRoundBlack")
  static let playRoundWhite = ImageAsset(name: "PlayRoundWhite")
  static let playWhiteLarge = ImageAsset(name: "PlayWhiteLarge")
  static let playWhiteMiniPlayer = ImageAsset(name: "PlayWhiteMiniPlayer")
  static let playerPause = ImageAsset(name: "PlayerPause")
  static let playerPlay = ImageAsset(name: "PlayerPlay")
  static let repeatOne = ImageAsset(name: "RepeatOne")
  static let repeatPlaylist = ImageAsset(name: "RepeatPlaylist")
  static let repeatVideo = ImageAsset(name: "RepeatVideo")
  static let repeatVideoSelected = ImageAsset(name: "RepeatVideoSelected")
  static let playerDemo = ImageAsset(name: "PlayerDemo")
  static let backArrowBlack = ImageAsset(name: "BackArrowBlack")
  static let changeEmail = ImageAsset(name: "ChangeEmail")
  static let changeMobile = ImageAsset(name: "ChangeMobile")
  static let changeName = ImageAsset(name: "ChangeName")
  static let changePassword = ImageAsset(name: "ChangePassword")
  static let chanting = ImageAsset(name: "Chanting")
  static let contactUs = ImageAsset(name: "ContactUs")
  static let diksha = ImageAsset(name: "Diksha")
  static let gift = ImageAsset(name: "Gift")
  static let giftOne = ImageAsset(name: "GiftOne")
  static let giftTwo = ImageAsset(name: "GiftTwo")
  static let logout = ImageAsset(name: "Logout")
  static let mantra = ImageAsset(name: "Mantra")
  static let mobileEdit = ImageAsset(name: "MobileEdit")
  static let nameEdit = ImageAsset(name: "NameEdit")
  static let paymentHistory = ImageAsset(name: "PaymentHistory")
  static let personalDetail = ImageAsset(name: "PersonalDetail")
  static let profileIcon = ImageAsset(name: "ProfileIcon")
  static let radioDeSelected = ImageAsset(name: "RadioDeSelected")
  static let radioSelected = ImageAsset(name: "RadioSelected")
  static let redeemCode = ImageAsset(name: "RedeemCode")
  static let redeemCongrats = ImageAsset(name: "RedeemCongrats")
  static let rightArrowRed = ImageAsset(name: "RightArrowRed")
  static let roundSelect = ImageAsset(name: "RoundSelect")
  static let sadhna = ImageAsset(name: "Sadhna")
  static let sankirtan = ImageAsset(name: "Sankirtan")
  static let settings = ImageAsset(name: "Settings")
  static let youth = ImageAsset(name: "Youth")
  static let rightArrowRedSmall = ImageAsset(name: "RightArrowRedSmall")
  static let selectedDot = ImageAsset(name: "SelectedDot")
  static let unSelectedDot = ImageAsset(name: "UnSelectedDot")
  static let yellowBadge = ImageAsset(name: "YellowBadge")
  static let yellowBadgeCombined = ImageAsset(name: "YellowBadgeCombined")
  static let yellowRoundCut = ImageAsset(name: "YellowRoundCut")
  static let temp1 = ImageAsset(name: "Temp1")
  static let temp10 = ImageAsset(name: "Temp10")
  static let temp11 = ImageAsset(name: "Temp11")
  static let temp12 = ImageAsset(name: "Temp12")
  static let temp13 = ImageAsset(name: "Temp13")
  static let temp14 = ImageAsset(name: "Temp14")
  static let temp15 = ImageAsset(name: "Temp15")
  static let temp16 = ImageAsset(name: "Temp16")
  static let temp17 = ImageAsset(name: "Temp17")
  static let temp18 = ImageAsset(name: "Temp18")
  static let temp2 = ImageAsset(name: "Temp2")
  static let temp3 = ImageAsset(name: "Temp3")
  static let temp4 = ImageAsset(name: "Temp4")
  static let temp5 = ImageAsset(name: "Temp5")
  static let temp6 = ImageAsset(name: "Temp6")
  static let temp7 = ImageAsset(name: "Temp7")
  static let temp8 = ImageAsset(name: "Temp8")
  static let temp9 = ImageAsset(name: "Temp9")
  static let videoPlayerOne = ImageAsset(name: "VideoPlayerOne")
  static let yogaFour = ImageAsset(name: "YogaFour")
  static let yogaOne = ImageAsset(name: "YogaOne")
  static let yogaThree = ImageAsset(name: "YogaThree")
  static let yogaTwo = ImageAsset(name: "YogaTwo")
}

internal extension UIColor {
  static let app_000000 = UIColor(named: "000000")!
  static let app_009999 = UIColor(named: "009999")!
  static let app_050505 = UIColor(named: "050505")!
  static let app_212121 = UIColor(named: "212121")!
  static let app_222222 = UIColor(named: "222222")!
  static let app_22222207 = UIColor(named: "22222207")!
  static let app_39321C = UIColor(named: "39321C")!
  static let app_434343 = UIColor(named: "434343")!
  static let app_4F4F4F = UIColor(named: "4F4F4F")!
  static let app_5E5E5E = UIColor(named: "5E5E5E")!
  static let app_929292 = UIColor(named: "929292")!
  static let app_979797 = UIColor(named: "979797")!
  static let app_990000 = UIColor(named: "990000")!
  static let appA6A6A6 = UIColor(named: "A6A6A6")!
  static let appAeaeae = UIColor(named: "AEAEAE")!
  static let appBdbdbd = UIColor(named: "BDBDBD")!
  static let appD9D9D9 = UIColor(named: "D9D9D9")!
  static let appE1Ddc6 = UIColor(named: "E1DDC6")!
  static let appE1E1E1 = UIColor(named: "E1E1E1")!
  static let appE8E8E8 = UIColor(named: "E8E8E8")!
  static let appEbffff50 = UIColor(named: "EBFFFF50")!
  static let appF1F1F1 = UIColor(named: "F1F1F1")!
  static let appF1F1F150 = UIColor(named: "F1F1F150")!
  static let appFbc300 = UIColor(named: "FBC300")!
  static let appFc0B9205 = UIColor(named: "FC0B9205")!
  static let appFfaa9205 = UIColor(named: "FFAA9205")!
  static let appFfffff = UIColor(named: "FFFFFF")!
}

internal enum Asset {
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal final class ColorAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Color = NSColor
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Color = UIColor
  #endif

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  internal private(set) lazy var color: Color = {
    guard let color = Color(asset: self) else {
      fatalError("Unable to load color asset named \(name).")
    }
    return color
  }()

  #if os(iOS) || os(tvOS)
  @available(iOS 11.0, tvOS 11.0, *)
  internal func color(compatibleWith traitCollection: UITraitCollection) -> Color {
    let bundle = BundleToken.bundle
    guard let color = Color(named: name, in: bundle, compatibleWith: traitCollection) else {
      fatalError("Unable to load color asset named \(name).")
    }
    return color
  }
  #endif

  fileprivate init(name: String) {
    self.name = name
  }
}

internal extension ColorAsset.Color {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  convenience init?(asset: ColorAsset) {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Image = UIImage
  #endif

  @available(iOS 8.0, tvOS 9.0, watchOS 2.0, macOS 10.7, *)
  internal var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }

  #if os(iOS) || os(tvOS)
  @available(iOS 8.0, tvOS 9.0, *)
  internal func image(compatibleWith traitCollection: UITraitCollection) -> Image {
    let bundle = BundleToken.bundle
    guard let result = Image(named: name, in: bundle, compatibleWith: traitCollection) else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }
  #endif
}

internal extension ImageAsset.Image {
  @available(iOS 8.0, tvOS 9.0, watchOS 2.0, *)
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init?(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type

