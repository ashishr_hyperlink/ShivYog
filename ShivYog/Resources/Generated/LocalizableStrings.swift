// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {
  /// %@ results
  internal static func results(_ p1: Any) -> String {
    return L10n.tr("Localizable", "%@ results", String(describing: p1))
  }
  /// A wonderful serenity has taken possession of my entire 
  /// sweet mornings of spring.
  internal static let aWonderfulSerenityHasTakenPossessionOfMyEntireSweetMorningsOfSpring = L10n.tr("Localizable", "A wonderful serenity has taken possession of my entire \nsweet mornings of spring.")
  /// About Us
  internal static let aboutUs = L10n.tr("Localizable", "About Us")
  /// Accept All
  internal static let acceptAll = L10n.tr("Localizable", "Accept All")
  /// Add To Playlist
  internal static let addToPlaylist = L10n.tr("Localizable", "Add To Playlist")
  /// Any question? Send us a message and
  /// we will get back to you shortly.
  internal static let anyQuestionSendUsAMessageAndWeWillGetBackToYouShortly = L10n.tr("Localizable", "Any question? Send us a message and\nwe will get back to you shortly.")
  /// Back to sign in
  internal static let backToSignIn = L10n.tr("Localizable", "Back to sign in")
  /// Cancel
  internal static let cancel = L10n.tr("Localizable", "Cancel")
  /// Change Email
  internal static let changeEmail = L10n.tr("Localizable", "Change Email")
  /// Change Name
  internal static let changeName = L10n.tr("Localizable", "Change Name")
  /// Change Number
  internal static let changeNumber = L10n.tr("Localizable", "Change Number")
  /// Change Password
  internal static let changePassword = L10n.tr("Localizable", "Change Password")
  /// Change Your Email
  internal static let changeYourEmail = L10n.tr("Localizable", "Change Your Email")
  /// Change Your Mobile Number
  internal static let changeYourMobileNumber = L10n.tr("Localizable", "Change Your Mobile Number")
  /// Change Your Name
  internal static let changeYourName = L10n.tr("Localizable", "Change Your Name")
  /// Confirm Password
  internal static let confirmPassword = L10n.tr("Localizable", "Confirm Password")
  /// Congratulations! You have successfully
  /// changed your password.
  internal static let congratulationsYouHaveSuccessfullyChangedYourPassword = L10n.tr("Localizable", "Congratulations! You have successfully\nchanged your password.")
  /// Congratulations! You have successfully
  /// created your playlist.
  internal static let congratulationsYouHaveSuccessfullyCreatedYourPlaylist = L10n.tr("Localizable", "Congratulations! You have successfully\ncreated your playlist.")
  /// Congratulations! You have successfully
  /// verified.
  internal static let congratulationsYouHaveSuccessfullyVerified = L10n.tr("Localizable", "Congratulations! You have successfully\nverified.")
  /// Contact Us
  internal static let contactUs = L10n.tr("Localizable", "Contact Us")
  /// Contemplation
  internal static let contemplation = L10n.tr("Localizable", "Contemplation")
  /// Continue
  internal static let `continue` = L10n.tr("Localizable", "Continue")
  /// Continue watching
  internal static let continueWatching = L10n.tr("Localizable", "Continue watching")
  /// Continue with Password?
  internal static let continueWithPassword = L10n.tr("Localizable", "Continue with Password?")
  /// Create a new password and please never
  /// share it with anyone for safe use.
  internal static let createANewPasswordAndPleaseNeverShareItWithAnyoneForSafeUse = L10n.tr("Localizable", "Create a new password and please never\nshare it with anyone for safe use.")
  /// Create an Account
  internal static let createAnAccount = L10n.tr("Localizable", "Create an Account")
  /// Create Password
  internal static let createPassword = L10n.tr("Localizable", "Create Password")
  /// Create Playlist
  internal static let createPlaylist = L10n.tr("Localizable", "Create Playlist")
  /// Current Password
  internal static let currentPassword = L10n.tr("Localizable", "Current Password")
  /// Delete
  internal static let delete = L10n.tr("Localizable", "Delete")
  /// Done
  internal static let done = L10n.tr("Localizable", "Done")
  /// Download
  internal static let download = L10n.tr("Localizable", "Download")
  /// Download Now
  internal static let downloadNow = L10n.tr("Localizable", "Download Now")
  /// Downloads
  internal static let downloads = L10n.tr("Localizable", "Downloads")
  /// Email Address
  internal static let emailAddress = L10n.tr("Localizable", "Email Address")
  /// Enter playlist name
  internal static let enterPlaylistName = L10n.tr("Localizable", "Enter playlist name")
  /// Everyday Training
  internal static let everydayTraining = L10n.tr("Localizable", "Everyday Training")
  /// FAQ
  internal static let faq = L10n.tr("Localizable", "FAQ")
  /// Favourites
  internal static let favourites = L10n.tr("Localizable", "Favourites")
  /// Featured Content
  internal static let featuredContent = L10n.tr("Localizable", "Featured Content")
  /// Featured Playlists
  internal static let featuredPlaylists = L10n.tr("Localizable", "Featured Playlists")
  /// Forgot Password ?
  internal static let forgetPassword = L10n.tr("Localizable", "Forget Password ?")
  /// Give Your Playlist A
  /// Name
  internal static let giveYourPlaylistAName = L10n.tr("Localizable", "Give Your Playlist A\nName")
  /// Go Back
  internal static let goBack = L10n.tr("Localizable", "Go Back")
  /// Go To Settings
  internal static let goToSettings = L10n.tr("Localizable", "Go To Settings")
  /// Help
  internal static let help = L10n.tr("Localizable", "Help")
  /// How to Navigate through this App?
  internal static let howToNavigateThroughThisApp = L10n.tr("Localizable", "How to Navigate through this App?")
  /// I am alone, and feel the charm of existence in this spot, which was created for mine.
  internal static let iAmAloneAndFeelTheCharmOfExistenceInThisSpotWhichWasCreatedForMine = L10n.tr("Localizable", "I am alone, and feel the charm of existence in this spot, which was created for mine.")
  /// I don’t receive a code! Please resend 
  internal static let iDonTReceiveACodePleaseResend = L10n.tr("Localizable", "I don’t receive a code! Please resend ")
  /// If you are facing any issue, Contact Us
  internal static let ifYouAreFacingAnyIssueContactUs = L10n.tr("Localizable", "If you are facing any issue,Contact Us")
  /// Log Out
  internal static let logOut = L10n.tr("Localizable", "Log Out")
  /// Logged In Devices
  internal static let loggedInDevices = L10n.tr("Localizable", "Logged In Devices")
  /// Logout
  internal static let logout = L10n.tr("Localizable", "Logout")
  /// Make Your First Playlist
  internal static let makeYourFirstPlaylist = L10n.tr("Localizable", "Make Your First Playlist")
  /// Mobile Number
  internal static let mobileNumber = L10n.tr("Localizable", "Mobile Number")
  /// My Journey
  internal static let myJourney = L10n.tr("Localizable", "My Journey")
  /// My Products
  internal static let myProducts = L10n.tr("Localizable", "My Products")
  /// New Password
  internal static let newPassword = L10n.tr("Localizable", "New Password")
  /// Next
  internal static let next = L10n.tr("Localizable", "Next")
  /// Notifications
  internal static let notifications = L10n.tr("Localizable", "Notifications")
  /// Password
  internal static let password = L10n.tr("Localizable", "Password")
  /// Password Changed
  internal static let passwordChanged = L10n.tr("Localizable", "Password Changed")
  /// Password mismatch
  internal static let passwordMismatch = L10n.tr("Localizable", "Password mismatch")
  /// Password must be different than current password
  internal static let passwordMustBeDifferentThanCurrentPassword = L10n.tr("Localizable", "Password must be different than current password")
  /// Payments History
  internal static let paymentsHistory = L10n.tr("Localizable", "Payments History")
  /// Personal Details
  internal static let personalDetails = L10n.tr("Localizable", "Personal Details")
  /// Phone Number
  internal static let phoneNumber = L10n.tr("Localizable", "Phone Number")
  /// Playlist Created!
  internal static let playlistCreated = L10n.tr("Localizable", "Playlist Created!")
  /// Playlists
  internal static let playlists = L10n.tr("Localizable", "Playlists")
  /// Please accept terms and conditions and privacy policy
  internal static let pleaseAcceptTermsAndConditionsAndPrivacyPolicy = L10n.tr("Localizable", "Please accept terms and conditions and privacy policy")
  /// Please add health card photo
  internal static let pleaseAddHealthCardPhoto = L10n.tr("Localizable", "Please add health card photo")
  /// Please Check Your Internet
  /// Connection!
  internal static let pleaseCheckYourInternetConnection = L10n.tr("Localizable", "Please Check Your Internet\nConnection!")
  /// Please eneter AHV number
  internal static let pleaseEneterAHVNumber = L10n.tr("Localizable", "Please eneter AHV number")
  /// Please enter address
  internal static let pleaseEnterAddress = L10n.tr("Localizable", "Please enter address")
  /// Please enter apt suite bldg#
  internal static let pleaseEnterAptSuiteBldg = L10n.tr("Localizable", "Please enter apt suite bldg#")
  /// Please enter at least 1 uppercase, 1 lowercase, 1 digit, 1 special character and minimum 8 character for password
  internal static let pleaseEnterAtLeast1Uppercase1Lowercase1Digit1SpecialCharacterAndMinimum8CharacterForPassword = L10n.tr("Localizable", "Please enter at least 1 uppercase, 1 lowercase, 1 digit, 1 special character and minimum 8 character for password")
  /// Please enter BAG number
  internal static let pleaseEnterBAGNumber = L10n.tr("Localizable", "Please enter BAG number")
  /// Please enter card number
  internal static let pleaseEnterCardNumber = L10n.tr("Localizable", "Please enter card number")
  /// Please enter comment
  internal static let pleaseEnterComment = L10n.tr("Localizable", "Please enter comment")
  /// Please enter company
  internal static let pleaseEnterCompany = L10n.tr("Localizable", "Please enter company")
  /// Please enter concordia number
  internal static let pleaseEnterConcordiaNumber = L10n.tr("Localizable", "Please enter concordia number")
  /// Please enter confirm password
  internal static let pleaseEnterConfirmPassword = L10n.tr("Localizable", "Please enter confirm password")
  /// Please enter current password
  internal static let pleaseEnterCurrentPassword = L10n.tr("Localizable", "Please enter current password")
  /// Please enter description
  internal static let pleaseEnterDescription = L10n.tr("Localizable", "Please enter description")
  /// Please enter email
  internal static let pleaseEnterEmail = L10n.tr("Localizable", "Please enter email")
  /// Please enter first name
  internal static let pleaseEnterFirstName = L10n.tr("Localizable", "Please enter first name")
  /// Please enter last name
  internal static let pleaseEnterLastName = L10n.tr("Localizable", "Please enter last name")
  /// Please enter minimum 10 and maximum 13 digit for mobile number
  internal static let pleaseEnterMinimum10AndMaximum13DigitForMobileNumber = L10n.tr("Localizable", "Please enter minimum 10 and maximum 13 digit for mobile number")
  /// Please enter minimum of 6 characters for create password
  internal static let pleaseEnterMinimumOf6CharactersForCreatePassword = L10n.tr("Localizable", "Please enter minimum of 6 characters for create password")
  /// Please enter minimum of 6 characters for new password.
  internal static let pleaseEnterMinimumOf6CharactersForNewPassword = L10n.tr("Localizable", "Please enter minimum of 6 characters for new password.")
  /// Please enter minimum of two characters in name
  internal static let pleaseEnterMinimumOfTwoCharactersInName = L10n.tr("Localizable", "Please enter minimum of two characters in name")
  /// Please enter mobile number
  internal static let pleaseEnterMobileNumber = L10n.tr("Localizable", "Please enter mobile number")
  /// Please enter name
  internal static let pleaseEnterName = L10n.tr("Localizable", "Please enter name")
  /// Please enter new password
  internal static let pleaseEnterNewPassword = L10n.tr("Localizable", "Please enter new password")
  /// Please enter OTP
  internal static let pleaseEnterOTP = L10n.tr("Localizable", "Please enter OTP")
  /// Please enter password
  internal static let pleaseEnterPassword = L10n.tr("Localizable", "Please enter password")
  /// Please enter phone number
  internal static let pleaseEnterPhoneNumber = L10n.tr("Localizable", "Please enter phone number")
  /// Please enter post code
  internal static let pleaseEnterPostCode = L10n.tr("Localizable", "Please enter post code")
  /// Please enter street address and city
  internal static let pleaseEnterStreetAddressAndCity = L10n.tr("Localizable", "Please enter street address and city")
  /// Please enter subject
  internal static let pleaseEnterSubject = L10n.tr("Localizable", "Please enter subject")
  /// Please enter valid email
  internal static let pleaseEnterValidEmail = L10n.tr("Localizable", "Please enter valid email")
  /// Please enter valid mobile number
  internal static let pleaseEnterValidMobileNumber = L10n.tr("Localizable", "Please enter valid mobile number")
  /// Please enter valid name
  internal static let pleaseEnterValidName = L10n.tr("Localizable", "Please enter valid name")
  /// Please enter valid OTP
  internal static let pleaseEnterValidOTP = L10n.tr("Localizable", "Please enter valid OTP")
  /// Please enter valid phone number
  internal static let pleaseEnterValidPhoneNumber = L10n.tr("Localizable", "Please enter valid phone number")
  /// Please enter your keywords to search
  internal static let pleaseEnterYourKeywordsToSearch = L10n.tr("Localizable", "Please enter your keywords to search")
  /// Please select country code
  internal static let pleaseSelectCountryCode = L10n.tr("Localizable", "Please select country code")
  /// Please select date of birth
  internal static let pleaseSelectDateOfBirth = L10n.tr("Localizable", "Please select date of birth")
  /// Please select expiry date
  internal static let pleaseSelectExpiryDate = L10n.tr("Localizable", "Please select expiry date")
  /// Please select gender
  internal static let pleaseSelectGender = L10n.tr("Localizable", "Please select gender")
  /// Please type the verification code sent to
  /// your moblie number
  internal static let pleaseTypeTheVerificationCodeSentToYourMoblieNumber = L10n.tr("Localizable", "Please type the verification code sent to\nyour moblie number")
  /// Privacy Policy
  internal static let privacyPolicy = L10n.tr("Localizable", "Privacy Policy")
  /// Profile
  internal static let profile = L10n.tr("Localizable", "Profile")
  /// Rate App
  internal static let rateApp = L10n.tr("Localizable", "Rate App")
  /// Redeem Code
  internal static let redeemCode = L10n.tr("Localizable", "Redeem Code")
  /// Repeat
  internal static let `repeat` = L10n.tr("Localizable", "Repeat")
  /// Reset Password
  internal static let resetPassword = L10n.tr("Localizable", "Reset Password")
  /// Reward Program
  internal static let rewardProgram = L10n.tr("Localizable", "Reward Program")
  /// Search
  internal static let search = L10n.tr("Localizable", "Search")
  /// See all
  internal static let seeAll = L10n.tr("Localizable", "See all")
  /// Select All
  internal static let selectAll = L10n.tr("Localizable", "Select All")
  /// Settings
  internal static let settings = L10n.tr("Localizable", "Settings")
  /// Share
  internal static let share = L10n.tr("Localizable", "Share")
  /// Sign In
  internal static let signIn = L10n.tr("Localizable", "Sign In")
  /// Sign Up
  internal static let signUp = L10n.tr("Localizable", "Sign Up")
  /// Skip
  internal static let skip = L10n.tr("Localizable", "Skip")
  /// Store Payments
  internal static let storePayments = L10n.tr("Localizable", "Store Payments")
  /// Subject
  internal static let subject = L10n.tr("Localizable", "Subject")
  /// Submit
  internal static let submit = L10n.tr("Localizable", "Submit")
  /// Subscription Payments
  internal static let subscriptionPayments = L10n.tr("Localizable", "Subscription Payments")
  /// Terms of Service
  internal static let termsOfService = L10n.tr("Localizable", "Terms of Service")
  /// Try Again
  internal static let tryAgain = L10n.tr("Localizable", "Try Again")
  /// Update
  internal static let update = L10n.tr("Localizable", "Update")
  /// Update Password
  internal static let updatePassword = L10n.tr("Localizable", "Update Password")
  /// Verification Code
  internal static let verificationCode = L10n.tr("Localizable", "Verification Code")
  /// Verification Done!
  internal static let verificationDone = L10n.tr("Localizable", "Verification Done!")
  /// Version %@
  internal static func version(_ p1: Any) -> String {
    return L10n.tr("Localizable", "Version %@", String(describing: p1))
  }
  /// View Downloads
  internal static let viewDownloads = L10n.tr("Localizable", "View Downloads")
  /// Welcome to Shivyog
  /// Play App
  internal static let welcomeToShivyogPlayApp = L10n.tr("Localizable", "Welcome to Shivyog\nPlay App")
  /// What’s New
  internal static let whatSNew = L10n.tr("Localizable", "What’s New")
  /// Workout List
  internal static let workoutList = L10n.tr("Localizable", "Workout List")
  /// Write description..
  internal static let writeDescription = L10n.tr("Localizable", "Write description..")
  /// Yoga Store
  internal static let yogaStore = L10n.tr("Localizable", "Yoga Store")
  /// You are currently logged into  %@ other devices
  internal static func youAreCurrentlyLoggedIntoOtherDevices(_ p1: Any) -> String {
    return L10n.tr("Localizable", "You are currently logged into  %@ other devices", String(describing: p1))
  }
  /// You can change your email address below.
  internal static let youCanChangeYourEmailAddressBelow = L10n.tr("Localizable", "You can change your email address below.")
  /// You can change your mobile number below.
  internal static let youCanChangeYourMobileNumberBelow = L10n.tr("Localizable", "You can change your mobile number below.")
  /// You can change your name below.
  internal static let youCanChangeYourNameBelow = L10n.tr("Localizable", "You can change your name below.")
  /// You may logout from one of them,to allow
  /// access on this device
  internal static let youMayLogoutFromOneOfThemToAllowAccessOnThisDevice = L10n.tr("Localizable", "You may logout from one of them,to allow\naccess on this device")
  /// Your Email
  internal static let yourEmail = L10n.tr("Localizable", "Your Email")
  /// Your new password must be different password.
  internal static let yourNewPasswordMustBeDifferentPassword = L10n.tr("Localizable", "Your new password must be different password.")
  /// Your Preferences
  internal static let yourPreferences = L10n.tr("Localizable", "Your Preferences")
  /// Your Purchase Is Successfully
  /// Completed!
  internal static let yourPurchaseIsSuccessfullyCompleted = L10n.tr("Localizable", "Your Purchase Is Successfully\nCompleted!")

  internal enum NowIsAsGoodATimeAsAnyToFocusOnGettingYourBodyIntoTheBestShapePossible {
    /// Now is as good a time as any to focus on getting your
    ///  body into the best shape possible. if you really want
    ///  make something happen.
    internal static let ifYouReallyWantMakeSomethingHappen = L10n.tr("Localizable", "Now is as good a time as any to focus on getting your\n body into the best shape possible. if you really want\n make something happen.")
  }

  internal enum PleaseEnterYourMobileNumber {
    /// Please enter your mobile number. You will get a
    ///  OTP on provided mobile
    /// number.
    internal static let youWillGetAOTPOnProvidedMobileNumber = L10n.tr("Localizable", "Please enter your mobile number. You will get a\n OTP on provided mobile\nnumber.")
  }

  internal enum SeemsLikeThereWasAnIssueWithThePayment {
    /// Seems like there was an issue
    /// with the payment.
    /// Please try again.
    internal static let pleaseTryAgain = L10n.tr("Localizable", "Seems like there was an issue\nwith the payment.\nPlease try again.")
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
