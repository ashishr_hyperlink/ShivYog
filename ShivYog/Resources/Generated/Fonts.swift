// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit.NSFont
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIFont
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "FontConvertible.Font", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias Font = FontConvertible.Font

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length
// swiftlint:disable implicit_return

// MARK: - Fonts

// swiftlint:disable identifier_name line_length type_body_length
internal enum FontFamily {
  internal enum CircularStd {
    internal static let black = FontConvertible(name: "CircularStd-Black", family: "Circular Std", path: "circularstdblack.otf")
    internal static let blackItalic = FontConvertible(name: "CircularStd-BlackItalic", family: "Circular Std", path: "circularstdblackitalic.otf")
    internal static let bold = FontConvertible(name: "CircularStd-Bold", family: "Circular Std", path: "CircularStd-Bold.otf")
    internal static let boldItalic = FontConvertible(name: "CircularStd-BoldItalic", family: "Circular Std", path: "circularstdbolditalic.otf")
    internal static let book = FontConvertible(name: "CircularStd-Book", family: "Circular Std", path: "circularstdbook.otf")
    internal static let bookItalic = FontConvertible(name: "CircularStd-BookItalic", family: "Circular Std", path: "circularstdbookitalic.otf")
    internal static let medium = FontConvertible(name: "CircularStd-Medium", family: "Circular Std", path: "circularstdmedium.otf")
    internal static let mediumItalic = FontConvertible(name: "CircularStd-MediumItalic", family: "Circular Std", path: "circularstdmediumitalic.otf")
    internal static let all: [FontConvertible] = [black, blackItalic, bold, boldItalic, book, bookItalic, medium, mediumItalic]
  }
  internal enum DMSans {
    internal static let bold = FontConvertible(name: "DMSans-Bold", family: "DM Sans", path: "DMSans-Bold.ttf")
    internal static let boldItalic = FontConvertible(name: "DMSans-BoldItalic", family: "DM Sans", path: "DMSans-BoldItalic.ttf")
    internal static let italic = FontConvertible(name: "DMSans-Italic", family: "DM Sans", path: "DMSans-Italic.ttf")
    internal static let medium = FontConvertible(name: "DMSans-Medium", family: "DM Sans", path: "DMSans-Medium.ttf")
    internal static let mediumItalic = FontConvertible(name: "DMSans-MediumItalic", family: "DM Sans", path: "DMSans-MediumItalic.ttf")
    internal static let regular = FontConvertible(name: "DMSans-Regular", family: "DM Sans", path: "DMSans-Regular.ttf")
    internal static let all: [FontConvertible] = [bold, boldItalic, italic, medium, mediumItalic, regular]
  }
  internal enum Inter {
    internal static let medium = FontConvertible(name: "Inter-Medium", family: "Inter", path: "Inter-Medium.ttf")
    internal static let regular = FontConvertible(name: "Inter-Regular", family: "Inter", path: "Inter-Regular.ttf")
    internal static let all: [FontConvertible] = [medium, regular]
  }
  internal enum PTSans {
    internal static let bold = FontConvertible(name: "PTSans-Bold", family: "PT Sans", path: "PTSans-Bold.ttf")
    internal static let boldItalic = FontConvertible(name: "PTSans-BoldItalic", family: "PT Sans", path: "PTSans-BoldItalic.ttf")
    internal static let italic = FontConvertible(name: "PTSans-Italic", family: "PT Sans", path: "PTSans-Italic.ttf")
    internal static let regular = FontConvertible(name: "PTSans-Regular", family: "PT Sans", path: "PTSans-Regular.ttf")
    internal static let all: [FontConvertible] = [bold, boldItalic, italic, regular]
  }
  internal enum ProximaNova {
    internal static let regular = FontConvertible(name: "ProximaNova-Regular", family: "Proxima Nova", path: "ProximaNova-Regular.otf")
    internal static let all: [FontConvertible] = [regular]
  }
  internal enum SFProDisplay {
    internal static let bold = FontConvertible(name: "SFProDisplay-Bold", family: "SF Pro Display", path: "SF-Pro-Display-Bold.otf")
    internal static let heavy = FontConvertible(name: "SFProDisplay-Heavy", family: "SF Pro Display", path: "SF-Pro-Display-Heavy.otf")
    internal static let light = FontConvertible(name: "SFProDisplay-Light", family: "SF Pro Display", path: "SF-Pro-Display-Light.otf")
    internal static let medium = FontConvertible(name: "SFProDisplay-Medium", family: "SF Pro Display", path: "SF-Pro-Display-Medium.otf")
    internal static let regular = FontConvertible(name: "SFProDisplay-Regular", family: "SF Pro Display", path: "SF-Pro-Display-Regular.otf")
    internal static let semibold = FontConvertible(name: "SFProDisplay-Semibold", family: "SF Pro Display", path: "SF-Pro-Display-Semibold.otf")
    internal static let all: [FontConvertible] = [bold, heavy, light, medium, regular, semibold]
  }
  internal static let allCustomFonts: [FontConvertible] = [CircularStd.all, DMSans.all, Inter.all, PTSans.all, ProximaNova.all, SFProDisplay.all].flatMap { $0 }
  internal static func registerAllCustomFonts() {
    allCustomFonts.forEach { $0.register() }
  }
}
// swiftlint:enable identifier_name line_length type_body_length

// MARK: - Implementation Details

internal struct FontConvertible {
  internal let name: String
  internal let family: String
  internal let path: String

  #if os(macOS)
  internal typealias Font = NSFont
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Font = UIFont
  #endif

  internal func font(size: CGFloat, enableAspectRatio isRatio: Bool = true) -> Font! {
    return Font(font: self, size: isRatio ? size * ScreenSize.fontAspectRatio : size)
  }

  internal func register() {
    // swiftlint:disable:next conditional_returns_on_newline
    guard let url = url else { return }
    CTFontManagerRegisterFontsForURL(url as CFURL, .process, nil)
  }

  fileprivate var url: URL? {
    return BundleToken.bundle.url(forResource: path, withExtension: nil)
  }
}

internal extension FontConvertible.Font {
  convenience init?(font: FontConvertible, size: CGFloat) {
    #if os(iOS) || os(tvOS) || os(watchOS)
    if !UIFont.fontNames(forFamilyName: font.family).contains(font.name) {
      font.register()
    }
    #elseif os(macOS)
    if let url = font.url, CTFontManagerGetScopeForURL(url as CFURL) == .none {
      font.register()
    }
    #endif

    self.init(name: font.name, size: size)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
