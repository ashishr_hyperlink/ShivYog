//
//  AppDelegate.swift
//  ShivYog
//
//  Created by 2022M30 on 23/11/22.
//

import UIKit
import FirebaseCore
import FirebaseDynamicLinks
import FirebaseAnalytics
import Mixpanel
@_exported import RevenueCat
//import OneSignal

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var orientationLock = UIInterfaceOrientationMask.portrait
    static let shared : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var appCoordinator: AppCoordinator!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        Mixpanel.initialize(token: AppCredential.mixPanelToken.rawValue, trackAutomaticEvents: true)
        //        Mixpanel.mainInstance().loggingEnabled = true
        QorumLogs.enabled = true
        Purchases.logLevel = .debug
        appCoordinator = AppCoordinator()
        appCoordinator.basicAppSetup()
        self.addAnalyticsEvent(screenName: .Splash, eventName: .Pageview, category: .Splash)
        //For Notifications
        // Remove this method to stop OneSignal Debugging
//        OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)
//
//        // OneSignal initialization
//        OneSignal.initWithLaunchOptions(launchOptions)
//        OneSignal.setAppId(AppCredential.oneSignalAPPID.rawValue)
        
        // promptForPushNotifications will show the native iOS notification permission prompt.
        // We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 8)
//        OneSignal.promptForPushNotifications(userResponse: { accepted in
//            print("User accepted notifications: \(accepted)")
//        })
        
        return true
    }
    

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if let rootViewController = UIApplication.topViewController() as? VideoPlayerVC {
            if (rootViewController.responds(to: Selector(("canRotate")))) {
                // Unlock landscape view orientations for this view controller
                return .landscape;
            }
            //            return .landscape
        } else if let _ = UIApplication.topViewController() as? UIAlertController {
            if UserDefaultsConfig.isFromVideoPlayer {
                return .landscape
            } else {
                return .portrait
            }
        } else if let _ = UIApplication.topViewController() as? UIActivityViewController {
            if UserDefaultsConfig.isFromVideoPlayer {
                return .landscape
            } else {
                return .portrait
            }
        }
        else{
            return .portrait
        }
        
        // Only allow portrait (standard behaviour)
        return .portrait;
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        if UserModel.isUserLoggedIn{
            GlobalAPI.shared.getUserData()
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        DownloadManager.shared.arrTaskDataForDownload.removeAll()
        let dataDecode = UserDefaults.standard.decode(for: DownloadMainModel.self, using: UserDefaults.Keys.downloadContent)
        if let decode = dataDecode {
            let completedData = decode.data.filter {  $0.isFileDownload }
            let mainData = DownloadMainModel(modelData: completedData)
            UserDefaults.standard.encode(for: mainData, using: UserDefaults.Keys.downloadContent)
        }
    }
    
    


    struct AppUtility {
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                delegate.orientationLock = orientation
            }
        }
        
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
            self.lockOrientation(orientation)
            UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        }
    }
}


extension AppDelegate{
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
      let handled = DynamicLinks.dynamicLinks()
        .handleUniversalLink(userActivity.webpageURL!) { dynamiclink, error in
            self.handleDynamicLink(dynamiclink: dynamiclink)
        }

      return handled
    }
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
      return application(app, open: url,
                         sourceApplication: options[UIApplication.OpenURLOptionsKey
                           .sourceApplication] as? String,
                         annotation: "")
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?,
                     annotation: Any) -> Bool {
        debugPrint("open url")
      if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
        // Handle the deep link. For example, show the deep-linked content or
        // apply a promotional offer to the user's account.
        // ...
          self.handleDynamicLink(dynamiclink: dynamicLink)
          debugPrint(dynamicLink)
        return true
      }
      return false
    }
    
    func handleDynamicLink(dynamiclink : DynamicLink?){
        print("test linkkk....")
        guard let link = dynamiclink, let linkURL = link.url else {
            return
        }
        print("SceneDelegate test linkkk....", linkURL.absoluteString)
        
        
        let lastPath = linkURL.lastPathComponent
        
        if lastPath == "contentshare", let params = linkURL.queryParameters, !JSON(params)["id"].stringValue.isEmpty{
            if JSON(params)["storesubcontent_id"].stringValue.isEmpty {
                GlobalAPI.shared.apiGetMediaContentByID(id: JSON(params)["id"].stringValue, withLoader: true)
            } else {
                GlobalAPI.shared.apiGetStoreMediaContentByID(id: JSON(params)["id"].stringValue, withLoader: true, storeContentId: JSON(params)["storesubcontent_id"].stringValue)
            }
        }
        
    
        if UserModel.accessToken == nil{
            UIApplication.shared.setLogoutFlow()
        }
    }
}


extension URL {
    public var queryParameters: [String: String]? {
        guard
            let components = URLComponents(url: self, resolvingAgainstBaseURL: true),
            let queryItems = components.queryItems else { return nil }
        return queryItems.reduce(into: [String: String]()) { (result, item) in
            result[item.name] = item.value
        }
    }
}

extension AppDelegate{
    func addAnalyticsEvent(screenName : AnalyticsEventScreenName, eventName : AnalyticsEventName, category : AnalyticsEventCategory, value : Int = 1, action : String = "", label : String = ""){
        guard let user = UserModel.currentUser else {return}
        Mixpanel.mainInstance().track(event: eventName.rawValue, properties: [
            AnalyticsParamName.ScreenName.rawValue : screenName.rawValue,
            AnalyticsParamName.Category.rawValue : category.rawValue,
            AnalyticsParamName.Value.rawValue : value,
            AnalyticsParamName.Action.rawValue : action,
            AnalyticsParamName.Label.rawValue : label,
            AnalyticsParamName.Name.rawValue : user.fullname ?? "",
            AnalyticsParamName.PhoneNumber.rawValue : user.phoneNumber ?? "",
            AnalyticsParamName.Platform.rawValue : "iOS"
        ])
//        Mixpanel.mainInstance().flush()
        
        
//        Analytics.logEvent(eventName.rawValue , parameters: [
//            AnalyticsParamName.ScreenName.rawValue : screenName.rawValue,
//            AnalyticsParamName.Category.rawValue : category.rawValue,
//            AnalyticsParamName.Value.rawValue : value,
//            AnalyticsParamName.Action.rawValue : action,
//            AnalyticsParamName.Label.rawValue : label,
//            AnalyticsParamName.Name.rawValue : user.fullname ?? "",
//            AnalyticsParamName.PhoneNumber.rawValue : user.phoneNumber ?? "",
//            AnalyticsParamName.Platform.rawValue : "iOS"
//        ])
    }
}


//MARK: - Push Notification -
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    func registerForNotification() {
        //For device token and push notifications.
        //        DispatchQueue.main.async {
        //            UIApplication.shared.registerForRemoteNotifications()
        //        }
        let center : UNUserNotificationCenter = UNUserNotificationCenter.current()
        center.delegate = self
        
        center.requestAuthorization(options: [.sound , .alert , .badge ], completionHandler: { (granted, error) in
            if ((error == nil)) {
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                    debugPrint("Registered:-", UIApplication.shared.isRegisteredForRemoteNotifications)
                }
            }
            else {
                debugPrint("Error in register token:- ", error?.localizedDescription as Any)
            }
        })
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print("<<< User Info : \(userInfo) >>>")
        if UserModel.isUserLoggedIn {
//            service.processPushNotification(userInfo, appState: UIApplication.shared.applicationState)
            completionHandler([.alert , .badge , .sound])
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        if UserModel.isUserLoggedIn {
            self.handlePushNotification(JSON(response.notification.request.content.userInfo))
        }
        
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if UserModel.isUserLoggedIn {
            self.handlePushNotification(JSON(userInfo))
        }
        
    }
    
    //------------------------------------------------------
    //TODO : This is custom notification method
    
    private func handlePushNotification(_ userInfo : JSON) {
        print(userInfo)
        
        let tag = JSON(userInfo["tag"]).stringValue
        
    }
    
}

