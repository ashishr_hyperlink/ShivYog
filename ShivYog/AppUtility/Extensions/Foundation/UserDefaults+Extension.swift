//
//  UserDefaults+Extension.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 22/09/20.
//  Copyright © 2020 KISHAN_RAJA. All rights reserved.
//


import Foundation

//MARK: User defaults keys
extension UserDefaults {
    public enum Keys {
        static var authorization      = "authorization" //isLogin
        static let accessToken        = "accessToken"
        static let currentUser        = "currentUser"
        static let appleLanguages     = "AppleLanguages"
        static let deviceToken        = "deviceToken"
        static let isShowTutorial     = "showTutorial"
        static let reviewWorthyActionCount = "reviewWorthyActionCount"
        static let isVerifiedUser       = "isVerifiedUser"
        static let isAdded            = "isAdded"
        static let miniplayer              = "miniplayer"
        static let firstTimePreferences  = "firstTimePreferences"
        static let firstTimeLogin       = "firstTimeLogin"
        static let downloadContent     =  "downloadContent"
        static let fromViedeoplayer     =   "fromViedeoplayer"
    }
}

extension UserDefaults {
    func decode<T : Codable>(for type : T.Type, using key : String) -> T? {
        let defaults = UserDefaults.standard
        guard let data = defaults.object(forKey: key) as? Data else {return nil}
        let decodedObject = try? PropertyListDecoder().decode(type, from: data)
        return decodedObject
    }
    
    func encode<T : Codable>(for type : T, using key : String) {
        let defaults = UserDefaults.standard
        let encodedData = try? PropertyListEncoder().encode(type)
        defaults.set(encodedData, forKey: key)
        defaults.synchronize()
    }
}


///// ProjectMagick: Retrieves a Codable object from UserDefaults.
//    ///
//    /// - Parameters:
//    ///   - type: Class that conforms to the Codable protocol.
//    ///   - key: Identifier of the object.
//    ///   - decoder: Custom JSONDecoder instance. Defaults to `JSONDecoder()`.
//    /// - Returns: Codable object for key (if exists).
//    func object<T: Codable>(_ type: T.Type, with key: String, usingDecoder decoder: JSONDecoder = JSONDecoder()) -> T? {
//        guard let data = value(forKey: key) as? Data else { return nil }
//        return try? decoder.decode(type.self, from: data)
//    }
//
//    /// ProjectMagick: Allows storing of Codable objects to UserDefaults.
//    ///
//    /// - Parameters:
//    ///   - object: Codable object to store.
//    ///   - key: Identifier of the object.
//    ///   - encoder: Custom JSONEncoder instance. Defaults to `JSONEncoder()`.
//    func set<T: Codable>(object: T, forKey key: String, usingEncoder encoder: JSONEncoder = JSONEncoder()) {
//        let data = try? encoder.encode(object)
//        set(data, forKey: key)
//    }

