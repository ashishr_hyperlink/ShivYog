//
//  GExtension+UIImageView.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 22/09/20.
//  Copyright © 2020 KISHAN_RAJA. All rights reserved.
//

import UIKit
import ImageIO
import SDWebImage

extension UIImageView {
    
    /// Sets image from image URL.
    /// - Parameters:
    ///   - url: Image URL
    ///   - placeholder: Placeholder image
    ///   - loader: Flag for show loader or not
    ///   - completed: Completion block after download image
    func setImage(with url: String, placeholder: UIImage? = nil , andLoader loader: Bool = true, completed: SDExternalCompletionBlock? = nil) {

          guard let imageURL = URL(string: url) else { return }

          //print("url--",imageURL)
          sd_imageTransition = .fade

          if loader {
              self.sd_imageIndicator = SDWebImageActivityIndicator.gray
          }
          sd_setImage(with: imageURL, placeholderImage: placeholder, options:[.retryFailed, .refreshCached], completed: completed)
      }
    
    /**
     Tap to open full screen image preview
     - Parameter image: UIImage for preview
     */
    func tapToZoom(with image: UIImage? = nil) {
        self.addTapGestureRecognizer {
            self.zoomImage(with: image)
        }
    }
    
    /**
     Full screen image preview
     - Parameter image: UIImage for preview
     */
    func zoomImage(with image: UIImage?) {
        
        if let _ = self.image {
            /*let configuration = ImageViewerConfiguration { config in
                config.imageView = self
                if image != nil {
                    config.image = image
                }
            }
            let imageViewerController = ImageViewerController(configuration: configuration)
            
            imageViewerController.navigationController?.navigationBar.isHidden = false
            UIApplication.topViewController()?.present(imageViewerController, animated: true, completion: nil)*/
            self.setupImageViewer()
        }
    }
    
    func gifImageWithData(_ data: Data) -> UIImage? {
            guard let source = CGImageSourceCreateWithData(data as CFData, nil) else {
                print("image doesn't exist")
                return nil
            }
            
            return self.animatedImageWithSource(source)
        }
        
        func gifImageWithURL(_ gifUrl:String) -> UIImage? {
            guard let bundleURL:URL? = URL(string: gifUrl)
                else {
                    print("image named \"\(gifUrl)\" doesn't exist")
                    return nil
            }
            guard let imageData = try? Data(contentsOf: bundleURL!) else {
                print("image named \"\(gifUrl)\" into NSData")
                return nil
            }
            
            return gifImageWithData(imageData)
        }
        
        func gifImageWithName(_ name: String)  {
            guard let bundleURL = Bundle.main
                .url(forResource: name, withExtension: "gif") else {
                    print("SwiftGif: This image named \"\(name)\" does not exist")
                    return
            }
            guard let imageData = try? Data(contentsOf: bundleURL) else {
                print("SwiftGif: Cannot turn image named \"\(name)\" into NSData")
                return
            }
            
            self.image = gifImageWithData(imageData)
        }
        
        func delayForImageAtIndex(_ index: Int, source: CGImageSource!) -> Double {
            var delay = 0.001
            
            let cfProperties = CGImageSourceCopyPropertiesAtIndex(source, index, nil)
            let gifProperties: CFDictionary = unsafeBitCast(
                CFDictionaryGetValue(cfProperties,
                    Unmanaged.passUnretained(kCGImagePropertyGIFDictionary).toOpaque()),
                to: CFDictionary.self)
            
            var delayObject: AnyObject = unsafeBitCast(
                CFDictionaryGetValue(gifProperties,
                    Unmanaged.passUnretained(kCGImagePropertyGIFUnclampedDelayTime).toOpaque()),
                to: AnyObject.self)
            if delayObject.doubleValue == 0 {
                delayObject = unsafeBitCast(CFDictionaryGetValue(gifProperties,
                    Unmanaged.passUnretained(kCGImagePropertyGIFDelayTime).toOpaque()), to: AnyObject.self)
            }
            
            delay = delayObject as! Double
            
            if delay < 0.1 {
                delay = 0.1
            }
            
            return delay
        }
        
        func gcdForPair(_ a: Int?, _ b: Int?) -> Int {
            var a = a
            var b = b
            if b == nil || a == nil {
                if b != nil {
                    return b!
                } else if a != nil {
                    return a!
                } else {
                    return 0
                }
            }
            
            if a < b {
                let c = a
                a = b
                b = c
            }
            
            var rest: Int
            while true {
                rest = a! % b!
                
                if rest == 0 {
                    return b!
                } else {
                    a = b
                    b = rest
                }
            }
        }
        
        func gcdForArray(_ array: Array<Int>) -> Int {
            if array.isEmpty {
                return 1
            }
            
            var gcd = array[0]
            
            for val in array {
                gcd = self.gcdForPair(val, gcd)
            }
            
            return gcd
        }
        
        func animatedImageWithSource(_ source: CGImageSource) -> UIImage? {
            let count = CGImageSourceGetCount(source)
            var images = [CGImage]()
            var delays = [Int]()
            
            for i in 0..<count {
                if let image = CGImageSourceCreateImageAtIndex(source, i, nil) {
                    images.append(image)
                }
                
                let delaySeconds = self.delayForImageAtIndex(Int(i),
                    source: source)
                delays.append(Int(delaySeconds * 1000.0)) // Seconds to ms
            }
            
            let duration: Int = {
                var sum = 0
                
                for val: Int in delays {
                    sum += val
                }
                
                return sum
            }()
            
            let gcd = gcdForArray(delays)
            var frames = [UIImage]()
            
            var frame: UIImage
            var frameCount: Int
            for i in 0..<count {
                frame = UIImage(cgImage: images[Int(i)])
                frameCount = Int(delays[Int(i)] / gcd)
                
                for _ in 0..<frameCount {
                    frames.append(frame)
                }
            }
            
            let animation = UIImage.animatedImage(with: frames,
                duration: Double(duration) / 1000.0)
            
            return animation
        }
}
