//
//  GExtension+UIFont.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 22/09/20.
//  Copyright © 2020 KISHAN_RAJA. All rights reserved.
//

import UIKit

extension UIFont {
    
    enum FontType: String {
        case regular                 = "Roboto-Regular"
        case medium                  = "Roboto-Medium"
        case bold                    = "Roboto-Bold"
    }
    
    /// Set custom font
    /// - Parameters:
    ///   - type: Font type.
    ///   - size: Size of font.
    ///   - isRatio: Whether set font size ratio or not. Default true.
    /// - Returns: Return font.
    class func customFont(ofType type: FontType, withSize size: CGFloat, enableAspectRatio isRatio: Bool = true) -> UIFont {
        return UIFont(name: type.rawValue, size: isRatio ? size * ScreenSize.fontAspectRatio : size) ?? UIFont.systemFont(ofSize: size)
    }
    //fixme
    class func cFont(ofType type: FontConvertible, withSize size: CGFloat, enableAspectRatio isRatio: Bool = true) -> UIFont{
        return UIFont(name: type.name, size: isRatio ? size * ScreenSize.fontAspectRatio : size) ?? UIFont.systemFont(ofSize: size)
    }
}

