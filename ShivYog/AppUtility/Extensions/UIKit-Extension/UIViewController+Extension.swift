//
//  GExtension+UIViewController.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 22/09/20.
//  Copyright © 2020 KISHAN_RAJA. All rights reserved.
//

import UIKit
import SafariServices
import AVKit
import DZNEmptyDataSet

extension UIViewController {
    ///Life Cycle.
    open override func awakeFromNib() {
        super.awakeFromNib()
        self.modalPresentationStyle = .overFullScreen
    }
    
    ///Return status bar height.
    public var statusBarHeight: CGFloat {
        if #available(iOS 13.0, *) {
            return view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
        } else {
            // Fallback on earlier versions
            return UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        }
    }
    
    ///Return top bar height : Navigation bar height + status bar height
    var topbarHeight: CGFloat {
        var top = self.navigationController?.navigationBar.frame.height ?? 0.0
        if #available(iOS 13.0, *) {
            top += UIApplication.shared.windows.first?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
        } else {
            top += UIApplication.shared.statusBarFrame.height
        }
        return top
    }
    
    var bottomBarHeight: CGFloat {
        if let tabBar = UIApplication.topViewController() as? TabVC {
            return tabBar.tabBar.frame.height //+ tabBar.tabBar.safeAreaInsets.bottom
        }
        return 0
    }
    
    var safebottomBarHeight: CGFloat {
        return UIApplication.shared.windows.first?.safeAreaInsets.bottom ?? 0
    }
    
    ///Return storyboard identifier
    class var storyboardID : String {
        return "\(self)"
    }
    
    /**
     To get storyboard view controller instantiate
     
     - Parameter appStoryboard: Pass storyboard
     - Returns: View controller
     
     */
    static func instantiate(fromAppStoryboard appStoryboard: UIStoryboard) -> Self {
        return appStoryboard.viewController(viewControllerClass: self)
    }
    
    /**
     To pop / back view controller
     
     - Parameter sender: AnyObject
     */
    
    @IBAction func popViewController(sender : AnyObject) {
        //        Vibration.medium.vibrate()
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     To dismiss view controller
     
     - Parameter sender: AnyObject
     */
    
    @IBAction func dismissViewController(sender : AnyObject) {
        //        Vibration.medium.vibrate()
        self.dismiss(animated: true, completion: nil)
    }
    
    /**
     To back root  view controller
     
     - Parameter sender: AnyObject
     */
    @IBAction func popToRootViewController(sender : AnyObject) {
        //        Vibration.medium.vibrate()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func openBottomSheetAnimate(vc : UIViewController, sizes : [SheetSize]){
        let options = SheetOptions(
            // The full height of the pull bar. The presented view controller will treat this area as a safearea inset on the top
            pullBarHeight: 0,
            
            // The corner radius of the shrunken presenting view controller
            presentingViewCornerRadius: 20,
            
            // Extends the background behind the pull bar or not
            shouldExtendBackground: true,
            
            // Attempts to use intrinsic heights on navigation controllers. This does not work well in combination with keyboards without your code handling it.
            setIntrinsicHeightOnNavigationControllers: false,
            
            // Pulls the view controller behind the safe area top, especially useful when embedding navigation controllers
            useFullScreenMode: true,
            
            // Shrinks the presenting view controller, similar to the native modal
            shrinkPresentingViewController: false,
            
            // Determines if using inline mode or not
            useInlineMode: true,
            
            // Adds a padding on the left and right of the sheet with this amount. Defaults to zero (no padding)
            horizontalPadding: 0,
            
            // Sets the maximum width allowed for the sheet. This defaults to nil and doesn't limit the width.
            maxWidth: nil
        )
        
        let sheet = SheetViewController(controller: vc, sizes: sizes, options: options)
        sheet.allowPullingPastMaxHeight = false
        sheet.modalPresentationStyle = .overFullScreen
        self.modalPresentationStyle = .overFullScreen
        sheet.animateIn(to: self.view, in: self)
    }
    
    @IBAction func animateOut(sender : AnyObject) {
        self.sheetViewController?.animateOut()
    }
}

extension UIViewController: SFSafariViewControllerDelegate {
    /**
     To open url in safari controller
     
     - Parameter url: Any web url
     */
    func openURL(_ url : String) {
        
        var url = url.url()
        if !(["http", "https"].contains(url.scheme?.lowercased())) {
            let appendedLink = "http://\(url)"
            
            url = appendedLink.url()
        }
        
        let safariVC = SFSafariViewController(url: url)
        safariVC.delegate = self
        UIApplication.topViewController()?.present(safariVC, animated: true, completion: nil)
        
    }
    
    public func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension UIViewController {
    /**
     To play youtube video in player view controller.
     
     - Parameter urlString: Video url string
     */
    func playYoutubeVideo(with urlString: String) {
        //First get video id from link and make playbel youtube link
        guard let url = URL(string: urlString) else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}


extension UIViewController : DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    //MARK:- Empty Data Set Delegate
    
    public func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage.signInLogo.image
    }
    
//    public func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
//        
//        let emptyDataSetText : String = AppMessages.ErrorEmptyDataSetTitle
//        
//        let attribute = GFunction.shared.getAttributedText(emptyDataSetText, attributeDic: [NSAttributedString.Key.foregroundColor : UIColor.app_222222 , NSAttributedString.Key.font : FontFamily.SFProDisplay.regular.font(size: 14)], range: (emptyDataSetText as NSString).range(of: emptyDataSetText))
//        
//        return attribute
//    }
    
    public func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        let emptyDataSetDiscription : String = AppMessages.ErrorEmptyDataSetDiscription
        
        let paragraphStyle1 = NSMutableParagraphStyle()
        paragraphStyle1.lineSpacing = 10
        paragraphStyle1.alignment = .center
        let attribute = GFunction.shared.getAttributedText(emptyDataSetDiscription, attributeDic: [NSAttributedString.Key.foregroundColor : UIColor.app_222222 , NSAttributedString.Key.font : FontFamily.SFProDisplay.regular.font(size: 12) , NSAttributedString.Key.paragraphStyle : paragraphStyle1], range: (emptyDataSetDiscription as NSString).range(of: emptyDataSetDiscription))
        
        return attribute
    }
}

extension UIViewController {
    static func swizzleViewWillAppear() {
        //Make sure This isn't a subclass of UIViewController, So that It applies to all UIViewController childs
        if self != UIViewController.self {
            return
        }
        let _: () = {
            let originalSelector = #selector(UIViewController.viewWillAppear(_:))
            let swizzledSelector = #selector(UIViewController.newViewWillAppear(_:))
            if let originalMethod = class_getInstanceMethod(self, originalSelector),
               let swizzledMethod = class_getInstanceMethod(self, swizzledSelector){
                method_exchangeImplementations(originalMethod, swizzledMethod)
            }
        }()
    }
    
    func hideShowMiniPlayer() {
        
        let enableMiniPlayerVC = [HomeVC.self, CategoriesVC.self, MySpaceVC.self, MyStoreVC.self, ProfileVC.self, ProductListingVC.self, SearchVC.self, ProductDescriptionVC.self, PaymentsVC.self, ChangeEmailNameNumberVC.self, WebContentVC.self, ContactUsVC.self, RewardVC.self]
        
        let resultFound = enableMiniPlayerVC.first { obj in
            return self.isKind(of: obj)
        }
        
//        if let vc = UIApplication.topViewController()?.tabBarController as? MiniPlayerVC , vc.norecordView.vwMiniPlayer != nil {
        
        if let vc = UIApplication.topViewController() as? TabVC , vc.miniPlayer.vwContainer != nil {
            
//            let detailVC = UIApplication.topViewController() as? NoteFullViewVC
//            if resultFound != nil { //found
////                vc.norecordView.isHidden = false
//                vc.norecordView.vwMiniPlayer.isHidden = false
//                vc.norecordView.btnClose.isHidden = false
//
//                if let _ = GAudioPlayer.shared.player {
//                    detailVC?.constViewComment.constant = 90.0
////                    detailVC?.vwComment.isHidden = false
//                }
//
//                    debugPrint("Const added")
////                }
//            } else {
////                vc.norecordView.isHidden = true
//                vc.norecordView.vwMiniPlayer.isHidden = true //hide player
//                vc.norecordView.btnClose.isHidden = true
////                DispatchQueue.main.async {
//                detailVC?.constViewComment.constant = 0.0
////                detailVC?.vwComment.isHidden = true
//                    debugPrint("Const removed", self)
////                }
//
//                if let _ = GAudioPlayer.shared.player, let detailsVC = UIApplication.topViewController() as? NoteFullViewVC {
////                    detailVC?.vwComment.isHidden = false
//                    detailsVC.constViewComment.constant = 75.0
////                    vc.norecordView.isHidden = false
//                    vc.norecordView.vwMiniPlayer.isHidden = false
//                    vc.norecordView.btnClose.isHidden = false
//                }
//            }
        }else {
            
            //Hide/show mini player
            if let vwPlayer = (UIApplication.topViewController()?.parent?.parent as? TabVC)?.miniPlayer.vwContainer {
                vwPlayer.isHidden = resultFound == nil
                (UIApplication.topViewController()?.parent?.parent as? TabVC)?.miniPlayer.btnCancel.isHidden = vwPlayer.isHidden
            }
        }
    }
    
    
    @objc func newViewWillAppear(_ animated: Bool) {
        self.newViewWillAppear(animated) //Incase we need to override this method
        
        ///MiniPlayer
        
        self.hideShowMiniPlayer()
        
        ///Tabbar
        let enableTaBar =  [ HomeVC.self, CategoriesVC.self, MyStoreVC.self, MySpaceVC.self, ProfileVC.self]//, ActiveUserProfileVC.self,EchoNoteVC.self, NotificationVC.self, NoteDescriptionVC.self ]
        
        let vc = UIApplication.topViewController()?.tabBarController as? TabVC
        for obj in enableTaBar {
            
            if let topVC = UIApplication.topViewController(), topVC.isKind(of: obj) {
                self.tabBarController?.tabBar.isHidden = false
                vc?.miniPlayer.frame = CGRect(x: 0, y: ScreenSize.height - (kMiniPlayerConstant + self.bottomBarHeight), width: ScreenSize.width, height: kMiniPlayerConstant)
                
                return
            } else {
                self.tabBarController?.tabBar.isHidden = true
                vc?.miniPlayer.frame = CGRect(x: 0, y: ScreenSize.height - (kMiniPlayerConstant + self.safebottomBarHeight), width: ScreenSize.width, height: kMiniPlayerConstant)
                
            }

        }
    }
}
