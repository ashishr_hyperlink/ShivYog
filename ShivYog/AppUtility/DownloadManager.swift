

import Foundation
import UIKit

class DownloadManager : NSObject, URLSessionDelegate, URLSessionDownloadDelegate {

    static var shared = DownloadManager()
    
    var arrTaskDataForDownload : [DownloadTaskModel] = []
    var downloadTaskCompletion : ((URLSessionDownloadTask) -> Void)?
    var newFileLocation : URL?
    var updateValueCompletion : ((Float, Int, Int) -> Void)?
    typealias ProgressHandler = (Float) -> ()

    var onProgress : ProgressHandler? {
        didSet {
//            if onProgress != nil {
//                let _ = activate()
//            }
        }
    }

    override private init() {
        super.init()
    }

    func activate() -> URLSession {
        let config = URLSessionConfiguration.background(withIdentifier: "\(Bundle.main.bundleIdentifier!).background")

        // Warning: If an URLSession still exists from a previous download, it doesn't create a new URLSession object but returns the existing one with the old delegate object attached!
        return URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue())
    }

    private func calculateProgress(session : URLSession, completionHandler : @escaping (Float) -> ()) {
        session.getTasksWithCompletionHandler { (tasks, uploads, downloads) in
            let bytesReceived = downloads.map{ $0.countOfBytesReceived }.reduce(0, +)
            let bytesExpectedToReceive = downloads.map{ $0.countOfBytesExpectedToReceive }.reduce(0, +)
            let progress = bytesExpectedToReceive > 0 ? Float(bytesReceived) / Float(bytesExpectedToReceive) : 0.0
//            self.updateCompeltedProgress(task: downloads, progress: progress)
            completionHandler(progress)
        }
    }

    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {

        if totalBytesExpectedToWrite > 0 {
            if let onProgress = onProgress {
                calculateProgress(session: session, completionHandler: onProgress)
            }
            //let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
           // debugPrint("Progress \(downloadTask) \(progress)")

        }
    }
    
    private func updateCompeltedProgress(task: [URLSessionDownloadTask], progress : Float) {
        var attachedProgress : Float = 0.0
        var thumbProgress : Float = 0.0
        var fileProgress : Float = 0.0
        
        let filterTaskData = self.arrTaskDataForDownload.filter { item in
            task.compactMap {
                $0.taskIdentifier == item.downloadTask.taskIdentifier
            }.first!
        }
        
        
        if let taskData = filterTaskData.first /*self.arrTaskDataForDownload.first(where: { taskModel in
                                                task.compactMap { $0.taskIdentifier == taskModel.downloadTask.taskIdentifier
                                                
                                                }.first
                                                }).first(where:  task.compactMap({  })  ) */{
            let dwnFileType = taskData.downloadingFileType
            
            let dataDecode = UserDefaults.standard.decode(for: DownloadMainModel.self, using: UserDefaults.Keys.downloadContent)
            if let decode = dataDecode {
                let downLoadData = decode.data
                
                if dwnFileType == .attachedFile {
                    attachedProgress = progress
                    
                } else if dwnFileType == .thumbImage {
                    thumbProgress = progress
                    
                } else if dwnFileType == .audio || dwnFileType == .video || dwnFileType == .pdf {
                    fileProgress = progress
                }
                
                if let arrDownload = downLoadData {
                    for (idx,item) in arrDownload.enumerated() {
                        let modelData = CategoryContentModel(fromJson: item.fileContent)
                        if modelData.id == taskData.contentModel.id {
                            //                            if modelData.contentType == .pdf {
                            attachedProgress = item.isAttachedFileDownload ? 1.0 : attachedProgress
                            thumbProgress = item.isThumbDownload ? 1.0 : thumbProgress
                            fileProgress = item.isFileDownload ? 1.0 : fileProgress
                            //                            } else {
                            item.downloadProgress = Double(attachedProgress + thumbProgress + fileProgress)
                            
                            item.downloadProgress = modelData.contentType == .pdf ? ((item.downloadProgress / 3) * 100) : ((item.downloadProgress / 2) * 100)
                            self.updateValueCompletion?(attachedProgress, idx, modelData.id)
                            
                        }
                    }
                }
                //                    let data =  DownloadMainModel(modelData: arrDownload)
                //                    UserDefaults.standard.encode(for: data, using: UserDefaults.Keys.downloadContent)
                //                }
            }
        }
    }
    
    

    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
//        DispatchQueue.main.async {
            
            URL.moveFile(oldLocation: location, fileName: String.uniqueRandom() + "." + (downloadTask.currentRequest?.url?.pathExtension ?? location.pathExtension))
//            debugPrint("TASK download location :- ", self.newFileLocation?.description as Any)
            //        onProgress?(1.0)
            //        self.arrTaskDataForDownload.forEach {
            for item in self.arrTaskDataForDownload {

                if  item.downloadTask.taskIdentifier == downloadTask.taskIdentifier {
                    item.status = .completed
//                    debugPrint("TASK completed donwload task:-", downloadTask.taskIdentifier)
//                    debugPrint("Download File type:-", item.downloadingFileType)
                    if let newLocation = self.newFileLocation {
                        item.localFileLocation = newLocation.description
                    }
//                    debugPrint("Model file location:-", item.localFileLocation as Any)
                    break
                }
            }
            //        self.newFileLocation = nil
            self.downloadTaskCompletion?(downloadTask)
//        }
    }

    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
         debugPrint("Task Error: \(task), error: \(String(describing: error))")
    }
    
}


extension URL {
    
    static func moveFile(oldLocation: URL,fileName: String) {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let dataPath = documentsDirectory.appendingPathComponent(Bundle.main.displayName ?? "app")
        
        //If Directory not exist create specific directory and then save file
        if !URL.checkPath(dataPath.path) {
            do {
                try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)
             } catch {
                 return
            }
        }
        
        let audioFile = dataPath.appendingPathComponent(fileName)
        if URL.checkPath(audioFile.path) {
            do {
                try FileManager.default.removeItem(atPath: audioFile.path)
            } catch {
                return
            }
        }
        
        do {
            try FileManager.default.copyItem(at: oldLocation, to: audioFile)
            DownloadManager.shared.newFileLocation = audioFile
        } catch {
            print("Error")
        }
    }
    
    static func checkPath(_ path: String) -> Bool {
        let isFileExist = FileManager.default.fileExists(atPath: path)
        return isFileExist
    }
    
    static func removItem(url: String) {
//        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
//        let dataPath = url.deletingLastPathComponent()
        
        if let url = URL(string: url) {
            
            if URL.checkPath(url.path) {
                do {
                    try FileManager.default.removeItem(at: url)
                } catch {
                    print("Delete error:-",error.localizedDescription)
                }
                
            }
        }
//        for item in documentsDirectory {
//
//            debugPrint("DownloadTASK:-", item.appendingPathComponent(Bundle.main.displayName ?? "app"))
//        }
        
        
        
//        let dataPath = url.deletingLastPathComponent()
//        if URL.checkPath(dataPath.path) {
//
//        }
//
//        do {
//            let fileName = try FileManager.default.contentsOfDirectory(at: documentsDirectory, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
//
//            for fileName in fileName {
//                if url == fileName {
//                    try FileManager.default.removeItem(at: url)
//                }
//            }
//        } catch let error {
//            print(error)
//        }
    }
}
