//
//  AppMessages.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 22/09/20.
//  Copyright © 2020 KISHAN_RAJA. All rights reserved.
//

import Foundation

//MARK: AppMessages
struct AppMessages {
    static let invalidOTP                 = "Invalid OTP"
    static let resendOTP                  = "OTP has been sent to your registered email"
    static let carrierNotAvailable        = "Carrier service not available"
    static let registerMessage            = "Are you sure you want to register in this webinar?"
    static let unregisterMessage          = "Are you sure you want to unregister from this webinar?"
    static let registerdSucess            = "You have successfully registered"
    static let unregisterdSucess          = "You have successfully unregistered"
    static let logoutMessage              = "Are you sure you want to log out?"
    static let clearHistoryMessage              = "Are you sure you want to clear history?"
    static let closeAccountMessage              = "Are you sure you want to close your account?"
    static let copyURLToClipBoard         = "Copied to clipboard"
    static let deleteNotification         = "Are you sure you want to delete notification?"
    static let passwordUpdate             = "Password change successfully"
    static let profileUpdate              = "Profile updated successfully"
    static let passwordReset              = "Password reset link has been sent successfully to your email id"
    static let internetConnectionMsg      = "Please, check your internet connection"
    static let errorOccurred              = "Error occurred ! Please try again"
    static let somethingWentWrong         = "Something went wrong"
    static let imagepickerHeading         = "CHOOSE IMAGE FROM"
    static let imagepickerHeadingForVideo = "CHOOSE VIDEO FROM"
    static let mediaHeading               = "CHOOSE MEDIA FROM"
    static let imagepickerCamera          = "Camera"
    static let imagepickerGallery         = "Gallery"
    static let imagepickerCameraError     = "Camera not supported"
    static let ok                         = "Ok"
    static let cancel                     = "Cancel"
    static let set                        = "Set"
    static let yes                        = "Yes"
    static let no                         = "No"
    static let ErrorEmptyDataSetTitle     = "Sorry, nothing found here."
    static let ErrorEmptyDataSetDiscription = "Please check back at another time and we might have something more."
    static let removeFromFavourite          = "Are you sure you want to remove this item from favorites?"
    static let removeFromPlaylist         = "Are you sure you want to remove this item from playlist?"
    static let removeFromDownloads         = "Are you sure you want to remove this item from downloads?"
    static let deleteMessageConfimation     = "Are you sure you want to delete?"
    static let removePlaylist               = "Are you sure you want to delete the   playlist?"
    
}
