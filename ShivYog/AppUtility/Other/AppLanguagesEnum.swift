//
//  AppLanguagesEnum.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 28/12/20.
//

import Foundation

//MARK:- App language
enum AppLanguages: String, Codable, CaseIterable{
    case english    = "en"
    case arabic     = "ar"
    
    var headerValue: String {
        switch self {
        case .english:
            return "en"
        case .arabic:
            return "ar"
        }
    }
    
    var displayName: String {
        switch self {
        case .english:
            return "English"
        case .arabic:
            return "Arabic"
        }
    }
}

enum HomePageTitle : String{
    case FeaturedContent        =   "Featured Content"
    case WhatsNew               =   "What’s New"
    case FeaturedPlaylists      =   "Featured Playlists"
    case ContinueWatching       =   "Continue Watching"
}

enum JourneyStatus : String{
    case Bronze                 =   "Bronze"
    case Silver                 =   "Silver"
    case Gold                   =   "Gold"
}
