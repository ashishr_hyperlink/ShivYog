//
//  WebViewLinks.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 28/02/21.
//

import Foundation

//MARK: WebViewLinks
enum WebViewLinks {
    case privacyPolicy
    case termsConditions
    case aboutUs
    case faq
    case navigateThroughApp
    case help
    case createAccount
    case home
    case subscription
    
    var url: URL? {
        switch self {
        case .privacyPolicy:
            return URL(string: APIEnvironment.getUrl(state: NetworkManager.environment).baseurl + "cms/privacy-policy/")
            
        case .termsConditions:
            return URL(string: APIEnvironment.getUrl(state: NetworkManager.environment).baseurl + "cms/terms-condition/")
            
        case .aboutUs:
            return URL(string: APIEnvironment.getUrl(state: NetworkManager.environment).baseurl + "cms/about-us/")
            
        case .faq:
            return URL(string: APIEnvironment.getUrl(state: NetworkManager.environment).baseurl + "cms/help-faq/")
            
        case .navigateThroughApp:
            return URL(string: "https://about.google/")
            
        case .help:
            return URL(string: APIEnvironment.getUrl(state: NetworkManager.environment).baseurl + "cms/help-faq/")
            
        case .createAccount:
            return URL(string: "http://web.shivyogapp.com/website/auth/signup")
        case .home:
            return URL(string: "https://shivyog.com/what-is-shivyog/")
            
        case .subscription:
            return URL(string: "http://web.shivyogapp.com/subscription/")
        }
    }
    
    var title: String {
        switch self {
        case .privacyPolicy:
            return "Privacy Policy"
        case .termsConditions:
            return "Terms of Service"
        case .aboutUs:
            return "About Us"
        case .faq:
            return "Help & Faq’s"
        case .navigateThroughApp:
            return "Navigate through app"
        case .help:
            return "Help"
        case .createAccount:
            return "Create Account"
        case .home:
            return "ShivYog"
        case .subscription:
            return "http://web.shivyogapp.com/subscription"
        default: return ""
        }
    }
}
