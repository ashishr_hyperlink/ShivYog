//
//  GAPI.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 10/10/20.
//  Copyright © 2020 KISHAN_RAJA. All rights reserved.
//

import Foundation
import UIKit

///Gloabl API for used API in whole project
class GlobalAPI : NSObject {
    
    ///Shared instance
    static let shared : GlobalAPI = GlobalAPI()
    private var arrPreferences : [PreferenceModel]?
    private var arrCategories : [CategoryModel] = []
    private(set) var isPreferenceFetchComplete = Bindable<Bool>()
    private(set) var isCategoriesFetchComplete = Bindable<Bool>()
    var arrGDownload : [DownloadModel] = []
}

// For Preferences
extension GlobalAPI{
    func getPreferences(withLoader isLoader : Bool = false){
        ApiManager.shared.makeRequest(endPoint: .category(.tag), methodType: .get, parameter: nil, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                
                self.arrPreferences = PreferenceModel.listFromArray(data: apiData.data["results"].arrayValue)
                self.updateToUserData()
                self.isPreferenceFetchComplete.value = true
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
    
    func getNumberOfPreferences() -> Int?{
        return self.arrPreferences?.count ?? nil
    }
    
    func getDataAtIndexForPreferences(index : Int) -> PreferenceModel?{
        return self.arrPreferences?[index]
    }
    
    func updateDataForPreference(index : Int){
        guard let preferences = self.arrPreferences else{ return }
        preferences[index].isSelected.toggle()
        self.arrPreferences = preferences
    }
    
    func updateAllPreference(isSelect : Bool){
        guard let preferences = self.arrPreferences else{ return }
        preferences.map{$0.isSelected = isSelect}
        self.arrPreferences = preferences
    }
    
    func isAllPreferenceSelected() -> Bool{
        guard let preferences = self.arrPreferences else{ return false}
        return ((preferences.filter{$0.isSelected}).count == self.arrPreferences?.count ?? 0)
    }
    
    func getSelectedTags() -> [PreferenceModel]{
        return self.arrPreferences?.filter{$0.isSelected} ?? []
    }
    
    func updateToUserData(){
        (self.arrPreferences ?? []).map{
            $0.isSelected = UserModel.currentUser?.tag.contains($0.id) ?? false
        }
    }
}

//For Category
extension GlobalAPI{
    func getCategories(withLoader isLoader : Bool = false){
        ApiManager.shared.makeRequest(endPoint: .category(.category), methodType: .get, parameter: nil, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                let selectedCat = self.arrCategories.filter{$0.isSelected}.first
                self.arrCategories = CategoryModel.listFromArray(data: apiData.data["results"].arrayValue)
                if let data = selectedCat{
                    self.arrCategories.map{
                        $0.isSelected = false
                        if $0.id == data.id{
                            $0.isSelected = true
                        }
                    }
                }
                else if self.arrCategories.count > 0{
                    self.arrCategories[0].isSelected = true
                }
                self.isCategoriesFetchComplete.value = true
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
    
    func getNumberOfCategories() -> Int{
        return self.arrCategories.count
    }
    
    func getDataAtIndexForCategories(index : Int) -> CategoryModel{
        return self .arrCategories[index]
    }
    
    func updateDataForCategories(index : Int){
        self.arrCategories.map{$0.isSelected = false}
        self.arrCategories[index].isSelected = true
    }
    
    func getSelectedCategories() -> CategoryModel? {
        return (self.arrCategories.filter{$0.isSelected}).first
    }
}

extension GlobalAPI{
    func markAsFav(withLoader isLoader : Bool = false, contentData : CategoryContentModel){
        var dictData = Dictionary<String,Any>()
        
        if contentData.content == "store media content" {
            dictData["store_media_content_id"] = contentData.id
        } else {
            dictData["media_content_id"] = contentData.id
        }
        ApiManager.shared.makeRequest(endPoint: .content(.favourite), methodType: .post, parameter: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                
//                self.isCategoriesFetchComplete.value = true
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
}

extension GlobalAPI{
    func getUserData(withLoader isLoader : Bool = false){
        ApiManager.shared.makeRequest(endPoint: .editProfile(.editProfile(UserModel.currentUser?.uuid ?? "")), methodType: .get, parameter: nil, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                debugPrint(apiData.data)
                UserModel.currentUser = UserModel(fromJson: apiData.data)
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
}

extension GlobalAPI{
    func addUpdateContinueWatching(withLoader isLoader : Bool = false, contentData : CategoryContentModel, watchedDuration : Int, interval : Int, isDelete : Bool = false){
        var dictData = Dictionary<String,Any>()
        if contentData.content == "store media content" {
            dictData["store_media_content"] = contentData.id
        } else {
            dictData["media_content"] = contentData.id
        }
        dictData["watched_duration"] = watchedDuration
        dictData["interval"] = interval
        dictData["is_delete"] = isDelete
        ApiManager.shared.makeRequest(endPoint: .content(.continueWatching), methodType: .post, parameter: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                if let topVC = UIApplication.topViewController() as? HomeVC{
                    topVC.viewWillAppear(false)
                }
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
    
    func removeContinueWatching(withLoader isLoader : Bool = false, contentData : CategoryContentModel, watchedDuration : Int, interval : Int){
        
        var dictData = Dictionary<String,Any>()
        if contentData.content == "store media content" {
            dictData["store_media_content"] = contentData.id
        } else {
            dictData["media_content"] = contentData.id
        }
        dictData["watched_duration"] = watchedDuration
        dictData["interval"] = interval
        dictData["is_delete"] = true
        
        ApiManager.shared.makeRequest(endPoint: .content(.continueWatching), methodType: .post, parameter: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
}

extension GlobalAPI{
    func apiGetMediaContentByID(id : String, withLoader isLoader : Bool = false){
        var dictData = Dictionary<String,String>()
        dictData["id"] = id
        
        ApiManager.shared.makeRequest(endPoint: .content(.mediacontent), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                let arrData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)
                debugPrint("Is User Subscribe:-", UserModel.isSubscribeUser)
                if let content = arrData.first{
//                    if content.isSubscribe{
//                        if !UserModel.isSubscribeUser{
//                            Alert.shared.showSnackBar("Please subscribe to watch this content")
//                            return
//                        }
//                    }
                    if !UserModel.canOpenMediaWithoutSubcription(content: content){
                        return
                    }
                    if content.contentType == .audio {
                        let vc = StoryboardScene.Home.audioPlayerVC.instantiate()
                        if GAudioPlayer.shared.isPlaying, GAudioPlayer.shared.mediaData?.id == content.id{
                            vc.isFrom = .miniPlayer
                        }
                        else{
                            vc.isFrom = .home
                        }
                        vc.mediaData = content
                        vc.hidesBottomBarWhenPushed = true
                        UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
                    } else if content.contentType == .video {
                        let vc = StoryboardScene.Home.videoPlayerVC.instantiate()
                        vc.isFrom = .home
                        vc.hidesBottomBarWhenPushed = true
                        vc.mediaData = content
                        UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
                        
                    } else if content.contentType == .pdf {
                        let vc = StoryboardScene.Home.pdfViewerVC.instantiate()
                        vc.hidesBottomBarWhenPushed = true
                        vc.mediaData = content
                        UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
    
    
    func apiGetStoreMediaContentByID(id : String, withLoader isLoader : Bool = false, storeContentId: String){
        var dictData = Dictionary<String,String>()
        dictData["id"] = id
        dictData["storesubcontent_id"] = storeContentId
        dictData["tag__id__in"] = UserModel.selectedPref
        
        ApiManager.shared.makeRequest(endPoint: .store(.storeProductShare), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                let arrData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)
                debugPrint("Is User Subscribe:-", UserModel.isSubscribeUser)
                if let content = arrData.first{
//                    if content.isSubscribe{
//                        if !UserModel.isSubscribeUser{
//                            Alert.shared.showSnackBar("Please subscribe to watch this content")
//                            return
//                        }
//                    }
                    if !UserModel.canOpenMediaWithoutSubcription(content: content){
                        return
                    }
                    if content.contentType == .audio {
                        let vc = StoryboardScene.Home.audioPlayerVC.instantiate()
                        if GAudioPlayer.shared.isPlaying, GAudioPlayer.shared.mediaData?.id == content.id{
                            vc.isFrom = .miniPlayer
                        }
                        else{
                            vc.isFrom = .home
                        }
                        vc.mediaData = content
                        vc.hidesBottomBarWhenPushed = true
                        UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
                    } else if content.contentType == .video {
                        let vc = StoryboardScene.Home.videoPlayerVC.instantiate()
                        vc.isFrom = .home
                        vc.hidesBottomBarWhenPushed = true
                        vc.mediaData = content
                        UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
                        
                    } else if content.contentType == .pdf {
                        let vc = StoryboardScene.Home.pdfViewerVC.instantiate()
                        vc.hidesBottomBarWhenPushed = true
                        vc.mediaData = content
                        UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
                    }
                } else {
                    self.apiStoreSubContent(id: storeContentId)
                }
                
                break
            case .failure(let error):
//                self.apiStoreSubContent(id: storeContentId)
                print("the error \(error)")
            }
        }
    }
    
    func apiStoreSubContent(id: String) {
        var dictData = Dictionary<String, String>()
        dictData["id"] = id
        
        ApiManager.shared.makeRequest(endPoint: .store(.storeSubcontent), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: true, withLoader: true, withdebugLog: true) { [weak self] (result) in
            guard let self = self else { return }
            
            switch result {
            case .success(let apiData):
                let vc = StoryboardScene.Store.productDescriptionVC.instantiate()
                let arrData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)
                if let data = arrData.first{
                    vc.storeData = data
                }
                vc.hidesBottomBarWhenPushed = true
                UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
                debugPrint(apiData)
            case .failure(let error):
                print("the error \(error)")
                break
            }
        }
    }
}
