//
//  LocationManager.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 18/12/20.
//

import UIKit
import CoreLocation

@objc protocol LocationManagerDelegate: AnyObject {
    @objc optional func didUpdateLocation(locations: CLLocation)
    @objc optional func didUpdateLocationOnAppDidBecomeActive(locations: CLLocation)
    @objc optional func didChangeAuthorizationStatus(status: CLAuthorizationStatus)
}

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    static let shared : LocationManager = LocationManager()
    
    var location : CLLocation = CLLocation()
    
    private var locationManager : CLLocationManager = CLLocationManager()
    
    weak var delegate: LocationManagerDelegate?
    var isNotifyOnLocationOff: Bool = true
    
    // Use For continues update
    var locationStatus = ""
    
    //---------------------------------------------------------------------
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override init() {
        super.init()
        locationManager.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(LocationManager.didBecomeActiveNotification(_:)), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    //MARK: - Current Lat Long
    
    //MARK: To get location permission just call this method
    
    func requestLocationAuth() {
        locationManager.requestWhenInUseAuthorization()
        
        if #available(iOS 14.0, *) {
            //            locationManager.desiredAccuracy = kCLLocationAccuracyReduced
        } else {
            // Fallback on earlier versions
        }
        
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways:
            locationStatus = "authorized always"
            checkLocationAccuracyAllowed()
        case .authorizedWhenInUse:
            locationStatus = "authorized when in use"
            checkLocationAccuracyAllowed()
        case .notDetermined:
            locationStatus = "not determined"
        case .restricted:
            locationStatus = "restricted"
        case .denied:
            locationStatus = "denied"
        default:
            locationStatus = "other"
        }
    }
    
    func checkLocationAccuracyAllowed() {
        if #available(iOS 14.0, *) {
            switch locationManager.accuracyAuthorization {
            case .reducedAccuracy:
                locationStatus = "approximate location"
            case .fullAccuracy:
                locationStatus = "accurate location"
            @unknown default:
                locationStatus = "unknown type"
            }
        } else {
            // Fallback on earlier versions
        }
        locationManager.startUpdatingLocation()
    }
    
    
    //MARK: To get permission is allowed or declined
    func checkStatus() -> CLAuthorizationStatus{
        return CLLocationManager.authorizationStatus()
    }
    
    //MARK: To get user's current location
    func getUserLocation() -> CLLocation {
        if isNotifyOnLocationOff && !self.isLocationServiceEnabled() {
            return CLLocation(latitude: 0.0, longitude: 0.0)
        }
        
        if location.coordinate.longitude == 0.0 {
            return CLLocation(latitude: 0.0, longitude: 0.0)
            
        }
        return location
    }
    
    //MARK: Delegate method
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        location = locations[0]
        
        if let _ = self.delegate {
            self.delegate?.didUpdateLocation?(locations: location)
        }
    }
    
    private func isLocationServiceEnabled() -> Bool {
        
        if CLLocationManager.authorizationStatus() == .denied {
            let alertController = UIAlertController(title: "App Permission Denied".localized, message: "To re-enable, please go to Settings and turn on Location Service for ".localized + "\(Bundle.appName())", preferredStyle: .alert)
            
            let setting = UIAlertAction(title: "Go to Settings".localized, style: .default, handler: { (UIAlertAction) in
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)! as URL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    UIApplication.shared.openURL(URL(string: UIApplication.openSettingsURLString)! as URL)
                }
            })
            
            let close = UIAlertAction(title: "Close".localized, style: .default, handler: { (UIAlertAction) in
                
            })
            
            alertController.addAction(setting)
            alertController.addAction(close)
            
            UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
            return false
        } else {
            return true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if let _ = self.delegate {
            self.delegate?.didChangeAuthorizationStatus?(status: status)
        }
        switch status {
        
        case .denied:
            print("Permission Denied")
            break
        case .notDetermined:
            print("Permission Not Determined G")
            break
            
        default:
            guard let location = manager.location else {
                return
            }
            self.location = location
            print("\(location.coordinate.latitude)")
            print("\(location.coordinate.longitude)")
            break
        }
    }
    
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if let _ = self.delegate {
            self.delegate?.didChangeAuthorizationStatus?(status: status)
        }
        switch status {
        
        case .denied:
            print("Permission Denied")
            break
        case .notDetermined:
            print("Permission Not Determined G")
            break
            
        default:
            print("\(location.coordinate.latitude)")
            print("\(location.coordinate.longitude)")
            break
        }
    }
    
    @objc func didBecomeActiveNotification(_ notification: Notification) {
        self.location = self.getUserLocation()
        if let _ = self.delegate {
            self.delegate?.didUpdateLocationOnAppDidBecomeActive?(locations: location)
        }
    }
}

extension LocationManager {
    func reverseGocode(_ location:CLLocation, completionHandler : @escaping ((_ reverseGecodeInfo:NSDictionary?,_ placemark:CLPlacemark?, _ error:String?)->Void)){
        
        let geocoder: CLGeocoder = CLGeocoder()
        
        let locale: String = "en"
        
        geocoder.reverseGeocodeLocation(location,preferredLocale: Locale(identifier: locale), completionHandler: {(placemarks, error)->Void in
            
            if (error != nil) {
                completionHandler(nil,nil, error!.localizedDescription)
                
            }
            else{
                
                if let placemark = placemarks?.first {
                    let address = AddressParser()
                    address.parseAppleLocationData(placemark)
                    let addressDict = address.getAddressDictionary()
                    completionHandler(addressDict,placemark,nil)
                }
                else {
                    completionHandler(nil,nil,"No Placemarks Found!")
                    return
                }
            }
        })
        
    }
}

private class AddressParser: NSObject{
    
    fileprivate var latitude = String()
    fileprivate var longitude  = String()
    fileprivate var streetNumber = String()
    fileprivate var route = String()
    fileprivate var locality = String()
    fileprivate var subLocality = String()
    fileprivate var formattedAddress = String()
    fileprivate var administrativeArea = String()
    fileprivate var administrativeAreaCode = String()
    fileprivate var subAdministrativeArea = String()
    fileprivate var postalCode = String()
    fileprivate var country = String()
    fileprivate var subThoroughfare = String()
    fileprivate var thoroughfare = String()
    fileprivate var ISOcountryCode = String()
    fileprivate var state = String()
    
    
    override init(){
        
        super.init()
        
    }
    
    fileprivate func getAddressDictionary()-> NSDictionary{
        
        let addressDict = NSMutableDictionary()
        
        addressDict.setValue(latitude, forKey: "latitude")
        addressDict.setValue(longitude, forKey: "longitude")
        addressDict.setValue(streetNumber, forKey: "streetNumber")
        addressDict.setValue(locality, forKey: "locality")
        addressDict.setValue(subLocality, forKey: "subLocality")
        addressDict.setValue(administrativeArea, forKey: "administrativeArea")
        addressDict.setValue(postalCode, forKey: "postalCode")
        addressDict.setValue(country, forKey: "country")
        addressDict.setValue(formattedAddress, forKey: "formattedAddress")
        
        return addressDict
    }
    
    
    fileprivate func parseAppleLocationData(_ placemark:CLPlacemark){
        
        //self.streetNumber = placemark.subThoroughfare ? placemark.subThoroughfare : ""
        self.streetNumber = placemark.thoroughfare ?? ""
        self.locality = placemark.locality ?? ""
        self.postalCode = placemark.postalCode ?? ""
        self.subLocality = placemark.subLocality ?? ""
        self.administrativeArea = placemark.administrativeArea ?? ""
        self.country = placemark.country ?? ""
        self.longitude = placemark.location?.coordinate.longitude.description ?? ""
        self.latitude = placemark.location?.coordinate.latitude.description ?? ""
        self.formattedAddress = self.setUpLocation(placemark: placemark)
    }
    
    fileprivate func setUpLocation(placemark: CLPlacemark?) -> String {
        guard let pm = placemark else { return "" }
        
        var addressString : String = ""
        if pm.subLocality != nil {
            addressString = addressString + pm.subLocality! + ", "
        }
        if pm.thoroughfare != nil {
            addressString = addressString + pm.thoroughfare! + ", "
        }
        if pm.locality != nil {
            addressString = addressString + pm.locality! + ", "
        }
        if pm.country != nil {
            addressString = addressString + pm.country! + ", "
        }
        if pm.postalCode != nil {
            addressString = addressString + pm.postalCode! + " "
        }
        
        return addressString
        
    }
}
