//
//  ValidationError.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 03/03/21.
//

import Foundation

extension AppError.Enums {
    enum ValidationError {
        case enterValidEmail
        case enterEmail
        case enterValidPhone
        case enterPhone
        case enterPassword
        case enterName
        case enterFirstName
        case enterLastName
        case enterValidName
        case enterMinName
        case enterMobileNumber
        case enterValidMobile
        case enterChangeNumber
        case enterValidChangeNumber
        case enterMinMobileNumber
        case enterCountryCode
        case enterConfirmPassword
        case passwordMismatch
        case enterMinPassword
        case enterNewPassword
        case enterValidPassword
        case enterCurrentPassword
        case enterMinNewPassword
        case enterOTP
        case validOTP

        case agreeTerms
        case addHealthcardPhoto
        case selectDOB
        case selectGender
        case enterAddress
        case enterCardNumber
        case enterBAGNumber
        case enterAHVNumber
        case selectExpiryDate
        case enterCompany
        case enterConcordiaNumber
        
        case enterStreetAddress
        case enterBuildingNo
        case enterPostCode
        
        case enterSubject
        case enterDescription
        case oldAndNewPasswordSame
        
        case enterComment
        case enterGiftCode
        case enterPlaylistName
        case enterValidPlaylistName
        case custom(errorDescription: String?)
    }
}

extension AppError.Enums.ValidationError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .enterValidEmail: return "Please enter valid email"
        case .enterEmail: return "Please enter email"
        case .enterPhone: return "Please enter phone number"
        case .enterValidPhone: return "Please enter valid phone number"
        case .enterPassword: return "Please enter password"
        case .enterName: return "Please enter name"
        case .enterFirstName: return "Please enter first name"
        case .enterLastName: return "Please enter last name"
        case .enterValidName: return "Please enter valid name"
        case .enterMinName: return "Please enter minimum of two characters in name"
        case .enterMobileNumber: return "Please enter phone number"
        case .enterValidMobile: return "Please enter valid phone number"
        case .enterChangeNumber : return "Please enter mobile number"
        case .enterValidChangeNumber : return "Please enter valid mobile number"
        case .enterMinMobileNumber: return "Please enter minimum 10 and maximum 13 digit for mobile number"
        case .enterCountryCode: return "Please select country code"
        case .enterConfirmPassword: return "Please enter confirm password"
        case .passwordMismatch: return "Password mismatch"
        case .enterMinPassword: return "Please enter minimum of 6 characters for create password"
        case .enterNewPassword: return "Please enter new password"
        case .enterValidPassword: return "Please enter at least 1 uppercase, 1 lowercase, 1 digit, 1 special character and minimum 8 character for password"//"Minimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character"
        case .enterCurrentPassword: return "Please enter current password"
        case .enterMinNewPassword: return "Please enter minimum of 6 characters for new password."
        case .enterOTP: return "Please enter OTP"
        case .validOTP: return "Please enter valid OTP"
            
        case .agreeTerms: return "Please accept terms and conditions and privacy policy"
        case .addHealthcardPhoto: return "Please add health card photo"
        case .selectDOB : return "Please select date of birth"
        case .selectGender : return "Please select gender"
        case .enterAddress : return "Please enter address"
        case .enterCardNumber : return "Please enter card number"
        case .enterBAGNumber : return "Please enter BAG number"
        case .enterAHVNumber : return "Please eneter AHV number"
        case .selectExpiryDate : return "Please select expiry date"
        case .enterCompany : return "Please enter company"
        case .enterConcordiaNumber : return "Please enter concordia number"
            
        case .enterStreetAddress : return "Please enter street address and city"
        case .enterBuildingNo : return "Please enter apt suite bldg#"
        case .enterPostCode : return "Please enter post code"
        case .enterComment : return "Please enter comment"
        case .enterSubject : return "Please enter subject"
        case .enterDescription : return "Please enter description"
        case .oldAndNewPasswordSame : return "Password must be different than current password"
        case .enterGiftCode : return "Please enter Gift Code"
        case .enterPlaylistName : return "Please enter playlist name"
        case .enterValidPlaylistName : return "Please enter Valid playlist name"
        case .custom(let errorDescription): return errorDescription
        }
    }
}
