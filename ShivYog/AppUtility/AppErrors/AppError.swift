//
//  AppError.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 03/03/21.
//

import Foundation
import UIKit

// Credit: https://stackoverflow.com/a/57081275/14733292

enum AppError {
    case network(type: Enums.NetworkError)
    case file(type: Enums.FileError)
    case validation(type: Enums.ValidationError)
    case dataSource(type: Enums.DataSourceError)
    case custom(errorDescription: String?)
    class Enums { }
}

extension AppError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .network(let type): return type.localizedDescription
        case .file(let type): return type.localizedDescription
        case .validation(let type): return type.localizedDescription
        case .dataSource(let type): return type.localizedDescription
        case .custom(let errorDescription): return errorDescription
        }
    }
}

enum ValidationEnum {
    enum RegexType : String {
        case AlpabetsAndSpace = "^[A-Za-z ]*$"
        case OnlyNumber = "[0-9]"
        case UserId = "^[a-z][a-z0-9._]*$"
        case AllowDecimal = "^[0-9]*((\\.|,)[0-9]{0,2})?$"
    }
    
    enum PhoneNumber : Int {
        case Minimum = 8
        case Maximum = 13
    }
        
    enum Password : Int {
        case Minimum = 6
    }
    
    enum CardNumber : Int {
        case Minimum = 14
        case Maximum = 16
    }
    
    enum SecurityCode : Int {
        case Minimum = 3
        case Maximum = 4
    }
    
    enum BankInfo : Int {
        case AccountNumber  = 16
        case Routing        = 9
        case SSN            = 4
        case postalCode     = 6
    }
    
    enum ServiceCost : Int {
        case Maximum = 6
    }
}

enum WebViewType : String{
    case TermsConditions    =   "Terms & Conditions"
    case PrivacyPolicy      =   "Privacy Policy"
    case FAQs               =   "FAQs"
    case AboutUs            =   "About Us"
    case help               =   "Help"
    
    var link : String{
        switch self{
        case .PrivacyPolicy:
            return "https://www.google.com"
        case .TermsConditions:
            return "https://www.google.com"
        case .FAQs:
            return "https://www.google.com"
        case .AboutUs:
            return "https://www.google.com"
        case .help:
            return "https://www.google.com"
        }
    }
}

enum IsFrom {
    case signIn, profile, subscriptionPayment, storePayment, myProducts, playlists, downloads, favourites, myJourney, yogaStore, home, forgotPassword, categories, mySpace, videoPlayer, miniPlayer, changeMobile, preferences, featuredContent, whatsNew, featuredPlaylists, continueWatching, deletePlaylist
}

enum isFromEdit {
    case newData, editData
}

enum VerificationType {
    case forgotPassword, signIn
}

enum ChangeDetails {
    case email, name, mobile
}

enum ContentType : String {
    case audio  =   "audio"
    case video  =   "video"
    case pdf    =   "pdf"
    case normal =   "normal"
    case folder =   "folder"
    case none   =   "none"
}

enum PaymentStatus {
    case success, failure//, redeemGift, inAppPurchase
}

enum PaymentScreens {
    case inStore, redeemGift, inAppPurchase
}

enum ProfileSubOptionType : String {
    case name = "Change Name"
    case number = "Change Number"
    case email = "Change Email"
    case password = "Change Password"
    case subsriptionPayments = "Subscription Payments"
    case storePayments = "Store Payments"
    case preferences = "Your Preferences"
    case redeem = "Redeem Gift"
    case loggedDevices = "Logged In Devices"
    case help = "Help"
    case privacyPolicy = "Privacy Policy"
    case aboutUs = "Abouts Us"
    case contactUs = "Contact Us"
    case rateApp = "Rate App"
    case terms = "Terms of Service"
    
}

enum ProfileMainOptions : String {
    case personalDetails = "Personal Details"
    case paymentHistory = "Payments History"
    case settings = "Settings"
    
    var image: UIImage {
        switch self {
        case .personalDetails:
            return .personalDetail.image
        case .paymentHistory:
            return .paymentHistory.image
        case .settings:
            return .settings.image
        }
        
    }
}

enum MyJourneyTimeType : String{
    case monthly = "Monthly"
    case weekly = "Weekly"
}

enum MyJourneyTimeChangeType {
    case previous, next, none
}

enum PlayerStateEnum : String
{
    case play = "Play"
    case pause = "Pause"
    case stop = "Stop"
    case resume = "resume"
    case off = "off"
    
    static func isPlaySelected(stateButton : PlayerStateEnum) -> Bool {
        return (stateButton == .play || stateButton == .resume) ? true : false
    }
}

enum PlaylistOptions {
    case addToPlaylist, downloadNow, share, remove, cancel, editPlaylist
}
