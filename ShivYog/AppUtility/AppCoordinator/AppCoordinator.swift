//
//  AppCoordinator.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 02/03/21.
//

@_exported import UIKit

// This is for handel keyboard

// This is for handel optional data
@_exported import SwiftyJSON
import AVFoundation


class AppCoordinator: NSObject {
    
    var completion : (() -> Void)?
    //fixme
    func basicAppSetup() {
        
        FontFamily.registerAllCustomFonts()
        
        //Application setup
        UIApplication.shared.windows.first?.isExclusiveTouch = true
        UITextField.appearance().tintColor = .app_990000
        UITextView.appearance().tintColor = .app_990000
        
        UITabBar.appearance().backgroundColor = .clear
//        UITabBar.appearance().backgroundImage = .menu.image
//        UITabBar.appearance().shadowImage = UIImage()
        UITabBar.appearance().contentMode = .scaleAspectFit
        
        let alertView = UIView.appearance(whenContainedInInstancesOf: [UIAlertController.self])
        alertView.tintColor = .black
        
        UINavigationBar.appearance().barStyle = .blackOpaque
        
        //manage login
        if #available(iOS 13.0, *) {
//            UIApplication.shared.manageLogin()
        } else {
            UIApplication.shared.manageLogin()
        }
        
        //Google sign in init
        //        GIDSignIn.sharedInstance().clientID = AppCredential.googleClientID.rawValue
        
        // AWS Image upload configration
//        AWSUploadManager.shared.configure()
        
        //IQKeyboard Setup
        self.setUpIQKeyBoardManager()
        
        //Push setup
        AppDelegate.shared.registerForNotification()
        
        //Network Observer
        ReachabilityManager.shared.startObserving()
//        self.checkInternet()
        
        QorumLogs.enabled = true
    }
    
    private func setUpIQKeyBoardManager() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.placeholderColor = .app_990000
        IQKeyboardManager.shared.placeholderFont = .cFont(ofType: FontFamily.SFProDisplay.regular, withSize: 16)
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 5
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.shouldToolbarUsesTextFieldTintColor = true
        IQKeyboardManager.shared.disabledToolbarClasses = [VerificationVC.self, CreatePlaylistVC.self, SearchVC.self]
        IQKeyboardManager.shared.disabledTouchResignedClasses = []

//        IQKeyboardManager.shared.disabledDistanceHandlingClasses = [CreatePasswordVC.self]

    }
    
    private func checkInternet() {
        if !ReachabilityManager.isReachable {
            if UserDefaultsConfig.isAuthorization{
                UIApplication.topViewController()?.tabBarController?.tabBar.isHidden = true
                let vc = NetWorkErrorVC(nibName: "NetWorkErrorVC", bundle: nil)
                UIApplication.topViewController()!.openBottomSheetAnimate(vc: vc, sizes: [.intrinsic])//present(vc, animated: true, completion: nil)
            }
        }
    }
    
    func downloadFile(contentData: CategoryContentModel, completion: ((Bool) -> Void)?) {
//        let dispatchGroup = DispatchGroup()
//        dispatchGroup.enter()
        var downLoadFileType : DownloadFileType = .none
//        DownloadManager.shared.onProgress = { (progress) in
//            OperationQueue.main.addOperation {
//                debugPrint("Download Progress",progress)
////                if progress == 1.0 {
////                }
//            }
//        }
        
        
        let contentDataType = contentData.contentType
        if contentDataType == .video {
            guard let videoURL = URL(string: /*"https://player.vimeo.com/progressive_redirect/playback/398176901/rendition/240p/file.mp4?loc=external&oauth2_token_id=1712248720&signature=816e6e8dc6d6bcdbd3a7f7ef20471803dc72fcfc7ab5d57643d63072a3e0ba5b"*/contentData.videoUrl) else { return }
            downLoadFileType = .video
//            let asset = AVURLAsset(url: videoURL)
//            let d = AVAssetDownloadURLSession(configuration: URLSessionConfiguration.background(withIdentifier: "\(Bundle.main.bundleIdentifier!).background"), assetDownloadDelegate: self, delegateQueue: OperationQueue).
            let fileTask = DownloadManager.shared.activate().downloadTask(with: videoURL)
            DownloadManager.shared.arrTaskDataForDownload.append(DownloadTaskModel(downloadTask: fileTask, contentModel: contentData, fileType: contentData.contentType, downloadingFileType: downLoadFileType, status: .downloading, localFileLocation: ""))
            fileTask.resume()
            
            
        } else if contentDataType == .audio {
            guard let mediaURL = URL(string: contentData.file) else { return }
            downLoadFileType = .audio
            let fileTask = DownloadManager.shared.activate().downloadTask(with: mediaURL)
            DownloadManager.shared.arrTaskDataForDownload.append(DownloadTaskModel(downloadTask: fileTask, contentModel: contentData, fileType: contentData.contentType, downloadingFileType: downLoadFileType, status: .downloading, localFileLocation: ""))
            fileTask.resume()
            
        } else if contentDataType == .pdf {
            guard let mediaURL = URL(string: contentData.file) else { return }
            
            
            downLoadFileType = .pdf
            
            if let urlStrAttached = contentData.attachAudioFile.first?.file {
                if let attachedURL = URL(string: urlStrAttached) {
                    let attachedTask = DownloadManager.shared.activate().downloadTask(with: attachedURL)
                    DownloadManager.shared.arrTaskDataForDownload.append(DownloadTaskModel(downloadTask: attachedTask, contentModel: contentData, fileType: contentData.contentType, downloadingFileType: .attachedFile, status: .downloading, localFileLocation: ""))
                    attachedTask.resume()
                }
            }
            
            let fileTask = DownloadManager.shared.activate().downloadTask(with: mediaURL)
            DownloadManager.shared.arrTaskDataForDownload.append(DownloadTaskModel(downloadTask: fileTask, contentModel: contentData, fileType: contentData.contentType, downloadingFileType: downLoadFileType, status: .downloading, localFileLocation: ""))
            fileTask.resume()
            
        }
        
        if let imgURL = URL(string: contentData.image) {
            let thumbTask = DownloadManager.shared.activate().downloadTask(with: imgURL)
            DownloadManager.shared.arrTaskDataForDownload.append(DownloadTaskModel(downloadTask: thumbTask, contentModel: contentData, fileType: contentData.contentType, downloadingFileType: .thumbImage, status: .downloading, localFileLocation: ""))
            thumbTask.resume()
        }
//        dispatchGroup.leave()
//
//        dispatchGroup.notify(queue: .main) {
            
            completion?(true)
//        }
    }
}

class CurrencyFormatter {
    static var outputFormatter = CurrencyFormatter.create()
    class func create(locale: Locale = Locale.current, groupingSeparator: String? = nil, decimalSeparator: String? = nil, style: NumberFormatter.Style = NumberFormatter.Style.currency) -> NumberFormatter {
        let outputFormatter = NumberFormatter()
        outputFormatter.locale = locale
        outputFormatter.decimalSeparator = decimalSeparator ?? locale.decimalSeparator
        outputFormatter.groupingSeparator = groupingSeparator ?? locale.groupingSeparator
        outputFormatter.numberStyle = style
        return outputFormatter
    }
}

extension Numeric {
    func toCurrency(formatter: NumberFormatter = CurrencyFormatter.outputFormatter) -> String? {
        guard let num = self as? NSNumber else { return nil }
        var formatedSting = formatter.string(from: num)
        guard let locale = formatter.locale else { return formatedSting }
        if let separator = formatter.groupingSeparator, let localeValue = locale.groupingSeparator {
            formatedSting = formatedSting?.replacingOccurrences(of: localeValue, with: separator)
        }
        if let separator = formatter.decimalSeparator, let localeValue = locale.decimalSeparator {
            formatedSting = formatedSting?.replacingOccurrences(of: localeValue, with: separator)
        }
        return formatedSting
    }
}
