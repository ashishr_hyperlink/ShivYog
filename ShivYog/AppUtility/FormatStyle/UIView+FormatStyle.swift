//
//  UIView+FormatStyle.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 22/09/20.
//  Copyright © 2020 KISHAN_RAJA. All rights reserved.
//

import UIKit



extension ViewStyle where Self: UIView {
    
    @discardableResult func setRound() -> Self {
        self.layer.cornerRadius = self.frame.size.height / 2.0
        self.clipsToBounds = true
        return self
    }
    
    @discardableResult func cornerRadius(cornerRadius: CGFloat,clips: Bool = true) -> Self {
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = clips
        return self
    }
    
    
    @discardableResult func borderColor(color: UIColor, borderWidth: CGFloat = 1.0) -> Self {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = borderWidth
        return self
    }
    
    @discardableResult func shadow(color: UIColor = .black, shadowOffset : CGSize = CGSize(width: 1.0, height: 1.0) , shadowOpacity : Float = 0.7) -> Self {
        
        /*let shadowPath0 = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius)
        let layer0 = CALayer()
        layer0.shadowPath = shadowPath0.cgPath
        layer0.shadowColor = color.cgColor
        layer0.shadowOpacity = shadowOpacity
        layer0.shadowRadius = self.cornerRadius
        layer0.shadowOffset = shadowOffset
        layer0.bounds = self.bounds
        layer0.position = self.center
        self.layer.addSublayer(layer0)*/
        
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowOffset = shadowOffset
        self.layer.masksToBounds = false
        self.layer.shadowRadius = self.layer.cornerRadius
        let shadowPath0 = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius)
        self.layer.shadowPath = shadowPath0.cgPath
        
        
        return self
    }
    
    @discardableResult func backGroundColor(color: UIColor) -> Self {
        self.backgroundColor = color
        return self
    }
}
