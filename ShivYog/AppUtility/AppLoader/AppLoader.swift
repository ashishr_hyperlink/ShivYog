//
//  AppLoader.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 18/12/20.
//

import UIKit

import NVActivityIndicatorView

class AppLoader {
    
    //MARK: Shared Instance
    static let shared: AppLoader = AppLoader()
    
    //MARK: Class Variables
    private let viewBGLoder: UIView = UIView()
    private var loader : NVActivityIndicatorView!
    
    //MARK: Class Funcation
    
    /**
     Add app loader
     */
    func addLoader() {
        removeLoader()
        self.viewBGLoder.frame = UIScreen.main.bounds
        self.viewBGLoder.tag = 1307966
        self.viewBGLoder.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        let height : CGFloat = 70
        let frame : CGRect = CGRect(x: (ScreenSize.width - height)/2, y: (ScreenSize.height - height)/2, width: height, height: height)
        loader = NVActivityIndicatorView(frame: frame, type: NVActivityIndicatorType.ballRotateChase, color: UIColor.clear, padding: 10.0)
        loader?.layer.cornerRadius = 5.0
        loader.color = .app_990000
        loader?.startAnimating()
    
        self.viewBGLoder.addSubview(self.loader)
        UIApplication.shared.windows.first?.addSubview(self.viewBGLoder)
        UIApplication.shared.windows.first?.isUserInteractionEnabled = false
    }
    
    /**
     Remove app loader
     */
    func removeLoader() {
        UIApplication.shared.windows.first?.isUserInteractionEnabled = true
        self.loader?.stopAnimating()
        self.loader?.removeFromSuperview()
        self.viewBGLoder.removeFromSuperview()
        UIApplication.shared.windows.first?.viewWithTag(1307966)?.removeFromSuperview()
    }
    
    //Check loader is Animating
    func isLoaderAnimating() -> Bool {
        return self.loader.isAnimating
    }
}
