//
//  UITextView+Class.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 22/09/20.
//  Copyright © 2020 KISHAN_RAJA. All rights reserved.
//

import UIKit

class DataDetectionTextView: UITextView, UITextViewDelegate {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.isEditable = false
        self.isSelectable = true
        
        self.dataDetectorTypes = .link
        self.showsVerticalScrollIndicator = false
        self.showsHorizontalScrollIndicator = false
        self.isScrollEnabled = false
        self.delegate = self
        self.contentInsetAdjustmentBehavior = .never
        self.textContainerInset = UIEdgeInsets(top: 0.0, left: 14, bottom: 0.0, right: 14)
    }
    
    override func addGestureRecognizer(_ gestureRecognizer: UIGestureRecognizer) {
        if gestureRecognizer.isKind(of: UILongPressGestureRecognizer.self) {
            gestureRecognizer.isEnabled = false
        }
        super.addGestureRecognizer(gestureRecognizer)
    }
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        print(URL)
        return true
    }
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
    
    override func selectionRects(for range: UITextRange) -> [UITextSelectionRect] {
        return []
    }
    override func caretRect(for position: UITextPosition) -> CGRect {
        return .zero
    }
}
//fixme

class AppTextView : MDFloatingTextView{
    //    @IBInspectable dynamic open var isShowTitle: Bool = false
    
    var padding: UIEdgeInsets = UIEdgeInsets()
    
    override func awakeFromNib() {
        DispatchQueue.main.async {
            
            let height : CGFloat = 91 * ScreenSize.fontAspectRatio
            let heightConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: height)
            self.addConstraint(heightConstraint)
            super.awakeFromNib()
        }
        //        padding = UIEdgeInsets(top: 0, left: -50, bottom: 0, right: -10)
        self.backgroundColor = .clear
        self.tintColor = .app_990000
        self.textColor = .app_222222
        //        self.backgroundColor = .appFfffff.withAlphaComponent(0.5)
        
        self.font = UIFont.cFont(ofType: FontFamily.SFProDisplay.bold, withSize: 16)
        self.placeholderFont = UIFont.cFont(ofType: FontFamily.SFProDisplay.regular, withSize: 16)
        self.titleFont = UIFont.cFont(ofType: FontFamily.SFProDisplay.regular, withSize: 12)
        if self.titleText != nil {
            //            self.titleText = self.isShowTitle ? self.titleText!.localized : ""
            self.titleText = ""
        }
        else{
            //            self.titleText = self.isShowTitle ? self.placeholderText == nil ? "" : self.placeholderText.localized : ""
            self.titleText = ""
        }
        
        if self.placeholderText != nil {
            self.placeholderText = self.placeholderText.localized
        }
        self.placeholderColor = .app_22222207
        self.titleColor = .app_22222207
        self.cornerRadius(cornerRadius: 30)
        self.borderColor(color: .app_990000, borderWidth: 1)
        self.titleBgColor  = .clear
        //        self.titleInsideEdgeInsets = padding
        //        self.flotingType = .onBorder
        //        self.titleLeadingSpace = 13
        DispatchQueue.main.async {
            //            self.setGradient()
            var gradient :CAGradientLayer = CAGradientLayer()
            let startColor: UIColor = .appFfaa9205
            let endColor : UIColor = .appFc0B9205
            
            gradient.colors = [startColor.cgColor, endColor.cgColor]
            gradient.locations = [0.0 , 1.0]
            gradient.cornerRadius =  30.0
            
            gradient.frame = self.bounds
            gradient.masksToBounds = false
            self.layer.addSublayer(gradient)
            
        }
    }
}
