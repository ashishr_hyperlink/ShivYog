//
//  UIBarButtonItem+Class.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 22/09/20.
//  Copyright © 2020 KISHAN_RAJA. All rights reserved.
//

import UIKit

class BarButtonItemFilp: UIBarButtonItem {
    open override func awakeFromNib() {
        super.awakeFromNib()
        if let textValue = self.title {
            self.title = textValue.localized
        }
        if Bundle.main.isArabicLanguage {
            self.image = self.image?.imageFlippedForRightToLeftLayoutDirection()
        }
    }
}

class BarButtonTheme : UIBarButtonItem{
    open override func awakeFromNib() {
        super.awakeFromNib()
        self.imageInsets = UIEdgeInsets(top: 0, left: -15, bottom: 0, right: 0)
    }
}

class RightBarButtonTheme : UIBarButtonItem{
    open override func awakeFromNib() {
        super.awakeFromNib()
        self.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -15)
    }
}
