//
//  UITextField+Class.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 22/09/20.
//  Copyright © 2020 KISHAN_RAJA. All rights reserved.
//

import UIKit


//dont select action in textfield
class RestrictionTextField: UITextField {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.autocorrectionType = .no
    }
    override func selectionRects(for range: UITextRange) -> [UITextSelectionRect] {
        return []
    }
    //    override func caretRect(for position: UITextPosition) -> CGRect {
    //        return CGRect.zero
    //    }
    
    override func closestPosition(to point: CGPoint) -> UITextPosition? {
        let beginning = self.beginningOfDocument
        let end = self.position(from: beginning, offset: self.text?.count ?? 0)
        return end
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.copy(_:)) || action == #selector(UIResponderStandardEditActions.select(_:)) || action == #selector(UIResponderStandardEditActions.cut(_:)) || action == #selector(UIResponderStandardEditActions.paste(_:)) || action == #selector(UIResponderStandardEditActions.selectAll(_:)){
            return false
        }
        else {
            return super.canPerformAction(action, withSender: sender)
        }
    }
}

class OTPTextField: GradientTextField {
    //    let padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//        self.borderColor(color: #colorLiteral(red: 0.8823529412, green: 0.8823529412, blue: 0.8823529412, alpha: 1), borderWidth: 1.4)
        self.borderStyle = .none
//        self.layer.cornerRadius = 8
        self.keyboardType = .asciiCapableNumberPad
        self.isSecureTextEntry = false
//        self.textColor = #colorLiteral(red: 0.1490196078, green: 0.2078431373, blue: 0.2509803922, alpha: 1)
//        self.font =  UIFont.customFont(ofType: .medium, withSize: 16)
        self.textAlignment = .center
        self.placeholder = "0"//"\u{2022}"
        self.placeholderColor = .app_990000.withAlphaComponent(0.50)
        self.delegate = self
        self.addTarget(self, action: #selector(textfieldIsEditing(_:)), for: .editingChanged)
        self.addTarget(self, action: #selector(textfieldIsEditing(_:)), for: .editingChanged)
    }
    
        override open func textRect(forBounds bounds: CGRect) -> CGRect {
            return bounds.inset(by: padding)
        }
    
        override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
            return bounds.inset(by: padding)
        }
    
        override open func editingRect(forBounds bounds: CGRect) -> CGRect {
            return bounds.inset(by: padding)
        }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("--------------------------------")
        if string.isEmpty {
            return true
        }
        if textField.text?.count == 1{
            textField.text = ""
            return true && string.isValid(.number)
        }
        print("--------------------------------")
        return true && string.isValid(.number)
    }
    
    @objc func textfieldIsEditing(_ textField:UITextField){
        if !textField.text!.isEmpty{
            if IQKeyboardManager.shared.canGoNext{
                IQKeyboardManager.shared.goNext()
            } else {
                self.resignFirstResponder()
            }
        }
    }
    override func deleteBackward() {
        super.deleteBackward()
        if IQKeyboardManager.shared.canGoPrevious{
            IQKeyboardManager.shared.goPrevious()
        }
    }
}

//fixme

class AppTextField: SkyFloatingLabelTextField  {

    /// A UIColor value that determines the color of the bottom line when in the normal state
//    @IBInspectable dynamic open var isShowTitle: Bool = false

    var padding: UIEdgeInsets = UIEdgeInsets()

    override func awakeFromNib() {
        super.awakeFromNib()
        let heightConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 50 * ScreenSize.fontAspectRatio)
        self.addConstraint(heightConstraint)

//        padding = UIEdgeInsets(top: 14, left: isShowTitle ? 13 : 20, bottom: 14, right: isShowTitle ? 8 : 20)
        padding = UIEdgeInsets(top: 14, left: 20, bottom: 14, right: 20)

        self.tintColor = .app_990000
        self.textColor = .appFfffff
        self.backgroundColor = .clear//.appFfffff.withAlphaComponent(0.5)
//
        self.font = UIFont.cFont(ofType: FontFamily.SFProDisplay.bold, withSize: 16)
        self.placeholderFont = UIFont.cFont(ofType: FontFamily.SFProDisplay.regular, withSize: 16)
        self.titleFont = UIFont.cFont(ofType: FontFamily.SFProDisplay.regular, withSize: 12)
        if self.title != nil {
//            self.title = self.isShowTitle ? "  " + self.title!.localized + "  " : ""
            self.title = ""
        }
        else{
//            self.title = self.isShowTitle ? self.placeholder == nil ? "" : "  " + self.placeholder!.localized + "  " : ""
            self.title = ""
        }
        if self.selectedTitle != nil {
//            self.selectedTitle = self.isShowTitle ? "  " + self.selectedTitle!.localized + "  " : ""
            self.selectedTitle = ""
        }
        else{
//            self.selectedTitle = self.isShowTitle ? self.placeholder == nil ? "" : "  " + self.placeholder!.localized + "  " : ""
            self.selectedTitle = ""
        }
        if self.placeholder != nil {
            self.placeholder = self.placeholder!.localized
        }
        self.placeholderColor = .appFfffff.withAlphaComponent(0.7)
        self.titleColor = .appFfffff
        self.selectedTitleColor = .appFfffff

        self.lineHeight = 1
        self.lineColor = .app_990000//.appFfffff
        self.selectedLineHeight = 1
        self.selectedLineColor = .app_990000//.appFfffff
        
//        self.cornerRadius(cornerRadius: 4)
        DispatchQueue.main.async {
//            self.setRound()
            self.borderCorner = self.frame.height / 2
        }
        self.clipsToBounds = true
        
        self.titleLabel.layer.backgroundColor = UIColor.clear.cgColor
        self.delegate = self
    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        self.setRound()
//        self.borderCorner = self.frame.height / 2
//    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.width - 30, y: 0, width: 20 , height: bounds.height)
    }

    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 10, y: 0, width: 20 , height: bounds.height)
    }

    override func titleLabelRectForBounds(_ bounds: CGRect, editing: Bool) -> CGRect {
        if editing {

            return CGRect(x: padding.left - 5, y: -titleHeight() / 2 , width: titleWidth(), height: titleHeight())
        }
        return CGRect(x: padding.left, y: titleHeight(), width: titleWidth(), height: titleHeight())
    }

}

extension AppTextField {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        /*if IQKeyboardManager.shared.canGoNext{
            textField.returnKeyType = .done
        }
        else{
            textField.returnKeyType = .next
        }*/
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if IQKeyboardManager.shared.canGoNext{
            textField.returnKeyType = .done
        }
        else{
            textField.returnKeyType = .next
        }
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if IQKeyboardManager.shared.canGoNext{
            IQKeyboardManager.shared.goNext()
        }
        else{
            textField.resignFirstResponder()
        }
        return true
    }
}


class GradientTextField: SkyFloatingLabelTextField  {

    /// A UIColor value that determines the color of the bottom line when in the normal state
    @IBInspectable dynamic open var isBorder: Bool = false
    var completion: (() -> Void)?

    var padding: UIEdgeInsets = UIEdgeInsets()

    override func awakeFromNib() {
        super.awakeFromNib()
        let heightConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 60 * ScreenSize.fontAspectRatio)
        self.addConstraint(heightConstraint)

//        padding = UIEdgeInsets(top: 14, left: isShowTitle ? 13 : 20, bottom: 14, right: isShowTitle ? 8 : 20)
        padding = UIEdgeInsets(top: 14, left: 20, bottom: 14, right: 20)

        if self.isBorder {
            self.font = UIFont.cFont(ofType: FontFamily.SFProDisplay.bold, withSize: 16.0)
            self.placeholderFont = UIFont.cFont(ofType: FontFamily.SFProDisplay.regular, withSize: 16.0)
            self.textColor = .app_222222
            self.tintColor = .app_990000
            DispatchQueue.main.async {
                self.borderCorner = self.frame.height / 2
            }
            self.placeholderColor = .app_22222207
            self.lineHeight = 1
            self.lineColor = .app_990000//.appFfffff
            self.selectedLineHeight = 1
            self.selectedLineColor = .app_990000//.appFfffff
        } else {
            self.font = UIFont.cFont(ofType: FontFamily.PTSans.bold, withSize: 26)
            self.placeholderFont = UIFont.cFont(ofType: FontFamily.PTSans.bold, withSize: 26)
            self.textColor = .app_990000
            self.tintColor = .app_990000
            self.borderCorner = 12.0
//            self.borderWidth = 0.0
            self.lineHeight = 0.0
            self.selectedLineHeight = 0.0
            self.placeholderColor = .app_990000
//            self.line
            
        }
        
//        self.tintColor = .app_990000
//        self.textColor = .appFfffff
//        self.backgroundColor = .clear//.appFfffff.withAlphaComponent(0.5)
//
//        self.font = UIFont.cFont(ofType: FontFamily.SFProDisplay.bold, withSize: 16)
//        self.placeholderFont = UIFont.cFont(ofType: FontFamily.SFProDisplay.regular, withSize: 16)
//        self.titleFont = UIFont.cFont(ofType: FontFamily.SFProDisplay.regular, withSize: 12)
        if self.title != nil {
            self.title = ""
        }
        else{
            self.title = ""
        }
        if self.selectedTitle != nil {
            self.selectedTitle = ""
        }
        else{
            self.selectedTitle = ""
        }
        if self.placeholder != nil {
            self.placeholder = self.placeholder!.localized
        }
        
        self.titleColor = .app_22222207
        self.selectedTitleColor = .app_990000

//        self.lineHeight = 1
//        self.lineColor = .app_990000//.appFfffff
//        self.selectedLineHeight = 1
//        self.selectedLineColor = .app_990000//.appFfffff
        
//        self.cornerRadius(cornerRadius: 4)
        DispatchQueue.main.async {
//            self.setRound()
            self.setGradient(isBorder : self.isBorder)
            
        }
        self.clipsToBounds = true
        
        self.titleLabel.layer.backgroundColor = UIColor.clear.cgColor
        self.delegate = self
        self.completion = {
            if let sublayers = self.layer.sublayers {
                for sublayer in sublayers where sublayer.isKind(of: CAGradientLayer.self) {
                    sublayer.removeFromSuperlayer()
                }
            }
            self.setGradient(isBorder: self.isBorder)
        }
        
    }
    
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        if let sublayers = self.layer.sublayers {
//            for sublayer in sublayers where sublayer.isKind(of: CAGradientLayer.self) {
//                    sublayer.removeFromSuperlayer()
//            }
//        }
//        self.setGradient(isBorder: self.isBorder)
////        self.setRound()
////        self.borderCorner = self.frame.height / 2
////        self.removeGradient()
////        self.setGradient(isBorder: isBorder)
//    }
    
    func setGradient(startColor: UIColor = .appFfaa9205, endColor: UIColor = .appFc0B9205, isBorder: Bool = false) {
        let gradient:CAGradientLayer = CAGradientLayer()
        gradient.colors = [startColor.cgColor, endColor.cgColor]
        gradient.locations = [0.0 , 1.0]
//        gradient.startPoint = CGPoint(x: 1.0 , y: 0.0)
//        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
//        gradient.borderWidth = 1.0
        DispatchQueue.main.async {
            gradient.cornerRadius = isBorder ? gradient.bounds.height / 2 : 12.0
            
        }
        
        gradient.frame = self.bounds
        gradient.shouldRasterize = true
        gradient.masksToBounds = false
        self.layer.addSublayer(gradient)
      }
    
    func removeGradient(startColor: UIColor = .clear, endColor: UIColor = .clear, isBorder: Bool = false) {
        let gradient:CAGradientLayer = CAGradientLayer()
        gradient.colors = [startColor.cgColor, endColor.cgColor]
        gradient.locations = [0.0 , 1.0]
//        gradient.startPoint = CGPoint(x: 1.0 , y: 0.0)
//        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
//        gradient.borderWidth = 1.0
//        let heightConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 60 * ScreenSize.fontAspectRatio)
//        self.addConstraint(heightConstraint)
        DispatchQueue.main.async {
            gradient.cornerRadius = isBorder ? gradient.bounds.height / 2 : 12.0
            
        }

        gradient.frame = self.bounds
        gradient.masksToBounds = false
        self.layer.addSublayer(gradient)
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.width - 40, y: 0, width: 20 , height: bounds.height)
    }

    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 10, y: 0, width: 20 , height: bounds.height)
    }

    override func titleLabelRectForBounds(_ bounds: CGRect, editing: Bool) -> CGRect {
        if editing {

            return CGRect(x: padding.left - 5, y: -titleHeight() / 2 , width: titleWidth(), height: titleHeight())
        }
        return CGRect(x: padding.left, y: titleHeight(), width: titleWidth(), height: titleHeight())
    }

}

extension GradientTextField {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        /*if IQKeyboardManager.shared.canGoNext{
            textField.returnKeyType = .done
        }
        else{
            textField.returnKeyType = .next
        }*/
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if IQKeyboardManager.shared.canGoNext{
            textField.returnKeyType = .done
        }
        else{
            textField.returnKeyType = .next
        }
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if IQKeyboardManager.shared.canGoNext{
            IQKeyboardManager.shared.goNext()
        }
        else{
            textField.resignFirstResponder()
        }
        return true
    }
}
