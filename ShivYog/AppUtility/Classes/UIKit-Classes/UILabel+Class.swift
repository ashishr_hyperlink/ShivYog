//
//  UILabel+Class.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 22/09/20.
//  Copyright © 2020 KISHAN_RAJA. All rights reserved.
//

import UIKit

//Bold Title Lable
class BoldBlackTitle: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.applyTheme(themeStyle: .boldBlackTitle)
    }
}

//Regular Sub Title Lable
class RegularGreySubTitle: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.applyTheme(themeStyle: .regularGreySubTitle)
    }
}

//Flip lable
class FlipLable: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        if Bundle.main.isArabicLanguage {
            self.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
}

class ChangeContentMode: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        if Bundle.main.isArabicLanguage {
            if self.textAlignment == .left {
                self.textAlignment = .right
            } else {
                self.textAlignment = .left
            }
        }
    }
}
