//
//  UIButton+Class.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 22/09/20.
//  Copyright © 2020 KISHAN_RAJA. All rights reserved.
//

import UIKit
import Lottie

var buttonHeight : CGFloat = 50 * ScreenSize.fontAspectRatio
var leftImageWidth : CGFloat = 50
let vwContainerCorner : CGFloat = 30

//Flip image button
class FlipImageButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        if Bundle.main.isArabicLanguage {
            self.setImage(self.currentImage?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        }
    }
}

//Right side icon button
class ButtonIconRight: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.semanticContentAttribute = .forceLeftToRight
    }
    override func imageRect(forContentRect contentRect:CGRect) -> CGRect {
        var imageFrame = super.imageRect(forContentRect: contentRect)
        imageFrame.origin.x = super.titleRect(forContentRect: contentRect).maxX - imageFrame.width
        return imageFrame
    }
    
    override func titleRect(forContentRect contentRect:CGRect) -> CGRect {
        var titleFrame = super.titleRect(forContentRect: contentRect)
        if (self.currentImage != nil) {
            titleFrame.origin.x = super.imageRect(forContentRect: contentRect).minX
        }
        return titleFrame
    }
}

class ThemeButton: UIButton {
    
    @IBInspectable var heightOfButton: CGFloat = buttonHeight {
        didSet{
            self.setHeight()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setHeight()
        self.cornerRadius(cornerRadius: 30)
        self.clipsToBounds = false
    }
    
    func setHeight(){
        let heightConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: heightOfButton)
        self.addConstraint(heightConstraint)
        DispatchQueue.main.async {
            self.setRound()
            self.font(name: FontFamily.SFProDisplay.bold, size: 16).textColor(color: .appFfffff).backGroundColor(color: .app_990000)
        }
        
    }
}

class ThemeButtonWhiteBG: ThemeButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyStyle()
    }
    
    func applyStyle(){
        DispatchQueue.main.async {
            self.shadow(color: .clear, shadowOffset: .zero, shadowOpacity: 0)
        }
        
    }
}

class ThemeButtonClearWhiteBG: ThemeButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyStyle()
    }
    
    func applyStyle(){

        DispatchQueue.main.async {
            self.shadow(color: .clear, shadowOffset: .zero, shadowOpacity: 0)
        }
    }
}


