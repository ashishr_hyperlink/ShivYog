//
//  DImagePicker.swift
//  ImagePickerDemo
//
//  Created by  on 8/10/17.
//  Copyright © 2017    . All rights reserved.
//

import UIKit
@_exported import CropViewController
import Photos
import AVFoundation

// Return Image Protocol
@objc protocol DImageViewReturnDelegate {
    func imagePickUpFinish(image: UIImage,imageView:DSquareImageView)
}

class DSquareImageView: UIImageView , UIImagePickerControllerDelegate , UINavigationControllerDelegate , CropViewControllerDelegate {
    
    static let shared : DSquareImageView = DSquareImageView()
    
    var imagePicker = UIImagePickerController()
    
    var delegate : DImageViewReturnDelegate?
    
    var getImage : ((UIImage) -> Void)?
    
    open var isAllowExternalEditing : Bool = true
    
    private var cropViewController = CropViewController(image: UIImage())
    
    /**
     ## Important Notes ##
     1. aspectRatioPreset will be ignored if isRoundCrop is set to true
     2. Please set mode to crop before using this
     */
    
    var aspectRatioPreset: CropViewControllerAspectRatioPreset?
    
    var isRoundCrop = false {
        didSet {
            if isRoundCrop {
                self.viewMode = .crop
            }
        }
    }
    
    // MARK: - Class Enums
    
    enum ViewMode {
        case crop, preview, none
    }
    
    enum SourceType {
        case camera, photoLibrary, both
    }
    
    enum CameraConstants: String {
        case selImage = "Select an Image"
        case camera = "Camera"
        case photoLibrary = "Gallery"
        case cancel = "Cancel"
        case okay = "OK"
        case noAccessCam = "The Sanzu does not have access to your camera. To enable access, tap settings and turn on Camera."
        case noAccessLib = "The Sanzu does not have access to your photo library. To enable access, tap settings and turn on Photo Library."
        case noCamera = "Sorry, this device has no camera."
        case settings = "Settings"
        case done = "Done"
        
        func localizedString() -> String {
            return NSLocalizedString(self.rawValue, comment: "")
        }
        
        static func getLocalized(title:CameraConstants) -> String {
            return title.localizedString()
        }
    }
    
    var viewMode : ViewMode = .none {
        didSet{
            self.addTapGestureRecognizer {
                
                switch self.viewMode {
                    
                case .crop:
                    self.isUserInteractionEnabled = true
                    self.imgTapHandle()
                    break
                    
                case .preview:
                    self.isUserInteractionEnabled = true
                    self.zoomImage(with: self.image)
                    break
                    
                default:
                    break
                    
                }
            }
        }
    }
    
    var sourceType : SourceType = .both
    //MARK:- ImageView Configure Method
    
    override func awakeFromNib() {
        
        self.contentMode = .scaleAspectFill
        self.clipsToBounds = true
        //        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.imgTapHandle(_:)))
        //        self.addGestureRecognizer(gesture)
        imagePicker.delegate = self
    }
    
    //----------------------------------------------------------------------------------------------------------------
    
    //MARK:- ImagePickerController Delegate Method
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: false) {
            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                if self.isAllowExternalEditing {
                    self.cropViewController = CropViewController(croppingStyle: self.isRoundCrop ? .circular : .default, image: image)
                    
                    if let preset = self.aspectRatioPreset , !self.isRoundCrop {
                        self.cropViewController.aspectRatioPreset = preset
                    }
                    
                    if self.isRoundCrop || self.aspectRatioPreset != nil {
                        self.cropViewController.aspectRatioLockEnabled = true
                        self.cropViewController.aspectRatioPickerButtonHidden = true
                    }
                    
                    self.cropViewController.delegate = self
                    self.cropViewController.cancelButtonTitle = CameraConstants.getLocalized(title: .cancel)
                    self.cropViewController.doneButtonTitle = CameraConstants.getLocalized(title: .done)
                    UIApplication.topViewController()?.present(self.cropViewController, animated: false, completion: nil)
                }
                else {
                    self.image = image
                    self.getImage?(image)
                    self.delegate?.imagePickUpFinish(image: image, imageView: self)
                }
                
            } else{
                print("Error")
            }
        }
    }
    
    
    
    //----------------------------------------------------------------------------------------------------------------
    
    //MARK:- CropViewControllerDelegate
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        cropViewController.dismiss(animated: true, completion: nil)
        self.image = image
        // if need a return image delegate call
        
        delegate?.imagePickUpFinish(image: image, imageView: self)
        self.getImage?(image)
        
    }
    
    
    //------------------------------------------------------
    
    //MARK:- ImageView TapGesture Handle Method
    
    @objc func imgTapHandle() {
        if let vc = UIApplication.shared.keyWindow?.rootViewController {
            imgPickerOpen(this: vc, imagePicker: imagePicker, sourceControl: self)
        }
    }
    
    @IBAction func imagePick(_ sender: Any) {
        imgTapHandle()
    }
    
    //----------------------------------------------------------------------------------------------------------------
    
    
    //MARK:- ActionSheet Method
    
    func imgPickerOpen(this: UIViewController,imagePicker: UIImagePickerController, sourceControl:
        UIView) {
        
        this.view.endEditing(true)
        
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        
        let actionSheet = UIAlertController(title: nil , message: nil, preferredStyle: .actionSheet)
        
        if sourceType == .both || sourceType == .camera {
            actionSheet.addAction(UIAlertAction(title: CameraConstants.getLocalized(title: .camera), style: .default, handler: { (_) -> Void in
                self.checkAuthorisationStatusFor(.camera)
            }))
        }
        
        if sourceType == .both || sourceType == .photoLibrary {
            actionSheet.addAction(UIAlertAction(title: CameraConstants.getLocalized(title: .photoLibrary), style: .default, handler: { (_) -> Void in
                self.checkAuthorisationStatusFor(.photoLibrary)
            }))
        }
        
        actionSheet.addAction(UIAlertAction(title: CameraConstants.getLocalized(title: .cancel), style: .cancel, handler: nil))
        
        
        if ( UIDevice.current.userInterfaceIdiom == .pad ) {
            actionSheet.popoverPresentationController?.sourceView = sourceControl
            actionSheet.popoverPresentationController?.sourceRect = sourceControl.bounds
        }
        
        this.present(actionSheet, animated: true, completion: nil)
        
    }
    //----------------------------------------------------------------------------------------------------------------
    
    //Check permission Status
    
    private func checkAuthorisationStatusFor(_ type: SourceType) {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            if type == .camera {
                openCamera()
            } else if type == .photoLibrary {
                openPhotoLibrary()
            }
        case .denied:
            addAlertForSettings(type)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ status in
                if status == PHAuthorizationStatus.authorized {
                    if type == .camera {
                        self.openCamera()
                    } else if type == .photoLibrary {
                        self.openPhotoLibrary()
                    }
                } else {
                    self.addAlertForSettings(type)
                }
            })
        case .restricted:
            addAlertForSettings(type)
        default:
            break
        }
    }
    
    private func addAlertForSettings(_ type: SourceType) {
        var alertTitle: String = ""
        
        if type == .camera {
            alertTitle = CameraConstants.getLocalized(title: .noAccessCam)
        } else if type == .photoLibrary {
            alertTitle = CameraConstants.getLocalized(title: .noAccessLib)
        }
        
        let alertController = UIAlertController(title: alertTitle, message: nil, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: CameraConstants.getLocalized(title: .cancel), style: .default, handler: nil))
        
        let settingsAction = UIAlertAction(title: CameraConstants.getLocalized(title: .settings), style: .destructive) { (_) -> Void in
            let settingsUrl = NSURL(string: UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                }
            }
        }
        
        alertController.addAction(settingsAction)
        DispatchQueue.main.async {
            UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    private func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
                // Already Authorized
                
                DispatchQueue.main.async {
                    self.imagePicker.sourceType = .camera
                    self.imagePicker.cameraDevice = .front
                    UIApplication.topViewController()?.present(self.imagePicker, animated: true, completion: nil)
                }
            } else {
                
                AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
                    if granted == true {
                        // User granted
                        
                        DispatchQueue.main.async {
                            self.imagePicker.sourceType = .camera
                            UIApplication.topViewController()?.present(self.imagePicker, animated: true, completion: nil)
                        }
                    } else {
                        DispatchQueue.main.async {
                            let alertController = UIAlertController(title: CameraConstants.getLocalized(title: .noAccessCam), message: nil, preferredStyle: .alert)
                            
                            alertController.addAction(UIAlertAction(title: CameraConstants.getLocalized(title: .okay), style: .default, handler: nil))
                            
                            UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
                        }
                    }
                })
            }
            
            
            
        } else {
            DispatchQueue.main.async {
                let alertController = UIAlertController(title: CameraConstants.getLocalized(title: .noCamera), message: nil, preferredStyle: .alert)
                
                alertController.addAction(UIAlertAction(title: CameraConstants.getLocalized(title: .okay), style: .default, handler: nil))
                
                UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    //Photo Library
    
    private func openPhotoLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            
            DispatchQueue.main.async {
                self.imagePicker.sourceType = .photoLibrary
                UIApplication.topViewController()?.present(self.imagePicker, animated: true, completion: nil)
            }
        }
    }
}
