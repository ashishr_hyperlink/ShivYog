//
//  ReachabilityManager.swift
//
//
//  Created by on 12/10/18.
//  Copyright © 2018 Hyperlink. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

// MARK: - Reachability Manager -

class ReachabilityManager: NSObject {
    
    // MARK: - Shared
    
    static let shared: ReachabilityManager = ReachabilityManager()
    let noNetworkVC = StoryboardScene.Authentication.noNetworkVC.instantiate()
    
    // -----------------------------------------------------------------------------------------------------------------------------
    
    // MARK: - Class Variables
    
    private var reachability: Reachability!
    private let snackBarNetworkReachability: TTGSnackbar = TTGSnackbar()
    
    let reachabilityChangedNotification = "ReachabilityChangedNotification"
    
    static var isReachable: Bool {
        return ReachabilityManager.shared.reachability.connection != .none
    }
    
    static var isReachableViaWWAN: Bool {
        return ReachabilityManager.shared.reachability.connection == .cellular
    }
    
    static var isReachableViaWiFi: Bool {
        return ReachabilityManager.shared.reachability.connection == .wifi
    }
    
    // -----------------------------------------------------------------------------------------------------------------------------
    
    // MARK: - Custom Methods
    
    func startObserving() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged), name: NSNotification.Name.reachabilityChanged, object: nil)
        do {
            try self.reachability.startNotifier()
        }
        catch(let error) {
            print("Error occured while starting reachability notifications : \(error.localizedDescription)")
        }
    }
    
    func stopObserving() {
        reachability.stopNotifier()
    }
    
    @objc func reachabilityChanged(notification: NSNotification) {
        let reachability = notification.object as! Reachability
        
        switch reachability.connection {
        case .cellular:
            print("Network available via Cellular Data.")
            removeNetworkSnackBar()
            break
        case .wifi:
            print("Network available via WiFi.")
            removeNetworkSnackBar()
            break
        case .none:
            print("Network is not available.")
            showNetworkSnackBar()
            break
        }
    }
    
    //Snackbar Methods
    
    private func showNetworkSnackBar(withMessage message: String = AppMessages.internetConnectionMsg) {
        //extra decoration
        // Change message text font and color
//        snackBarNetworkReachability.dismiss()
//        snackBarNetworkReachability.messageTextColor = UIColor.white
//        snackBarNetworkReachability.messageTextFont = .customFont(ofType: .bold, withSize: 15)
//        
//        snackBarNetworkReachability.messageTextColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        snackBarNetworkReachability.backgroundColor = .black
//        
//        snackBarNetworkReachability.animationType = .slideFromBottomToTop
//        snackBarNetworkReachability.message = message.localized
//        snackBarNetworkReachability.duration = .long
//        snackBarNetworkReachability.messageTextAlign = .center
//        snackBarNetworkReachability.shouldDismissOnSwipe = true
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
//            self?.snackBarNetworkReachability.show()
//        }
        if UserDefaultsConfig.isAuthorization{
            noNetworkVC.modalPresentationStyle = .overFullScreen
            if let tabBar = UIApplication.topViewController()?.tabBarController{
                tabBar.openBottomSheetAnimate(vc: noNetworkVC, sizes: [.intrinsic])
            }
            else{
                UIApplication.topViewController()!.openBottomSheetAnimate(vc: noNetworkVC, sizes: [.intrinsic])
            }
        }
    }
    
    func removeNetworkSnackBar() {
        noNetworkVC.sheetViewController?.animateOut()
    }
    
    func showNoNetworkMessage() {
        if ReachabilityManager.isReachable {
            self.showNetworkSnackBar()
        }
    }
    
    // MARK: - Life Cycle Methods
    
    override init() {
        super.init()
        self.reachability = Reachability()
    }
    
}
