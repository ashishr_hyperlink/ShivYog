import UIKit
import AudioToolbox

// MARK: - Initializers
extension UIAlertController {
	
    /// Create new alert view controller.
    ///
    /// - Parameters:
    ///   - style: alert controller's style.
    ///   - title: alert controller's title.
    ///   - message: alert controller's message (default is nil).
    ///   - defaultActionButtonTitle: default action button title (default is "OK")
    ///   - tintColor: alert controller's tint color (default is nil)
    convenience init(style: UIAlertController.Style, source: UIView? = nil, title: String? = nil, message: String? = nil, tintColor: UIColor? = nil) {
        self.init(title: title, message: message, preferredStyle: style)
        
        // TODO: for iPad or other views
        let isPad: Bool = UIDevice.current.userInterfaceIdiom == .pad
        let root = UIApplication.shared.keyWindow?.rootViewController?.view
        
        //self.responds(to: #selector(getter: popoverPresentationController))
        if let source = source {
            Log("----- source")
            popoverPresentationController?.sourceView = source
            popoverPresentationController?.sourceRect = source.bounds
        } else if isPad, let source = root, style == .actionSheet {
            Log("----- is pad")
            popoverPresentationController?.sourceView = source
            popoverPresentationController?.sourceRect = CGRect(x: source.bounds.midX, y: source.bounds.midY, width: 0, height: 0)
            //popoverPresentationController?.permittedArrowDirections = .down
            popoverPresentationController?.permittedArrowDirections = .init(rawValue: 0)
        }
        
        if let color = tintColor {
            self.view.tintColor = color
        }
    }
}


// MARK: - Methods
extension UIAlertController {
    
    /// Present alert view controller in the current view controller.
    ///
    /// - Parameters:
    ///   - animated: set true to animate presentation of alert controller (default is true).
    ///   - vibrate: set true to vibrate the device while presenting the alert (default is false).
    ///   - completion: an optional completion handler to be called after presenting alert controller (default is nil).
    public func show(animated: Bool = true, vibrate: Bool = false, style: UIBlurEffect.Style? = nil, completion: (() -> Void)? = nil) {
        
        /// TODO: change UIBlurEffectStyle
        if let style = style {
            for subview in view.allSubViewsOf(type: UIVisualEffectView.self) {
                subview.effect = UIBlurEffect(style: style)
            }
        }
        
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow?.rootViewController?.present(self, animated: animated, completion: completion)
            if vibrate {
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
            }
        }
    }
    
    /// Add an action to Alert
    ///
    /// - Parameters:
    ///   - title: action title
    ///   - style: action style (default is UIAlertActionStyle.default)
    ///   - isEnabled: isEnabled status for action (default is true)
    ///   - handler: optional action handler to be called when button is tapped (default is nil)
    func addAction(image: UIImage? = nil, title: String, color: UIColor? = nil, style: UIAlertAction.Style = .default, isEnabled: Bool = true, handler: ((UIAlertAction) -> Void)? = nil) {
        //let isPad: Bool = UIDevice.current.userInterfaceIdiom == .pad
        //let action = UIAlertAction(title: title, style: isPad && style == .cancel ? .default : style, handler: handler)
        let action = UIAlertAction(title: title, style: style, handler: handler)
        action.isEnabled = isEnabled
        
        // button image
        if let image = image {
            action.setValue(image, forKey: "image")
        }
        
        // button title color
        if let color = color {
            action.setValue(color, forKey: "titleTextColor")
        }
        
        addAction(action)
    }
    
    /// Set alert's title, font and color
    ///
    /// - Parameters:
    ///   - title: alert title
    ///   - font: alert title font
    ///   - color: alert title color
    func set(title: String?, font: UIFont, color: UIColor) {
        if title != nil {
            self.title = title
        }
        setTitle(font: font, color: color)
    }
    
    func setTitle(font: UIFont, color: UIColor) {
        guard let title = self.title else { return }
        let attributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: color]
        let attributedTitle = NSMutableAttributedString(string: title, attributes: attributes)
        setValue(attributedTitle, forKey: "attributedTitle")
    }
    
    /// Set alert's message, font and color
    ///
    /// - Parameters:
    ///   - message: alert message
    ///   - font: alert message font
    ///   - color: alert message color
    func set(message: String?, font: UIFont, color: UIColor) {
        if message != nil {
            self.message = message
        }
        setMessage(font: font, color: color)
    }
    
    func setMessage(font: UIFont, color: UIColor) {
        guard let message = self.message else { return }
        let attributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: color]
        let attributedMessage = NSMutableAttributedString(string: message, attributes: attributes)
        setValue(attributedMessage, forKey: "attributedMessage")
    }
    
    /// Set alert's content viewController
    ///
    /// - Parameters:
    ///   - vc: ViewController
    ///   - height: height of content viewController
    func set(vc: UIViewController?, width: CGFloat? = nil, height: CGFloat? = nil) {
        guard let vc = vc else { return }
        setValue(vc, forKey: "contentViewController")
        if let height = height {
            vc.preferredContentSize.height = height
            preferredContentSize.height = height
        }
    }
}


extension UIAlertController {
     
    public func present(animated: Bool = true,
                        vibrate: Bool = false,
                        backgroundColor: UIColor? = nil,
                        titleColor: UIColor? = nil,
                        textColor: UIColor? = nil,
                        titleImage: UIImage? = nil,
                        vc : UIViewController = UIApplication.shared.keyWindow!.rootViewController!,
                        completion: (() -> Void)? = nil) {
        
        
        
        //Set title color
        if let textColor = textColor {
            
            //set titnt color
            setTint(color: textColor)
            
            //set button title color
            if let title = title {
                if let image = titleImage {
                    let imgViewTitle = UIImageView(frame: CGRect(x: 0, y: 10, width: 270, height: 20))
                    imgViewTitle.image = image
                    imgViewTitle.contentMode = .scaleAspectFit
                    view.addSubview(imgViewTitle)
                    
                    //clear title
                    setValue(NSAttributedString(string: title, attributes: [NSAttributedString.Key.foregroundColor : UIColor.clear]), forKey: "attributedTitle")
                }else {
                    setValue(NSAttributedString(string: title, attributes: [NSAttributedString.Key.foregroundColor : textColor]), forKey: "attributedTitle")
                }
            }
            
            //set cancel button color
            if let message = message {
                setValue(NSAttributedString(string: message, attributes: [NSAttributedString.Key.foregroundColor : textColor]), forKey: "attributedMessage")
            }
        }
        
        //Set background color
        if let backgroundColor = backgroundColor {
            
            if let firstSubview = view.subviews.first, let alertContentView = firstSubview.subviews.first {
                for view in alertContentView.subviews {
                    view.backgroundColor = backgroundColor
                }
            }
            
            //Set cancel button background color
            if let cancelBackgroundViewType = NSClassFromString("_UIAlertControlleriOSActionSheetCancelBackgroundView") as? UIView.Type {
                cancelBackgroundViewType.appearance().subviewsBackgroundColor = backgroundColor
            }
        }
        
        // Present alet sheet
        DispatchQueue.main.async {
            
            UIApplication.topViewController()?.present(self, animated: animated, completion: completion)
            //vc.present(self, animated: animated, completion: completion)
            if vibrate {
                if #available(iOS 13.0, *) {
                    UIImpactFeedbackGenerator(style: .soft).impactOccurred()
                } else {
                    UIImpactFeedbackGenerator(style: .light).impactOccurred()
                }
            }
        }
    }
    
    public func present() {
        present(animated: true, vibrate: true, backgroundColor: .white, titleColor: .black, textColor: .black, titleImage: #imageLiteral(resourceName: "LoginLogo"))
    }
    
    // Uncomment when its required
    /*
    func setTitle(font: UIFont?, color: UIColor?) {
      guard let title = self.title else { return }
      let attributeString = NSMutableAttributedString(string: title)//1
      if let titleFont = font {
        attributeString.addAttributes([NSAttributedString.Key.font : titleFont],//2
          range: NSMakeRange(0, title.utf8.count))
      }
      if let titleColor = color {
        attributeString.addAttributes([NSAttributedString.Key.foregroundColor : titleColor],//3
          range: NSMakeRange(0, title.utf8.count))
      }
      self.setValue(attributeString, forKey: "attributedTitle")//4
    }

    //Set message font and message color
    func setMessage(font: UIFont?, color: UIColor?) {
      guard let title = self.message else {
        return
      }
      let attributedString = NSMutableAttributedString(string: title)
      if let titleFont = font {
        attributedString.addAttributes([NSAttributedString.Key.font : titleFont], range: NSMakeRange(0, title.utf8.count))
      }
      if let titleColor = color {
        attributedString.addAttributes([NSAttributedString.Key.foregroundColor : titleColor], range: NSMakeRange(0, title.utf8.count))
      }
      self.setValue(attributedString, forKey: "attributedMessage")//4
    }*/

    //Set tint color of UIAlertController
    func setTint(color: UIColor) {
      self.view.tintColor = color
    }
}


fileprivate extension UIView {
    private struct AssociatedKey {
        static var subviewsBackgroundColor = "subviewsBackgroundColor"
    }

    @objc dynamic var subviewsBackgroundColor: UIColor? {
        get {
          return objc_getAssociatedObject(self, &AssociatedKey.subviewsBackgroundColor) as? UIColor
        }

        set {
          objc_setAssociatedObject(self,
                                   &AssociatedKey.subviewsBackgroundColor,
                                   newValue,
                                   .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
          subviews.forEach { $0.backgroundColor = newValue }
        }
    }
}
