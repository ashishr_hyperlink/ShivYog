//
//  BarChartMarker.swift
//  KitchkoDriver
//
//  Created by hyperlink on 21/04/21.
//

import UIKit

public class BarChartMarker: MarkerView {

    @IBOutlet weak var viewLblContainer: UIView!
    @IBOutlet weak var lblEarnings: UILabel!
    
    public override func awakeFromNib() {
        lblEarnings.font(name: FontFamily.SFProDisplay.regular, size: 13)

        DispatchQueue.main.async {
            self.viewLblContainer.borderColor(color: .app_5E5E5E, borderWidth: 1.0)
        }
    }
    
    public override func refreshContent(entry: ChartDataEntry, highlight: Highlight) {
        lblEarnings.text = String.init(format: "%d %%", Int(round(entry.y)))
        layoutIfNeeded()
    }

}
