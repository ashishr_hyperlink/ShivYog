//
//  ProfileVC.swift
//  ShivYog
//
//  Created by 2022M30 on 28/11/22.
//

import UIKit
import StoreKit

class ProfileVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var tblProfile: SelfSizedTableView!
    @IBOutlet weak var vwLogout: UIView!
    @IBOutlet weak var lblLogout: UILabel!
    @IBOutlet weak var lblAppVersion: UILabel!
    
    
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var profileVM : ProfileViewModel = ProfileViewModel()
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.clearNavigation()
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        AppDelegate.shared.addAnalyticsEvent(screenName: .Profile, eventName: .Pageview, category: .Profile)
//        self.tblProfile.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if ((self.tabBarController as? TabVC)?.miniPlayer.isHidden ?? true){
            self.tblProfile.addFooterView(bottomSpace: 0)
        }
        else{
            self.tblProfile.addFooterView(bottomSpace: kMiniPlayerConstant)
        }
        self.tblProfile.reloadData()        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.clearNavigation()

    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.lblLogout.font(name: FontFamily.SFProDisplay.medium, size: 18.0).textColor(color: .app_000000)
        self.lblAppVersion.font(name: FontFamily.SFProDisplay.medium, size: 14.0).textColor(color: .app_222222.withAlphaComponent(0.70))
        self.lblAppVersion.text = "Version " + Bundle.main.releaseVersionNumber!
                                                            
        
        
        
        self.tblProfile.registerNib(forCellWithClass: ProfileCell.self)
        self.tblProfile.registerNib(forCellWithClass: ProfileHeaderCell.self)
        self.tblProfile.delegate = self
        self.tblProfile.dataSource = self
        
        self.vwLogout.addTapGestureRecognizer {
//            UserDefaultsConfig.isAuthorization
            let actionYes = "Yes".addAction(style: .default, handler: { (action) in
                if let _ = GAudioPlayer.shared.player {
                    GAudioPlayer.shared.managedState(url: "123", state: .off, speed: "1")
                }
                self.profileVM.logout()
                AppDelegate.shared.addAnalyticsEvent(screenName: .Profile, eventName: .Logout, category: .Profile)
                UIApplication.shared.logoutAppUser()
            })
            let actionNo = "No".addAction(style: .cancel, handler: nil)
            UIAlertController.Style.alert.showAlert(title: "", message: "Are you sure you want to logout?", actions: [actionYes , actionNo])
            
        }
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })*/
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    @IBAction func btnRewardClicked(_ sender: UIBarButtonItem) {
        let vc = StoryboardScene.Profile.sendGiftVC.instantiate()//rewardVC.instantiate()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //------------------------------------------------------
    
}

//MARK: - UITableview delegate and Datasource -

extension ProfileVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.profileVM.numberOfSection()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.profileVM.numberOfProfileItemsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: ProfileCell.self)
        cell.isZeorIndex = indexPath.row == 0
        cell.isLastIndex = indexPath.row == self.profileVM.numberOfProfileItemsInSection(section: indexPath.section) - 1
        cell.data = self.profileVM.profileItemAtSection(section: indexPath.section).arrOptions[indexPath.row]
        DispatchQueue.main.async {

            if indexPath.row == 0 {
                cell.vwContainer.roundCorners([.topLeft, .topRight], radius: 24.0)
                cell.imgBottom.isHidden = false
            } else if indexPath.row == self.profileVM.numberOfProfileItemsInSection(section: indexPath.section) - 1 {
                cell.vwContainer.roundCorners([.bottomLeft, .bottomRight], radius: 24.0)
                cell.imgBottom.isHidden = true
            }
            else{
//                cell.vwContainer.cornerRadius = 0
                cell.vwContainer.roundCorners([], radius: 0.0)
                cell.imgBottom.isHidden = false
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withClass: ProfileHeaderCell.self)
        cell.data = self.profileVM.profileItemAtSection(section: section)
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.profileVM.profileItemAtSection(section: indexPath.section).arrOptions[indexPath.row].optionType
        
        switch data?.rawValue {
            
        case ProfileSubOptionType.name.rawValue :
            let vc = StoryboardScene.Profile.changeEmailNameNumberVC.instantiate()
            vc.changeDetailType = .name
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case ProfileSubOptionType.number.rawValue :
            let vc = StoryboardScene.Profile.changeEmailNameNumberVC.instantiate()
            vc.changeDetailType = .mobile
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case ProfileSubOptionType.email.rawValue :
            let vc = StoryboardScene.Profile.changeEmailNameNumberVC.instantiate()
            vc.changeDetailType = .email
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case ProfileSubOptionType.password.rawValue :
            let vc = StoryboardScene.Profile.changePasswordVC.instantiate()
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case ProfileSubOptionType.subsriptionPayments.rawValue :
            let vc = StoryboardScene.Profile.paymentsVC.instantiate()
            vc.isFrom = .subscriptionPayment
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case ProfileSubOptionType.storePayments.rawValue :
            let vc = StoryboardScene.Profile.paymentsVC.instantiate()
            vc.isFrom = .storePayment
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case ProfileSubOptionType.preferences.rawValue :
            let vc = StoryboardScene.Profile.preferencesVC.instantiate()
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case ProfileSubOptionType.redeem.rawValue :
            let vc = StoryboardScene.Profile.redeemGiftVC.instantiate()
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            
            break
        case ProfileSubOptionType.loggedDevices.rawValue :
            let vc = StoryboardScene.Authentication.loggedInDevicesVC.instantiate()
            vc.hidesBottomBarWhenPushed = true
            vc.modalPresentationStyle = .overFullScreen
            vc.isFrom = .profile
            let nav = UINavigationController(rootViewController: vc)
            nav.modalPresentationStyle = .overFullScreen
            nav.awakeFromNib()
            self.navigationController?.present(nav, animated: true, completion: nil)
            break
            
        case ProfileSubOptionType.help.rawValue :
            
            let vc = StoryboardScene.Profile.webContentVC.instantiate()
            vc.type = .help
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case ProfileSubOptionType.privacyPolicy.rawValue :
            let vc = StoryboardScene.Profile.webContentVC.instantiate()
            vc.type = .privacyPolicy
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case ProfileSubOptionType.aboutUs.rawValue :
            let vc = StoryboardScene.Profile.webContentVC.instantiate()
            vc.type = .aboutUs
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case ProfileSubOptionType.contactUs.rawValue :
            let vc = StoryboardScene.Profile.contactUsVC.instantiate()
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case ProfileSubOptionType.rateApp.rawValue :
            AppDelegate.shared.addAnalyticsEvent(screenName: .RateApp, eventName: .Pageview, category: .RateApp)
            Alert.shared.showSnackBar("Under development")
//            DispatchQueue.main.async {
//                SKStoreReviewController.requestReview()
//            }
            break
            
        case ProfileSubOptionType.terms.rawValue :
            let vc = StoryboardScene.Profile.webContentVC.instantiate()
            vc.type = .termsConditions
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        default :
            break
        }
    }
}
