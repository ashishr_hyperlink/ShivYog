//
//  ChangePasswordVC.swift
//  ShivYog
//
//  Created by 2022M30 on 29/11/22.
//

import UIKit

class ChangePasswordVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var txtCurrentPassword: GradientTextField!
    @IBOutlet weak var txtNewPassword: GradientTextField!
    @IBOutlet weak var txtConfirmPassword: GradientTextField!
    @IBOutlet weak var btnEyeCurrentPassword: UIButton!
    @IBOutlet weak var btnEyeNewPassword: UIButton!
    @IBOutlet weak var btnEyeConfirmPassword: UIButton!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var profileVM : ProfileViewModel = ProfileViewModel()
    var userData : JSON = JSON.null
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        AppDelegate.shared.addAnalyticsEvent(screenName: .ChnagePassword, eventName: .Pageview, category: .ChnagePassword)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.lblTitle.font(name: FontFamily.SFProDisplay.bold, size: 24.0).textColor(color: .app_000000)
        self.lblSubtitle.font(name: FontFamily.SFProDisplay.medium, size: 14.0).textColor(color: .app_5E5E5E)
        
        self.txtCurrentPassword.rightView = self.btnEyeCurrentPassword
        self.txtCurrentPassword.rightViewMode = .always
        self.txtCurrentPassword.delegate = self
        
        self.txtNewPassword.rightView = self.btnEyeNewPassword
        self.txtNewPassword.rightViewMode = .always
        self.txtNewPassword.delegate = self
        
        self.txtConfirmPassword.rightView = self.btnEyeConfirmPassword
        self.txtConfirmPassword.rightViewMode = .always
        self.txtConfirmPassword.delegate = self
        
        self.btnEyeCurrentPassword.addTapGestureRecognizer {
            self.txtCurrentPassword.isSecureTextEntry = self.btnEyeCurrentPassword.isSelected
            self.btnEyeCurrentPassword.isSelected.toggle()
        }
        
        self.btnEyeNewPassword.addTapGestureRecognizer {
            self.txtNewPassword.isSecureTextEntry = self.btnEyeNewPassword.isSelected
            self.btnEyeNewPassword.isSelected.toggle()
        }
        
        self.btnEyeConfirmPassword.addTapGestureRecognizer {
            self.txtConfirmPassword.isSecureTextEntry = self.btnEyeConfirmPassword.isSelected
            self.btnEyeConfirmPassword.isSelected.toggle()
        }
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        self.profileVM.changeCurrentPasswordResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(let msg):
             Alert.shared.showSnackBar(msg!)
             
             DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
                 self.navigationController?.popViewController(animated: true)
             }
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    
    @IBAction func btnSubmitClicked(_ sender: ThemeButton) {
        self.profileVM.apiChangePassword(currentPass: self.txtCurrentPassword.text!, newPass: self.txtNewPassword.text!, confirmPass: self.txtConfirmPassword.text!)
    }
    
    //------------------------------------------------------
    
}

//MARK: - UItextfield delegate -

extension ChangePasswordVC : UITextFieldDelegate {
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//    }
}
