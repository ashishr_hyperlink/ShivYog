//
//  PaymentsVC.swift
//  ShivYog
//
//  Created by 2022M30 on 29/11/22.
//

import UIKit

class PaymentsVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var btnManageSubscription: UIButton!
    
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var profileVM : ProfileViewModel = ProfileViewModel()
    var isFrom : IsFrom = .subscriptionPayment
    var arrData : [SubScriptionPaymentModel]?
    var currentPage = 1
    var isCallingApi = true
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        if self.isFrom == .subscriptionPayment {
            AppDelegate.shared.addAnalyticsEvent(screenName: .SubscriptionPayments, eventName: .Pageview, category: .SubscriptionPayments)
        } else if self.isFrom == .storePayment {
            AppDelegate.shared.addAnalyticsEvent(screenName: .StorePayments, eventName: .Pageview, category: .StorePayments)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.setupViewModelObserver()
        self.applyStyle()
    }

    private func applyStyle(){
        self.title = self.isFrom == .subscriptionPayment ? "Subscription Payment" : "Store Payment"
        
        self.btnManageSubscription.isHidden = self.isFrom == .storePayment
        
        self.tblList.registerNib(forCellWithClass: PaymentsCell.self)
        self.tblList.delegate = self
        self.tblList.dataSource = self
        
        self.btnManageSubscription.font(name: FontFamily.SFProDisplay.regular, size: 16.0).textColor(color: .app_990000, state: .normal)
        self.btnManageSubscription.borderColor(color: .app_000000.withAlphaComponent(0.5), borderWidth: 1.0)
        
        DispatchQueue.main.async {
            self.btnManageSubscription.cornerRadius(cornerRadius: 5.0)
        }
        
        
        self.tblList.addRefreshControl {
            self.isCallingApi = true
            self.currentPage = 1
            self.fetchData(isLoader: false)
        }
        
        self.fetchData(isLoader: true)
        
    }
    
    private func fetchData(isLoader: Bool) {
        if self.isFrom == .subscriptionPayment {
            self.profileVM.apiSubscriptionList(withLoader: isLoader, page: self.currentPage)
        } else {
            self.profileVM.apiStorePayment(withLoader: isLoader, isAddData: false, page: self.currentPage)
        }
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })*/
        
        self.profileVM.arrSubscriptionValue.bind { result in
            self.tblList.endRefreshing()
            self.tblList.emptyDataSetSource = self
            self.tblList.emptyDataSetDelegate = self
            if (result ?? []).count == 0 {
                self.isCallingApi = false
                return
            }
            
            if let data = result {
                self.isCallingApi = true
                self.currentPage += 1
                debugPrint("Self Current Page:-", self.currentPage)
                self.arrData = data
            }
            self.tblList.reloadData()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    @IBAction func btnManageSubscriptionClicked(_ sender: UIButton) {
        if let user = UserModel.currentUser {
            if let url = URL(string: WebViewLinks.subscription.title + "?token=\(UserModel.accessToken ?? "")&uuid=\(user.uuid ?? "")"){
                print("Subscribe URL:-",url)
                //                                url = url.description + "?token=\(accessToken ?? "")"
                UIApplication.shared.open(url)
            }
        }
        
    }
    
    
    //------------------------------------------------------
    
}

//MARK: - UItableView delegate and Datasource -

extension PaymentsVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.arrData?.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: PaymentsCell.self, for: indexPath)
        if let data = arrData {
            cell.isFrom = self.isFrom
            cell.data = data[indexPath.row]//self.isFrom == .storePayment ? self.profileVM.dataAtIndexForPayment(index: indexPath.row) : self.profileVM.dataAtForSubscription()
        }
        return cell
    }
}


extension PaymentsVC {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.isAtBottom {
            if self.isCallingApi {
//                self.callAPI(strSearch: self.txtSearch.text ?? "")
                self.isCallingApi = false
                self.fetchData(isLoader: false)
            }
        }
    }
}
