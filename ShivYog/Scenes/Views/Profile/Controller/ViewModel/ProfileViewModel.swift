//
//  ProfileViewModel.swift
//  ShivYog
//
//  Created by 2022M30 on 28/11/22.
//

import Foundation
import UIKit

class ProfileMainModel {
//    var image: UIImage!
//    var title: String!
    var profileOptions : ProfileMainOptions!
    var arrOptions: [ProfileModel]!
    
    init(profileOptions: ProfileMainOptions/*, image: UIImage, title: String*/, arrOptions: [ProfileModel]) {
        self.profileOptions = profileOptions
        self.arrOptions = arrOptions
    }
    
}

struct PaymentModel {
    var title : String!
}

class ProfileModel {
    var optionType : ProfileSubOptionType!
    
    init(optionType: ProfileSubOptionType) {
        self.optionType = optionType
    }
}

class PreferencesModel {
    var image: UIImage!
    var title: String!
    var isSelect: Bool!
    var isFrom: IsFrom!
    
    init(image: UIImage, title: String, isSelect: Bool = false, isFrom: IsFrom = .myProducts) {
        self.image = image
        self.title = title
        self.isSelect = isSelect
        self.isFrom = isFrom
    }
}


class ProfileViewModel {
    private var arrProfileMainModel = [
        ProfileMainModel(profileOptions: .personalDetails, arrOptions: [
            ProfileModel(optionType: .name),
            ProfileModel(optionType: .number),
            ProfileModel(optionType: .email),
            ProfileModel(optionType: .password)]),
        ProfileMainModel(profileOptions: .paymentHistory, arrOptions: [
            ProfileModel(optionType: .subsriptionPayments),
            ProfileModel(optionType: .storePayments)]),
        ProfileMainModel(profileOptions: .settings, arrOptions: [
            ProfileModel(optionType: .preferences),
            ProfileModel(optionType: .redeem),
            ProfileModel(optionType: .loggedDevices),
            ProfileModel(optionType: .help),
            ProfileModel(optionType: .privacyPolicy),
            ProfileModel(optionType: .aboutUs),
            ProfileModel(optionType: .contactUs),
            ProfileModel(optionType: .rateApp),
            ProfileModel(optionType: .terms),])]
    
    
//    private var arrStorePayment = [
//        PaymentModel(title: "You Have Successfully Purchased Yoga video 1."),
//        PaymentModel(title: "You Have Successfully Purchased Meditation Book 1."),
//        PaymentModel(title: "You Have Successfully Purchased Meditation Song 1."),
//        PaymentModel(title: "You Have Successfully Purchased Yoga video 1."),
//        PaymentModel(title: "You Have Successfully Purchased Meditation Book 1."),
//        PaymentModel(title: "You Have Successfully Purchased Meditation Song 1."),
//        PaymentModel(title: "You Have Successfully Purchased Yoga video 1."),
//        PaymentModel(title: "You Have Successfully Purchased Meditation Book 1."),
//        PaymentModel(title: "You Have Successfully Purchased Meditation Song 1."),
//        PaymentModel(title: "You Have Successfully Purchased Yoga video 1."),
//        PaymentModel(title: "You Have Successfully Purchased Meditation Book 1."),
//        PaymentModel(title: "You Have Successfully Purchased Meditation Song 1.")]
    
    
//    private var arrPreferencesModel = [
//        PreferencesModel(image: .sadhna.image, title: "Sadhna"),
//        PreferencesModel(image: .chanting.image, title: "Chanting"),
//        PreferencesModel(image: .sankirtan.image, title: "Sankirtan"),
//        PreferencesModel(image: .diksha.image, title: "Diksha"),
//        PreferencesModel(image: .mantra.image, title: "Mantra")]
    
    
    private var arrMySpace = [
        PreferencesModel(image: .myProducts.image, title: "My Products",isFrom: .myProducts),
        PreferencesModel(image: .playlists.image, title: "Playlists",isFrom: .playlists),
        PreferencesModel(image: .downloads.image, title: "Downloads", isFrom: .downloads),
        PreferencesModel(image: .favourites.image, title: "Favourites", isFrom: .favourites),
        PreferencesModel(image: .sadhna.image, title: "My Journey", isFrom: .myJourney)]
    

    private(set) var changeEmailResult = Bindable<Result<String?, AppError>>()
    private(set) var changeMobileResult = Bindable<Result<String?, AppError>>()
    private(set) var changeNameResult = Bindable<Result<String?, AppError>>()
    private(set) var changeCurrentPasswordResult = Bindable<Result<String?, AppError>>()
    private(set) var contactUsResult = Bindable<Result<String?, AppError>>()
    private(set) var giftCodeResult = Bindable<Result<String?, AppError>>()
    private(set) var sendGiftResult = Bindable<Result<String?, AppError>>()
    private(set) var checkInAppPurchaseResult = Bindable<Result<String?, AppError>>()
    private(set) var updatePreferences = Bindable<Result<String?, AppError>>()
    private(set) var isSubscriptionFetched = Bindable<Bool>()
    private(set) var arrSubscriptionValue = Bindable<[SubScriptionPaymentModel]>()
    private(set) var isAPIFetchComplete = Bindable<Bool>()//Bindable<[CategoryContentModel]>()
    private(set) var addStorePayment = Bindable<Bool>()
    private(set) var sendGiftInAppResult = Bindable<Result<String?, AppError>>()
    var arrCategoryData : [CategoryContentModel] = []
}


//MARK: - Change Email -

extension ProfileViewModel {
    private func isValidChangeEmail(email: String) -> AppError? {
        guard !email.isEmpty else { return AppError.validation(type: .enterEmail)}
        guard email.isValid(.email) else { return AppError.validation(type: .enterValidEmail)}
        return nil
    }
    
    func apiChangeEmail(email: String) {
        if let err = self.isValidChangeEmail(email: email) {
            self.changeEmailResult.value = .failure(err)
            return
        }
        var dictData = Dictionary<String,Any>()
        dictData["email"] = email
        self.updateProfile(id: UserModel.currentUser?.uuid ?? "", parameter: dictData, completion: self.changeNameResult, msg: "Email updated successfully")
    }
}


//MARK: - Change Name -

extension ProfileViewModel {
    private func isValidChangeName(name: String) -> AppError? {
        guard !name.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else { return AppError.validation(type: .enterName)}
        return nil
    }
    
    func apiChangeName(name: String) {
        if let err = self.isValidChangeName(name: name) {
            self.changeNameResult.value = .failure(err)
            return
        }
        var dictData = Dictionary<String,Any>()
        dictData["fullname"] = name
        self.updateProfile(id: UserModel.currentUser?.uuid ?? "", parameter: dictData, completion: self.changeNameResult, msg: "Name updated successfully")
    }
}


//MARK: - Change Mobile -

extension ProfileViewModel {
    private func isValidChangeMobile(mobile: String) -> AppError? {
        guard !mobile.isEmpty else { return AppError.validation(type: .enterChangeNumber)}
        guard mobile.isValid(.mobileNumber) else { return AppError.validation(type: .enterValidChangeNumber)}
        return nil
    }
    
    func apiChangeMobile(code: String ,mobile: String) {
        if let err = self.isValidChangeMobile(mobile: mobile) {
            self.changeMobileResult.value = .failure(err)
            return
        }
        var dictData = Dictionary<String,Any>()
        dictData["phone"] = code + mobile
        self.sendOTPForUpdateMobileNumber(parameter: dictData, completion: self.changeMobileResult, msg: "Mobile number updated successfully")
//        self.changeMobileResult.value = .success("Mobile number updated successfully")
    }
}

//MARK: - Update Preferences -

extension ProfileViewModel{
    func updateUsePreference(){ 
        var dictData = Dictionary<String,Any>()
        dictData["tag"] = GlobalAPI.shared.getSelectedTags().flatMap{$0.id}
        self.updateProfile(id: UserModel.currentUser?.uuid ?? "", parameter: dictData, completion: self.updatePreferences, msg: "Your preference updated successfully")
    }
}

//MARK: - Profile data -

extension ProfileViewModel {
    func numberOfSection() -> Int{
        return self.arrProfileMainModel.count
    }
    
    func numberOfProfileItemsInSection(section: Int) -> Int {
        return self.arrProfileMainModel[section].arrOptions.count
    }
    
    func profileItemAtSection(section: Int) -> ProfileMainModel{
        return self.arrProfileMainModel[section]
    }
    
    
}


//MARK: - extension Store Payment data -

//extension ProfileViewModel {
//    func numberOfPaymentItem() -> Int {
//        return self.arrStorePayment.count
//    }
//    
//    func dataAtIndexForPayment(index: Int) -> PaymentModel {
//        return self.arrStorePayment[index]
//    }
//    
//    func dataAtForSubscription() -> PaymentModel {
//        return PaymentModel(title: "You Have Successfully Purchased Premium Plan.")
//    }
//}

//MARK: - Change Password -

extension ProfileViewModel {
    private func isValidChangePass(currentPass: String, newPass: String, confirmPass: String) -> AppError? {
        guard !currentPass.isEmpty else { return AppError.validation(type: .enterCurrentPassword) }
        guard !newPass.isEmpty else { return AppError.validation(type: .enterNewPassword) }
        guard newPass.isValid(.password) else { return AppError.validation(type: .enterValidPassword) }
        guard !confirmPass.isEmpty else { return AppError.validation(type: .enterConfirmPassword) }
        guard newPass == confirmPass else { return AppError.validation(type: .passwordMismatch)}
        return nil
    }
    
    func apiChangePassword(currentPass: String, newPass: String, confirmPass: String) {
        if let err = self.isValidChangePass(currentPass: currentPass, newPass: newPass, confirmPass: confirmPass) {
            self.changeCurrentPasswordResult.value = .failure(err)
            return
        }
        
//        self.changeCurrentPasswordResult.value = .success("Password updated successfully")
        
        var dictData = Dictionary<String,Any>()
        dictData["old_password"] = currentPass
        dictData["new_password"] = newPass
        self.changePassword(parameter: dictData, completion: self.changeCurrentPasswordResult, msg: "Password updated successfully")
    }
}

//MARK: - Contact Us -

extension ProfileViewModel {
    private func isValidContactUs(subject: String, email: String, description: String) -> AppError? {
        guard !subject.isEmpty else {  return AppError.validation(type: .enterSubject)}
        guard !email.isEmpty else { return AppError.validation(type: .enterEmail)}
        guard email.isValid(.email) else { return AppError.validation(type: .enterValidEmail) }
        guard !description.isEmpty else { return AppError.validation(type: .enterDescription) }
        
        return nil
    }
    
    func apiContactUs(subject: String, email: String, description: String) {
        if let err = self.isValidContactUs(subject: subject, email: email, description: description) {
            self.contactUsResult.value = .failure(err)
            return
        }
        var dictData = Dictionary<String,Any>()
        dictData["subject"] = subject
        dictData["email"] = email
        dictData["description"] = description
        self.contactUs(parameter: dictData, completion: self.contactUsResult, msg: "Your inquiry submitted successfully")
//        self.contactUsResult.value = .success("Your inquiry submitted successfully")
    }
}

//MARK: - Preferences -

extension ProfileViewModel {
    func numberOfPreferencesItem(isFromSpace: Bool = false) -> Int {
        return self.arrMySpace.count /*isFromSpace ? self.arrMySpace.count : self.arrPreferencesModel.count*/
    }
    
    func dataAtIndexForPreferences(isFromSpace: Bool = false, index: Int) -> PreferencesModel {
        return self.arrMySpace[index] /*isFromSpace ? self.arrMySpace[index] : self.arrPreferencesModel[index]*/
    }
    
//    func updataDataForPreferences(index: Int) {
//        self.arrPreferencesModel[index].isSelect.toggle()
//    }
    
//    func selectAllPreferences() {
//        let _ = self.arrPreferencesModel.map {
//            $0.isSelect = true
//        }
//    }
    
//    func deSelectAllPreferences() {
//        let _ = self.arrPreferencesModel.map {
//            $0.isSelect = false
//        }
//    }
    
    
//    func isAllSelected() -> Bool{
//        return self.arrPreferencesModel.count == self.arrPreferencesModel.filter{$0.isSelect}.count
//    }
}

//MARK: - Redeem gift -

extension ProfileViewModel {
    private func isValidRedeemCode(code: String) -> AppError? {
        guard !code.isEmpty else { return AppError.validation(type: .enterGiftCode) }
        return nil
    }
    
    func apiRedeemGiftCode(code: String) {
        if let err = self.isValidRedeemCode(code: code) {
            self.giftCodeResult.value = .failure(err)
            return
        }
        
//        self.giftCodeResult.value = .success(nil)
        var dictData = Dictionary<String,String>()
        dictData["redeem_code"] = code
        
        ApiManager.shared.makeRequest(endPoint: .store(.storeProduct), methodType: .post, parameter: dictData, queryParameters: [:], withErrorAlert: true, withLoader: true, withdebugLog: true) { [weak self] (result) in
            guard let self = self else { return }
            
            switch result {
            case .success(let apiData):
                self.giftCodeResult.value = .success(apiData.message)
            case .failure(let error):
                print("the error \(error)")
                break
            }
        }
    }
}

//MARK: - Send Gift -

extension ProfileViewModel {
    private func isValidSendGift(countryCode: String,phone: String, email: String) -> AppError? {
        guard !countryCode.isEmpty else { return AppError.validation(type: .enterCountryCode) }
        guard !phone.isEmpty else { return AppError.validation(type: .enterPhone) }
        guard phone.isValid(.mobileNumber) else { return AppError.validation(type: .enterValidPhone) }
        if !email.trim().isEmpty {
            guard email.isValid(.email) else { return AppError.validation(type: .enterValidEmail) }
        }
        return nil
    }
    
    func sendGift(countryCode: String,phone: String, email: String) {
        if let err = self.isValidSendGift(countryCode: countryCode, phone: phone, email: email) {
            self.sendGiftResult.value = .failure(err)
            return
        }
        self.sendGiftResult.value = .success(nil)
        
    }
    
    func checkInAppPurchase(countryCode: String,phone: String, email: String, storesubContentId : Int) {
        var dictData = Dictionary<String, String>()
        dictData["phone"] = countryCode + phone
        dictData["email"] = email
        dictData["gift_type"] = "App_purchase"
        dictData["storesubcontent"] = storesubContentId.description
        
        
        ApiManager.shared.makeRequest(endPoint: .store(.sendGiftCheck), methodType: .post, parameter: dictData, queryParameters: [:], withErrorAlert: true, withLoader: true, withdebugLog: true) { [weak self] (result) in
            
            guard let self = self else { return }
            
            switch result {
            case .success(let apiData):
                self.checkInAppPurchaseResult.value = .success(nil)
            case .failure(let error):
                print("the error \(error)")
                break
            }
        }
    }
    
    func sendGiftInAppPurchase(phone: String, email: String, storeSubContent: Int, paymentID: String, paymentCurrency: String, price: String) {
        var dictData = Dictionary<String, String>()
        dictData["phone"] = phone
        dictData["email"] = email
        dictData["gift_type"] = "App_purchase"
        dictData["storesubcontent"] = storeSubContent.description
        dictData["paymentid"] = paymentID
        dictData["payment_currency"] = paymentCurrency
        dictData["price"] = price
        dictData["paymenttag"] = "InAppPurchase"
        ApiManager.shared.makeRequest(endPoint: .store(.sendGift), methodType: .post, parameter: dictData, queryParameters: [:], withErrorAlert: true, withLoader: true, withdebugLog: true) { [weak self] (result) in
            
            guard let self = self else { return }
            
            switch result {
            case .success(let apiData):
                print(apiData)
                self.sendGiftInAppResult.value = .success(nil)
                
            case .failure(let error):
                print("the error \(error)")
                break
            }
        }
    }
}

extension ProfileViewModel{
    func updateProfile(id : String, parameter : Dictionary<String,Any>, completion : Bindable<Result<String?, AppError>>, msg : String){
        ApiManager.shared.makeRequestWithModel(endPoint: .editProfile(.editProfile(id)), modelType: UserModel.self, methodType: .patch, responseModelType: .dictonary, parameter: parameter, queryParameters: [:], withErrorAlert: true, withLoader: true, withdebugLog: true) { [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                UserModel.currentUser = apiData.data
                GlobalAPI.shared.updateToUserData()
                completion.value = .success(msg)
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
    
    func changePassword(parameter : Dictionary<String,Any>, completion : Bindable<Result<String?, AppError>>, msg : String){
        ApiManager.shared.makeRequest(endPoint: .user(.changePassword), methodType: .post, parameter: parameter, withErrorAlert: true, withLoader: true, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                completion.value = .success(msg)
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
    
    func sendOTPForUpdateMobileNumber(parameter : Dictionary<String,Any>, completion : Bindable<Result<String?, AppError>>, msg : String){
        ApiManager.shared.makeRequest(endPoint: .user(.sendOtpForUpdatePhone), methodType: .post, parameter: parameter, withErrorAlert: true, withLoader: true, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                completion.value = .success(msg)
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
    
    func contactUs(parameter : Dictionary<String,Any>, completion : Bindable<Result<String?, AppError>>, msg : String){
        ApiManager.shared.makeRequest(endPoint: .cms(.contactus), methodType: .post, parameter: parameter, withErrorAlert: true, withLoader: true, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                completion.value = .success(msg)
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
    
    func logout(){
        ApiManager.shared.makeRequest(endPoint: .auth(.logout), methodType: .delete, parameter: [:], withErrorAlert: false, withLoader: false, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
//                completion.value = .success(nil)
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
}

extension ProfileViewModel {
    func apiSubscriptionList(withLoader isLoader: Bool = false, page: Int = 1) {
        var dictData = Dictionary<String, String>()
        dictData["page"] = page.description
        
        ApiManager.shared.makeRequest(endPoint: .payment(.subscriptionPayment), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true) { [weak self] (result) in
            
            guard let self = self else { return }
            switch result {
            case .success(let apiData):
                if page == 1 {
                    self.arrSubscriptionValue.value = SubScriptionPaymentModel.listFromArray(data: apiData.data["results"].arrayValue)
                } else {
                    self.arrSubscriptionValue.value?.append(SubScriptionPaymentModel.listFromArray(data: apiData.data["results"].arrayValue))
                }
                
                
            case .failure(let error):
                self.arrSubscriptionValue.value = []
                print("the error \(error)")
            }
            
        }
    }
    
    
    func apiStorePayment(withLoader isLoader: Bool = false, isAddData: Bool = false , parameter: [String: String] = [:], page: Int = -1) {
        if isAddData {
            self.storePaymentAdd(parameter: parameter, isLoader: isLoader)
        } else {
            self.storePaymentGet(isLoader: isLoader, page: page)
        }
    }
    
    func storePaymentAdd(parameter: [String: String], isLoader: Bool) {
        ApiManager.shared.makeRequest(endPoint: .payment(.storePayment), methodType: .post, parameter: parameter, queryParameters:  [:], withErrorAlert: true, withLoader: isLoader, withdebugLog: true) { [weak self] (result) in
            
            guard let self = self else { return }
            
            switch result {
            case .success(let apiData):
//                if page == 1 {
//                    self.arrSubscriptionValue.value = SubScriptionPaymentModel.listFromArray(data: apiData.data["results"].arrayValue)
//                } else {
//                    self.arrSubscriptionValue.value?.append(SubScriptionPaymentModel.listFromArray(data: apiData.data["results"].arrayValue))
                
                
//                }
                
                self.addToStore(storeSubContent: (parameter["storesubcontent"] ?? ""))
            case .failure(let error):
//                self.arrSubscriptionValue.value = []
                
                print("the error \(error)")
            }
        }
    }
    
    
    func addToStore(storeSubContent: String) {
        var dictData = Dictionary<String, String>()
        dictData["storesubcontent"] = storeSubContent
        ApiManager.shared.makeRequest(endPoint: .store(.storeProduct), methodType: .post, parameter: dictData, queryParameters: [:], withErrorAlert: true, withLoader: true, withdebugLog: true) { [weak self] (result) in
            guard let self = self else { return }
            
            switch result {
            case .success(let apiData):
                self.addStorePayment.value = true
            case .failure(let error):
                self.addStorePayment.value = false
                print("the error \(error)")
                break
            }
        }
    }
    
    func storePaymentGet(isLoader: Bool,page: Int) {
        var dictData = Dictionary<String, String>()
        
        if page != -1 {
            dictData["page"] = page.description
        }
        ApiManager.shared.makeRequest(endPoint: .payment(.storePayment), methodType: .get, parameter: nil, queryParameters:  dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true) { [weak self] (result) in
            
            guard let self = self else { return }
            
            switch result {
            case .success(let apiData):
                if page == 1 {
                    self.arrSubscriptionValue.value = SubScriptionPaymentModel.listFromArray(data: apiData.data["results"].arrayValue)
                } else {
                    self.arrSubscriptionValue.value?.append(SubScriptionPaymentModel.listFromArray(data: apiData.data["results"].arrayValue))
                }

            case .failure(let error):
                self.arrSubscriptionValue.value = []
                print("the error \(error)")
            }
        }
    }
    
}

//MARK: - InApp Purchase gift -

extension ProfileViewModel {
    func getGiftStoreSubContent(withLoader isLoader: Bool = false, page: Int = 1, search: String = "") {
        ApiManager.shared.cancelRequest(endpoint: .store(.storeSubcontent))
        var dictData = Dictionary<String,String>()
        
        dictData["is_gift"] = true.description
        dictData["page"] = page.description
        
        if !search.trim().isEmpty {
            dictData["search"] = search
        }
        
        ApiManager.shared.makeRequest(endPoint: .store(.storeSubcontent), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true) { [weak self] (result) in
            
            guard let self = self else { return }
            
            switch result {
            case .success(let apiData):
                if page == 1 {
                    self.arrCategoryData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)
                } else {
                    self.arrCategoryData.append(CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue))
                }
                self.isAPIFetchComplete.value = true
            case .failure(let error):
                self.isAPIFetchComplete.value = false//[]
                print("ther error \(error)")
            }
        }
    }
}
