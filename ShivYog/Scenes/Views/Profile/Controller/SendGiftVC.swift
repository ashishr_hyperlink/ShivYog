//
//  SendGiftVC.swift
//  ShivYog
//
//  Created by 2022M30 on 26/01/23.
//

import UIKit
import RevenueCat

class SendGiftVC: UIViewController {
    
    //MARK: - Outlet -
    @IBOutlet weak var lblSendGiftTitle: UILabel!
    @IBOutlet weak var lblEnterDetail: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var vwCode: UIView!
    @IBOutlet weak var txtMobile: GradientTextField!
    @IBOutlet weak var txtEmail: GradientTextField!
    @IBOutlet weak var btnSubscription: UIButton!
    @IBOutlet weak var btnInAppPurchase: UIButton!
    @IBOutlet weak var btnSelectContent: UIButton!
    
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var btnContentTime: UIButton!
    @IBOutlet weak var lblProductTitle: UILabel!
    @IBOutlet weak var lblProductType: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnRemove: UIButton!
    
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var vwSelectContentType: UIView!
    @IBOutlet weak var vwSelectedContent: UIView!
    @IBOutlet var btnDownArrow: UIButton!
    @IBOutlet weak var lblSelectContent: UILabel!
    
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var profileVM : ProfileViewModel = ProfileViewModel()
    var countryCode : String!
    var isContentAdded : Bool = false
    var selectedGiftData : CategoryContentModel?
    var storeProduct : StoreProduct?
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        AppDelegate.shared.addAnalyticsEvent(screenName: .SendGift, eventName: .Pageview, category: .SendGift)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.lblSendGiftTitle.font(name: FontFamily.PTSans.bold, size: 20.0).textColor(color: .app_222222)
        self.lblEnterDetail.font(name: FontFamily.SFProDisplay.regular, size: 14.0).textColor(color: .app_434343)
        self.btnSubscription.font(name: FontFamily.SFProDisplay.regular, size: 16.0).textColor(color: .app_222222, state: .normal).textColor(color: .app_222222, state: .selected)
        self.btnInAppPurchase.font(name: FontFamily.SFProDisplay.regular, size: 16.0).textColor(color: .app_222222, state: .normal).textColor(color: .app_222222, state: .selected)
        self.btnSelectContent.font(name: FontFamily.SFProDisplay.regular, size: 16.0).textColor(color: .appBdbdbd, state: .normal).borderColor(color: .appD9D9D9, borderWidth: 1.0).imageRightSide()
        self.lblSelectContent.font(name: FontFamily.SFProDisplay.regular, size: 16.0).textColor(color: .appBdbdbd)
        
        
        self.lblProductTitle.font(name: FontFamily.SFProDisplay.regular, size: 14.0).textColor(color: .app_222222)
        self.lblProductType.font(name: FontFamily.PTSans.regular, size: 12.0).textColor(color: .app_990000)
        self.lblDescription.font(name: FontFamily.CircularStd.book, size: 12.0).textColor(color: .app_5E5E5E)
        self.btnContentTime.font(name: FontFamily.SFProDisplay.regular, size: 12.0).textColor(color: .appFfffff, state: .normal).backGroundColor(color: .app_990000).borderColor(color: .appFfffff, borderWidth: 1.0).imageRightSide()
        self.btnContentTime.centerTextAndImage(spacing: 5.0)
        
        self.btnRemove.font(name: FontFamily.SFProDisplay.regular, size: 14.0).textColor(color: .app_990000, state: .normal)
        
        self.lblTotalAmount.font(name: FontFamily.SFProDisplay.bold, size: 16.0).textColor(color: .app_222222)
        self.lblPrice.font(name: FontFamily.SFProDisplay.bold, size: 16.0).textColor(color: .app_222222)
        
        self.lblCode.font(name: FontFamily.SFProDisplay.bold, size: 16.0).textColor(color: .app_222222)
        
        
        
        
        self.vwCode.borderColor(color: .app_990000, borderWidth: 1.0)
        
        self.txtMobile.delegate = self
        
        let currentCountryCode = GFunction.shared.getCurrentCountryCode()
        let bundle = Bundle.init(for: LocalePickerViewController.self)
        self.lblCode.text = currentCountryCode.0
        
        DispatchQueue.main.async {
            self.vwCode.setRound()
            self.vwCode.setGradient()
            self.btnContentTime.setRound()
            self.btnSelectContent.cornerRadius(cornerRadius: 5.0)
            self.imgProduct.cornerRadius(cornerRadius: 10.0)
        }
        
        self.setButtons(self.btnSubscription)
        self.setTapGesture()
        
        #if (targetEnvironment(simulator))
        self.txtEmail.text = "abc@abc.com"
        self.txtMobile.text = "12345678900"
        #endif
    }
    
    private func setTapGesture() {
        self.vwCode.addTapGestureRecognizer {
            let alert = UIAlertController(style: .actionSheet,  title: "Select country code", tintColor: .app_990000)
            alert.addLocalePicker(type: .phoneCode) { info in
                if let info = info {
                    self.lblCode.text = info.phoneCode
                    self.countryCode = info.phoneCode
                    
                    DispatchQueue.main.async {
                        guard let sublayers = self.vwCode.layer.sublayers else { return }
                        for sublayer in sublayers where sublayer.isKind(of: CAGradientLayer.self) {
                                sublayer.removeFromSuperlayer()
                        }
                        self.txtMobile.completion?()
                        self.vwCode.setGradient()
                        
//                        guard let txtSublayers = self.txtEmail.layer.sublayers else { return }
//                        for txtSublayer in txtSublayers where txtSublayer.isKind(of: CAGradientLayer.self) {
//                            txtSublayer.removeFromSuperlayer()
//                        }
//
//                        self.txtEmail.setGradient(isBorder: true)
                    }
                    
                }
            }
            alert.addAction(title: "Cancel", style: .destructive)
            alert.show()
        }
    }
    
    
    private func setButtons(_ sender: UIButton) {
        self.btnSubscription.isSelected = false
        self.btnInAppPurchase.isSelected = false
        sender.isSelected = true
        if self.btnInAppPurchase.isSelected {
            self.setUpInApppurchases()
        } else {
            self.vwSelectedContent.isHidden = true
            self.vwSelectContentType.isHidden = true
        }
    }
    
    private func setUpInApppurchases() {
        self.vwSelectedContent.isHidden = !self.isContentAdded
        self.vwSelectContentType.isHidden = self.isContentAdded
    }
    
    private func setSelectedGiftData() {
        if let data = selectedGiftData {
//            self.fetchProduct(productId: data.iosBundleId)
            self.imgProduct.setImage(with: data.image)
            self.lblProductTitle.text = data.title
            self.lblDescription.text = data.description
            self.btnContentTime.isHidden = true
            self.lblProductType.isHidden = true
            if UserModel.isIndianCountryCode() {
                self.lblPrice.text = "₹ " + data.inrPrice
            } else {
                self.lblPrice.text = "$ " + data.dollarPrice
            }
        }
    }
    
    private func fetchProduct(productId: String) {
        AppLoader.shared.addLoader()
        Purchases.shared.getProducts([productId]) { items in
            if AppLoader.shared.isLoaderAnimating() {
                AppLoader.shared.removeLoader()
            }
            
            print("Products items count:-",items.count)
            if items.count > 0 {
                //                for item in items {
                //                    print("Products item:-",item)
                //                    self.selectedPackage = item
                //                    debugPrint(item.productIdentifier)
                self.storeProduct = items.first(where: { $0.productIdentifier == productId })//item
                //                    if let _ = self.storeProduct {
                self.setSelectedGiftData()
                self.isContentAdded = true
                self.setButtons(self.btnInAppPurchase)
                //                    } else {
                //                        Alert.shared.showSnackBar("Something went wrong.")
                //                    }
                //                    break
                //                }
            } else {
                Alert.shared.showSnackBar("Something went wrong.")
            }
            
            
        }
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
        
        self.profileVM.sendGiftResult.bind(observer: { [unowned self] (result) in
            switch result {
            case .success(_):
                if self.btnInAppPurchase.isSelected {
                    if !self.isContentAdded {
                        Alert.shared.showSnackBar("Please select item from the store.")
                        return
                    } else {
//                        let vc = StoryboardScene.Store.paymentResponseVC.instantiate()
//                        vc.paymentResponse = .inAppPurchase
//                        vc.modalPresentationStyle = .overFullScreen
//                        vc.completion = {
//                            self.navigationController?.popViewController(animated: true)
//                        }
//                        self.openBottomSheetAnimate(vc: vc, sizes: [.intrinsic])
                        self.profileVM.checkInAppPurchase(countryCode: self.lblCode.text!, phone: self.txtMobile.text!, email: self.txtEmail.text! ,storesubContentId: self.selectedGiftData?.id ?? 0)
                        
                    }
                }// else {
//                    let vc = StoryboardScene.Store.paymentResponseVC.instantiate()
//                    vc.paymentResponse = .inAppPurchase
//                    vc.modalPresentationStyle = .overFullScreen
//                    vc.completion = {
//                        self.navigationController?.popViewController(animated: true)
//                    }
//                    self.openBottomSheetAnimate(vc: vc, sizes: [.intrinsic])
//                }
                break
                
            case .failure(let error):
                Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
                
            case .none: break
            }
        })
        
        
        self.profileVM.checkInAppPurchaseResult.bind(observer: { [unowned self] (result) in
            
            switch result {
            case .success(_):
//                debugPrint("print")
                if let storeProduct = storeProduct {
                    AppLoader.shared.addLoader()
                    
                    Purchases.shared.purchase(product: storeProduct) { transaction, customerInfo, error, isCancelled in
                        if AppLoader.shared.isLoaderAnimating() {
                            AppLoader.shared.removeLoader()
                        }
                        
                        if let error = error as? RevenueCat.ErrorCode{
//                            AppDelegate.shared.addAnalyticsEvent(screenName: .SendGift, eventName: .PurchaseSuccessful, category: .Purchase, label: self.selectedGiftData?.title ?? "")
                            print("Product Error:-", error)
//                            self.goToPaymentResponseScreen(response: .failure)
                            switch error{
                            case .purchaseCancelledError:
                                break
                            case .networkError:
                                self.goToPaymentResponseScreen(response: .failure, msg: "Network issue \n Please check your network connection.")
                                break
                            case .paymentPendingError:
                                self.goToPaymentResponseScreen(response: .failure)
                                break
                            default:
                                break
                            }
                            
                        }
                        
                        if let trans = transaction {
                            
                            if let skPayment = trans.sk1Transaction {
                                if skPayment.transactionState == .purchased {
                                    
                                    let phone = self.lblCode.text! + self.txtMobile.text!
                                    if let selectedGiftData = selectedGiftData {
                                        var strPrice : String = ""
                                        var strCurrency : String = ""
                                        if UserModel.isIndianCountryCode() {
                                            strPrice = selectedGiftData.inrPrice
                                            strCurrency = "INR"
                                        } else {
                                            strPrice = selectedGiftData.dollarPrice
                                            strCurrency = "Dollar"
                                        }
                                        
                                        AppDelegate.shared.addAnalyticsEvent(screenName: .SendGift, eventName: .PurchaseSuccessful, category: .Purchase, label: selectedGiftData.title)
                                        self.profileVM.sendGiftInAppPurchase(phone: phone, email: self.txtEmail.text ?? "", storeSubContent: selectedGiftData.id, paymentID: trans.transactionIdentifier, paymentCurrency: strCurrency, price: strPrice)
                                    }
                                    
                                } else if skPayment.transactionState == .failed {
                                    AppDelegate.shared.addAnalyticsEvent(screenName: .SendGift, eventName: .PurchaseSuccessful, category: .Purchase, label: self.selectedGiftData?.title ?? "")
//                                    self.goToPaymentResponseScreen(response: .failure)
                                    
                                }
                            }
                            //                            let phone = self.lblCode.text! + self.txtMobile.text!
                            //                            if let selectedGiftData = selectedGiftData {
                            //                                var strPrice : String = ""
                            //                                var strCurrency : String = ""
                            //                                if UserModel.isIndianCountryCode() {
                            //                                    strPrice = selectedGiftData.inrPrice
                            //                                    strCurrency = "INR"
                            //                                } else {
                            //                                    strPrice = selectedGiftData.dollarPrice
                            //                                    strCurrency = "Dollar"
                            //                                }
                            //
                            //                                AppDelegate.shared.addAnalyticsEvent(screenName: .SendGift, eventName: .PurchaseSuccessful, category: .Purchase, label: selectedGiftData.title)
                            //                                self.profileVM.sendGiftInAppPurchase(phone: phone, email: self.txtEmail.text ?? "", storeSubContent: selectedGiftData.id, paymentID: trans.transactionIdentifier, paymentCurrency: strCurrency, price: strPrice)
                            //                            }
                        }
                    }
                }
                
            case .failure(let error):
                Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            case .none:
                break
            }
        })
        
        self.profileVM.sendGiftInAppResult.bind(observer: {[unowned self] (result) in
            switch result {
            case .success(_):
                self.goToPaymentResponseScreen(response: .success)
            case .failure(let error):
                break
            case .none:
                break
            }
        })
    }
    
    
    private func goToPaymentResponseScreen(response: PaymentStatus, msg: String = "") {
        let vc = StoryboardScene.Store.paymentResponseVC.instantiate()
        vc.paymentResponse = response
        vc.isFromPaymentScreen = .inAppPurchase
        vc.msg = msg
        vc.modalPresentationStyle = .overFullScreen
        vc.completion = {
            if response == .success {
                self.navigationController?.popViewController(animated: true)
            }
        }
        self.openBottomSheetAnimate(vc: vc, sizes: [.intrinsic])//navigationController?.present(vc, animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    
    @IBAction func btnRemoveClicked(_ sender: UIButton) {
        self.isContentAdded = false
        self.selectedGiftData = nil
        self.storeProduct = nil
        self.setUpInApppurchases()
    }
    
    @IBAction func btnSubscriptionClicked(_ sender: UIButton) {
        self.setButtons(sender)
    }
    
    @IBAction func btnInAppPurchaseClicked(_ sender: UIButton) {
        self.setButtons(sender)
    }
    
    @IBAction func btnContentTypeClicked(_ sender: UIButton) {
        let vc = StoryboardScene.Profile.inAppPurchaseVC.instantiate()
        vc.completion = { giftItem in
            if let data = giftItem {
                self.selectedGiftData = data
                self.fetchProduct(productId: data.iosBundleId)
                
//                self.setSelectedGiftData()
               
            }
//            self.isContentAdded = true
//            self.setButtons(self.btnInAppPurchase)
            
        }
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnPaymentClicked(_ sender: ThemeButton) {
        self.view.endEditing(true)
        if self.btnInAppPurchase.isSelected {
//            if !self.isContentAdded {
//                Alert.shared.showSnackBar("Please select item from the store.")
                self.profileVM.sendGift(countryCode: self.lblCode.text!, phone: self.txtMobile.text!, email: self.txtEmail.text ?? "" )
            } else {
//                let vc = StoryboardScene.Store.paymentResponseVC.instantiate()
//                vc.paymentResponse = .inAppPurchase
//                vc.modalPresentationStyle = .overFullScreen
//                vc.completion = {
//                    self.navigationController?.popViewController(animated: true)
//                }
//                self.openBottomSheetAnimate(vc: vc, sizes: [.intrinsic])
                
            }
//        }
        
    }
    
    
    //------------------------------------------------------
    
}

//MARK: - UITextfield delegate and Datasource -

extension SendGiftVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if GFunction.shared.isBackspace(string) {
            return true
        }
        
        
        if textField == self.txtMobile {
            if self.txtMobile.text!.count >=  ValidationEnum.PhoneNumber.Maximum.rawValue {
                return false
            } else if textField == self.txtMobile {
                return string.isValid(.number)
            }
        }
        
        return true
    }
}
