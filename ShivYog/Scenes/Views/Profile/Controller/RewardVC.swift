//
//  RewardVC.swift
//  ShivYog
//
//  Created by 2022M30 on 30/11/22.
//

import UIKit

class RewardVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var btnSeeMore: UIButton!
    @IBOutlet weak var lblMoreOffers: UILabel!
    @IBOutlet weak var lblCongrats: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
//    private var viewModel : AuthViewModel = AuthViewModel()
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.lblCongrats.font(name: FontFamily.Inter.medium, size: 24.0).textColor(color: .app_222222)
        self.lblMessage.font(name: FontFamily.SFProDisplay.medium, size: 15.0).textColor(color: .app_222222)
        self.lblMoreOffers.font(name: FontFamily.SFProDisplay.medium, size: 16.0).textColor(color: .appFfffff)
        self.btnSeeMore.font(name: FontFamily.SFProDisplay.regular, size: 13.0).textColor(color: .appA6A6A6).backGroundColor(color: .clear)
        
        self.tblList.registerNib(forCellWithClass: RewardCell.self)
        self.tblList.delegate = self
        self.tblList.dataSource = self
        
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })*/
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    
    @IBAction func btnSeeMoreClicked(_ sender: UIButton) {
        Alert.shared.showSnackBar("Under development")
    }
    
    //------------------------------------------------------
    
}


//MARK: - UITableView delegate and datasource -

extension RewardVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: RewardCell.self, for: indexPath)
        cell.btnRedeem.addTapGestureRecognizer {
            Alert.shared.showSnackBar("Under development")
        }
        return cell
    }
}
