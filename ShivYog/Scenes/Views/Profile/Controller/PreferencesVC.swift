//
//  PreferencesVC.swift
//  ShivYog
//
//  Created by 2022M30 on 29/11/22.
//

import UIKit

class PreferencesVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var colList: UICollectionView!
    @IBOutlet weak var btnSelectAll: UIBarButtonItem!
    @IBOutlet weak var btnSkip: UIBarButtonItem!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var profileVM : ProfileViewModel = ProfileViewModel()
    var isFrom : IsFrom = .profile
    var isItemSelected : Bool = false
    var completion : ((Bool) -> Void)?
    
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        AppDelegate.shared.addAnalyticsEvent(screenName: .YourPreferences, eventName: .Pageview, category: .YourPreferences)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.btnSkip.setTitleTextAttributes([.font : FontFamily.SFProDisplay.medium.font(size: 14.0)!,.foregroundColor : UIColor.app_222222], for: .normal)
        self.btnSkip.setTitleTextAttributes([.font : FontFamily.SFProDisplay.medium.font(size: 14.0)!,.foregroundColor : UIColor.app_222222], for: .selected)
        
        self.btnSelectAll.setTitleTextAttributes([.font : FontFamily.SFProDisplay.medium.font(size: 14.0)!,.foregroundColor : UIColor.app_222222], for: .normal)
        self.btnSelectAll.setTitleTextAttributes([.font : FontFamily.SFProDisplay.medium.font(size: 14.0)!,.foregroundColor : UIColor.app_222222], for: .selected)
        
        self.colList.registerNib(forCellWithClass: PreferencesCell.self)
        self.colList.delegate = self
        self.colList.dataSource = self
        
        if GlobalAPI.shared.getNumberOfPreferences() == nil{
            GlobalAPI.shared.getPreferences()
        }
        
        self.colList.addRefreshControl {
            GlobalAPI.shared.getPreferences()
        }
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
        
        GlobalAPI.shared.isPreferenceFetchComplete.bind(observer: { [unowned self] (result) in
            if result ?? false{
                self.colList.endRefreshing()
                self.colList.reloadData()
                
                if GlobalAPI.shared.isAllPreferenceSelected(){
                    self.btnSelectAll.title = "Deselect All"
                }
                else{
                    self.btnSelectAll.title = "Select All"
                }
            }
        })
        
        profileVM.updatePreferences.bind { [unowned self] (result) in
            if self.isFrom == .signIn {
                UserDefaultsConfig.isAuthorization = true
                UserDefaultsConfig.isShowTutorial = true
//                UserDefaultsConfig.firstTimePreferences = false
//                UserDefaults.standard.synchronize()
                UIApplication.shared.manageLogin()
            } else {
                self.completion?(true)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    
    @IBAction func btnDoneClicked(_ sender: ThemeButton) {
        /*if GlobalAPI.shared.getSelectedTags().count <= 0 {
            Alert.shared.showSnackBar("Please select your preferences")
            return
        } else {*/
            self.profileVM.updateUsePreference()
//        }
//        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSelectAllClicked(_ sender: UIBarButtonItem) {
        if self.isFrom == .signIn {
            UserDefaultsConfig.isAuthorization = true
        }
        
        if GlobalAPI.shared.isAllPreferenceSelected(){
            GlobalAPI.shared.updateAllPreference(isSelect: false)
            self.colList.reloadData()
            self.isItemSelected = false
            self.btnSelectAll.title = "Select All"
        }
        else{
            GlobalAPI.shared.updateAllPreference(isSelect: true)
            self.isItemSelected = true
            self.colList.reloadData()
            self.btnSelectAll.title = "Deselect All"
        }
    }
    
    @IBAction func btnSkipClicked(_ sender: UIBarButtonItem) {
        if self.isFrom == .signIn {
            UserDefaultsConfig.isAuthorization = true
            UserDefaultsConfig.isShowTutorial = true
            UIApplication.shared.manageLogin()
        } else {
            GlobalAPI.shared.getPreferences()
            self.navigationController?.popViewController(animated: true)
        }
//        self.navigationController?.popViewController(animated: true)
    }
    //------------------------------------------------------
    
}

//MARK: - UICollectionview delegate and Datasource -

extension PreferencesVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return GlobalAPI.shared.getNumberOfPreferences() ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: PreferencesCell.self, for: indexPath)
        let data = GlobalAPI.shared.getDataAtIndexForPreferences(index: indexPath.row)
        cell.data = data
        
        if let data = data, data.isSelected {
            self.isItemSelected = true
        }
//        if self.profileVM.dataAtIndexForPreferences(index: indexPath.row).isSelect {
//            self.isItemSelected = true
//        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ( ScreenSize.width - 50) / 2
        
        return CGSize(width: width, height:/* 150 * ScreenSize.fontAspectRatio*/ width * 1.15)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.isItemSelected = false
        let _ = GlobalAPI.shared.updateDataForPreference(index: indexPath.row)
        self.colList.reloadData()
        
        if GlobalAPI.shared.isAllPreferenceSelected(){
            self.btnSelectAll.title = "Deselect All"
        }
        else{
            self.btnSelectAll.title = "Select All"
        }
    }
}
