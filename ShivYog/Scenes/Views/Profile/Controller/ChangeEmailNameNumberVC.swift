//
//  ChangeEmailNameNumberVC.swift
//  ShivYog
//
//  Created by 2022M30 on 28/11/22.
//

import UIKit
import SwiftyJSON

class ChangeEmailNameNumberVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var imgMain: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var txtEmail: GradientTextField!
    @IBOutlet weak var txtName: GradientTextField!
    @IBOutlet weak var txtPhone: GradientTextField!
    @IBOutlet weak var vwCode: UIView!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var btnEmailRightView: UIButton!
    @IBOutlet weak var btnNameRightView: UIButton!
    @IBOutlet weak var btnMobileRightView: UIButton!
    
    
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var profileVM : ProfileViewModel = ProfileViewModel()
    var changeDetailType : ChangeDetails = .email
    var countryCode : String!
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        if self.changeDetailType == .email {
            AppDelegate.shared.addAnalyticsEvent(screenName: .ChangeEmail, eventName: .Pageview, category: .ChangeEmail)
            
        } else if self.changeDetailType == .name {
            AppDelegate.shared.addAnalyticsEvent(screenName: .ChangeName, eventName: .Pageview, category: .ChangeName)
            
        } else if self.changeDetailType == .mobile {
            AppDelegate.shared.addAnalyticsEvent(screenName: .ChangeNumber, eventName: .Pageview, category: .ChangeNumber)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        
        self.txtPhone.rightView = self.btnMobileRightView
        self.txtPhone.rightViewMode = .always
        self.txtPhone.delegate = self
        
        self.txtEmail.rightView = self.btnEmailRightView
        self.txtEmail.rightViewMode = .always
        
        
        self.txtName.rightView = self.btnNameRightView
        self.txtName.rightViewMode = .always
        self.txtName.delegate = self
        
        self.lblTitle.font(name: FontFamily.SFProDisplay.bold, size: 24.0).textColor(color: .app_000000)
        self.lblSubTitle.font(name: FontFamily.SFProDisplay.medium, size: 14.0).textColor(color: .app_5E5E5E)
        self.setTextFields()
        
        self.lblCode.font(name: FontFamily.SFProDisplay.bold, size: 16.0).textColor(color: .app_222222)
        
        let currentCountryCode = GFunction.shared.getCurrentCountryCode()
        
        
        self.vwCode.borderColor(color: .app_990000, borderWidth: 1.0)
        
        DispatchQueue.main.async {
            self.vwCode.setRound()
            self.vwCode.setGradient()
        }
        
        self.vwCode.addTapGestureRecognizer {
            let alert = UIAlertController(style: .actionSheet,  title: "Select country code", tintColor: .app_990000)
            alert.addLocalePicker(type: .phoneCode) { info in
                if let info = info {
                    self.lblCode.text = info.phoneCode
                    self.countryCode = info.phoneCode
                }
                
                DispatchQueue.main.async {
                    guard let sublayers = self.vwCode.layer.sublayers else { return }
                    for sublayer in sublayers where sublayer.isKind(of: CAGradientLayer.self) {
                            sublayer.removeFromSuperlayer()
                    }
                    self.txtPhone.completion?()
                    self.vwCode.setGradient()
                }
            }
            alert.addAction(title: "Cancel", style: .destructive)
            alert.show()
        }
    }
    
    private func setTextFields() {
        guard let userData = UserModel.currentUser else{return}
        if self.changeDetailType == .email {
            self.txtEmail.text = userData.email
            self.imgMain.image = .changeEmail.image
            self.lblTitle.text = "Change Your Email"
            self.lblSubTitle.text = "You can change your email address below."
            
            self.txtName.isHidden = true
            self.txtPhone.superview?.isHidden = true
            
        } else if self.changeDetailType == .name  {
            self.txtName.text = userData.fullname
            self.imgMain.image = .changeName.image
            self.lblTitle.text = "Change Your Name"
            self.lblSubTitle.text = "You can change your name below."
            self.txtEmail.isHidden = true
            self.txtPhone.superview?.isHidden = true
            
        } else if self.changeDetailType == .mobile {
            self.txtPhone.text = userData.phoneNumber
            self.lblCode.text = userData.countryCode
            self.imgMain.image = .changeMobile.image
            self.lblTitle.text = "Change Your Mobile Number"
            self.lblSubTitle.text = "You can change your mobile number below."
            self.txtName.isHidden = true
            self.txtEmail.isHidden = true
        }
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        self.profileVM.changeNameResult.bind(observer: { [unowned self] (result) in
            switch result {
            case .success(let msg):
                Alert.shared.showSnackBar(msg!)
                DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
                    self.navigationController?.popViewController(animated: true)
                }
                
                break
                
            case .failure(let error):
                Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
                
            case .none: break
            }
        })
        
        
        self.profileVM.changeEmailResult.bind(observer: { [unowned self] (result) in
            switch result {
            case .success(let msg):
                Alert.shared.showSnackBar(msg!)
                
                DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
                    self.navigationController?.popViewController(animated: true)
                }
                break
                
            case .failure(let error):
                Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
                
            case .none: break
            }
        })
        
        
        self.profileVM.changeMobileResult.bind(observer: { [unowned self] (result) in
            switch result {
            case .success(let msg):
//                Alert.shared.showSnackBar(msg!)
//
//                DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
//                    self.navigationController?.popViewController(animated: true)
//                }
                let vc = StoryboardScene.Authentication.verificationVC.instantiate()
                vc.isFrom = .changeMobile
                var data = JSON(["phone" : self.lblCode.text! + self.txtPhone.text!, "country_code" : self.lblCode.text!, "phone_number" : self.txtPhone.text!])
                vc.userData = data
                self.navigationController?.pushViewController(vc, animated: true)
                
                break
                
            case .failure(let error):
                Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
                
            case .none: break
            }
        })
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    
    @IBAction func btnUpdateClicked(_ sender: ThemeButton) {
        self.view.endEditing(true)
        if self.changeDetailType == .email {
            self.profileVM.apiChangeEmail(email: self.txtEmail.text!.trim())
            
        } else if self.changeDetailType == .name {
            self.profileVM.apiChangeName(name: self.txtName.text!)
            
        } else if self.changeDetailType == .mobile {
            self.profileVM.apiChangeMobile(code: self.lblCode.text!, mobile: self.txtPhone.text!)
        }
    }
    
    //------------------------------------------------------
    
}


//MARK: - UITextfield delegate -

extension ChangeEmailNameNumberVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if GFunction.shared.isBackspace(string) {
            return true
        }
        
        if textField == self.txtName {
            return string.isValid(.name)
        }
        
        if textField == self.txtPhone {
            if self.txtPhone.text!.count >=  ValidationEnum.PhoneNumber.Maximum.rawValue {
                return false
            } else if textField == txtPhone {
                return string.isValid(.number)
            }
        }
        
        return !string.contains("")
    }
}
