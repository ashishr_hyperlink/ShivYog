//
//  RedeemGiftVC.swift
//  ShivYog
//
//  Created by 2022M30 on 25/01/23.
//

import UIKit

class RedeemGiftVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var lblRedeemGiftTitle: UILabel!
    @IBOutlet weak var lblEnterCodeMsg: UILabel!
    @IBOutlet weak var txtCode: GradientTextField!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var profileVM : ProfileViewModel = ProfileViewModel()
    private var homeVM : HomeViewModel = HomeViewModel()
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        AppDelegate.shared.addAnalyticsEvent(screenName: .RedeemGift, eventName: .Pageview, category: .RedeemGift)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.txtCode.delegate = self
        self.lblRedeemGiftTitle.font(name: FontFamily.PTSans.bold, size: 20.0).textColor(color: .app_222222)
        self.lblEnterCodeMsg.font(name: FontFamily.SFProDisplay.regular, size: 14.0).textColor(color: .app_434343).text = "Enter the redeem code below which you have\nreceived on mobile or email."
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
        
        self.profileVM.giftCodeResult.bind(observer: { [unowned self] (result) in
            switch result {
            case .success(_):
                // Redirect to next screen or home screen
                //                UIApplication.shared.setHome()
                AppDelegate.shared.addAnalyticsEvent(screenName: .RedeemGift, eventName: .RedeemSuccessful, category: .RedeemGift)
                let vc = StoryboardScene.Store.paymentResponseVC.instantiate()
                vc.paymentResponse = .success
                vc.isFromPaymentScreen = .redeemGift
                vc.modalPresentationStyle = .overFullScreen
                vc.completion = {
                    self.navigationController?.popToRootViewController(animated: false)
                    
                    if let tabVC = UIApplication.topViewController()?.parent?.parent as? TabVC {
                        tabVC.selectedIndex = 2
//                        self.navigationController?.popToRootViewController(animated: true)
//                        UIApplication.topViewController()?.navigationController?.popToRootViewController(animated: false)
                        if let topVC = UIApplication.topViewController() as? MySpaceVC {
                            let vc = StoryboardScene.Home.productListingVC.instantiate()
//                            vc.arrData = self.homeVM.arrNewPlaylist
                            vc.screenTitle = "My Products"
                            vc.isFrom = .mySpace
                            topVC.navigationController?.pushViewController(vc, animated: true)
                        } else {
                            UIApplication.topViewController()?.navigationController?.popToRootViewController(animated: false)
                            if let topVC = UIApplication.topViewController() as? MySpaceVC {
                                let vc = StoryboardScene.Home.productListingVC.instantiate()
//                                vc.arrData = self.homeVM.arrNewPlaylist
                                vc.screenTitle = "My Products"
                                vc.isFrom = .mySpace
                                topVC.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                        
                    }
                }
                self.openBottomSheetAnimate(vc: vc, sizes: [.intrinsic])
                break
                
            case .failure(let error):
                AppDelegate.shared.addAnalyticsEvent(screenName: .RedeemGift, eventName: .RedeemFailed, category: .RedeemGift)
                Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
                
            case .none: break
            }
        })
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    @IBAction func btnRedeemClicked(_ sender: ThemeButton) {
        self.profileVM.apiRedeemGiftCode(code: self.txtCode.text!)
    }
    
    
    //------------------------------------------------------
    
}

//MARK: - UITextfield Delegate -

extension RedeemGiftVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if GFunction.shared.isBackspace(string) {
            return true
        }
        
        if textField == self.txtCode {
            return string.isValid(.alphabetNum)
        }
        return true
    }
}
