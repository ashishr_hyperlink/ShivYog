//
//  WebContentVC.swift
//  ShivYog
//
//  Created by 2022M30 on 25/11/22.
//

import UIKit
import WebKit
class WebContentVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var constWebToSafeArea: NSLayoutConstraint!
    @IBOutlet weak var constWebToButton: NSLayoutConstraint!
    @IBOutlet weak var btnAcceptAll: ThemeButton!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
//    private var viewModel : AuthViewModel = AuthViewModel()
    var type : WebViewLinks = .aboutUs
    var isFrom : IsFrom = .profile
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        if type == .faq {
            AppDelegate.shared.addAnalyticsEvent(screenName: .FAQ, eventName: .Pageview, category: .FAQ)
        } else if type == .termsConditions {
            AppDelegate.shared.addAnalyticsEvent(screenName: .TermsofService, eventName: .Pageview, category: .TermsofService)
        } else if type == .aboutUs {
            AppDelegate.shared.addAnalyticsEvent(screenName: .AboutUs, eventName: .Pageview, category: .AboutUs)
        } else if type == .privacyPolicy {
            AppDelegate.shared.addAnalyticsEvent(screenName: .PrivacyPolicy, eventName: .Pageview, category: .PrivacyPolicy)
        } else if type == .help {
            AppDelegate.shared.addAnalyticsEvent(screenName: .Help, eventName: .Pageview, category: .Help)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.title = self.type.title
        self.btnAcceptAll.isHidden = self.isFrom == .profile
        self.constWebToButton.priority = self.btnAcceptAll.isHidden ? .defaultLow : .defaultHigh
        self.constWebToSafeArea.priority = self.btnAcceptAll.isHidden ? .defaultHigh : .defaultLow
        if let url = self.type.url {
            self.webView.load(URLRequest(url: url))
        }
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })*/
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    
    @IBAction func btnAcceptAllClicked(_ sender: ThemeButton) {
        UserDefaultsConfig.firstTimeLogin = false
        UserDefaults.standard.synchronize()
        let vc = StoryboardScene.Authentication.loggedInDevicesVC.instantiate()
        vc.modalPresentationStyle = .overFullScreen
        vc.completion = {
            if UserDefaultsConfig.firstTimePreferences {
                let vc = StoryboardScene.Profile.preferencesVC.instantiate()
                vc.isFrom = .signIn
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                UserDefaultsConfig.isShowTutorial = true
                UserDefaultsConfig.isAuthorization = true
                UserDefaultsConfig.firstTimeLogin = false
                UIApplication.shared.manageLogin()
            }
        }
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
}
