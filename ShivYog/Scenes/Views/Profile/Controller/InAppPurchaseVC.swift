//
//  InAppPurchaseVC.swift
//  ShivYog
//
//  Created by 2022M30 on 26/01/23.
//

import UIKit

class InAppPurchaseVC: UIViewController {
    
    //MARK: - Outlet -
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSelectContentTitle: UILabel!
    @IBOutlet weak var colList: UICollectionView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnClearText: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var vwSearch: UIView!
    
    

    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var profileVM : ProfileViewModel = ProfileViewModel()
    var completion : ((CategoryContentModel?) -> Void)?
    var currentPage = 1
    var isCallingApi = true
    var arrSubData : [CategoryContentModel] = []
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        self.fetchData(withLoader: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.setupViewModelObserver()
        self.applyStyle()
    }

    private func applyStyle(){
        self.lblTitle.font(name: FontFamily.SFProDisplay.bold, size: 18.0).textColor(color: .appFfffff)
        self.lblSelectContentTitle.font(name: FontFamily.CircularStd.book, size: 16.0).textColor(color: .app_000000)
        
        self.colList.registerNib(forCellWithClass: InAppPurchaseCell.self)
        self.colList.delegate = self
        self.colList.dataSource = self
        
        self.btnClearText.font(name: FontFamily.CircularStd.bold, size: 12.0).textColor(color: .app_222222, state: .normal).textColor(color: .app_222222, state: .selected)
        self.txtSearch.font(name: FontFamily.CircularStd.book, size: 12.0).textColor(color: .app_222222)
        self.txtSearch.attributedPlaceholder = NSAttributedString(string: "Search tips & trick yoga", attributes: [.font: FontFamily.CircularStd.book.font(size: 12.0)!, .foregroundColor: UIColor.app_222222.withAlphaComponent(0.2)])
        self.txtSearch.addTarget(self, action: #selector(self.txtDidChange(_:)), for: .editingChanged)
        self.vwSearch.superview?.isHidden = true
        self.btnSearch.isHidden = false
        DispatchQueue.main.async {
            self.vwSearch.cornerRadius(cornerRadius: self.vwSearch.frame.height / 2)
        }
        self.colList.addRefreshControl {
            self.currentPage = 1
            self.isCallingApi = true
            self.fetchData(withLoader: true)
        }
        
    }
    
    private func fetchData(withLoader: Bool) {
        self.profileVM.getGiftStoreSubContent(withLoader: withLoader, page: self.currentPage, search: self.txtSearch.text!)
    }
    
    @objc func txtDidChange(_ textField: UITextField) {
//        self.imgSearch.isHighlighted = textField.isFirstResponder
        self.currentPage = 1
        self.isCallingApi = true
        self.fetchData(withLoader: false)
        
        
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
        
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
         // Redirect to next screen or home screen
         //                UIApplication.shared.setHome()
         break
         
         case .failure(let error):
         Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
         
         case .none: break
         }
         })*/
        
        self.profileVM.isAPIFetchComplete.bind { result in
            self.colList.endRefreshing()
            if result ?? false {
                self.arrSubData = self.profileVM.arrCategoryData
                self.isCallingApi = true
                self.currentPage += 1
                self.colList.reloadData()
            } else {
                self.isCallingApi = false
            }
            self.colList.emptyDataSetSource = self
            self.colList.emptyDataSetDelegate = self
//            if let result = result {
//                self.arrSubData.append(result)
//                self.isCallingApi = true
//                self.currentPage += 1
//                self.colList.reloadData()
//            } else {
//                self.isCallingApi = false
//
//            }
////            self.arrSubData = result ?? []
////            if self.arrSubData?.count == 0 {
//////                self.arrSubData?.removeAll()
////                self.isCallingApi = false
////                self.colList.reloadData()
////
////                //                self.colList.reloadData()
////                return
////            }
////            self.currentPage += 1
////            self.isCallingApi = true
////            self.colList.reloadData()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    @IBAction func btnCancelClicked(_ sender: UIButton) {
//        self.completion?()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSearchClicked(_ sender: UIButton) {
        self.btnSearch.isHidden = true
        self.vwSearch.superview?.isHidden = false
    }
    
    @IBAction func btnClearTextClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        self.txtSearch.text?.removeAll()
        self.btnSearch.isHidden = false
        self.vwSearch.superview?.isHidden = true
    }
    
    
    //------------------------------------------------------
    
}

//MARK: - UICollectionview Delegate and Datasource -

extension InAppPurchaseVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrSubData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: InAppPurchaseCell.self, for: indexPath)
//        if let arr = self.arrSubData {
            let data = self.arrSubData[indexPath.row]
            cell.data = data
            cell.btnSelect.addTapGestureRecognizer {
                self.dismiss(animated: true) {
                    self.completion?(data)
                }
            }
//        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ScreenSize.width
        let height = width * 0.4
        return CGSize(width: width, height: height)
    }
}

extension InAppPurchaseVC {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.isAtBottom {
            if self.isCallingApi {
                self.isCallingApi = false
                self.fetchData(withLoader: false)
            }
        }
    }
}
