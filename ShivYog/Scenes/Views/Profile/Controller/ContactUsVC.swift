//
//  ContactUsVC.swift
//  ShivYog
//
//  Created by 2022M30 on 29/11/22.
//

import UIKit

class ContactUsVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var txtSubject: GradientTextField!
    @IBOutlet weak var txtEmail: GradientTextField!
    @IBOutlet weak var tvDescription: AppTextView!
    
    @IBOutlet var vwGradient: UIView!
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var profileVM : ProfileViewModel = ProfileViewModel()
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        AppDelegate.shared.addAnalyticsEvent(screenName: .ContactUs, eventName: .Pageview, category: .ContactUs)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.lblMessage.font(name: FontFamily.SFProDisplay.medium, size: 14.0).textColor(color: .app_22222207)
//        self.vwGradient.backgroundColor = .red
        DispatchQueue.main.asyncAfter(deadline: .now()+2, execute: {
            //            self.setGradient()
            let startColor: UIColor = .appFfaa9205
            let endColor : UIColor = .appFc0B9205
            let gradient:CAGradientLayer = CAGradientLayer()
            gradient.colors = [startColor.cgColor, endColor.cgColor]
            gradient.locations = [0.0 , 1.0]
            gradient.cornerRadius =  30.0
            
            gradient.frame = self.vwGradient.bounds
            gradient.masksToBounds = false
//            self.vwGradient.layer.addSublayer(gradient)
        })
        
        self.txtEmail.text = UserModel.currentUser?.email ?? ""
    }
    
    

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        self.profileVM.contactUsResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(let msg):
             AppDelegate.shared.addAnalyticsEvent(screenName: .ContactUs, eventName: .FormSubmission, category: .ContactUs)
             Alert.shared.showSnackBar(msg!)
             
             DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
                 self.navigationController?.popViewController(animated: true)
             }
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    @IBAction func btnSubmitClicked(_ sender: ThemeButton) {
        self.profileVM.apiContactUs(subject: self.txtSubject.text!, email: self.txtEmail.text!, description: self.tvDescription.text!)
    }
    
    
    //------------------------------------------------------
    
}
