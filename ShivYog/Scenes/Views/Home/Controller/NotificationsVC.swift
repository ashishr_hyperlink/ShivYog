//
//  NotificationsVC.swift
//  ShivYog
//
//  Created by 2022M30 on 05/12/22.
//

import UIKit

class NotificationsVC: UIViewController {
    
    //MARK: - Outlet -
    
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var lblMarkAllRead: UILabel!
    @IBOutlet weak var vwMarkAllRead: UIView!
    
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var homeVM : HomeViewModel = HomeViewModel()
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        AppDelegate.shared.addAnalyticsEvent(screenName: .Notifications, eventName: .Pageview, category: .Notifications)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.lblMarkAllRead.font(name: FontFamily.SFProDisplay.bold, size: 14.0).textColor(color: .app_990000)
        self.lblMarkAllRead.attributedText = self.lblMarkAllRead.text?.getAttributedText(defaultDic: [.font: self.lblMarkAllRead.text!, .foregroundColor : self.lblMarkAllRead.textColor!], attributeDic: [.underlineStyle : 1.0, .underlineColor : UIColor.app_990000], attributedStrings: ["Mark All Read"])
        
        self.tblList.registerNib(forCellWithClass: NotificationHeaderCell.self)
        self.tblList.registerNib(forCellWithClass: NotificationNormalCell.self)
        self.tblList.registerNib(forCellWithClass: NotificationWithMediaCell.self)
        self.tblList.delegate = self
        self.tblList.dataSource = self
        
        self.vwMarkAllRead.addTapGestureRecognizer {
            self.homeVM.markAllReadNotification()
            self.tblList.reloadData()
        }
    
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })*/
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    
    
    //------------------------------------------------------
    
}

//MARK: - UItableView delegate and datasource -

extension NotificationsVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.homeVM.numberOfItemsAtNotificationSection()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.homeVM.dataAtSectionForNotification(section: section).arrNotificationModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = self.homeVM.dataAtSectionForNotification(section: indexPath.section).arrNotificationModel[indexPath.row]
        
        if data.contentType == .pdf {
            let cell = tableView.dequeueReusableCell(withClass: NotificationNormalCell.self, for: indexPath)
            cell.data = data
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withClass: NotificationWithMediaCell.self, for: indexPath)
            cell.data = data
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withClass: NotificationHeaderCell.self)
        cell.lblHeaderTitle.text = self.homeVM.dataAtSectionForNotification(section: section).day
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.homeVM.updataDataAtIndexForNotification(section: indexPath.section, row: indexPath.row)
        self.tblList.reloadData()
    }
}
