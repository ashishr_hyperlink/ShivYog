//
//  VideoPlayerVC.swift
//  ShivYog
//
//  Created by 2022M30 on 06/12/22.
//

import UIKit
import AVFoundation
import AVKit

class VideoPlayerVC: UIViewController {
    
    //MARK: - Outlet

    @IBOutlet weak var btnPlayPause: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var vwVideo: UIView!
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var vwControls: UIView!
    @IBOutlet weak var btnRepeat: UIButton!
    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var vwRangeSlider: RangeSeekSlider!
    @IBOutlet weak var btnFavourite: UIButton!
    @IBOutlet weak var btnMoreTop: UIButton!
    @IBOutlet weak var lblTotalDuration: UILabel!
    @IBOutlet weak var lblCurrentTime: UILabel!
    @IBOutlet weak var lblUserInfo: UILabel!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
//    private var viewModel : AuthViewModel = AuthViewModel()
    var player : AVPlayer?
    var arrUrl = [String]()
    var videoURL: URL! = nil
    var index = Int()
    var isVideoPlaying = true
    var sliderTimer : Timer!
    var isRepeat : Bool = false
    var controlTimer : Timer!
    var forceLandscape: Bool = false
    var mediaData : CategoryContentModel?
    var continueWatchingTimer : Timer!
    var lastUpdatedDuration : Double = 0
    var continueWatchingUpdateInterval : Int = 60
    var isFrom : IsFrom = .home
    var completionRemovedFromFav : ((Bool) -> Void)?
    var isOptionsClicked : Bool = false
    var contentTitle : String?
    var arrMediaData : [CategoryContentModel]?
    var currentMediaIndex : Int = -1
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscape
    }

    override var shouldAutorotate: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let value = UIInterfaceOrientation.landscapeLeft.rawValue
//        UIDevice.current.setValue(value, forKey: "orientation")
        
        //TODO: - Uncomment for iOS 16.0 and newer when xcode updated -
//        if #available(iOS 16, *) {
//            DispatchQueue.main.async {
//                let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene
//                self.setNeedsUpdateOfSupportedInterfaceOrientations()
//                self.navigationController?.setNeedsUpdateOfSupportedInterfaceOrientations()
//                windowScene?.requestGeometryUpdate(.iOS(interfaceOrientations: .landscape)) { error in
//                    print(error)
//                    print(windowScene?.effectiveGeometry ?? "")
//                }
//            }
//
//        } else {
        
            let value = UIInterfaceOrientationMask.landscape.rawValue//UIInterfaceOrientation.landscapeLeft.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
            UIView.setAnimationsEnabled(true)
//        }
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        //TODO: - Uncomment for iOS 16.0 and newer when xcode updated -
//        if #available(iOS 16, *) {
//            DispatchQueue.main.async {
//                let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene
//                self.setNeedsUpdateOfSupportedInterfaceOrientations()
//                self.navigationController?.setNeedsUpdateOfSupportedInterfaceOrientations()
//                windowScene?.requestGeometryUpdate(.iOS(interfaceOrientations: .landscape)) { error in
//                    print(error)
//                    print(windowScene?.effectiveGeometry ?? "")
//                }
//            }
//
//        } else {
            let value = UIInterfaceOrientationMask.landscape.rawValue//UIInterfaceOrientation.landscapeLeft.rawValue
            UIDevice.current.setValue(value, forKey: "orientation")
            UIView.setAnimationsEnabled(true)
//        }
        self.setUpPlayer()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.sliderTimer.isValid {
            self.sliderTimer.invalidate()
            self.isVideoPlaying.toggle()
            
        }
        if self.continueWatchingTimer.isValid {
            self.continueWatchingTimer.invalidate()
        }
        if player != nil {
            player = nil
        }
        
            AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
            self.navigationController?.navigationBar.isHidden = false
        
        if self.isFrom == .downloads {
            self.completionRemovedFromFav?(true)
        }
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        if let _ = GAudioPlayer.shared.player {
            GAudioPlayer.shared.managedState(url: "123", state: .off, speed: "1")
        }
        guard let uData = UserModel.currentUser else{return}
        
        self.lblUserInfo.font(name: FontFamily.SFProDisplay.medium, size: 16.0).textColor(color: .appEbffff50).text = "%@ - %@".localized(uData.phone, uData.fullname)
//        self.lblUserInfo.alpha = 0.5
        self.lblTitle.font(name: FontFamily.SFProDisplay.bold, size: 18.0).textColor(color: .appFfffff)
        self.lblType.font(name: FontFamily.SFProDisplay.bold, size: 14.0).textColor(color: .appFbc300)
        self.lblTotalDuration.font(name: FontFamily.SFProDisplay.regular, size: 18.0).textColor(color: .white)
        self.lblCurrentTime.font(name: FontFamily.SFProDisplay.regular, size: 18.0).textColor(color: .white)
        self.btnRepeat.font(name: FontFamily.PTSans.regular, size: 14.0).textColor(color: .appFfffff, state: .normal).centerTextAndImage(spacing: 5.0)
        self.btnRepeat.font(name: FontFamily.PTSans.regular, size: 14.0).textColor(color: .appFbc300, state: .selected).centerTextAndImage(spacing: 5.0)
        self.btnDownload.font(name: FontFamily.PTSans.regular, size: 14.0).textColor(color: .appFfffff).centerTextAndImage(spacing: 5.0)
//        self.vwRangeSlider.isUserInteractionEnabled = true
        self.vwRangeSlider.minValue = 0.0
        self.vwRangeSlider.selectedMinValue = 0.0
        self.lblTotalDuration.text = "123456789"
        self.vwRangeSlider.tintColor = .app_222222.withAlphaComponent(0.70)
        self.vwRangeSlider.colorBetweenHandles = .app_990000
        self.vwRangeSlider.delegate = self
        self.vwVideo.addTapGestureRecognizer {

            if self.controlTimer != nil{
                self.controlTimer.invalidate()
            }
            self.vwTop.isHidden = !self.vwTop.isHidden
            self.vwControls.isHidden = !self.vwControls.isHidden
//            self.btnMoreTop.isHidden = true
            if !self.vwControls.isHidden{
                self.controlTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.hideControlls), userInfo: nil, repeats: false)
            }
            /*DispatchQueue.main.asyncAfter(deadline: .now()+5.0, execute: {
                
            })*/
        }
        
        self.vwTop.addTapGestureRecognizer {
            if self.controlTimer != nil{
                self.controlTimer.invalidate()
            }
            self.vwTop.isHidden = !self.vwTop.isHidden
            self.vwControls.isHidden = !self.vwControls.isHidden
//            self.btnMoreTop.isHidden = true
            if !self.vwControls.isHidden{
                self.controlTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.hideControlls), userInfo: nil, repeats: false)
            }
        }
        self.getCurrentIndexOfItem()
        
    }
    
    @objc func hideControlls(){
        self.vwTop.isHidden = true
        self.vwControls.isHidden = true
    }
    
    
    @objc func canRotate() -> Void {}
    
    private func setUpPlayer() {
        AppLoader.shared.addLoader()
        let videoUrl = self.mediaData?.videoUrl ?? "https://player.vimeo.com/external/398176901.m3u8?s=9c464c95b053dbb91d366316d54c38954ade8bb1"
        if let data = mediaData{
            self.lblTitle.text = data.title
            self.btnFavourite.isSelected = data.favourite
            self.lblType.text = data.tag.flatMap{$0.title}.joined(separator: " • ")
            
            if let detail = data.subcategorycontentDetails {
                self.contentTitle = detail.title
            }
            if let detail = data.storesubcontentDetails {
                self.contentTitle = detail.title
            }
        }
        //        var url = URL(string:  videoUrl)
        //        if url == nil{
        //           url = URL(fileURLWithPath: videoUrl)
        //        }
        //        debugPrint("local url:-", url as Any)
        
        var url = URL(string:  videoUrl)
        if self.isFrom == .downloads{
            let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            let documentsDirectory = paths[0]
            
            let videoDataPath = documentsDirectory + "/" + (Bundle.main.displayName ?? "app") + "/" + videoUrl.lastPathComponent
            
            url = URL(fileURLWithPath: videoDataPath)
        }
        
        if let urlString = url/*URL(string:  videoUrl) , self.isFrom != .downloads*/  {
            do {
                try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
                let asset : AVURLAsset = AVURLAsset(url: urlString, options: nil)
                let statusKey = "tracks"
                
                asset.loadValuesAsynchronously(forKeys: [statusKey]) {
                    var error: NSError? = nil
                    
                    DispatchQueue.main.async {
                        let status : AVKeyValueStatus = asset.statusOfValue(forKey: statusKey, error: &error)
                        debugPrint("main async error-", error as Any)
                        if status == AVKeyValueStatus.loaded {
                            let playerItem = AVPlayerItem(asset: asset)
                            self.player = AVPlayer(playerItem: playerItem)
                            
                            let playerLayer = AVPlayerLayer(player: self.player)
                            if #available(iOS 16.0, *) {
                                DispatchQueue.main.asyncAfter(deadline: .now()) {
                                    playerLayer.position = .zero
                                    playerLayer.frame.origin = .zero
                                    playerLayer.frame.size = CGSize(width: ScreenSize.width, height: ScreenSize.height)//CGSize(width: ScreenSize.height, height: ScreenSize.width)
                                    playerLayer.videoGravity = .resizeAspect
                                    self.vwVideo.layer.addSublayer(playerLayer)
                                }
                            } else {
                                DispatchQueue.main.asyncAfter(deadline: .now()) {
                                    playerLayer.frame.size = CGSize(width: ScreenSize.width, height: ScreenSize.height)
                                    playerLayer.videoGravity = .resizeAspect
                                    self.vwVideo.layer.addSublayer(playerLayer)
                                }
                            }
                            
                            AppDelegate.shared.addAnalyticsEvent(screenName: .ContentDetail, eventName: .Videoview, category: .Content,action: "Video view", label: "\(self.contentTitle ?? "") : \(self.mediaData?.title ?? "")")
                            self.player?.play()
                            self.btnPlayPause.isSelected = !self.isVideoPlaying
                            
                            self.vwTop.isHidden = self.isVideoPlaying
                            self.vwControls.isHidden = self.isVideoPlaying
                            self.btnPlayPause.isSelected = !self.isVideoPlaying
                            
                            self.startTimer()
                            
                            let newTime = self.mediaData?.watchedDuration ?? 0
                            if newTime > 0{
                                let time: CMTime = CMTimeMake(value: Int64(newTime * 1000), timescale: 1000)
                                self.player?.seek(to: time)
                            }/* else {
                              guard let duration = self.player?.currentItem?.duration else { return }
                              self.player?.seek(to: duration)
                              }*/
                            
                            NotificationCenter.default.addObserver(self, selector: #selector(self.videoDidEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
                            NotificationCenter.default.addObserver(self, selector: #selector(self.playerStalled), name: NSNotification.Name.AVPlayerItemPlaybackStalled, object: nil)
                            
                            if AppLoader.shared.isLoaderAnimating() {
                                AppLoader.shared.removeLoader()
                            }
                        }
                    }
                }
                
            }
            catch {
                print("Setting category to AVAudioSessionCategoryPlayback failed.")
            }
        } else {
            debugPrint("Error")
        }
        
    }
    
    @objc func playerStalled() {
        AppLoader.shared.addLoader()
    }
    
    @objc func videoDidEnd(){
        debugPrint("videoDidEnd")
        if self.isRepeat{
            player?.seek(to: .zero)
            player?.play()
        }
        else {
            if !self.setNextItemToPlay() {
                self.btnBackClicked(UIButton())
            }
        }
    }
    
    private func getCurrentIndexOfItem() {
        if let data = self.arrMediaData {
            for (idx, item) in data.enumerated() {
                if let data = mediaData, data.id == item.id {
                    self.currentMediaIndex = idx
                    break
                }
            }
        }
    }
    
    private func setNextItemToPlay() -> Bool{
        self.currentMediaIndex += 1
        if let data = self.arrMediaData , !self.isRepeat, data.count - 1 >= self.currentMediaIndex {
            self.continueWatchingUpdate()
            self.continueWatchingTimer?.invalidate()
            self.player = nil
            self.mediaData = data[self.currentMediaIndex]
            self.setUpPlayer()
            return true
        }
        return false
    }
    
    private func startTimer() {
        
        self.sliderTimer = Timer.scheduledTimer(timeInterval: 0.001, target: self, selector: #selector(self.updateSlider), userInfo: nil, repeats: true)
        
        if self.continueWatchingTimer == nil{
            self.continueWatchingTimer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(self.continueWatchingUpdate), userInfo: nil, repeats: true)
            self.continueWatchingTimer.fire()
        }
    }
    
    @objc func continueWatchingUpdate(){
        let currentTime = CMTimeGetSeconds(self.player?.currentTime() ?? CMTime.zero)
        let interval = self.continueWatchingUpdateInterval - Int(Date().timeIntervalSince(self.continueWatchingTimer.fireDate).truncatingRemainder(dividingBy: Double(self.continueWatchingUpdateInterval)))
        debugPrint("interval", interval)
        //60 //- self.continueWatchingTimer.fireDate.timeIntervalSince(Date())
        
        if interval > 0{
            guard let mediaData = self.mediaData else { return }
            GlobalAPI.shared.addUpdateContinueWatching(contentData: mediaData, watchedDuration: Int(currentTime), interval: Int(interval))
            self.lastUpdatedDuration = currentTime
        }
    }
    
    @objc func updateSlider() {
        
        if self.sliderTimer.isValid /*&& player != nil*/{
            self.vwRangeSlider.maxValue = CMTimeGetSeconds((player?.currentItem?.duration) ?? CMTime.zero)
            self.vwRangeSlider.selectedMaxValue = CMTimeGetSeconds((player?.currentTime()) ?? CMTime.zero)
            let duration = CMTime(seconds: CMTimeGetSeconds((player?.currentItem?.duration) ?? CMTime.zero), preferredTimescale: 1000000).durationText//Int(Float(CMTimeGetSeconds((player?.currentItem!.duration)!))).secondsToMinutesSeconds
            let currentTime = Int(Float(CMTimeGetSeconds((player?.currentTime()) ?? CMTime.zero))).secondsToMinutesSeconds
            self.lblTotalDuration.text =  duration.description
            self.lblCurrentTime.text = currentTime.description
            
            if self.player?.timeControlStatus != .waitingToPlayAtSpecifiedRate {
                if AppLoader.shared.isLoaderAnimating() {
                    AppLoader.shared.removeLoader()
                }
            }
        }
    }


    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })*/
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    
    @IBAction func btnFastBackwardClicked(_ sender: UIButton) {
        let currentTime = CMTimeGetSeconds((player?.currentTime())!)
        var newTime = currentTime - 10.0
        if newTime < 0{
            newTime = 0
        }
        let time: CMTime = CMTimeMake(value: Int64(newTime * 1000), timescale: 1000)
        player?.seek(to: time)
        if !self.sliderTimer.isValid {
            self.startTimer()
            
        }
    }
    
    @IBAction func btnFastForwardClicked(_ sender: UIButton) {
        guard let duration = player?.currentItem?.duration else { return }
        let currentTime = CMTimeGetSeconds((player?.currentTime())!)
        let newTime = currentTime + 10.0
        if newTime < (CMTimeGetSeconds(duration) - 10.0){
            let time: CMTime = CMTimeMake(value: Int64(newTime * 1000), timescale: 1000)
            player?.seek(to: time)
        } else {
            guard let duration = player?.currentItem?.duration else { return }
            player?.seek(to: duration)
        }
    }
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.continueWatchingUpdate()
        if isVideoPlaying {
            player?.pause()
        }
        player = nil
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPlayPauseClicked(_ sender: UIButton) {
        if isVideoPlaying{
            player?.pause()
        }else{
            player?.play()
        }
        self.btnPlayPause.isSelected = isVideoPlaying
        isVideoPlaying = !isVideoPlaying
    }
    
    @IBAction func btnRepeatClicked(_ sender: UIButton) {
        AppDelegate.shared.addAnalyticsEvent(screenName: .VideoAudio, eventName: .RepeatToggle, category: .Content, action: "Repeat Toggle", label: "\(self.contentTitle ?? "") : \(self.mediaData?.title ?? "")")
        self.isRepeat.toggle()
        self.btnRepeat.isSelected.toggle()
        if self.isRepeat{
//            Alert.shared.showSnackBar("Video repeat on", isError: false)
        }
        else{
//            Alert.shared.showSnackBar("Video repeat off", isError: false)
        }
    }
    
    @IBAction func btnDownloadClicked(_ sender: UIButton) {
        Alert.shared.showSnackBar("Under development")
    }
    
    @IBAction func btnMoreClicked(_ sender: UIButton) {
        if isVideoPlaying{
            player?.pause()
            self.btnPlayPause.isSelected = isVideoPlaying
            isVideoPlaying = !isVideoPlaying
        }
        let vc = StoryboardScene.MySpace.playlistOptionsVC.instantiate()
        vc.isFromPlayer = true
        vc.mediaData = self.mediaData
        vc.completion = { (isCompleted, type) in
            if isCompleted, type == .cancel{
                self.btnPlayPauseClicked(self.btnPlayPause)
            } else if self.isFrom == .downloads, type == .remove {
                self.btnBackClicked(UIButton())
            }
        }
        vc.modalPresentationStyle = .overFullScreen
        
        vc.isFrom = self.isFrom//.videoPlayer
        self.openBottomSheetAnimate(vc: vc, sizes: [.intrinsic])//navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnFavouriteClicked(_ sender: UIButton) {
        
        
        if self.btnFavourite.isSelected {
            UserDefaultsConfig.isFromVideoPlayer = true
            Alert.shared.showAlert(message: AppMessages.removeFromFavourite, actionTitles: [AppMessages.no, AppMessages.yes], actions: [{ (no) in
                UserDefaultsConfig.isFromVideoPlayer = false
            }, { [weak self] (yes) in
                guard let _ = self else { return }
                UserDefaultsConfig.isFromVideoPlayer = false
                if let data = self?.mediaData{
                    AppDelegate.shared.addAnalyticsEvent(screenName: .ContentDetail, eventName: .Removefavorite, category: .Content, action: "Remove Favorite", label: "\(self?.contentTitle ?? "") : \(data.title ?? "")")
                    GlobalAPI.shared.markAsFav(contentData: data)
                    self?.mediaData?.favourite.toggle()
                }
                self?.btnFavourite.isSelected.toggle()
            }])
            return
        }
        else{
            if let data = self.mediaData{
                AppDelegate.shared.addAnalyticsEvent(screenName: .ContentDetail, eventName: .Addfavorite, category: .Content, action: "Add Favorite", label: "\(self.contentTitle ?? "") : \(data.title ?? "")")
                GlobalAPI.shared.markAsFav(contentData: data)
                self.mediaData?.favourite.toggle()
            }
            self.btnFavourite.isSelected.toggle()
        }
        
        /*if self.btnFavourite.isSelected {
            Alert.shared.showAlert(message: AppMessages.removeFromFavourite, actionTitles: [AppMessages.no, AppMessages.yes], actions: [{ (no) in }, { [weak self] (yes) in
                guard let _ = self else { return }
                self?.btnFavourite.isSelected.toggle()
            }])
            return
        }
        self.btnFavourite.isSelected.toggle()*/
    }
    
    @IBAction func btnMoreTopClicked(_ sender: UIButton) {
    }
    //------------------------------------------------------
    
}


//MARK: - RangeSeek slider delegate -

extension VideoPlayerVC: RangeSeekSliderDelegate {
    func didStartTouches(in slider: RangeSeekSlider) {
//        self.player?.pause()
        self.btnPlayPauseClicked(UIButton())
        self.sliderTimer.invalidate()
    }
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
//        guard let duration = player?.currentItem?.duration else { return }
//        let currentTime = CMTimeGetSeconds((player?.currentTime())!)
//        let newTime = currentTime + 10.0
//        if newTime < (CMTimeGetSeconds(duration) - 10.0){
            let time: CMTime = CMTimeMake(value: Int64(maxValue * 1000), timescale: 1000)
            player?.seek(to: time)
        slider.selectedMaxValue = maxValue
//        } else {
//            guard let duration = player?.currentItem?.duration else { return }
//            player?.seek(to: duration)
//        }
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
//        self.player?.play()
        self.btnPlayPauseClicked(UIButton())
        self.startTimer()
    }
}

extension CMTime {
    var durationText:String {
        let totalSeconds = Int(CMTimeGetSeconds(self))
        let hours:Int = Int(totalSeconds / 3600)
        let minutes:Int = Int(totalSeconds % 3600 / 60)
        let seconds:Int = Int((totalSeconds % 3600) % 60)

        if hours > 0 {
            return String(format: "%i:%02i:%02i", hours, minutes, seconds)
        } else {
            return String(format: "%02i:%02i", minutes, seconds)
        }
    }
}
