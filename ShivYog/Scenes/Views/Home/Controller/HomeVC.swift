//
//  HomeVC.swift
//  ShivYog
//
//  Created by 2022M30 on 28/11/22.
//

import UIKit

class HomeVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var lblGreetings: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnNotification: UIButton!
    @IBOutlet weak var vwSearch: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var imgSearch: UIImageView!
    @IBOutlet weak var colBanner: UICollectionView!
    @IBOutlet weak var tblList: SelfSizedTableView!
    @IBOutlet weak var vwPageControl: ZPageControl!
    @IBOutlet weak var constColHeight: NSLayoutConstraint!
    @IBOutlet var vwScroll: UIScrollView!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var homeVM : HomeViewModel = HomeViewModel()
    var centeredCollectionView : CenteredCollectionViewFlowLayout!
    var timer: Timer!
    var greeting = ""
    var isFrom : IsFrom = .home
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
//        self.autoScrollBanner()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
//        self.greetingLogic()
        AppDelegate.shared.addAnalyticsEvent(screenName: .Home, eventName: .Pageview, category: .Home)
        self.lblUserName.text = UserModel.currentUser?.fullname ?? ""
        debugPrint("Test")
        if self.homeVM.getBannerCount() > 0{
            self.autoScrollBanner()
        }
        self.homeVM.getContinueWatchingContent()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if ((self.tabBarController as? TabVC)?.miniPlayer.isHidden ?? true){
            self.tblList.addFooterView(bottomSpace: 0)
        }
        else{
            self.tblList.addFooterView(bottomSpace: kMiniPlayerConstant)
        }
        self.tblList.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        if let timer = timer{
            timer.invalidate()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.callAPIs(true)
        self.lblGreetings.font(name: FontFamily.SFProDisplay.medium, size: 22.0).textColor(color: .app_222222)
        self.lblUserName.font(name: FontFamily.CircularStd.medium, size: 16.0).textColor(color: .app_222222)
        self.lblGreetings.text = "Namah Shivay"
        self.txtSearch.font(name: FontFamily.CircularStd.book, size: 12.0).textColor(color: .app_222222)
        self.txtSearch.attributedPlaceholder = NSAttributedString(string: "Search tips & trick yoga", attributes: [.font: FontFamily.CircularStd.book.font(size: 12.0)!, .foregroundColor: UIColor.app_222222.withAlphaComponent(0.2)])
        self.txtSearch.addTarget(self, action: #selector(self.txtDidChange(_:)), for: .editingChanged)
        self.txtSearch.delegate = self
        
        self.vwPageControl.currentPage = 0
        self.vwPageControl.numberOfPages = 0
        self.vwPageControl.currentDotSize = CGSize(width: 25.0, height: 8.0)
        self.vwPageControl.dotSize = CGSize(width: 8.0, height: 8.0)
        self.vwPageControl.currentDotImage = .selectedDot.image
        self.vwPageControl.dotImage = .unSelectedDot.image
        
        DispatchQueue.main.async {
            self.vwSearch.cornerRadius(cornerRadius: self.vwSearch.frame.height / 2)
        }
        
        self.constColHeight.constant = (ScreenSize.width - 36) * 0.52
        
        self.centeredCollectionView = self.colBanner.collectionViewLayout as? CenteredCollectionViewFlowLayout
        self.colBanner.decelerationRate = .fast
        self.colBanner.registerNib(forCellWithClass: HomeBannerCell.self)
        self.colBanner.delegate = self
        self.colBanner.dataSource = self
        
        let width = (ScreenSize.width - 24) / 1.035
        let height = width * 0.51
        self.centeredCollectionView.itemSize = CGSize(width: width, height: height)
        self.centeredCollectionView.minimumLineSpacing = 6.0
        self.tblList.registerNib(forCellWithClass: CategoriesCell.self)
        self.tblList.registerNib(forCellWithClass: YogaStoreCell.self)
        self.tblList.delegate = self
        self.tblList.dataSource = self
        
        self.vwScroll.addRefreshControl {
            self.callAPIs()
        }
    }

    @objc func txtDidChange(_ textField: UITextField) {
        self.imgSearch.isHighlighted = textField.isFirstResponder
    }
    
    private func autoScrollBanner() {
        if self.vwPageControl.numberOfPages > 1{
            self.timer = Timer.scheduledTimer(timeInterval: 8.0, target: self, selector: #selector(self.startTimer), userInfo: nil, repeats: true)
        }
    }
    
    @objc func startTimer() {
        if var indexPath = self.colBanner.getVisibleCellIndexPath(){
            if indexPath.row == self.homeVM.getBannerCount() - 1 {
                indexPath.row = 0
                self.colBanner.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            } else {
                indexPath.row += 1
                self.colBanner.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            }
//            self.vwPageControl.currentPage = indexPath.row
        }
    }
    
    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        self.homeVM.isBannerFetchComplete.bind(observer: { [unowned self] (result) in
            if result ?? false{
                self.vwPageControl.currentPage = 0
                self.vwPageControl.numberOfPages = self.homeVM.getBannerCount()
                self.colBanner.reloadData()
                if let timer = timer{
                    timer.invalidate()
                }
                if self.vwPageControl.numberOfPages > 0{
                    self.colBanner.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
                    self.autoScrollBanner()
                }
            }
         })
        
        self.homeVM.isHomeFetchComplete.bind(observer: { [unowned self] (result) in
            self.vwScroll.endRefreshing()
            if result ?? false{
                self.tblList.reloadData()
            }
         })
    }
    
    func callAPIs(_ withLoader : Bool = false){
        self.homeVM.getBanners()
        self.homeVM.getHomeData(withLoader: withLoader)
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    
    @IBAction func btnFilterClicked(_ sender: UIButton) {
        let vc = StoryboardScene.Profile.preferencesVC.instantiate()
        vc.completion = { isSuccess in
            if isSuccess{
                self.callAPIs()
                self.vwScroll.scrollToTop()
                self.vwScroll.startRefreshing()
            }
        }
        vc.isFrom = self.isFrom
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnNotificationsClicked(_ sender: UIButton) {
        let vc = StoryboardScene.Home.notificationsVC.instantiate()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //------------------------------------------------------
    
}


//MARK: - UITextfield delegate -

extension HomeVC: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.imgSearch.isHighlighted = false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let vc = StoryboardScene.Home.searchVC.instantiate()
        vc.isFrom = .home
        vc.modalPresentationStyle = .overFullScreen
        vc.hidesBottomBarWhenPushed = true
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .overFullScreen
        nav.awakeFromNib()
        self.navigationController?.present(nav, animated: true, completion: nil)
        return false
    }
}

//MARK: - UICollectionView delegate and datasource -

extension HomeVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.homeVM.getBannerCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: HomeBannerCell.self, for: indexPath)
        let dataAtIndex = self.homeVM.getBannerAtIndex(index: indexPath.row)
        cell.data = dataAtIndex
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dataAtIndex = self.homeVM.getBannerAtIndex(index: indexPath.row)
        var strLink = dataAtIndex.link ?? ""
        if !strLink.localizedStandardContains("http"){
            strLink = "http://" + strLink
        }
        if let url = URL(string: strLink){
            UIApplication.shared.open(url) { isSuccess in
                AppDelegate.shared.addAnalyticsEvent(screenName: .Home, eventName: .ViewBanner, category: .Home,action: "Banner View",label: dataAtIndex.title)
                debugPrint(isSuccess)
            }
//            UIApplication.shared.open(url)
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        if let indexPath = self.colBanner.getVisibleCellIndexPath(){
            self.vwPageControl.currentPage = indexPath.row
        }
    }
}

//MARK: - UItableview delegate and Datasource -

extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.homeVM.arrFeatured.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withClass: CategoriesCell.self, for: indexPath)
        let dataAtIndex = self.homeVM.arrFeatured[indexPath.row]
        cell.shouldShowRemove = dataAtIndex.title == HomePageTitle.ContinueWatching.rawValue
        cell.data = dataAtIndex
        
        cell.completion = { str, arr in
            let vc = StoryboardScene.Home.productItemsVC.instantiate()
            vc.contentId = arr.id
            vc.isFrom = .home
            if dataAtIndex.title == HomePageTitle.FeaturedPlaylists.rawValue{
                vc.arrFeaturedData = arr.mediaContent
                vc.isFrom = .featuredPlaylists
            }
            else{
                vc.selCat = arr
            }
            vc.screenTitle = str
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        cell.openMediaScreensCompletion = { type, content in
            debugPrint(type)
            if !UserModel.canOpenMediaWithoutSubcription(content: content){
                return
            }
            if dataAtIndex.title == HomePageTitle.ContinueWatching.rawValue {
                AppDelegate.shared.addAnalyticsEvent(screenName: .Home, eventName: .ResumeContent, category: .Home, action: "Resume Content")
            }
            if type == .audio {
                let vc = StoryboardScene.Home.audioPlayerVC.instantiate()
                if GAudioPlayer.shared.isPlaying, GAudioPlayer.shared.mediaData?.id == content.id{
                    vc.isFrom = .miniPlayer
                }
                else{
                    vc.isFrom = .home
                }
                vc.mediaData = content
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            } else if type == .video {
                let vc = StoryboardScene.Home.videoPlayerVC.instantiate()
                vc.isFrom = .home
                vc.hidesBottomBarWhenPushed = true
                vc.mediaData = content
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else if type == .pdf {
                let vc = StoryboardScene.Home.pdfViewerVC.instantiate()
                vc.hidesBottomBarWhenPushed = true
                vc.mediaData = content
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        cell.btnSeeAll.addTapGestureRecognizer {
            
            if dataAtIndex.title ==  HomePageTitle.FeaturedContent.rawValue{
                let vc = StoryboardScene.Home.productListingVC.instantiate()
//                vc.arrSubData = dataAtIndex.categoryContent
                vc.screenTitle = dataAtIndex.title
                vc.isFrom = .featuredContent
                self.navigationController?.pushViewController(vc, animated: true)
            } else if dataAtIndex.title ==  HomePageTitle.WhatsNew.rawValue {
                let vc = StoryboardScene.Home.productListingVC.instantiate()
                vc.screenTitle = dataAtIndex.title
//                vc.arrSubData = dataAtIndex.categoryContent
                vc.isFrom = .whatsNew
                self.navigationController?.pushViewController(vc, animated: true)
            } else if dataAtIndex.title ==  HomePageTitle.FeaturedPlaylists.rawValue {
                let vc = StoryboardScene.Home.productListingVC.instantiate()
                vc.screenTitle = dataAtIndex.title
//                vc.arrSubData = dataAtIndex.categoryContent
                vc.isFrom = .featuredPlaylists
                self.navigationController?.pushViewController(vc, animated: true)
            } else if dataAtIndex.title ==  HomePageTitle.ContinueWatching.rawValue {
                let vc = StoryboardScene.Home.productItemsVC.instantiate()
                vc.screenTitle = dataAtIndex.title
                vc.isFrom = .continueWatching
                vc.arrFeaturedData = dataAtIndex.categoryContent
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
        cell.completionRemove = { removedData in
            AppDelegate.shared.addAnalyticsEvent(screenName: .Home, eventName: .RemovePausedContent, category: .Home, action: "Remove Paused Content", label: removedData.title)
            GlobalAPI.shared.removeContinueWatching(contentData: removedData, watchedDuration: 0, interval: 0)
            self.callAPIs()
        }
        
        return cell
    }
}
