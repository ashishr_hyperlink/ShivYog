//
//  TabVC.swift
//  ShivYog
//
//  Created by 2022M30 on 28/11/22.
//

import Foundation
import UIKit

var delegateTabVC : TabVC?

class TabVC : UITabBarController {
    
    @IBOutlet weak var vwBottom: UIView!
    
    
    //MARK: - Class Variable -
    var miniPlayer = MiniPlayerView()
    var isPlaying = Bool()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegateTabVC = self
        self.vwBottom.frame = CGRect(x: 0.0, y: -15.0, width: ScreenSize.width, height: self.tabBar.frame.height + 10.0)
        DispatchQueue.main.async {
            
            self.vwBottom.roundCorners([.topLeft,.topRight], radius: 10.0)
        }
        self.vwBottom.shadow(color: .app_000000.withAlphaComponent(0.16), shadowOffset: CGSize(width: 0.0, height: 0.0), shadowOpacity: 1.0)
        self.vwBottom.shadowRadius = 74.0
        //        self.vwBottom.backGroundColor(color: .app_990000)
        self.tabBar.addSubview(self.vwBottom)
       
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//            self.addview()
//    }
//
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//        self.addview()
//    }
    //    override func viewDidLayoutSubviews() {
    //        super.viewDidLayoutSubviews()
    //        self.addview()
    //    }
    
    func addview(frame: Bool = true) {
        //        self.vwMiniPlayer.removeFromSuperview()
        self.miniPlayer.removeFromSuperview()
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
        
        self.miniPlayer = MiniPlayerView.instanceFromNib() as! MiniPlayerView
        UserDefaults.standard.set(true, forKey: UserDefaults.Keys.miniplayer)
//        let y = self.tabBar.isHidden ? ScreenSize.height - kMiniPlayerConstant : ScreenSize.height - (kMiniPlayerConstant + self.tabBar.frame.height + 5.0/*self.bottomBarHeight*/)
//        self.miniPlayer.frame = CGRect(x: 0, y: /*ScreenSize.height - (kMiniPlayerConstant + self.bottomBarHeight)*/ y, width: ScreenSize.width, height: kMiniPlayerConstant)
        
        //            if self.tabBar.isHidden == false {
        //                self.miniPlayer.frame  = CGRect(x: 0, y: ScreenSize.height - (kMiniPlayerConstant + self.bottomBarHeight) - 85, width: ScreenSize.width, height: 85)
        //                print(self.miniPlayer.frame)
        //            } else{
        //                self.miniPlayer.frame = CGRect(x: 0, y: ScreenSize.height - 100, width: ScreenSize.width, height: 85)
        //            }
        //            if frame {
        //                self.miniPlayer.frame  = CGRect(x: 0, y: ScreenSize.height - (self.tabBar.frame.height) - 85, width: ScreenSize.width, height: 85)
        //
        //            }
//        self.miniPlayer.addTapGestureRecognizer {
//            let vc = StoryboardScene.Home.audioPlayerVC.instantiate()
//            if (UIApplication.topViewController() is HomeVC) ||
//                (UIApplication.topViewController() is CategoriesVC) ||
//                (UIApplication.topViewController() is MySpaceVC) ||
//                (UIApplication.topViewController() is MyStoreVC) ||
//                (UIApplication.topViewController() is ProfileVC) {
//                vc.hidesBottomBarWhenPushed = true
//
//            }
//            UIApplication.push(viewController: vc)
//        }
                    /*if (UIApplication.topViewController() is HomeVC) ||
                        (UIApplication.topViewController() is CategoriesVC) ||
                        (UIApplication.topViewController() is MySpaceVC) ||
                        (UIApplication.topViewController() is MyStoreVC) ||
////                        (UIApplication.topViewController() is ProfileVC) !(self.tabBar.isHidden) {*/
        self.miniPlayer.frame = CGRect(x: 0, y: ScreenSize.height - (kMiniPlayerConstant + self.tabBar.frame.height + 10.0), width: ScreenSize.width, height: kMiniPlayerConstant)//kMiniPlayerConstant)
        
//                    } else {
//                        self.miniPlayer.frame = CGRect(x: 0, y: ScreenSize.height - (kMiniPlayerConstant + self.safebottomBarHeight + 5.0) , width: 0, height: 0)
//                    }
        self.miniPlayer.isHidden = frame
        self.view.addSubview(self.miniPlayer)
        self.view.bringSubviewToFront(self.miniPlayer)
        //            self.miniPlayer.backgroundColor = .clear
        
//        self.view.layoutIfNeeded()
//        if !frame {
//            self.view.bringSubviewToFront(self.miniPlayer)
//        }
        //            self.norecordView.vwMiniPlayer.isHidden = true
        //        }
    }
}
