//
//  ProductItemsVC.swift
//  ShivYog
//
//  Created by 2022M30 on 24/01/23.
//

import UIKit
import DZNEmptyDataSet

class ProductItemsVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var colList : UICollectionView!
    @IBOutlet weak var btnSearch: UIButton!
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var viewModel : CategoryViewModel = CategoryViewModel()
    private var mySpaveVM : MySpaceViewModel = MySpaceViewModel()
    var screenTitle : String?
    var isFrom : IsFrom = .signIn
    lazy var listLayout: UICollectionViewFlowLayout = {
        
        let collectionFlowLayout = UICollectionViewFlowLayout()
        collectionFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        collectionFlowLayout.itemSize = CGSize(width: ScreenSize.width, height: ScreenSize.width * 0.39)
        collectionFlowLayout.minimumInteritemSpacing = 0
        collectionFlowLayout.minimumLineSpacing = 0
        collectionFlowLayout.scrollDirection = .vertical
        return collectionFlowLayout
    }()
    var selCat : CategoryContentModel?
    var arrFeaturedData : [CategoryContentModel] = []
    var isRemoveMediaFromPlaylist : ((CategoryContentModel) -> Void)?
    var downloadMainModel : DownloadMainModel?
    var contentId : Int = -1
    var arrDownloadData : [DownloadModel] = []
    var downloadTimer : Timer!
    var currentPage = 1
    var isCallingApi = true
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        if self.isFrom == .continueWatching {
            AppDelegate.shared.addAnalyticsEvent(screenName: .ContinueWatching, eventName: .Pageview, category: .ContinueWatching)
        } else if self.isFrom == .downloads {
            AppDelegate.shared.addAnalyticsEvent(screenName: .Downloads, eventName: .Pageview, category: .Downloads)
            self.fetchData(true)
        } else if self.isFrom == .favourites {
            AppDelegate.shared.addAnalyticsEvent(screenName: .Favorites, eventName: .Pageview, category: .Favorites)
        } else if self.isFrom == .featuredPlaylists {
            AppDelegate.shared.addAnalyticsEvent(screenName: .PlaylistDetail, eventName: .Pageview, category: .FeaturedPlaylists, label: self.screenTitle ?? "")
        } else if self.isFrom == .categories {
            AppDelegate.shared.addAnalyticsEvent(screenName: .ContentDetail, eventName: .Pageview, category: .Content, action: "Detail view", label: self.screenTitle ?? "")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.colList.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        
        if let timer = self.downloadTimer {
            timer.invalidate()
        }
    }
    
    
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
        private func setUpView() {
        self.setupViewModelObserver()
        self.applyStyle()
    }

    private func applyStyle(){
        self.title = self.screenTitle ?? ""
        self.colList.registerNib(forCellWithClass: ProductItemCell.self)
        self.colList.delegate = self
        self.colList.dataSource = self
        self.colList.setCollectionViewLayout(self.listLayout, animated: true)
        self.btnSearch.isHidden = (self.isFrom == .yogaStore || self.isFrom == .featuredPlaylists || self.isFrom == .playlists || self.isFrom == .mySpace)
        
        
        
        self.colList.addRefreshControl {
            self.currentPage = 1
            self.isCallingApi = true
            
//            if self.isFrom == .playlists{
//                if let data = self.selCat{
//                    self.getPlaylistContent(playListID: data.id)
//                }
//            }
//            else{
                self.fetchData()
//            }
        }
        self.fetchData(true)
        
    }
    
    func getPlaylistContent(withLoader isLoader : Bool = false, playListID : Int){
        var dictData = Dictionary<String,String>()
        dictData["id"] = playListID.description
        dictData["page"] = self.currentPage.description
        ApiManager.shared.makeRequest(endPoint: .playlist(.playlist), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            self.colList.endRefreshing()
            switch result{
                
            case .success(let apiData):
                if self.currentPage == 1 {
                    self.arrFeaturedData.removeAll()
                    self.arrFeaturedData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue.first?["playlists_details"].arrayValue ?? [])
                } else {
                    self.arrFeaturedData.append(CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue.first?["playlists_details"].arrayValue ?? []))
                }
                self.isCallingApi = true
                self.currentPage += 1
                self.colList.reloadData()
                break
            case .failure(let error):
//                if self.currentPage != 1 {
//                    self.arrFeaturedData = []
//                }
                self.isCallingApi = false
            
                self.colList.reloadData()
                print("the error \(error)")
            }
        }
    }
    
    func fetchData(_ withLoader : Bool = false){
        if self.isFrom == .downloads {
            self.downloadMainModel = UserDefaults.standard.decode(for: DownloadMainModel.self, using: UserDefaults.Keys.downloadContent)
            if let data = downloadMainModel {
                self.arrDownloadData = data.data
                self.arrFeaturedData = self.arrDownloadData.map({ CategoryContentModel(fromJson: $0.fileContent) })
                
                if let timer = self.downloadTimer {
                    timer.invalidate()
                }
            }
        }
        
        if let selectedSubCat = self.selCat, !(self.isFrom == .playlists || self.isFrom == .featuredPlaylists || self.isFrom == .mySpace){
            self.viewModel.getContentData(catID: selectedSubCat.id, isFrom: self.isFrom, withLoader: withLoader, page: self.currentPage)
        }
        else if self.arrFeaturedData.count > 0{
            self.viewModel.setContentData(data: self.arrFeaturedData)
        }
        else{
            self.viewModel.getContentData(catID: 0, isFrom: self.isFrom, withLoader: false, page: self.currentPage)
        }
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
        
        self.viewModel.isProductFetchComplete.bind(observer: { [unowned self] (result) in
            self.colList.endRefreshing()
            DispatchQueue.main.async {
                self.checkDownloadPendigStatus()
            }
            
            if result ?? false{
                self.isCallingApi = true
                self.currentPage += 1
                self.colList.reloadData()
            } else {
                self.isCallingApi = false
                self.colList.reloadData()
            }
            self.colList.emptyDataSetSource = self
            self.colList.emptyDataSetDelegate = self
        })
    }
    
    private func checkDownloadPendigStatus() {
        self.downloadTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.startTimer), userInfo: nil, repeats: true)
    }
    
    @objc func startTimer() {
        let filterData = self.arrDownloadData.filter { !$0.isDownloaded }
        if filterData.count == 0 {
            self.downloadTimer.invalidate()
        } else {
            self.fetchData()

        }
    }
    
    private func updateProgressBar() {
        for (idx,item) in self.arrDownloadData.enumerated() {
            if !item.isDownloaded {
                self.colList.reloadItems(at: [[0,idx]])
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method -
    
    @IBAction func btnSearchClicked(_ sender : UIButton) {
        let vc = StoryboardScene.Home.searchVC.instantiate()
        vc.isFrom = self.isFrom
        vc.subcategorycontentId = self.contentId
        vc.modalPresentationStyle = .overFullScreen
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
}

//MARK: - UICollectionview Delegate and Datasource -

extension ProductItemsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.getMediaCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: ProductItemCell.self, for: indexPath)
        let data = self.viewModel.getMediaAtIndex(index: indexPath.row)
        cell.isFrom = self.isFrom
        cell.data = data

        cell.vwProgressContainer.isHidden = true
        if let _ = self.downloadMainModel {
            let downloadData = self.arrDownloadData[indexPath.row]
            cell.vwProgressContainer.isHidden = self.isFrom == .downloads && downloadData.isDownloaded
            if self.isFrom == .downloads {
                
                let filter = DownloadManager.shared.arrTaskDataForDownload.filter { $0.contentModel.id == data.id }
                var totalPercentage : Float = 0.0
                if filter.count > 0 {
                    filter.forEach {
                        let received =  $0.downloadTask.countOfBytesReceived
                        let pending = $0.downloadTask.countOfBytesExpectedToReceive
                        let percentage = pending > 0 ? Float(received) / Float(pending) : 0.0

                        totalPercentage =  totalPercentage + ((percentage == 1) ? 0.0 : percentage)
                    }
                }
                totalPercentage = totalPercentage * 100.0
                cell.progressRing.endPointValue = CGFloat(totalPercentage)
                cell.lblPercentages.text = Int(totalPercentage).description + " %"
                cell.vwProgressContainer.isHidden = (filter.count > 0) ? (totalPercentage == 100.0) : true
            }
        }
        
        cell.btnMore.addTapGestureRecognizer {
            if !UserModel.canOpenMediaWithoutSubcription(content: data){
                return
            }
            
            let vc = StoryboardScene.MySpace.playlistOptionsVC.instantiate()
            //                vc.modalPresentationStyle = .overFullScreen
            vc.isFrom = self.isFrom
            vc.mediaData = data
            vc.completion = { (isSuccess, type) in
                if self.isFrom == .favourites, type == .remove{
                    self.viewModel.deleteItemAtIndex(index: indexPath.row)
                    self.colList.reloadData()
                }
                else if self.isFrom == .playlists, type == .remove, let playlist = self.selCat{
                    let isStoreContent = playlist.content == "store media content"
                    self.mySpaveVM.apiDeleteMediaToPlaylist(playlistId: playlist.id.description, mediaId: data.id.description, isStoreMedia: isStoreContent)
                    self.viewModel.deleteItemAtIndex(index: indexPath.row)
//                    self.isRemoveMediaFromPlaylist?(data)
//                    if self.viewModel.getMediaCount() == 0 {
//                        self.navigationController?.popViewController(animated: true)
//                    } else {
                        self.colList.reloadData()
//                    }
                } else if self.isFrom == .downloads , type == .remove {
                    self.fetchData()
                }
            }
            if let tabBar = self.tabBarController{
                tabBar.openBottomSheetAnimate(vc: vc, sizes: [.intrinsic])
            }
            else{
                vc.hidesBottomBarWhenPushed = true
                self.openBottomSheetAnimate(vc: vc, sizes: [.intrinsic])
            }
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = self.viewModel.getMediaAtIndex(index: indexPath.row)
        if self.isFrom == .yogaStore {
            return
        }
        if !UserModel.canOpenMediaWithoutSubcription(content: data){
            return
        }
        if self.isFrom == .downloads && !self.arrDownloadData[indexPath.row].isDownloaded {
            Alert.shared.showSnackBar("Downloading")
            return
        }
        
        if data.contentType == .audio {
            let vc = StoryboardScene.Home.audioPlayerVC.instantiate()
            vc.isFrom = self.isFrom
            if GAudioPlayer.shared.isPlaying, GAudioPlayer.shared.mediaData?.id == data.id{
                vc.isFrom = .miniPlayer
            }
            if self.isFrom == .favourites{
                vc.completionRemovedFromFav = { isRemoved in
                    if isRemoved{
                        self.viewModel.deleteItemAtIndex(index: indexPath.row)
                    }
                }
            } else if self.isFrom == .downloads {
                vc.completionRemovedFromFav = { isRemoved in
                    self.fetchData()
                }
            }
            vc.mediaData = data
            vc.arrMediaData = self.viewModel.getFilterdData(type: data.contentType)
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)

        } else if data.contentType == .video {
            let vc = StoryboardScene.Home.videoPlayerVC.instantiate()
            vc.isFrom = self.isFrom
            vc.hidesBottomBarWhenPushed = true
            vc.mediaData = data
            vc.arrMediaData = self.viewModel.getFilterdData(type: data.contentType)
            if self.isFrom == .downloads {
                vc.completionRemovedFromFav = { isRemoved in
                    self.fetchData()
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        } else if data.contentType == .pdf {
            DispatchQueue.main.async {
                let vc = StoryboardScene.Home.pdfViewerVC.instantiate()
                vc.mediaData = data
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        var edgeInset = UIEdgeInsets.zero
        if !((self.tabBarController as? TabVC)?.miniPlayer.isHidden ?? true){
            edgeInset.bottom = kMiniPlayerConstant
        }
        return edgeInset
    }
}

extension ProductItemsVC {
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        var emptyDataSetText : String = AppMessages.ErrorEmptyDataSetTitle
        if self.isFrom == .downloads {
            emptyDataSetText = "Currently there are no downloaded audio or videos."
        }

        let attribute = GFunction.shared.getAttributedText(emptyDataSetText, attributeDic: [NSAttributedString.Key.foregroundColor : UIColor.app_222222 , NSAttributedString.Key.font : FontFamily.SFProDisplay.regular.font(size: 14) as Any], range: (emptyDataSetText as NSString).range(of: emptyDataSetText))
        
        return attribute
    }
}


extension ProductItemsVC {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.isAtBottom {
            if  self.isCallingApi {
                self.isCallingApi = false
                if self.isFrom != .downloads {
                    self.fetchData()
                }
            }
        }
        
        
        
        //        let currentOffset : CGFloat = scrollView.contentOffset.y
        //        let maximumOffset : CGFloat = scrollView.contentSize.height - scrollView.frame.size.height + 20
        //
        //        if (maximumOffset - currentOffset <= 1) && self.isCallingApi {
        //            self.isCallingApi = false
        //            if self.isFrom != .downloads {
        //                self.fetchData()
        //            }
        //        }
    }
}
