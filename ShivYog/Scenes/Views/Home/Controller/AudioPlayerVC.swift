//
//  AudioPlayerVC.swift
//  ShivYog
//
//  Created by 2022M30 on 06/12/22.
//

import UIKit
import AVKit
import AVFoundation

class AudioPlayerVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var imgFavourite: UIImageView!
    @IBOutlet weak var imgMore: UIImageView!
    @IBOutlet weak var imgMain: UIImageView!
    @IBOutlet var imgCenter: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnPlayPause: UIButton!
    @IBOutlet weak var vwSlider: RangeSeekSlider!
    @IBOutlet var btnRepeat: [UIButton]!
    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var lblStartTime: UILabel!
    @IBOutlet weak var lblEndTime: UILabel!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
//    private var viewModel : AuthViewModel = AuthViewModel()
//    var player = AVAudioPlayer()
//    var arrUrl = [String]()
//    var videoURL: URL! = nil
//    var index = Int()
//    var isVideoPlaying = true
    var sliderTimer : Timer!
    var isFrom : IsFrom = .home
//    var audioDuration : Double!
//    var currentTime : Double!
    var timerMaxValue : CGFloat = 0.0
    var mediaData : CategoryContentModel?{
        didSet{
            GAudioPlayer.shared.mediaData = mediaData
        }
    }
    var isRepeat: Bool = false
    var isRepeatAll: Bool = false
    var completionRemovedFromFav : ((Bool) -> Void)?
    var contentTitle : String?
    var arrMediaData : [CategoryContentModel]?
    var currentMediaIndex : Int = -1
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if GAudioPlayer.shared.isPlaying && self.isFrom != .miniPlayer {
            GAudioPlayer.shared.managedState(url: self.mediaData?.file,state: .off, speed: "1", showPlayer: !self.btnPlayPause.isSelected)
        }
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        if self.isFrom == .miniPlayer {
//            self.startTimer()
            (UIApplication.topViewController()?.parent?.parent as? TabVC)?.miniPlayer.isHidden = true
//            if GAudioPlayer.shared.isPlaying {
                self.setSliderValues()
//            }
            
        } else if GAudioPlayer.shared.player == nil {
            AppLoader.shared.addLoader()
            self.setPlayer()
            
        }/* else if GAudioPlayer.shared.isPlaying {
            AppLoader.shared.addLoader()
            GAudioPlayer.shared.managedState(url: self.mediaData?.file,state: .off, speed: "1", showPlayer: !self.btnPlayPause.isSelected)
            self.setPlayer()
            
        } else {
            GAudioPlayer.shared.managedState(url: self.mediaData?.file,state: .play, speed: "1", showPlayer: !self.btnPlayPause.isSelected)
        }*/
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if GAudioPlayer.shared.currentStateOfPlayer == .play {
            GAudioPlayer.shared.managedState(url: self.mediaData?.file,state: .off , speed: "1")
        }
        self.completionRemovedFromFav?(!(self.mediaData?.favourite ?? true))
//        self.sliderTimer.invalidate()

    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.lblTitle.font(name: FontFamily.SFProDisplay.medium, size: 15.0).textColor(color: .app_222222)
        self.lblStartTime.font(name: FontFamily.SFProDisplay.regular, size: 12.0).textColor(color: .app_4F4F4F)
        self.lblEndTime.font(name: FontFamily.SFProDisplay.regular, size: 12.0).textColor(color: .app_4F4F4F)
        
        self.vwSlider.delegate = self
        self.vwSlider.minValue = 0.0
        self.vwSlider.selectedMinValue = 0.0
        self.vwSlider.selectedMaxValue = 0.0
        self.lblStartTime.text = "00:00"
        self.lblEndTime.text = "00:00"
        self.vwSlider.tintColor = .appE1Ddc6
        self.vwSlider.colorBetweenHandles = .app_990000
        self.vwSlider.hideLabels = true
        
//        self.btnRepeat[0].isHidden = true
//        self.btnRepeat[1].isHidden = true
//        self.btnRepeat[2].isHidden = false
//        self.btnRepeat[2].isSelected = GAudioPlayer.shared.isRepeat
        self.setRepeatButtons()
        self.getCurrentIndexOfItem()
        DispatchQueue.main.async {
            self.vwContainer.roundCorners([.topLeft, .topRight], radius: 6.0)
        }
        self.setTapGestures()
        self.startTimer()
        self.setPlayerData()
        
    }
    
    func setPlayerData() {
        if let data = self.mediaData{
            self.lblTitle.text = data.title
            self.imgMain.setImage(with: data.image)
            self.imgCenter.setImage(with: data.image)
            self.imgFavourite.isHighlighted = data.favourite
            if let detail = data.subcategorycontentDetails {
                self.contentTitle = detail.title
            }
            if let detail = data.storesubcontentDetails {
                self.contentTitle = detail.title
            }
        }
    }
    
    private func startTimer() {
        GAudioPlayer.shared.updateAudioPlayerCompletion = {
            self.setSliderValues()
        }
    }

    
    private func getCurrentIndexOfItem() {
        if let data = self.arrMediaData {
            for (idx, item) in data.enumerated() {
                if let data = mediaData, data.id == item.id {
                    self.currentMediaIndex = idx
                    GAudioPlayer.shared.globalCurrentMediaIndex = idx
                    GAudioPlayer.shared.globalArrMediaData = self.arrMediaData
                    break
                }
            }
        }
    }
    
    private func setRepeatButtons() {
        self.isRepeat = GAudioPlayer.shared.isRepeat
        self.isRepeatAll = GAudioPlayer.shared.isRepeatAll
        
        self.btnRepeat[1].isHidden = !self.isRepeatAll
        self.btnRepeat[0].isHidden = !self.isRepeat
        self.btnRepeat[2].isHidden = !(!self.isRepeat && !self.isRepeatAll)
    }
    
    private func setSliderValues() {
        self.btnPlayPause.isSelected = GAudioPlayer.shared.isPlaying
        self.vwSlider.maxValue = GAudioPlayer.shared.audioDuration
        self.lblEndTime.text = Int(GAudioPlayer.shared.audioDuration).secondsToMinutesSeconds
        self.vwSlider.selectedMaxValue = GAudioPlayer.shared.currentTime
        self.lblStartTime.text = Int(GAudioPlayer.shared.currentTime).secondsToMinutesSeconds
        
        if GAudioPlayer.shared.player == nil{
            self.vwSlider.isUserInteractionEnabled = false
            self.btnPrevious.isUserInteractionEnabled = false
            self.btnNext.isUserInteractionEnabled = false
            self.btnPlayPause.isUserInteractionEnabled = false
        }
        else if GAudioPlayer.shared.player.timeControlStatus == .waitingToPlayAtSpecifiedRate {
            self.vwSlider.isUserInteractionEnabled = false
            self.btnPrevious.isUserInteractionEnabled = false
            self.btnNext.isUserInteractionEnabled = false
            self.btnPlayPause.isUserInteractionEnabled = false
        } else {
            if AppLoader.shared.isLoaderAnimating() {
                AppLoader.shared.removeLoader()
            }
            self.vwSlider.isUserInteractionEnabled = true
            self.btnNext.isUserInteractionEnabled = true
            self.btnPrevious.isUserInteractionEnabled = true
            self.btnPlayPause.isUserInteractionEnabled = true
        }
    }
    
    private func setPlayer() {

        GAudioPlayer.shared.currentVC = self
        GAudioPlayer.shared.managedState(url: self.mediaData?.file,state: .stop , speed: "1")
        if let mediaData = mediaData {
            
            AppDelegate.shared.addAnalyticsEvent(screenName: .ContentDetail, eventName: .Audioview, category: .Content,action: "Audio view",label: "\(self.contentTitle ?? "") : \(mediaData.title ?? "")")
        }
        
//        self.startTimer()

    }
    
    private func setTapGestures() {
        self.imgFavourite.addTapGestureRecognizer {

            if self.imgFavourite.isHighlighted {
                Alert.shared.showAlert(message: AppMessages.removeFromFavourite, actionTitles: [AppMessages.no, AppMessages.yes], actions: [{ (no) in }, { [weak self] (yes) in
                    guard let _ = self else { return }
                    if let data = self?.mediaData{
                        AppDelegate.shared.addAnalyticsEvent(screenName: .ContentDetail, eventName: .Removefavorite, category: .Content, action: "Remove Favorite", label: "\(self?.contentTitle ?? "") : \(data.title ?? "")")
                        GlobalAPI.shared.markAsFav(contentData: data)
                        self?.mediaData?.favourite.toggle()
                    }
                    self?.imgFavourite.isHighlighted.toggle()
                }])
                return
            }
            else{
                if let data = self.mediaData{
                    AppDelegate.shared.addAnalyticsEvent(screenName: .ContentDetail, eventName: .Addfavorite, category: .Content, action: "Add Favorite", label: "\(self.contentTitle ?? "") : \(data.title ?? "")")
                    GlobalAPI.shared.markAsFav(contentData: data)
                    self.mediaData?.favourite.toggle()
                }
                self.imgFavourite.isHighlighted.toggle()
            }
        }
        
        self.imgMore.addTapGestureRecognizer {

//            GAudioPlayer.shared.managedState(url: self.mediaData?.file,state: .play, showPlayer: !self.btnPlayPause.isSelected)
//            self.btnPlayPause.isSelected.toggle()
            let vc = StoryboardScene.MySpace.playlistOptionsVC.instantiate()
            vc.isFrom = self.isFrom
            vc.mediaData = self.mediaData
//            vc.isFromPlayer = true
            vc.completion = { success, type in
                if self.isFrom == .downloads, type == .remove {
                    GAudioPlayer.shared.managedState(url: self.mediaData?.file,state: .off , speed: "1")
                    
                    self.navigationController?.popViewController(animated: true)
                }
            }
            if let tabBar = self.tabBarController{
                tabBar.openBottomSheetAnimate(vc: vc, sizes: [.intrinsic])
            }
            else{
                vc.hidesBottomBarWhenPushed = true
                self.openBottomSheetAnimate(vc: vc, sizes: [.intrinsic])
            }
        }
    }
    
//    private func seekSlider(time: Double) {
//        
//    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })*/
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    
    @IBAction func btnRepeatClicked(_ sender: UIButton) {
        let _ = self.btnRepeat.map {
            $0.isHidden = true
        }
        switch sender.tag {
        case 100:
            self.btnRepeat[1].isHidden = false
//            player.numberOfLoops = 1
            debugPrint("BTN Single repeat Clicked")
            self.isRepeat = false
            self.isRepeatAll = true
            
        case 200:
            self.btnRepeat[2].isHidden = false
//            player.numberOfLoops = 0
            debugPrint("BTN Repeat Playlist cicked")
            self.isRepeat = false
            self.isRepeatAll = false
            
    
        case 300:
            self.btnRepeat[0].isHidden = false
//            player.numberOfLoops = 1
            debugPrint("BTN No repeat clicked")
            self.isRepeat = true
            self.isRepeatAll = false

        default:
            break
        }
        AppDelegate.shared.addAnalyticsEvent(screenName: .VideoAudio, eventName: .RepeatToggle, category: .Content, action: "Repeat Toggle")
        if GAudioPlayer.shared.isPlaying{
//            sender.isSelected.toggle()
            GAudioPlayer.shared.isRepeat = self.isRepeat//sender.isSelected
            GAudioPlayer.shared.isRepeatAll = self.isRepeatAll
        }
        
    }
    
    
    @IBAction func btnNextClicked(_ sender: UIButton) {
//        self.setPlayer()
//        self.sliderTimer.invalidate()
        GAudioPlayer.shared.timer.invalidate()
        GAudioPlayer.shared.currentVC = self
        GAudioPlayer.shared.managedState(url: self.mediaData?.file, state: self.btnPlayPause.isSelected ? .pause : .play, speed: "1")
        if GAudioPlayer.shared.audioDuration <= (GAudioPlayer.shared.currentTime + 10.0) {
            GAudioPlayer.shared.audioSeek(seekToTime: GAudioPlayer.shared.audioDuration)
        } else {
            GAudioPlayer.shared.audioSeek(seekToTime: (GAudioPlayer.shared.currentTime + 10.0))
        }
        
//        GAudioPlayer.shared.audioSeek(seekToTime: (GAudioPlayer.shared.player.currentTime + 10.0))
//        self.startTimer()
        GAudioPlayer.shared.startTimer()
        
    }
    
    @IBAction func btnPreviousClicked(_ sender: UIButton) {
//        self.setPlayer()
//        self.sliderTimer.invalidate()
        GAudioPlayer.shared.timer.invalidate()
        GAudioPlayer.shared.currentVC = self
        GAudioPlayer.shared.managedState(url: self.mediaData?.file, state: self.btnPlayPause.isSelected ? .pause : .play, speed: "1")
        if GAudioPlayer.shared.currentTime - 10.0 <= 0 {
            GAudioPlayer.shared.audioSeek(seekToTime: 0.0)
        } else {
            GAudioPlayer.shared.audioSeek(seekToTime: (GAudioPlayer.shared.currentTime - 10.0))
        }
//        self.startTimer()
        GAudioPlayer.shared.startTimer()
    }
    
    
    @IBAction func btnPlayPauseClicked(_ sender: UIButton) {
//        if isVideoPlaying{
//            player.pause()
//        }else{
//            player.play()
//        }
//        self.btnPlayPause.isSelected = !isVideoPlaying
//        isVideoPlaying = !isVideoPlaying
        GAudioPlayer.shared.currentVC = self
        GAudioPlayer.shared.managedState(url: self.mediaData?.file,state: self.btnPlayPause.isSelected ? .play : (GAudioPlayer.shared.player == nil) ? .stop : .pause, speed: "1", showPlayer: !self.btnPlayPause.isSelected)
        self.btnPlayPause.isSelected.toggle() //= GAudioPlayer.shared.player.isPlaying
//        if !self.sliderTimer.isValid {
            self.startTimer()
//        }
    }
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
//        if player.isPlaying {
//            self.player.stop()
//        }
        self.navigationController?.popViewController(animated: true)
    }
    //------------------------------------------------------
    
}

//MARK: - RangeSeek slider delegate -

extension AudioPlayerVC: RangeSeekSliderDelegate {
    func didStartTouches(in slider: RangeSeekSlider) {
        GAudioPlayer.shared.timer.invalidate()
        GAudioPlayer.shared.managedState(url: self.mediaData?.file,state: .play, speed: "1", showPlayer: !self.btnPlayPause.isSelected)
    }
    
    
    func didEndTouches(in slider: RangeSeekSlider) {
        
//        GAudioPlayer.shared.startTimer()
        GAudioPlayer.shared.managedState(url: self.mediaData?.file,state: .pause, speed: "1", showPlayer: !self.btnPlayPause.isSelected)
        
    }
    
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
//        if GAudioPlayer.shared.timer == nil{
//            return
//        }
//        self.sliderTimer.invalidate()
//        GAudioPlayer.shared.timer.invalidate()
            debugPrint("MinValue :- ", minValue)
            debugPrint("MaxValue :- ", maxValue)
            debugPrint("Slider Maxvalue :- ", slider.maxValue)
            debugPrint("Slider MinValue :- ", slider.minValue)
            GAudioPlayer.shared.currentVC = self
        GAudioPlayer.shared.audioSeek(seekToTime: maxValue)
//        self.startTimer()
//        GAudioPlayer.shared.startTimer()
        
//        player.stop()
//        let changeTime = Double(maxValue)
//        self.player.currentTime = TimeInterval(maxValue)//Double(maxValue)
//        slider.selectedMaxValue = Double(maxValue)
//
//        player.prepareToPlay()
////        player.play(atTime: maxValue)
//        self.lblStartTime.text = Int(maxValue).secondsToMinutesSeconds
//        self.startTimer()
    }
    
    
}


//MARK: - AVAudio player delegate -

extension AudioPlayerVC: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        debugPrint(flag)
        debugPrint("Audio Finished :- ")
        
        if flag {
            debugPrint(player.isPlaying)
            self.btnPlayPause.isSelected.toggle()
//            self.isVideoPlaying.toggle()
        }
    }
}
