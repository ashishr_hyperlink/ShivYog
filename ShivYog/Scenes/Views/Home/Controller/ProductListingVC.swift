//
//  ProductListingVC.swift
//  ShivYog
//
//  Created by 2022M30 on 30/11/22.
//

import UIKit
import DZNEmptyDataSet

class ProductListingVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var colList: UICollectionView!
    
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var homeVM : HomeViewModel = HomeViewModel()
    private var mySpaceVM : MySpaceViewModel = MySpaceViewModel()
    var isFromPlaylist : Bool = false
    var screenTitle : String = ""
    var isFrom: IsFrom = .playlists
    var contentType : ContentType = .normal
//    var arrData : [ProductMainModel]?
    var arrSubData : [CategoryContentModel] = []
    var subId: Int = -1
    var currentPage = 1
    var isCallingApi = true
    var totalCount = 0
    
    lazy var listLayout: UICollectionViewFlowLayout = {
        
        let collectionFlowLayout = UICollectionViewFlowLayout()
        collectionFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        collectionFlowLayout.itemSize = CGSize(width: ScreenSize.width, height: ScreenSize.width * 0.39)
        collectionFlowLayout.minimumInteritemSpacing = 0
        collectionFlowLayout.minimumLineSpacing = 0
        collectionFlowLayout.scrollDirection = .vertical
        return collectionFlowLayout
    }()
    
    lazy var gridLayout: UICollectionViewFlowLayout = {
        
        let collectionFlowLayout = UICollectionViewFlowLayout()
        collectionFlowLayout.sectionInset = UIEdgeInsets(top: 15, left: 20, bottom: 15, right: 20)
        collectionFlowLayout.itemSize = CGSize(width: (ScreenSize.width - 55) / 2 , height: ((ScreenSize.width - 55) / 2) * 1.27)
        collectionFlowLayout.minimumInteritemSpacing = 0
        collectionFlowLayout.minimumLineSpacing = 15
        collectionFlowLayout.scrollDirection = .vertical
        return collectionFlowLayout
    }()
    
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        if self.isFrom == .featuredContent {
            AppDelegate.shared.addAnalyticsEvent(screenName: .FeaturedContent, eventName: .Pageview, category: .FeaturedContent)
        } else if self.isFrom == .whatsNew {
            AppDelegate.shared.addAnalyticsEvent(screenName: .WhatsNew, eventName: .Pageview, category: .WhatsNew)
        } else if self.isFrom == .featuredPlaylists {
            AppDelegate.shared.addAnalyticsEvent(screenName: .FeaturedPlaylists, eventName: .Pageview, category: .FeaturedPlaylists)
        } else if self.isFrom == .mySpace {
            AppDelegate.shared.addAnalyticsEvent(screenName: .MyProducts, eventName: .Pageview, category: .MyProducts)
        } else if self.isFrom == .playlists {
            AppDelegate.shared.addAnalyticsEvent(screenName: .MyPlaylists, eventName: .Pageview, category: .MyPlaylists)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.colList.reloadData()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.setupViewModelObserver()
        self.applyStyle()
    }

    private func applyStyle(){
        self.title = self.screenTitle
        self.colList.registerNib(forCellWithClass: CategoryImageCell.self)
        self.colList.delegate = self
        self.colList.dataSource = self
        self.colList.setCollectionViewLayout(self.gridLayout, animated: true)
        if self.isFrom == .yogaStore {
            self.navigationItem.rightBarButtonItem = nil
        }
        
        self.getData(withLoader: true)
        self.colList.addRefreshControl {
//            self.isCallingApi = true
            self.currentPage = 1
            self.getData(withLoader: true)
        }
    }
    
    func getData(withLoader: Bool){
        if self.isFrom == .featuredContent{
            self.homeVM.getFeaturedContent(withLoader: withLoader, page: self.currentPage)
        }
        else if self.isFrom == .whatsNew{
            self.homeVM.getWhatsNewContent(withLoader: withLoader, page: self.currentPage)
        }
        else if self.isFrom == .featuredPlaylists{
            self.homeVM.getFeaturedPlaylistContent(withLoader: withLoader, page: self.currentPage)
        }
        else if self.isFrom == .playlists{
            self.homeVM.getPlaylistContent(withLoader: withLoader, page: self.currentPage)
        }
        else if self.isFrom == .yogaStore {
            self.homeVM.getStoreSubContent(withLoader: withLoader, storeContentId: self.subId, page: self.currentPage)
        }
        else if self.isFrom == .mySpace {
            self.homeVM.getMyProductContent(withLoader: withLoader, page: self.currentPage)
        }
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        self.homeVM.isAPIFetchComplete.bind(observer: { [unowned self] (result) in
            self.colList.endRefreshing()
            
            if result ?? false {
                self.arrSubData = self.homeVM.arrCategoryData//.append(result)
                self.isCallingApi = true
                self.currentPage += 1
                self.colList.reloadData()
            } else {
                self.isCallingApi = false
                self.colList.reloadData()
            }
            self.colList.emptyDataSetSource = self
            self.colList.emptyDataSetDelegate = self
            
//            if let result = result {
//                self.arrSubData.append(result)
//                self.isCallingApi = true
//                self.currentPage += 1
//                self.colList.reloadData()
//            } else {
//                self.isCallingApi = false
//                self.colList.reloadData()
//            }
////            if self.arrSubData?.count == 0 {
////                self.isCallingApi = false
//////                self.arrSubData?.removeAll()
////                self.colList.reloadData()
////                return
////            }
////            self.arrSubData = result ?? []
//////            if self.isFrom == .playlists, self.arrSubData != nil{
//////                self.arrSubData = self.arrSubData!.filter{!$0.mediaContent.isEmpty}
//////            }
////            self.currentPage += 1
////            self.isCallingApi = true
////            self.colList.reloadData()
            
        })
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    @IBAction func btnSearchClicked(_ sender: UIBarButtonItem) {
        let vc = StoryboardScene.Home.searchVC.instantiate()
        vc.isFrom = self.isFrom
        vc.modalPresentationStyle = .overFullScreen
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
}

//MARK: - UICollectionview Delegate and Datasource -

extension ProductListingVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return /*(self.arrData != nil) ? (self.arrData?.count ?? 0) : */(self.arrSubData.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //        if self.contentType == .folder {
        let cell = collectionView.dequeueReusableCell(withClass: CategoryImageCell.self, for: indexPath)
//        if let arr = self.arrData {
//            let data = arr[indexPath.row]
//            cell.imgProduct.image = data.image
//            cell.imgPlay.isHidden = (data.contentType == .folder || (self.isFrom == .mySpace ? ((indexPath.row % 2 == 0) ? true : false) : false))
//            cell.imgPlay.image = data.contentType == .pdf ? .pdfWhite.image : .playRoundWhite.image
//            cell.btnFreeTag.isHidden = !data.isFree
//            cell.lblTitle.text = data.title
//        }
//        if let arr = self.arrSubData {
            let data = self.arrSubData[indexPath.row]
            if let image = UIImage(named: data.image){
                cell.imgProduct.image = image
            }
            else{
                cell.imgProduct.setImage(with: data.image)
            }
            cell.btnMore.isHidden = !(self.isFrom == .playlists)
            cell.imgPlay.isHidden = (data.contentType == .folder)
            cell.imgPlay.image = data.contentType == .pdf ? .pdfWhite.image : .playRoundWhite.image
            cell.btnFreeTag.isSelected = UserModel.isSubscribeUser
            cell.btnFreeTag.isHidden = data.contentType != .folder ? !data.isSubscribe : true//!data.featuredContent
//            if data.title != ""{
                
                cell.lblTitle.text = data.title
//            } else if data.storecontentDetails.title != ""{
//                cell.lblTitle.text = data.storecontentDetails.title
//            }
//            if self.isFrom == .featuredPlaylists || self.isFrom == .playlists{
//                cell.imgPlay.isHidden = false
//            }
            
            cell.btnMore.addTapGestureRecognizer {
                let vc = StoryboardScene.MySpace.playlistOptionsVC.instantiate()
                //                vc.modalPresentationStyle = .overFullScreen
                vc.isFrom = .deletePlaylist
                vc.mediaData = data
                vc.deletePlaylistCompletion = { (type, media) in
                    if self.isFrom == .playlists, type == .remove  {
                      self.mySpaceVM.apiDeleteToPlaylist(playlistId: media.id.description)
                      self.arrSubData.remove(at: indexPath.row)
                      self.colList.reloadData()
//                      Alert.shared.showSnackBar("Playlist Deleted")
                    } else if type == .editPlaylist {
                        let vc = StoryboardScene.MySpace.createPlaylistVC.instantiate()
                        vc.mediaContent = media
                        vc.isFromEdit = true
                        vc.modalPresentationStyle = .overFullScreen
                        vc.completion = {
                            self.currentPage = 1
                            self.isCallingApi = true
                            self.getData(withLoader: false)
                        }
                        self.navigationController?.present(vc, animated: true, completion: nil)
                    }
                }
                vc.completion = { (isChange, type) in
                    
                }
                if let tabBar = self.tabBarController{
                    tabBar.openBottomSheetAnimate(vc: vc, sizes: [.intrinsic])
                }
                else{
                    vc.hidesBottomBarWhenPushed = true
                    self.openBottomSheetAnimate(vc: vc, sizes: [.intrinsic])
                }
            }
//        }
        return cell
        //        } else {
        //            let cell = collectionView.dequeueReusableCell(withClass: ProductItemCell.self, for: indexPath)
        //            return cell
        //
        //        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if let arr = self.arrData {
//            let data = arr[indexPath.row]
//
//            if self.isFrom == .playlists {
//                let vc = StoryboardScene.Home.productItemsVC.instantiate()
//                vc.screenTitle = data.title
//                vc.isFrom = self.isFrom
//                self.navigationController?.pushViewController(vc, animated: true)
//            }else if self.isFrom == .mySpace {
//                if indexPath.row % 2 == 0 {
//                    let vc = StoryboardScene.Home.productItemsVC.instantiate()
//                    vc.screenTitle = data.title
//                    vc.isFrom = self.isFrom
//                    self.navigationController?.pushViewController(vc, animated: true)
//                } else {
//
//                   if data.contentType == .audio {
//                        let vc = StoryboardScene.Home.audioPlayerVC.instantiate()
//                        vc.isFrom = self.isFrom
//                        vc.hidesBottomBarWhenPushed = true
//                        self.navigationController?.pushViewController(vc, animated: true)
//                    } else if data.contentType == .video {
//                        let vc = StoryboardScene.Home.videoPlayerVC.instantiate()
//                        vc.isFrom = self.isFrom
//                        vc.hidesBottomBarWhenPushed = true
//                        self.navigationController?.pushViewController(vc, animated: true)
//
//                    } else if data.contentType == .pdf {
//                        let vc = StoryboardScene.Home.pdfViewerVC.instantiate()
//                        vc.hidesBottomBarWhenPushed = true
//                        self.navigationController?.pushViewController(vc, animated: true)
//                    }
//                }
//
//
//            }
//            else if data.contentType == .folder {
//                let vc = StoryboardScene.Home.productItemsVC.instantiate()
//                vc.isFrom = .home
//                vc.screenTitle = data.title
//                self.navigationController?.pushViewController(vc, animated: true)
//            } else if data.contentType == .audio {
//                let vc = StoryboardScene.Home.audioPlayerVC.instantiate()
//                vc.isFrom = self.isFrom
//                vc.hidesBottomBarWhenPushed = true
//                self.navigationController?.pushViewController(vc, animated: true)
//            } else if data.contentType == .video {
//                let vc = StoryboardScene.Home.videoPlayerVC.instantiate()
//                vc.isFrom = self.isFrom
//                vc.hidesBottomBarWhenPushed = true
//                self.navigationController?.pushViewController(vc, animated: true)
//
//            } else if data.contentType == .pdf {
//                let vc = StoryboardScene.Home.pdfViewerVC.instantiate()
//                vc.hidesBottomBarWhenPushed = true
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
//        }
        
//        if let arr = self.arrSubData {
            let data = self.arrSubData[indexPath.row]
            if self.isFrom == .yogaStore {
                return
            }
            if !UserModel.canOpenMediaWithoutSubcription(content: data){
                return
            }
            if data.contentType == .audio {
                let vc = StoryboardScene.Home.audioPlayerVC.instantiate()
                vc.isFrom = self.isFrom
                vc.hidesBottomBarWhenPushed = true
                vc.mediaData = data
                self.navigationController?.pushViewController(vc, animated: true)
            } else if data.contentType == .video {
                let vc = StoryboardScene.Home.videoPlayerVC.instantiate()
                vc.isFrom = self.isFrom
                vc.hidesBottomBarWhenPushed = true
                vc.mediaData = data
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else if data.contentType == .pdf {
                let vc = StoryboardScene.Home.pdfViewerVC.instantiate()
                vc.hidesBottomBarWhenPushed = true
                vc.mediaData = data
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if data.contentType == .folder {
                let vc = StoryboardScene.Home.productItemsVC.instantiate()
                vc.screenTitle = data.title
                if self.isFrom == .featuredPlaylists || self.isFrom == .playlists || self.isFrom == .mySpace{
                    vc.arrFeaturedData = data.mediaContent
                    vc.selCat = data
//                    vc.isRemoveMediaFromPlaylist = { deletedContent in
//                        self.arrSubData?[safe: indexPath.row]?.mediaContent = self.arrSubData?[safe: indexPath.row]?.mediaContent.filter{$0.id != deletedContent.id} ?? []
//                        if self.arrSubData?[safe: indexPath.row]?.mediaContent.isEmpty ?? false{
//                            self.mySpaceVM.apiDeleteToPlaylist(playlistId: data.id.description)
//                            self.arrSubData?.remove(at: indexPath.row)
//                        }
//                        self.colList.reloadData()
//                    }
                }
                else{
                    vc.selCat = data
                    vc.contentId = data.id
                }
                vc.isFrom = (self.isFrom == .playlists || self.isFrom == .featuredPlaylists || self.isFrom == .mySpace) ? self.isFrom : .home
                self.navigationController?.pushViewController(vc, animated: true)
            }
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        var edgeInset = UIEdgeInsets(top: 15, left: 20, bottom: 15, right: 20)
        if !((self.tabBarController as? TabVC)?.miniPlayer.isHidden ?? true){
            edgeInset.bottom = 15 + kMiniPlayerConstant
        }
        return edgeInset
    }
}

extension ProductListingVC {
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        
        var emptyDataSetText : String = AppMessages.ErrorEmptyDataSetTitle
        if self.isFrom == .playlists {
            emptyDataSetText = "Currently there are no playlists created."
        }
        
        
        let attribute = GFunction.shared.getAttributedText(emptyDataSetText, attributeDic: [NSAttributedString.Key.foregroundColor : UIColor.app_222222 , NSAttributedString.Key.font : FontFamily.SFProDisplay.regular.font(size: 14) as Any], range: (emptyDataSetText as NSString).range(of: emptyDataSetText))
        
        return attribute
    }
}

extension ProductListingVC {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.isAtBottom {
            if self.isCallingApi {
                self.isCallingApi = false
                print("Temp scroll api called")
                self.getData(withLoader: false)
            }
        }
    }
}

