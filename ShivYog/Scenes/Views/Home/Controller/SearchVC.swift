//
//  SearchVC.swift
//  ShivYog
//
//  Created by 2022M30 on 30/11/22.
//

import UIKit

class SearchVC: UIViewController {
    
    //MARK: - Outlet

    @IBOutlet weak var lblSearch: UILabel!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblSearchResult: UILabel!
    @IBOutlet weak var tblList: SelfSizedTableView!
    @IBOutlet weak var vwSearch: UIView!
    @IBOutlet weak var imgSearch: UIImageView!
    @IBOutlet weak var vwTableView: UIView!
    @IBOutlet weak var vwEmpty: UIView!
    @IBOutlet weak var lblEmptyMsg: UILabel!
    
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var viewModel : HomeViewModel = HomeViewModel()
    private var categoryVM : CategoryViewModel = CategoryViewModel()
    let padding = UIEdgeInsets(top: 0.0, left: 50.0, bottom: 0.0, right: 0.0)
    
    var isFrom : IsFrom = .home
    var subcategorycontentId : Int = -1
    var categoryId : Int = -1
    var tempArrDownloadModel : [DownloadModel] = []
    var arrDownloadModel : [DownloadModel] = []
    var arrSubData : [CategoryContentModel] = []
    var isHideSearch : Bool = false
    var isSearching : Bool = false
    var currentPage = 1
    var isCallingApi = true
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        self.navigationController?.navigationBar.isHidden = true
        self.txtSearch.becomeFirstResponder()
        AppDelegate.shared.addAnalyticsEvent(screenName: .Search, eventName: .Pageview, category: .Search)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        self.txtSearch.resignFirstResponder()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.setupViewModelObserver()
        self.applyStyle()
    }

    private func applyStyle(){
        
        
        self.txtSearch.font(name: FontFamily.CircularStd.book, size: 12.0).textColor(color: .app_222222)
        self.txtSearch.attributedPlaceholder = NSAttributedString(string: "Search tips & trick yoga", attributes: [.font: FontFamily.CircularStd.book.font(size: 12.0)!, .foregroundColor: UIColor.app_222222.withAlphaComponent(0.2)])
        self.txtSearch.addTarget(self, action: #selector(self.txtDidChange(_:)), for: .editingChanged)
        self.txtSearch.delegate = self
        self.lblSearch.font(name: FontFamily.SFProDisplay.bold, size: 24.0).textColor(color: .app_222222)
        self.lblSearchResult.font(name: FontFamily.SFProDisplay.medium, size: 16.0).textColor(color: .app_990000)
        self.lblEmptyMsg.font(name: FontFamily.CircularStd.book, size: 12.0).textColor(color: .app_222222.withAlphaComponent(0.5))
        self.tblList.registerNib(forCellWithClass: SearchCell.self)
        self.tblList.delegate = self
        self.tblList.dataSource = self
        self.tblList.addFooterView(bottomSpace: 40.0)
        
        DispatchQueue.main.async {
            self.vwSearch.cornerRadius(cornerRadius: self.vwSearch.frame.height / 2)
        }
        self.setUpEmptyView()
        
        if self.isFrom == .downloads {
            self.getDownloadData()
        }
    }

    @objc func txtDidChange(_ textField: UITextField) {
        self.imgSearch.isHighlighted = textField.isFirstResponder
        debugPrint("Search Screen", textField.text ?? "")
        self.isSearching = true
        /*self.arrTempSearchModel.removeAll()
         if !textField.text!.isEmpty {
         self.arrTempSearchModel = self.arrSearchModel.filter({ $0.title.localizedCaseInsensitiveContains(textField.text!)})
         self.lblSearchResult.text = self.arrTempSearchModel.count.description + " results"
         } else {
         
         }*/
        self.currentPage = 1
        self.isCallingApi = true
        self.callAPI(strSearch: textField.text ?? "")
    }
    
    private func callAPI(strSearch: String) {
        if self.isFrom == .favourites {
            self.categoryVM.getContentData(catID: 0, isFrom: self.isFrom, withLoader: false, searchText: strSearch ,isFromSearch: true, page: self.currentPage)
            
        } else if self.isFrom == .downloads {
            self.filterDownloadData(searchText: strSearch)
            
        }else if self.isFrom == .featuredPlaylists {
            self.viewModel.getFeaturedPlaylistContentSearch(searchText: strSearch, page: self.currentPage)
            
        } else if self.isFrom == .whatsNew {
            self.viewModel.getWhatsNewContentSearch(searchText: strSearch, page: self.currentPage)
            
        } else if self.isFrom == .featuredContent {
            self.viewModel.getFeaturedContentSearch(searchText: strSearch, page: self.currentPage)
            
        } else if self.isFrom == .playlists {
            self.viewModel.getPlaylistContentSearch(searchText: strSearch, page: self.currentPage)
            
        } else if self.isFrom == .mySpace {
//            self.viewModel.getMyProductContent(withLoader: false, searchText: textField.text ?? "", isFromSearch: true)
            self.viewModel.getMyProductContentSearch(withLoader: false, search: strSearch, page: self.currentPage)
        } else {
            self.viewModel.getSearcheData(searchText: strSearch,subcategorycontentId: self.subcategorycontentId,categoryId: self.categoryId, page: self.currentPage)
        }
    }
    
    private func getDownloadData() {
        let decodeData = UserDefaults.standard.decode(for: DownloadMainModel.self, using: UserDefaults.Keys.downloadContent)
        if let data = decodeData {
            self.arrDownloadModel = data.data.filter{ $0.isDownloaded }//.map{ CategoryContentModel(fromJson: $0.fileContent) }
        }
    }
    
    private func filterDownloadData(searchText : String) {
        self.tempArrDownloadModel.removeAll()
        if !searchText.trim().isEmpty {
            self.tempArrDownloadModel = self.arrDownloadModel.filter({ CategoryContentModel(fromJson: $0.fileContent).title.localizedStandardContains(searchText) })
        }
        self.lblSearchResult.text = "%@ results".localized(self.tempArrDownloadModel.count.description)
        self.setUpEmptyView()
        self.tblList.reloadData()
    }

    private func setUpEmptyView() {
        if self.isFrom == .favourites {
            self.vwTableView.isHidden = self.categoryVM.getMediaCount() <= 0
        } else if self.isFrom == .downloads {
            self.vwTableView.isHidden = self.tempArrDownloadModel.count <= 0
        }else if self.isFrom == .featuredContent || self.isFrom == .whatsNew || self.isFrom == .featuredPlaylists || self.isFrom == .playlists || self.isFrom == .mySpace {
            self.vwTableView.isHidden = self.arrSubData.count <= 0
        } else {
            self.vwTableView.isHidden = self.viewModel.getSearchCount() <= 0
        }
//        self.vwTableView.isHidden = self.isFrom == .favourites ?  (self.categoryVM.getMediaCount() <= 0) : (self.viewModel.getSearchCount() <= 0)
        self.vwEmpty.isHidden = !self.vwTableView.isHidden
       
        if self.isSearching {
            if self.vwTableView.isHidden {
                AppDelegate.shared.addAnalyticsEvent(screenName: .SearchResults, eventName: .Pageview, category: .SearchResultsNotFound, label: self.txtSearch.text ?? "")
            } else {
                AppDelegate.shared.addAnalyticsEvent(screenName: .SearchResults, eventName: .Pageview, category: .SearchResultsFound, label: self.txtSearch.text ?? "")
            }
        }
        
    }
    
    private func openContentScreen(data : CategoryContentModel){
        if !UserModel.canOpenMediaWithoutSubcription(content: data){
            return
        }
        if data.contentType == .audio {
            let vc = StoryboardScene.Home.audioPlayerVC.instantiate()
            vc.mediaData = data
            vc.isFrom = self.isFrom
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        } else if data.contentType == .video {
            let vc = StoryboardScene.Home.videoPlayerVC.instantiate()
            vc.isFrom = self.isFrom
            vc.hidesBottomBarWhenPushed = true
            vc.mediaData = data
            self.navigationController?.pushViewController(vc, animated: true)
        } else if data.contentType == .pdf {
            DispatchQueue.main.async {
                let vc = StoryboardScene.Home.pdfViewerVC.instantiate()
                vc.mediaData = data
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        self.viewModel.isSearchFetchComplete.bind(observer: { [unowned self] (result) in
            if !(result ?? true) {
                self.isCallingApi = false
                return
            }
            self.currentPage += 1
            self.isCallingApi = true
            self.lblSearchResult.text = "%@ results".localized(self.viewModel.getSearchCount().description)
            self.setUpEmptyView()
            self.tblList.reloadData()
            
         })
        
        self.categoryVM.isProductFetchComplete.bind (observer: { [unowned self] (result) in
            if !(result ?? true) {
                self.isCallingApi = false
                return
            }
            self.currentPage += 1
            self.isCallingApi = true
            self.lblSearchResult.text = "%@ results".localized(self.categoryVM.getMediaCount().description)
            self.setUpEmptyView()
            self.tblList.reloadData()
         })
        
        self.viewModel.isAPIFetchComplete.bind(observer: { [unowned self] (result) in
//            self.tblList.endRefreshing()
            if result ?? false {
                self.arrSubData = self.viewModel.arrCategoryData
                self.isCallingApi = true
                self.currentPage += 1
                self.lblSearchResult.text = "%@ results".localized((self.arrSubData.count).description)
                self.setUpEmptyView()
                self.tblList.reloadData()
            } else {
                self.isCallingApi = false
            }
//            if (result ?? []).count == 0 {
//                self.isCallingApi = false
//                return
//            }
//
//            self.arrSubData.append(result ?? [])// = result ?? []
//            self.isCallingApi = true
//            self.currentPage += 1
//            self.lblSearchResult.text = "%@ results".localized((self.arrSubData.count).description)
////            if self.isFrom == .playlists, self.arrSubData != nil{
////                self.arrSubData = self.arrSubData!.filter{!$0.mediaContent.isEmpty}
////            }
//            self.setUpEmptyView()
//            self.tblList.reloadData()
////            self.colList.emptyDataSetSource = self
////            self.colList.emptyDataSetDelegate = self
        })
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //------------------------------------------------------
    
}

//MARK: - UITextfield delegate -

extension SearchVC: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.imgSearch.isHighlighted = false
        self.isSearching = false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}


//MARK: - UITableView delegate and Datasource -

extension SearchVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isFrom == .favourites {
            return self.categoryVM.getMediaCount()
            
        } else if self.isFrom == .downloads {
            return self.tempArrDownloadModel.count
            
        } else if self.isFrom == .featuredContent || self.isFrom == .whatsNew || self.isFrom == .featuredPlaylists || self.isFrom == .playlists || self.isFrom == .mySpace{
            return self.arrSubData.count
        } else {
            return self.viewModel.getSearchCount()
        }
//        return self.isFrom == .favourites ? self.categoryVM.getMediaCount() : self.viewModel.getSearchCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: SearchCell.self, for: indexPath)
        var dataAtIndex : CategoryContentModel = CategoryContentModel(fromJson: JSON())
        
        if self.isFrom == .favourites {
            dataAtIndex = self.categoryVM.getMediaAtIndex(index: indexPath.row)
        } else if self.isFrom == .downloads {
            dataAtIndex = CategoryContentModel(fromJson: self.tempArrDownloadModel[indexPath.row].fileContent)
        } else if self.isFrom == .featuredContent || self.isFrom == .whatsNew || self.isFrom == .featuredPlaylists || self.isFrom == .playlists || self.isFrom == .mySpace{
            
//            if let arr = self.arrSubData {
                dataAtIndex = self.arrSubData[indexPath.row]
//            }
            
        } else {
            dataAtIndex = self.viewModel.getSearchAtIndex(index: indexPath.row)
        }
        
        cell.data = dataAtIndex
        cell.btnPlay.addTapGestureRecognizer {
            self.openContentScreen(data: dataAtIndex)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var dataAtIndex : CategoryContentModel = CategoryContentModel(fromJson: JSON())
        if self.isFrom == .favourites {
            dataAtIndex = self.categoryVM.getMediaAtIndex(index: indexPath.row)
        } else if self.isFrom == .downloads {
            dataAtIndex = CategoryContentModel(fromJson: self.tempArrDownloadModel[indexPath.row].fileContent)
        }  else if self.isFrom == .featuredContent || self.isFrom == .whatsNew || self.isFrom == .featuredPlaylists || self.isFrom == .playlists || self.isFrom == .mySpace {
//            if let arr = self.arrSubData {
                dataAtIndex = self.arrSubData[indexPath.row]
//            }
        } else {
            dataAtIndex = self.viewModel.getSearchAtIndex(index: indexPath.row)
        }
//        let dataAtIndex = self.viewModel.getSearchAtIndex(index: indexPath.row)
        self.openContentScreen(data: dataAtIndex)
    }
}

extension SearchVC {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.isAtBottom {
            if self.isCallingApi {
                self.isCallingApi = false
                self.callAPI(strSearch: self.txtSearch.text ?? "")
            }
        }
    }
}
