//
//  PDFViewerVC.swift
//  ShivYog
//
//  Created by 2022M30 on 07/12/22.
//

import UIKit
import AVKit
import AVFoundation
import PDFKit

class PDFViewerVC: UIViewController {
    
    //MARK: - Outlet

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnPlayPause: UIButton!
    @IBOutlet weak var vwSlider: RangeSeekSlider!
    @IBOutlet var btnRepeat: [UIButton]!
    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var lblStartTime: UILabel!
    @IBOutlet weak var lblEndTime: UILabel!
    @IBOutlet weak var btnPlayPauseTop: UIButton!
    @IBOutlet weak var vwPDF: UIView!
//    @IBOutlet weak var vwTopForeground: UIView!
    @IBOutlet weak var constBottomToSafeArea: NSLayoutConstraint!
    @IBOutlet weak var constBottomToContainer: NSLayoutConstraint!
    @IBOutlet weak var lblPageNumber: UILabel!
    
    
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
//    private var viewModel : AuthViewModel = AuthViewModel()
    var player : AVPlayer?//AVAudioPlayer()
//    var arrUrl = [String]()
//    var videoURL: URL! = nil
//    var index = Int()
    var currentTime = Double()
    var audioDuration = Double()
    var isVideoPlaying = false
    var sliderTimer : Timer!
    private var pdfView: PDFView!
    var pdfUrl: URL! = nil
    var isFirstTimePlaying : Bool = true
    
    var mediaData : CategoryContentModel?
    
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.player != nil{
            self.player?.pause()
            self.player = nil
            self.sliderTimer.invalidate()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        if let _ = GAudioPlayer.shared.player {
            GAudioPlayer.shared.managedState(url: "123", state: .off, speed: "1")
        }
        
        self.lblTitle.font(name: FontFamily.SFProDisplay.medium, size: 15.0).textColor(color: .app_222222)
        self.lblStartTime.font(name: FontFamily.SFProDisplay.regular, size: 12.0).textColor(color: .app_4F4F4F)
        self.lblEndTime.font(name: FontFamily.SFProDisplay.regular, size: 12.0).textColor(color: .app_4F4F4F)
        self.lblPageNumber.font(name: FontFamily.SFProDisplay.medium, size: 15.0).textColor(color: .app_222222).text = "1"
        self.vwSlider.delegate = self
        self.vwSlider.minValue = 0.0
        self.vwSlider.selectedMinValue = 0.0
//        self.vwSlider.selectedMaxValue = 0.0
        self.vwSlider.tintColor = .appE1Ddc6
        self.vwSlider.colorBetweenHandles = .app_990000
        self.vwSlider.hideLabels = true
        self.btnRepeat[0].isHidden = true
        self.btnRepeat[1].isHidden = true
        self.btnRepeat[2].isHidden = false
        self.btnPlayPauseTop.isSelected = false
        
        DispatchQueue.main.async {
            self.vwContainer.roundCorners([.topLeft,.topRight], radius: 6.0)
        }
        
        self.vwContainer.isHidden = true
//        self.vwTopForeground.isHidden = true
        self.setUpPDFView()
        self.setPdfData()
//        self.setPlayer()
    }
    
    private func setUpPDFView() {
        self.pdfView = PDFView(frame: self.vwPDF.bounds)
        self.pdfView.maxScaleFactor = 3;
        self.pdfView.minScaleFactor = self.pdfView.scaleFactorForSizeToFit;
        self.pdfView.autoScales = true;
        self.pdfView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.vwPDF.addSubview(self.pdfView)
    }
    
    private func setPdfData() {
        if let data = mediaData, let url = URL(string: data.file){
            self.navigationItem.title = data.title
            self.btnPlayPauseTop.isHidden = data.attachAudioFile.count <= 0
            self.lblTitle.text = data.title
            if let document = PDFDocument(url: url) {
                self.pdfView.document = document
    //            let pageNumber = document.index(for: self.pdfView.currentPage!)
                NotificationCenter.default.addObserver (self, selector: #selector(handlePageChange), name: Notification.Name.PDFViewPageChanged, object: nil)
            }
        }
        else{
            let urlString = Bundle.main.path(forResource: "demopdf", ofType: "pdf")
            if let data = try? Data(contentsOf: URL(fileURLWithPath: urlString!)), let document = PDFDocument(data: data) {
                self.pdfView.document = document
                //            let pageNumber = document.index(for: self.pdfView.currentPage!)
                NotificationCenter.default.addObserver (self, selector: #selector(handlePageChange), name: Notification.Name.PDFViewPageChanged, object: nil)
            }
        }
    }
    
    @objc func handlePageChange() {
        if let document = self.pdfView.document {
            let pageNumber = document.index(for: self.pdfView.currentPage!)
                debugPrint(pageNumber + 1)
            self.lblPageNumber.text = (pageNumber + 1).description
        }
    }
    
    private func startTimer() {
        self.sliderTimer = Timer.scheduledTimer(timeInterval: 0.001, target: self, selector: #selector(self.updateSlider), userInfo: nil, repeats: true)
    }
    
    @objc func updateSlider() {
        if self.sliderTimer.isValid {
            self.currentTime = CMTimeGetSeconds((self.player!.currentTime()))
            self.vwSlider.maxValue = self.audioDuration//Double(player.duration)
            self.vwSlider.selectedMaxValue = self.currentTime//Double(player.currentTime)
            self.lblStartTime.text = Int(self.currentTime).secondsToMinutesSeconds
            self.lblEndTime.text = Int(self.audioDuration).secondsToMinutesSeconds
            
            if self.player?.timeControlStatus != .waitingToPlayAtSpecifiedRate {
                if AppLoader.shared.isLoaderAnimating() {
                    AppLoader.shared.removeLoader()
                }
            }
        }
    }
    
    private func setPlayer() {
        
        var videoUrl = "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3"
        if let mediaData = mediaData , let audioData = mediaData.attachAudioFile.first{
            videoUrl = audioData.file
        }
        else{
            return
        }
        if let urlString = URL(string: videoUrl) {
            self.vwContainer.isHidden = false
            self.vwContainer.isUserInteractionEnabled = false
            self.btnPlayPauseTop.isSelected = true
            do {
                try AVAudioSession.sharedInstance().setCategory(.playback)
                let asset : AVURLAsset = AVURLAsset(url: urlString, options: nil)
                let statusKey = "tracks"
                
                asset.loadValuesAsynchronously(forKeys: [statusKey]) {
                    var error: NSError? = nil
                    
                    DispatchQueue.main.async {
                        let status : AVKeyValueStatus = asset.statusOfValue(forKey: statusKey, error: &error)
                        debugPrint("main async error-", error)
                        if status == AVKeyValueStatus.loaded {
                            let playerItem = AVPlayerItem(asset: asset)
                            self.player = AVPlayer(playerItem: playerItem)
                            self.player?.play()
                            self.audioDuration = CMTimeGetSeconds((self.player?.currentItem!.asset.duration)!)
                            self.vwContainer.isHidden = false
                            self.vwContainer.isUserInteractionEnabled = true
                            self.isVideoPlaying = !self.isVideoPlaying
                            self.btnPlayPause.isSelected = self.isVideoPlaying
                            self.constBottomToSafeArea.priority = .defaultLow
                            self.constBottomToContainer.priority = .defaultHigh
                            self.startTimer()
                            AppLoader.shared.removeLoader()
                            NotificationCenter.default.addObserver(self, selector: #selector(self.videoDidEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
                            NotificationCenter.default.addObserver(self, selector: #selector(self.playerStalled), name: NSNotification.Name.AVPlayerItemPlaybackStalled, object: nil)
                        }
                    }
                }
    //            player = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: urlString!))//AVPlayer(url: videoURL!)
            } catch {
                print("Something went wrong")
            }
        } else {
            debugPrint("Error")
        }
        //        self.videoURL = URL(string: obj)
        
        //        let playerLayer = AVPlayerLayer(player: player)
    }
    
    @objc func playerStalled() {
        AppLoader.shared.addLoader()
    }
    
    @objc func videoDidEnd(){
        debugPrint("videoDidEnd")
        player?.seek(to: .zero)
        if self.btnRepeat[2].isSelected{
            player?.play()
        }
        else{
            self.playPauseAudio()
        }
    }
    
    private func playPauseAudio() {
        if self.isVideoPlaying{
            self.player?.pause()
            self.sliderTimer.invalidate()
        }else{
            self.player?.play()
            self.startTimer()
        }
        isVideoPlaying = !isVideoPlaying
        self.btnPlayPauseTop.isSelected = isVideoPlaying
        self.btnPlayPause.isSelected = isVideoPlaying
        self.vwContainer.isHidden = !isVideoPlaying
        self.constBottomToSafeArea.priority = isVideoPlaying ? .defaultLow : .defaultHigh
        self.constBottomToContainer.priority = isVideoPlaying ? .defaultHigh : .defaultLow
//        self.vwTopForeground.isHidden = true
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })*/
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    
    @IBAction func btnRepeatClicked(_ sender: UIButton) {
        /*let _ = self.btnRepeat.map {
            $0.isHidden = true
        }
        switch sender.tag {
        case 100:
            self.btnRepeat[1].isHidden = false
//            self.player.numberOfLoops = 1
//            sender.isHidden = true
//            self.btnRepeat[1].isHidden = false
//            self.btnRepeat[2].isHidden = false
        case 200:
            self.btnRepeat[2].isHidden = false
//            self.player.numberOfLoops = 0
//            sender.isHidden = true
//            self.btnRepeat[0].isHidden = false
//            self.btnRepeat[2].isHidden = false
        case 300:
            self.btnRepeat[0].isHidden = false
//            self.player.numberOfLoops = 1
//            sender.isHidden = true
//            self.btnRepeat[0].isHidden = false
//            self.btnRepeat[1].isHidden = false
        default:
            break
        }*/
        if self.isVideoPlaying{
            sender.isSelected.toggle()
            GAudioPlayer.shared.isRepeat = sender.isSelected
        }
        
    }
    
    
    @IBAction func btnNextClicked(_ sender: UIButton) {
//        self.setPlayer()
        self.sliderTimer.invalidate()
        let newTime = self.currentTime + 10.0
        if newTime < (self.audioDuration - 10.0) {
            let time : CMTime = CMTimeMake(value: Int64(newTime * 1000), timescale: 1000)
            self.player?.seek(to: time)
        } else {
            guard let duration = player?.currentItem?.duration else { return }
            self.player?.seek(to: duration)
        }
        self.startTimer()
    }
    
    @IBAction func btnPreviousClicked(_ sender: UIButton) {
//        self.setPlayer()
        self.sliderTimer.invalidate()
        var newTime = self.currentTime - 10.0
        if newTime < 0 {
            newTime = 0
        }
        let time : CMTime = CMTimeMake(value: Int64(newTime * 1000), timescale: 1000)
        player?.seek(to: time)
        self.startTimer()
    }
    
    
    @IBAction func btnPlayPauseClicked(_ sender: UIButton) {
        self.playPauseAudio()
    }
    
    
    @IBAction func btnTopPlayPauseClicked(_ sender: UIButton) {
        if self.isFirstTimePlaying {
            AppLoader.shared.addLoader()
            self.setPlayer()
            self.isFirstTimePlaying = false
        } else {

            self.playPauseAudio()
        }

    }
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {

        self.navigationController?.popViewController(animated: true)
    }
    
    //------------------------------------------------------
    
}

//MARK: - RangeSeek slider delegate -

extension PDFViewerVC: RangeSeekSliderDelegate {
    func didStartTouches(in slider: RangeSeekSlider) {
        self.sliderTimer.invalidate()
//        self.playPauseAudio()
        self.player?.pause()
    }
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {

//        self.sliderTimer.invalidate()
        let time = CMTimeMake(value: Int64(maxValue * 1000), timescale: 1000)
        self.player?.seek(to: time)//currentTime = TimeInterval(maxValue)//Double(maxValue)
//        slider.selectedMaxValue = Double(maxValue)

        self.lblStartTime.text = Int(maxValue).secondsToMinutesSeconds
//        self.startTimer()
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
//        self.playPauseAudio()
        self.player?.play()
        self.startTimer()
//        self.startTimer()
    }
}

//MARK: - PDF View delegate -

extension PDFViewerVC: PDFViewDelegate {
    
}
