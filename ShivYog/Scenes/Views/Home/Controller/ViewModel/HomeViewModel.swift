//
//  HomeViewModel.swift
//  ShivYog
//
//  Created by 2022M30 on 30/11/22.
//

import Foundation
import UIKit

//class SearchModel {
//    var image: UIImage!
//    var title: String!
//    var type : ContentType!
//    var subType: String!
//
//    init(image: UIImage, title: String, type: ContentType = .video, subType: String) {
//        self.image = image
//        self.title = title
//        self.type = type
//        self.subType = subType
//
//    }
//}

//class ProductMainModel {
//    var title: String!
//    var image: UIImage!
//    var isFree: Bool!
//    var contentType : ContentType!
//    var arrProducts : [ProductModel]
//
//    init(title: String, image: UIImage!,isFree: Bool = false, contentType: ContentType, arrProducts : [ProductModel]) {
//        self.title = title
//        self.image = image
//        self.contentType = contentType
//        self.arrProducts = arrProducts
//        self.isFree = isFree
//    }
//}

//class ProductModel {
//    var image: UIImage!
//    var title: String!
//    var subTitle: String!
//    var isFree: Bool!
//    var contentType: ContentType!
//    var isPlaylist: Bool!
//    var description : String!
//
//    init(image: UIImage = .temp7.image, title: String, subTitle: String, isFree: Bool = false, contentType: ContentType, isPlaylist: Bool = false, description : String = "Amet minim mollit non deserunt ullamco est sit aliqua dolor.") {
//        self.image = image
//        self.title = title
//        self.subTitle = subTitle
//        self.isFree = isFree
//        self.contentType = contentType
//        self.isPlaylist = isPlaylist
//        self.description = description
//    }
//}

class NotificationMainModel {
    var day: String!
    var arrNotificationModel: [NotificationModel]!
    
    init(day: String, arrNotificationModel: [NotificationModel]){
        self.day = day
        self.arrNotificationModel = arrNotificationModel
    }
}

class NotificationModel {
    var title: String!
    var time: String!
    var isSelected: Bool!
    var contentType: ContentType!
    
    init(title: String = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Orci a nec ", time: String = "6:50 PM", isSelected: Bool = false, contentType : ContentType = .video) {
        self.title = title
        self.time = time
        self.isSelected = isSelected
        self.contentType = contentType
    }
}

class HomeViewModel {
    
    
    private var arrBanner : [BannerModel] = []
    private(set) var isBannerFetchComplete = Bindable<Bool>()
    private(set) var isHomeFetchComplete = Bindable<Bool>()
    private(set) var isSearchFetchComplete = Bindable<Bool>()
    private(set) var isAPIFetchComplete = Bindable<Bool>()//Bindable<[CategoryContentModel]>()
    var arrCategoryData : [CategoryContentModel] = []
    
    var arrFeatured : [SubCategoryModel] = [
        /*SubCategoryModel(fromJson: JSON(["title" : HomePageTitle.FeaturedContent.rawValue, "category_content" : Array(repeating:JSON(["image" : UIImage.temp9.name, "title" : "Navaratri Special", "contentType" : ContentType.folder.rawValue]), count: 2)])),
        SubCategoryModel(fromJson: JSON(["title" : HomePageTitle.WhatsNew.rawValue, "category_content" : Array(repeating:JSON(["image" : UIImage.temp5.name, "title" : "Shree Lalita Rudram", "contentType" : ContentType.folder.rawValue]), count: 2)])),
        SubCategoryModel(fromJson: JSON(["title" : HomePageTitle.FeaturedPlaylists.rawValue, "category_content" : Array(repeating:JSON(["image" : UIImage.temp3.name, "title" : "Kathopanishad Vol 1", "contentType" : ContentType.audio.rawValue]), count: 2)])),
        SubCategoryModel(fromJson: JSON(["title" : HomePageTitle.ContinueWatching.rawValue, "category_content" : Array(repeating:JSON(["image" : UIImage.temp8.name, "title" : "Kathopanishad Vol 1", "contentType" : ContentType.video.rawValue]), count: 2)]))*/
        
    ]
    
    private var arrSearchData : [CategoryContentModel] = []
    
//    var arrNewPlaylist : [ProductMainModel] = [ProductMainModel(title: "Kathopanishad Vol 1", image: .temp7.image, contentType: .audio, arrProducts: []),
//                                            ProductMainModel(title: "Kathopanishad Vol 1", image: .temp7.image, contentType: .video, arrProducts: []),
//                                            ProductMainModel(title: "Kathopanishad Vol 1", image: .temp7.image, contentType: .pdf, arrProducts: []),
//                                            ProductMainModel(title: "Kathopanishad Vol 1", image: .temp7.image, contentType: .audio, arrProducts: []),
//                                            ProductMainModel(title: "Kathopanishad Vol 1", image: .temp7.image, contentType: .video, arrProducts: []),
//                                            ProductMainModel(title: "Kathopanishad Vol 1", image: .temp7.image, contentType: .pdf, arrProducts: []),
//                                            ProductMainModel(title: "Kathopanishad Vol 1", image: .temp7.image, contentType: .audio, arrProducts: []),
//                                            ProductMainModel(title: "Kathopanishad Vol 1", image: .temp7.image, contentType: .video, arrProducts: []),
//                                            ProductMainModel(title: "Kathopanishad Vol 1", image: .temp7.image, contentType: .pdf, arrProducts: []),
//                                            ProductMainModel(title: "Kathopanishad Vol 1", image: .temp7.image, contentType: .audio, arrProducts: []),
//                                            ProductMainModel(title: "Kathopanishad Vol 1", image: .temp7.image, contentType: .video, arrProducts: [])]
    
//    var arrProductModel : [ProductModel] = [
//        ProductModel(title: "Yoga Meditation", subTitle: "Meditation • Video", isFree: true, contentType: .video),
//        ProductModel(title: "Yoga Meditation", subTitle: "Meditation • Audio", isFree: false, contentType: .audio),
//        ProductModel(title: "Yoga Meditation", subTitle: "Meditation • PDF", isFree: true, contentType: .pdf),
//        ProductModel(title: "Yoga Meditation", subTitle: "Meditation • Audio", isFree: true, contentType: .audio),
//        ProductModel(title: "Yoga Meditation", subTitle: "Meditation • Video", isFree: false, contentType: .video),
//        ProductModel(title: "Yoga Meditation", subTitle: "Meditation • Video", isFree: true, contentType: .video),
//        ProductModel(title: "Yoga Meditation", subTitle: "Meditation • PDF", isFree: true, contentType: .pdf),
//        ProductModel(title: "Yoga Meditation", subTitle: "Meditation • Video", isFree: false, contentType: .video),
//        ProductModel(title: "Yoga Meditation", subTitle: "Meditation • Audio", isFree: true, contentType: .audio),
//        ProductModel(title: "Yoga Meditation", subTitle: "Meditation • Video", isFree: true, contentType: .video),
//        ProductModel(title: "Yoga Meditation", subTitle: "Meditation • Audio", isFree: false, contentType: .audio),
//        ProductModel(title: "Yoga Meditation", subTitle: "Meditation • Video", isFree: true, contentType: .video),
//        ProductModel(title: "Yoga Meditation", subTitle: "Meditation • PDF", isFree: false, contentType: .pdf),
//        ProductModel(title: "Yoga Meditation", subTitle: "Meditation • Video", isFree: true, contentType: .video)]
    
//    private var arrPlaylist : [ProductModel] = [
//        ProductModel(title: "Yoga Meditation", subTitle: "5 Tracks", isFree: true, contentType: .video, isPlaylist: true),
//        ProductModel(title: "New Workout", subTitle: "2 Tracks", isFree: false, contentType: .pdf, isPlaylist: true),
//        ProductModel(title: "Dhyaan Practice", subTitle: "3 Tracks", isFree: true, contentType: .video, isPlaylist: true),
//        ProductModel(title: "Yoga Meditation", subTitle: "10 Tracks", isFree: true, contentType: .audio, isPlaylist: true)]
    
    
    
    private var arrNotifications : [NotificationMainModel] = [
        NotificationMainModel(day: "Today", arrNotificationModel: [
            NotificationModel(isSelected: true,contentType: .audio),
            NotificationModel(isSelected: false,contentType: .pdf),
            NotificationModel(isSelected: false,contentType: .video)]),
        NotificationMainModel(day: "Yesterday", arrNotificationModel: [
            NotificationModel(isSelected: true,contentType: .video),
            NotificationModel(isSelected: false,contentType: .pdf),
            NotificationModel(isSelected: true,contentType: .video)]),
        NotificationMainModel(day: "30 July, 2022", arrNotificationModel: [
            NotificationModel(isSelected: false,contentType: .audio),
            NotificationModel(isSelected: false,contentType: .pdf),
            NotificationModel(isSelected: false,contentType: .pdf)]),
        NotificationMainModel(day: "28 July, 2022", arrNotificationModel: [
            NotificationModel(isSelected: true,contentType: .pdf),
            NotificationModel(isSelected: false,contentType: .pdf),
            NotificationModel(isSelected: true,contentType: .audio)])]
}

//extension HomeViewModel {
//    func numberOfProducts(isPlaylist: Bool = false) -> Int {
//        return isPlaylist ? self.arrPlaylist.count : self.arrProductModel.count
//    }
//    
//    func dataAtIndexForProduct(isPlaylist: Bool = false, index: Int) -> ProductModel {
//        return isPlaylist ? self.arrPlaylist[index] : self.arrProductModel[index]
//    }
//}

    
extension HomeViewModel {
    func numberOfItemsAtNotificationSection() -> Int {
        return self.arrNotifications.count
    }
    
    func dataAtSectionForNotification(section: Int) -> NotificationMainModel {
        return self.arrNotifications[section]
    }
    
    func updataDataAtIndexForNotification(section: Int, row: Int) {
        self.arrNotifications[section].arrNotificationModel[row].isSelected = true
    }
    
    func markAllReadNotification() {
        self.arrNotifications.forEach {
            $0.arrNotificationModel.forEach {
                $0.isSelected = true
            }
        }
    }
}

extension HomeViewModel{
    func getBanners(withLoader isLoader : Bool = false){
        ApiManager.shared.makeRequest(endPoint: .banner(.Banner), methodType: .get, parameter: nil, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                
                self.arrBanner = BannerModel.listFromArray(data: apiData.data["results"].arrayValue)
                self.isBannerFetchComplete.value = true
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
    
    func getHomeData(withLoader isLoader : Bool = false){
        var dictData = Dictionary<String,String>()
//        if !UserModel.selectedPref.isEmpty{
            dictData["tag__id__in"] = UserModel.selectedPref
//        }
        ApiManager.shared.makeRequest(endPoint: .content(.home), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                debugPrint(apiData.data)
                self.arrFeatured = []
                if let data = apiData.data["results"].arrayValue.first{
                    if data["featured_content"].arrayValue.count > 0{
                        self.arrFeatured.append(SubCategoryModel(fromJson: JSON(["title" : HomePageTitle.FeaturedContent.rawValue, "category_content" : data["featured_content"].arrayValue])))
                    }
                    if data["whatsnew"].arrayValue.count > 0{
                        self.arrFeatured.append(SubCategoryModel(fromJson: JSON(["title" : HomePageTitle.WhatsNew.rawValue, "category_content" : data["whatsnew"].arrayValue])))
                    }
                    if data["featured_playlist"].arrayValue.count > 0{
                        self.arrFeatured.append(SubCategoryModel(fromJson: JSON(["title" : HomePageTitle.FeaturedPlaylists.rawValue, "category_content" : data["featured_playlist"].arrayValue])))
                    }
                    if data["continue_watching"].arrayValue.count > 0{
                        self.arrFeatured.append(SubCategoryModel(fromJson: JSON(["title" : HomePageTitle.ContinueWatching.rawValue, "category_content" : data["continue_watching"].arrayValue])))
                    }
                }
                self.isHomeFetchComplete.value = true
                break
            case .failure(let error):
                self.isHomeFetchComplete.value = true
                print("the error \(error)")
            }
        }
    }
    
    func getSearcheData(_ isLoader : Bool = false, searchText : String, subcategorycontentId : Int = -1, categoryId : Int = -1, page : Int = 1){
        ApiManager.shared.cancelRequest(endpoint: .content(.mediacontent))
        var dictData = Dictionary<String,String>()
        if !UserModel.selectedPref.isEmpty{
            dictData["tag__id__in"] = UserModel.selectedPref
        }
        
        if subcategorycontentId != -1 {
            dictData["subcategorycontent__id"] = subcategorycontentId.description
        }
        
        if categoryId != -1 {
            dictData["category__id"] = categoryId.description
        }
        
        dictData["page"] = page.description
        
        if searchText.trim().isEmpty{
            self.arrSearchData.removeAll()
            self.isSearchFetchComplete.value = true
            return
        }
        dictData["search"] = searchText
        debugPrint("Search Text", searchText)
        ApiManager.shared.makeRequest(endPoint: .content(.mediacontent), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                debugPrint(apiData.data)
                
                if page == 1 {
                    self.arrSearchData = []
                    self.arrSearchData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)
                } else {
                    self.arrSearchData.append(CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue))
                }
                self.isSearchFetchComplete.value = true
                break
            case .failure(let error):
                self.isSearchFetchComplete.value = false
                print("the error \(error)")
            }
        }
    }
    
    func getFeaturedContentSearch(withLoader isLoader : Bool = false, searchText : String = "", page: Int = 1){
        ApiManager.shared.cancelRequest(endpoint: .content(.featuredcontentSearch))
        var dictData = Dictionary<String,String>()
        dictData["tag__id__in"] = UserModel.selectedPref
        if searchText.trim().isEmpty {
            self.arrCategoryData = []
            self.isAPIFetchComplete.value = false//[]
            return
        }
        dictData["search"] = searchText
        dictData["page"] = page.description
        ApiManager.shared.makeRequest(endPoint: .content(.featuredcontentSearch), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                if page == 1 {
                    self.arrCategoryData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)
                } else {
                    self.arrCategoryData.append(CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue))
                }
                self.isAPIFetchComplete.value = true
                break
            case .failure(let error):
                self.isAPIFetchComplete.value = false//[]
                print("the error \(error)")
            }
        }
    }
    
    
    func getFeaturedContent(withLoader isLoader : Bool = false, page: Int = 1){
        var dictData = Dictionary<String,String>()
        dictData["tag__id__in"] = UserModel.selectedPref
        dictData["page"] = page.description
        ApiManager.shared.makeRequest(endPoint: .content(.feturecontent), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                if page == 1 {
                    self.arrCategoryData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)
                } else {
                    self.arrCategoryData.append(CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue))
                }
                self.isAPIFetchComplete.value = true
                break
            case .failure(let error):
                self.isAPIFetchComplete.value = false//[]
                print("the error \(error)")
            }
        }
    }
    
    func getWhatsNewContentSearch(withLoader isLoader : Bool = false, searchText : String = "", page: Int = 1){
        ApiManager.shared.cancelRequest(endpoint: .content(.whatsnewSearch))
        var dictData = Dictionary<String,String>()
        dictData["tag__id__in"] = UserModel.selectedPref
        if searchText.trim().isEmpty {
            self.arrCategoryData = []
            self.isAPIFetchComplete.value = false//[]
            return 
        }
        dictData["search"] = searchText
        dictData["page"] = page.description
        ApiManager.shared.makeRequest(endPoint: .content(.whatsnewSearch), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                if page == 1 {
                    self.arrCategoryData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)
                } else {
                    self.arrCategoryData.append(CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue))
                }
                break
                self.isAPIFetchComplete.value = true
            case .failure(let error):
                self.isAPIFetchComplete.value = false//[]
                print("the error \(error)")
            }
        }
    }
    
    func getWhatsNewContent(withLoader isLoader : Bool = false, page: Int = 1){
        var dictData = Dictionary<String,String>()
        dictData["tag__id__in"] = UserModel.selectedPref
        dictData["page"] = page.description
        ApiManager.shared.makeRequest(endPoint: .content(.whatsnew), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                if page == 1 {
                    self.arrCategoryData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)
                } else {
                    self.arrCategoryData.append(CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue))
                }
                self.isAPIFetchComplete.value = true
                break
            case .failure(let error):
                self.isAPIFetchComplete.value = false//[]
                print("the error \(error)")
            }
        }
    }
    
    func getFeaturedPlaylistContent(withLoader isLoader : Bool = false, searchText : String = "", isFromSearch : Bool = false, page: Int = 1){
        var dictData = Dictionary<String,String>()
        dictData["tag__id__in"] = UserModel.selectedPref
        dictData["page"] = page.description
        if isFromSearch {
            ApiManager.shared.cancelRequest(endpoint: .playlist(.featuredPlaylist))
            if searchText.trim().isEmpty {
                self.arrCategoryData = []
                self.isAPIFetchComplete.value = false//.removeAll()
                
                return
            }
            dictData["search"] = searchText
        }
        
        ApiManager.shared.makeRequest(endPoint: .playlist(.featuredPlaylist), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                if page == 1 {
                    self.arrCategoryData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)
                } else {
                    self.arrCategoryData.append(CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue))
                }
                self.isAPIFetchComplete.value = true
                break
            case .failure(let error):
                self.isAPIFetchComplete.value = false//[]
                print("the error \(error)")
            }
        }
    }
    
    
    func getFeaturedPlaylistContentSearch(withLoader isLoader : Bool = false, searchText : String = "", page: Int = 1){
        var dictData = Dictionary<String,String>()
        dictData["tag__id__in"] = UserModel.selectedPref
        ApiManager.shared.cancelRequest(endpoint: .playlist(.featuredplaylistSearch))
        if searchText.trim().isEmpty {
            self.arrCategoryData = []
            self.isAPIFetchComplete.value = false//?.removeAll()
            return
        }
        dictData["search"] = searchText
        dictData["page"] = page.description
        
        ApiManager.shared.makeRequest(endPoint: .playlist(.featuredplaylistSearch), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                if page == 1 {
                    self.arrCategoryData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)
                } else {
                    self.arrCategoryData.append(CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue))
                }
                self.isAPIFetchComplete.value = true
                break
            case .failure(let error):
                self.isAPIFetchComplete.value = false//[]
                print("the error \(error)")
            }
        }
    }
    
    func getPlaylistContentSearch(withLoader isLoader : Bool = false, searchText : String = "", page: Int = 1){
        var dictData = Dictionary<String,String>()
        dictData["tag__id__in"] = UserModel.selectedPref
        
        
        ApiManager.shared.cancelRequest(endpoint: .playlist(.mediaplaylistSearch))
        if searchText.trim().isEmpty {
            self.arrCategoryData = []
            self.isAPIFetchComplete.value = false
            return
        }
        dictData["search"] = searchText
        dictData["page"] = page.description
        ApiManager.shared.makeRequest(endPoint: .playlist(.mediaplaylistSearch), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                if page == 1 {
                    self.arrCategoryData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)
                } else {
                    self.arrCategoryData.append(CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue))
                }
                self.isAPIFetchComplete.value = true
                break
            case .failure(let error):
                self.isAPIFetchComplete.value = false//[]
                print("the error \(error)")
            }
        }
    }
    
    func getContinueWatchingContent(withLoader isLoader : Bool = false){
        
        ApiManager.shared.makeRequest(endPoint: .content(.getContinueWatching), methodType: .get, parameter: nil, queryParameters: [:], withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                var newData = apiData.data["results"].arrayValue
                if newData.count > 0{
                    if let continueWatching = self.arrFeatured.filter{$0.title == HomePageTitle.ContinueWatching.rawValue}.first{
                        if let index = self.arrFeatured.firstIndex{$0 === continueWatching}, index < self.arrFeatured.count {
                            self.arrFeatured[index].categoryContent = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)
                            self.isHomeFetchComplete.value = true
                        }
                    }
                    else{
                        self.arrFeatured.append(SubCategoryModel(fromJson: JSON(["title" : HomePageTitle.ContinueWatching.rawValue, "category_content" : newData])))
                    }
                }
                else{
                    self.arrFeatured = self.arrFeatured.filter{$0.title != HomePageTitle.ContinueWatching.rawValue}
                }
                self.isHomeFetchComplete.value = true
                
                break
            case .failure(let error):
                self.isAPIFetchComplete.value = false//[]
                print("the error \(error)")
            }
        }
    }
    
    func getBannerCount() -> Int{
        return self.arrBanner.count
    }
    
    func getBannerAtIndex(index : Int) -> BannerModel{
        return self.arrBanner[index]
    }
    
    func getSearchCount() -> Int{
        return self.arrSearchData.count
    }
    
    func getSearchAtIndex(index : Int) -> CategoryContentModel{
        return self.arrSearchData[index]
    }
    
    
    func getPlaylistContent(withLoader isLoader : Bool = false, page: Int = 1){
        var dictData = Dictionary<String,String>()
        dictData["page"] = page.description
        
        ApiManager.shared.makeRequest(endPoint: .playlist(.playlist), methodType: .get, parameter: nil,queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                if page == 1 {
                    self.arrCategoryData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)
                } else {
                    self.arrCategoryData.append(CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue))
                }
                self.isAPIFetchComplete.value = true
                break
            case .failure(let error):
                self.isAPIFetchComplete.value = false//[]
                print("the error \(error)")
            }
        }
    }
    
    func getStoreSubContent(withLoader isLoader: Bool = false, storeContentId : Int, page: Int = 1) {
        var dictData = Dictionary<String,String>()
        
        dictData["storecontent__id"] = storeContentId.description
        dictData["page"] = page.description
        ApiManager.shared.makeRequest(endPoint: .store(.storeSubcontent), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true) { [weak self] (result) in
            
            guard let self = self else { return }
            
            switch result {
            case .success(let apiData):
                if page == 1 {
                    self.arrCategoryData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)
                } else {
                    self.arrCategoryData.append(CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue))
                }
                self.isAPIFetchComplete.value = true
            case .failure(let error):
                self.isAPIFetchComplete.value = false//[]
                print("ther error \(error)")
            }
        }
    }
    
    func getMyProductContent(withLoader isLoader: Bool = false, searchText: String = "", isFromSearch: Bool = false, page: Int = 1) {
        if isFromSearch {
            ApiManager.shared.cancelRequest(endpoint: .store(.storeProduct))
        }
        
        var dictData = Dictionary<String,String>()
        
        if !UserModel.selectedPref.isEmpty {
            dictData["tag__id__in"] = UserModel.selectedPref
        }
        
        if isFromSearch {
            if searchText.trim().isEmpty {
                self.arrCategoryData = []
                self.isAPIFetchComplete.value = false//?.removeAll()
                return
            }
            dictData["search"] = searchText
        }
        dictData["page"] = page.description
        ApiManager.shared.makeRequest(endPoint: .store(.storeProduct), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true) { [weak self] (result) in
            
            guard let self = self else { return }
            
            switch result {
                
            case .success(let apiData):
//                self.isAPIFetchComplete.value = apiData.data[]
                
//                let myProductData : [MyProductsModel] = MyProductsModel.listFromArray(data: apiData.data["results"].arrayValue)
//                var tempFolderData : [CategoryContentModel] = []
//                myProductData.forEach { item in
//                    
//                    tempFolderData.append(item.storeSubcontentDetails)
//                    tempFolderData.forEach { data in
//                        item.storeMedias.forEach {
//                            if data.id == $0.storesubcontent {
//                                data.mediaContent.append($0)
//                            }
//                        }
//                    }
//                }
                if page == 1 {
                    self.arrCategoryData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)//tempFolderData//?.append(data.compactMap { $0. })
                } else {
                    self.arrCategoryData.append(CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue))
                }
                self.isAPIFetchComplete.value = true
            case .failure(let error):
                self.isAPIFetchComplete.value = false//[]
                print("the error \(error)")
            }
        }
        
    }
    
    
    func getMyProductContentSearch(withLoader isLoader: Bool, search : String, page: Int = 1) {
        ApiManager.shared.cancelRequest(endpoint: .store(.storeProductSearch))
        
        var dictData = Dictionary<String,String> ()
        
//        if !UserModel.selectedPref.isEmpty {
            dictData["tag__id__in"] = UserModel.selectedPref
//        } else {
//            dictData["tag__id__in"] = ""
//        }
        
        if search.trim().isEmpty {
            self.arrCategoryData = []
            self.isAPIFetchComplete.value = false//?.removeAll()
            return
        }
        dictData["search"] = search
        dictData["page"] = page.description
        ApiManager.shared.makeRequest(endPoint: .store(.storeProductSearch), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true) { [weak self] (result) in
            
            guard let self = self else { return }
            
            switch result {
                
            case .success(let apiData):
                if page == 1 {
                    self.arrCategoryData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)//tempFolderData//?.append(data.compactMap { $0. })
                } else {
                    self.arrCategoryData.append(CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue))
                }
                self.isAPIFetchComplete.value = true
                
            case .failure(let error):
                self.isAPIFetchComplete.value = false//[]
                print("the error \(error)")
            }
        }
        
    }
    
}


