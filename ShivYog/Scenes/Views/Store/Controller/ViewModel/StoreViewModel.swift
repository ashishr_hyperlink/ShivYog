//
//  StoreViewModel.swift
//  ShivYog
//
//  Created by 2022M30 on 11/04/23.
//

import Foundation
import UIKit

class StoreViewModel {
    private(set) var isStoreResultFetched = Bindable<Bool>()
    private var arrStoreContent : [SubCategoryModel] = []
}

extension StoreViewModel {
    func getStoreContent(withLoader isLoader : Bool = false, page: Int = 1){
        var dictData = Dictionary<String,String>()
//        if !UserModel.selectedPref.isEmpty{
            dictData["tag__id__in"] = UserModel.selectedPref
        dictData["page"] = page.description
        ApiManager.shared.makeRequest(endPoint: .store(.storeContent), methodType: .get, parameter: nil,queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                if page == 1 {
                    self.arrStoreContent.removeAll()
                    self.arrStoreContent = SubCategoryModel.listFromArray(data: apiData.data["results"].arrayValue)
                } else {
                    self.arrStoreContent.append(SubCategoryModel.listFromArray(data: apiData.data["results"].arrayValue))
                }
//                self.arrStoreContent.append(SubCategoryModel(fromJson: JSON(["title" : HomePageTitle.ContinueWatching.rawValue, "category_content" : data["continue_watching"].arrayValue])))
                self.isStoreResultFetched.value = true
                break
            case .failure(let error):
                self.isStoreResultFetched.value = false
                print("the error \(error)")
            }
        }
    }
    
    func getStoreContentCount() -> Int{
        return self.arrStoreContent.count
    }
    
    func getStoreContentAtIndex(index : Int) -> SubCategoryModel{
        return self.arrStoreContent[index]
    }
}
