//
//  PaymentResponseVC.swift
//  ShivYog
//
//  Created by 2022M30 on 02/12/22.
//

import UIKit

class PaymentResponseVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var imgResponse: UIImageView!
    @IBOutlet weak var lblPaymentStatus: UILabel!
    @IBOutlet weak var btnPayment: ThemeButton!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var homeVM : HomeViewModel = HomeViewModel()
    var paymentResponse : PaymentStatus = .success
    var isFromPaymentScreen : PaymentScreens = .inStore
    var completion : (() -> Void)?
    var msg : String = ""
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        DispatchQueue.main.async {
            self.btnPayment.setRound()
        }
        
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.lblPaymentStatus.font(name: FontFamily.SFProDisplay.bold, size: 22.0).textColor(color: .app_000000)
        self.setData()
    }

    
    private func setData() {
        
        switch self.isFromPaymentScreen {
        case .inStore:
            
            if self.paymentResponse == .success {
                self.imgResponse.image = .successGreen.image
                self.lblPaymentStatus.text = "Your Purchase Is Successfully\n Completed!"
                self.btnPayment.setTitle("Go Back", for: .normal)
                self.btnPayment.setTitle("Go Back", for: .selected)
            } else {
                self.imgResponse.image = .error.image
                if self.msg != "" {
                    self.lblPaymentStatus.text = self.msg
                } else {
                    self.lblPaymentStatus.text = "Seems like there was an issue\n with the payment.\nPlease try again." 
                }
                self.btnPayment.setTitle("Try Again", for: .normal)
                self.btnPayment.setTitle("Try Again", for: .selected)
            }
            
        case .inAppPurchase:
            if self.paymentResponse == .success {
                self.imgResponse.image = .successGreen.image
                DispatchQueue.main.async {
                    self.lblPaymentStatus.text = "You have Successfully \n paid for In-purchase store"
                }
                self.btnPayment.setTitle("Go Back", for: .normal)
                self.btnPayment.setTitle("Go Back", for: .selected)
            } else {
                self.imgResponse.image = .error.image
                if self.msg != "" {
                    self.lblPaymentStatus.text = self.msg
                } else {
                    self.lblPaymentStatus.text = "Seems like there was an issue\n with the payment.\nPlease try again."
                }
                self.btnPayment.setTitle("Try Again", for: .normal)
                self.btnPayment.setTitle("Try Again", for: .selected)
            }
            
        case .redeemGift:
            if self.paymentResponse == .success {
                self.imgResponse.image = .successGreen.image
                DispatchQueue.main.async {
                    self.lblPaymentStatus.lineBreakMode = .byWordWrapping
                    self.lblPaymentStatus.text = "Successfully redeemed \n"
                }
                self.btnPayment.setTitle("Go To My Product", for: .normal)
                self.btnPayment.setTitle("Go To My Product", for: .selected)
            } else {
                self.imgResponse.image = .error.image
                self.lblPaymentStatus.text = "Seems like there was an issue\n with the code redeem"
                self.btnPayment.setTitle("Try Again", for: .normal)
                self.btnPayment.setTitle("Try Again", for: .selected)
            }
            
        default:
            break
        }
            
        
        
        
        
//        if paymentResponse == .success {
//            self.imgResponse.image = .successGreen.image
//            self.lblPaymentStatus.text = "Your Purchase Is Successfully\n Completed!"
//            self.btnPayment.setTitle("Go Back", for: .normal)
//            self.btnPayment.setTitle("Go Back", for: .selected)
//        } else if paymentResponse == .failure {
//            self.imgResponse.image = .error.image
//            self.lblPaymentStatus.text = "Seems like there was an issue\n with the payment.\nPlease try again."
//            self.btnPayment.setTitle("Try Again", for: .normal)
//            self.btnPayment.setTitle("Try Again", for: .selected)
//        } else if self.paymentResponse == .redeemGift {
//            self.imgResponse.image = .successGreen.image
//            DispatchQueue.main.async {
//                self.lblPaymentStatus.lineBreakMode = .byWordWrapping
//                self.lblPaymentStatus.text = "Successfully redeemed \n"
//            }
//            self.btnPayment.setTitle("Go To My Product", for: .normal)
//            self.btnPayment.setTitle("Go To My Product", for: .selected)
//        } else if self.paymentResponse == .inAppPurchase {
//            self.imgResponse.image = .successGreen.image
//            DispatchQueue.main.async {
//                self.lblPaymentStatus.text = "You have Successfully \n paid for In-purchase store"
//            }
//            self.btnPayment.setTitle("Go Back", for: .normal)
//            self.btnPayment.setTitle("Go Back", for: .selected)
//        }
    }
    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })*/
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    @IBAction func btnPaymentClicked(_ sender: ThemeButton) {
        
        //        self.dismiss(animated: true, completion: nil)
        //        if self.paymentResponse == .redeemGift || self.paymentResponse == .inAppPurchase {
        
//        if self.paymentResponse == .redeemGift {
//            self.sheetViewController?.animateOut(duration: 0.0, completion: {
            
//            })
//        } else {
            
            self.completion?()
            self.sheetViewController?.animateOut()
//        }
        //        }
    }
    
    
    //------------------------------------------------------
    
}
