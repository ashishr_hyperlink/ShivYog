//
//  MyStoreVC.swift
//  ShivYog
//
//  Created by 2022M30 on 02/12/22.
//

import UIKit

class MyStoreVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var tblList: UITableView!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var homeVM : HomeViewModel = HomeViewModel()
    private var storeVM : StoreViewModel = StoreViewModel()
//    var arrData : [ProductMainModel] = []
//    var arrSubData : [ProductModel] = []
    var currentPage = 1
    var isCallingApi = true
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        debugPrint("load")
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        self.navigationController?.clearNavigation(textColor: .appFfffff, navigationColor: .app_990000)
        AppDelegate.shared.addAnalyticsEvent(screenName: .StoreHome, eventName: .Pageview, category: .StoreHome)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.clearNavigation(textColor: .appFfffff, navigationColor: .app_990000)
        if ((self.tabBarController as? TabVC)?.miniPlayer.isHidden ?? true){
            self.tblList.addFooterView(bottomSpace: 0)
        }
        else{
            self.tblList.addFooterView(bottomSpace: kMiniPlayerConstant)
        }
        self.tblList.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.clearNavigation()
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.setupViewModelObserver()
        self.applyStyle()
    }

    private func applyStyle(){
        self.tblList.registerNib(forCellWithClass: CategoriesCell.self)
//        self.tblList.registerNib(forCellWithClass: YogaStoreCell.self)
        self.tblList.delegate = self
        self.tblList.dataSource = self
        
//        self.homeVM.arrFeatured = [
//            SubCategoryModel(fromJson: JSON(["title" : HomePageTitle.FeaturedContent.rawValue, "category_content" : Array(repeating:JSON(["image" : UIImage.temp9.name, "title" : "Navaratri Special", "contentType" : ContentType.folder.rawValue]), count: 2)])),
//            SubCategoryModel(fromJson: JSON(["title" : HomePageTitle.WhatsNew.rawValue, "category_content" : Array(repeating:JSON(["image" : UIImage.temp5.name, "title" : "Shree Lalita Rudram", "contentType" : ContentType.folder.rawValue]), count: 2)]))
//        ]
//        for _ in 1..<10 {
//            self.arrData.append(ProductMainModel(title: "Kathopanishad Vol 1", image: .temp7.image, contentType: .folder, arrProducts: self.homeVM.arrProductModel))
//        }
        
        self.tblList.addRefreshControl {
            self.isCallingApi = true
            self.currentPage = 1
            self.fetchData(isLoader: false)
        }
        
        self.fetchData(isLoader: true)
    
    }
    
    private func fetchData(isLoader: Bool) {
        self.storeVM.getStoreContent(withLoader: isLoader, page: self.currentPage)
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })*/
        
        self.storeVM.isStoreResultFetched.bind { result in
            self.tblList.endRefreshing()
            if result ?? false {
                self.isCallingApi = true
                self.currentPage += 1
                self.tblList.reloadData()
            } else {
                self.isCallingApi = false
            }
            self.tblList.emptyDataSetSource = self
            self.tblList.emptyDataSetDelegate = self
        }
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    @IBAction func btnSearchClicked(_ sender: UIBarButtonItem) {
        let vc = StoryboardScene.Home.searchVC.instantiate()
        vc.isFrom = .yogaStore
        vc.modalPresentationStyle = .overFullScreen
//        vc.hidesBottomBarWhenPushed = true
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .overFullScreen
//        nav.awakeFromNib()
        self.navigationController?.present(nav, animated: true, completion: nil)
        
    }
    
    @IBAction func btnFilterClicked(_ sender: UIBarButtonItem) {
        let vc = StoryboardScene.Profile.preferencesVC.instantiate()
        vc.hidesBottomBarWhenPushed = true
        vc.isFrom = .yogaStore
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //------------------------------------------------------
    
}

//MARK: - UItableView delegate and Datasource -

extension MyStoreVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.storeVM.getStoreContentCount()//2//self.arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        
        let cell = tableView.dequeueReusableCell(withClass: CategoriesCell.self, for: indexPath)
        let dataAtIndex = self.storeVM.getStoreContentAtIndex(index: indexPath.row)
        cell.isFrom = .yogaStore
        
        cell.data = dataAtIndex//self.homeVM.arrFeatured[0]
//        cell.lblCategoryTitle.text = indexPath.row == 0 ? "Sri Vidhya (English)" : "Sri Vidhya (Hindi)"
//        cell.data = self.arrData
//        cell.isFrom = .categories
//        cell.index = indexPath.row
//        cell.btnSeeAll.isHidden = true
        cell.completion = { str, arr in
//            let vc = StoryboardScene.Home.productItemsVC.instantiate()
//            vc.screenTitle = str
//            vc.arrSubData = arr
////            vc.hidesBottomBarWhenPushed = true
//            vc.isFrom = .storePayment
//            self.navigationController?.pushViewController(vc, animated: true)
            let vc = StoryboardScene.Store.productDescriptionVC.instantiate()
//            vc.paymentResponse = indexPath.row % 2 == 0 ? .success : .failure
            vc.storeData = arr
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        cell.btnSeeAll.addTapGestureRecognizer {
            let vc = StoryboardScene.Home.productListingVC.instantiate()
            vc.screenTitle = dataAtIndex.title
            vc.isFrom = .yogaStore
            vc.subId = dataAtIndex.id
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        return cell
    }
}

extension MyStoreVC {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.isAtBottom {
            if self.isCallingApi {
                self.isCallingApi = false
                self.fetchData(isLoader: false)
            }
        }
    }
}
