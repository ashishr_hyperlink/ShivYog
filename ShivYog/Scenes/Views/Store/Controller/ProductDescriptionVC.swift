//
//  ProductDescriptionVC.swift
//  ShivYog
//
//  Created by 2022M30 on 05/12/22.
//

import UIKit
import RevenueCat

class ProductDescriptionVC: UIViewController {
    
    //MARK: - Outlet -
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescriptionTitle: UILabel!
    @IBOutlet weak var tvDescription: UITextView!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var imgTopCover: UIImageView!
    @IBOutlet weak var btnBuyNow: UIButton!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var homeVM : HomeViewModel = HomeViewModel()
    var paymentResponse : PaymentStatus = .success
    var storeData : CategoryContentModel?
    var profileVM : ProfileViewModel = ProfileViewModel()
//    var offerings : Offering?
//    var selectedPackage : Package?
//    var totalPackage : [Package]?
    var customerInfo : CustomerInfo?
    var storeProduct : StoreProduct?
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        AppDelegate.shared.addAnalyticsEvent(screenName: .StoreProductDetail, eventName: .Pageview, category: .StoreProductDetail, label: "\(self.storeData?.title ?? "")")
        self.getOfferingsAndUserInfo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.setupViewModelObserver()
        self.applyStyle()
    }

    private func applyStyle(){
        self.lblTitle.font(name: FontFamily.CircularStd.bold, size: 20.0).textColor(color: .appFfffff)
        self.lblDescriptionTitle.font(name: FontFamily.CircularStd.bold, size: 18.0).textColor(color: .app_222222)
        self.tvDescription.font(name: FontFamily.Inter.regular, size: 16.0).textColor(color: .app_000000)
        self.tvDescription.isEditable = false
        self.tvDescription.isSelectable = false
        self.lblTotalPrice.font(name: FontFamily.SFProDisplay.medium, size: 12.0).textColor(color: .appFfffff)
        self.lblAmount.font(name: FontFamily.SFProDisplay.bold, size: 14.0).textColor(color: .appFfffff)
        self.btnBuyNow.font(name: FontFamily.SFProDisplay.bold, size: 16.0).textColor(color: .app_990000, state: .normal).backGroundColor(color: .appFfffff)
        DispatchQueue.main.async {
            self.imgProduct.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 20.0)
            self.imgTopCover.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 20.0)
            self.btnBuyNow.setRound()
        }
        self.setData()
        
    }
    
    private func setData() {
        if let storeData = storeData {
            self.imgProduct.setImage(with: storeData.image)
            print("title:-", storeData.title)
            self.lblTitle.text = storeData.title
            self.tvDescription.text = storeData.description
            
            if UserModel.isIndianCountryCode() {
                self.lblAmount.text = "Amount: %@ ₹".localized(storeData.inrPrice)
            } else {
                self.lblAmount.text = "Amount: %@ $".localized(storeData.dollarPrice)
            }
        }
    }
    
    private func getOfferingsAndUserInfo() {
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         
         })*/
        if let storeData = storeData {
            AppLoader.shared.addLoader()
            debugPrint("Store Product BundleId:-",storeData.iosBundleId as Any)
            Purchases.shared.getProducts([storeData.iosBundleId]) { items in
                print("Products items count:-",items.count)
                for item in items {
                    print("Products item:-",item)
//                    self.selectedPackage = item
                    debugPrint(item.productIdentifier)
                    self.storeProduct = item
                    break
                }
                if AppLoader.shared.isLoaderAnimating() {
                    AppLoader.shared.removeLoader()
                }
            }
        }
        
//        Purchases.shared.getOfferings { offers, error in
//            if let error = error {
//                print("offering error \(error)")
//            }
//
//            print("all offering count:-",offers?.current)
//            self.offerings = offers?.current
//
//            if let offers = offers {
//                if let current = offers.current {
//                    for item in current.availablePackages {
//                        if let storeData = self.storeData, storeData.iosBundleId == item.storeProduct.productIdentifier {
//                            debugPrint("Current package title:- ",item.storeProduct.localizedTitle)
//                            debugPrint("Current package:- ",item)
//                            self.selectedPackage = item
//
//                            break
//                        }
//                    }
//                }
//            }
////            if let offerings = self.offerings {
////                for item in offerings.availablePackages {
//
////                }
////            }
//        }
        
        Purchases.shared.getCustomerInfo { userInfo, error in
            print("UserInfo:-", userInfo)
            self.customerInfo = userInfo
            if let error = error {
                print("UserInfo error:-",error)
            }
        }
        
        self.profileVM.addStorePayment.bind { result in
            
            if result ?? false {
                //                self.navigationController?.popToRootViewController(animated: true)
                //                self.navigationController?.popViewController(animated: true)
                self.goToPaymentResponseScreen(response: .success)
                
            } else {
                debugPrint("message")
            }
        }
    }
    
    private func goToPaymentResponseScreen(response: PaymentStatus, msg: String = "") {
        let vc = StoryboardScene.Store.paymentResponseVC.instantiate()
        vc.paymentResponse = response
        vc.isFromPaymentScreen = .inStore
        vc.msg = msg
        vc.modalPresentationStyle = .overFullScreen
        vc.completion = {
            if response == .success {
                self.navigationController?.popViewController(animated: true)
            }
        }
        self.openBottomSheetAnimate(vc: vc, sizes: [.intrinsic])//navigationController?.present(vc, animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    @IBAction func btnBuyNowClicked(_ sender: UIButton) {
        AppDelegate.shared.addAnalyticsEvent(screenName: .StoreProductDetail, eventName: .BuyNow, category: .Purchase, label: "\(self.storeData?.title ?? "")")
        AppLoader.shared.addLoader()
        //        let vc = StoryboardScene.Store.paymentResponseVC.instantiate()
        //        vc.paymentResponse = self.paymentResponse
        //        vc.modalPresentationStyle = .overFullScreen
        //        self.openBottomSheetAnimate(vc: vc, sizes: [.intrinsic])//navigationController?.present(vc, animated: true, completion: nil)
        //        self.profileVM.apiStorePayment(withLoader: true, isAddData: true, parameter: ["key" : "Value"])
//        if let selectedPackage = selectedPackage {
//            Purchases.shared.purchase(package: selectedPackage) { transcation, customerInfo, error, isCancelled in
//                if let error = error {
//                    print("purchase error:-", error)
//                }
//                print("Purchase is Cancelled?:-",isCancelled)
//                print("Purchase transaction:-", transcation)
//                print("Purchase customerInfo:-", customerInfo)
//
//            }
//        }
        
        if let storeProduct = storeProduct {
            if AppLoader.shared.isLoaderAnimating() {
                AppLoader.shared.removeLoader()
            }
            
            Purchases.shared.purchase(product: storeProduct) { transaction, customerInfo, error, isCancelled in
                
                
                if let error = error as? RevenueCat.ErrorCode {
                    
//                    AppDelegate.shared.addAnalyticsEvent(screenName: .StoreProductDetail, eventName: .PurchaseFailed, category: .Purchase, label: self.storeData?.title ?? "")
//                    self.goToPaymentResponseScreen(response: .failure)
                    print("Product Error:-", error)
                    switch error {
                    case .purchaseCancelledError:
//                        self.goToPaymentResponseScreen(response: .failure, msg: "Payment Cancelled.")
                        break
                    case .networkError:
                        self.goToPaymentResponseScreen(response: .failure, msg: "Network issue \n Please check your network connection.")
                        break
                    case .paymentPendingError:
                        self.goToPaymentResponseScreen(response: .failure)
                        break
                    default:
                        break
                    }
                    
                }
                
                if let trans = transaction {
                    
                    if let skPayment = trans.sk1Transaction {
                        debugPrint("Transaction:-", dump(transaction) as Any)
                        if skPayment.transactionState == .purchased {
                            debugPrint("SK1 Transaction:- Purchased")
                            
                            var dictData = Dictionary<String,String>()
                            dictData["storesubcontent"] = self.storeData?.id.description
                            dictData["paymentid"] = trans.transactionIdentifier
                            
                            if UserModel.isIndianCountryCode() {
                                dictData["payment_currency"] = "INR"
                                dictData["price"] = self.storeData?.inrPrice
                            } else {
                                dictData["payment_currency"] = "Dollar"
                                dictData["price"] = self.storeData?.dollarPrice
                            }
                            AppDelegate.shared.addAnalyticsEvent(screenName: .StoreProductDetail, eventName: .PurchaseSuccessful, category: .Purchase, label: self.storeData?.title ?? "")
                            self.profileVM.apiStorePayment(withLoader: true, isAddData: true, parameter: dictData)
                            
                            
                        } else if skPayment.transactionState == .failed {
                            debugPrint("SK1 Transaction:- Failed")
                            AppDelegate.shared.addAnalyticsEvent(screenName: .StoreProductDetail, eventName: .PurchaseFailed, category: .Purchase, label: self.storeData?.title ?? "")
                            
                        }
                    }
//                    var dictData = Dictionary<String,String>()
//                    dictData["storesubcontent"] = self.storeData?.id.description
//                    dictData["paymentid"] = trans.transactionIdentifier
//
//                    if UserModel.isIndianCountryCode() {
//                        dictData["payment_currency"] = "INR"
//                        dictData["price"] = self.storeData?.inrPrice
//                    } else {
//                        dictData["payment_currency"] = "Dollar"
//                        dictData["price"] = self.storeData?.dollarPrice
//                    }
//                    AppDelegate.shared.addAnalyticsEvent(screenName: .StoreProductDetail, eventName: .PurchaseSuccessful, category: .Purchase, label: self.storeData?.title ?? "")
//                    self.profileVM.apiStorePayment(withLoader: true, isAddData: true, parameter: dictData)
                }
//                print("StoreProduct is Cancelled?:-",dump(isCancelled))
//                print("StoreProduct transaction:-",dump(transaction))
//                print("StoreProduct customerInfo:-",dump(customerInfo) )
            }
        } else {
            Alert.shared.showSnackBar("Something went wrong.")
        }
        
    }
    
    @IBAction func btnInfoClicked(_ sender: UIBarButtonItem) {
        let vc = StoryboardScene.Home.productItemsVC.instantiate()
        vc.screenTitle = self.lblTitle.text!
        vc.isFrom = .yogaStore
        vc.selCat = self.storeData
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //------------------------------------------------------
    
}
