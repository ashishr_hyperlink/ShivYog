//
//  CreatePasswordVC.swift
//  ShivYog
//
//  Created by 2022M30 on 25/11/22.
//

import UIKit

class CreatePasswordVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var lblCreatePassword: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var txtNewPassword: GradientTextField!
    @IBOutlet weak var txtConfirmPassword: GradientTextField!
    @IBOutlet weak var btnEyeNew: UIButton!
    @IBOutlet weak var btnEyeConfirm: UIButton!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var authVM : AuthViewModel = AuthViewModel()
    var userData : JSON = JSON.null
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.lblCreatePassword.font(name: FontFamily.SFProDisplay.medium, size: 28.0).textColor(color: .app_000000)
        self.lblSubTitle.font(name: FontFamily.SFProDisplay.regular, size: 15.0).textColor(color: .app_000000.withAlphaComponent(0.5))
        
        self.txtNewPassword.rightView = self.btnEyeNew
        self.txtNewPassword.rightViewMode = .always
        
        self.txtConfirmPassword.rightView = self.btnEyeConfirm
        self.txtConfirmPassword.rightViewMode = .always
        
        self.btnEyeNew.addTapGestureRecognizer {
            self.btnEyeNew.isSelected.toggle()
            self.txtNewPassword.isSecureTextEntry = !self.btnEyeNew.isSelected
        }
        
        self.btnEyeConfirm.addTapGestureRecognizer {
            self.btnEyeConfirm.isSelected.toggle()
            self.txtConfirmPassword.isSecureTextEntry = !self.btnEyeConfirm.isSelected
        }
        
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        self.authVM.createPassResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
             let vc = StoryboardScene.Authentication.successVC.instantiate()
             vc.verificationType = .forgotPassword
             vc.showSignInButton = true
             vc.strTitle = "Password Changed"
             vc.strSubTitle = "Congratulations! You have successfully\nchanged your password."
             vc.showSignInButton = true
             vc.modalPresentationStyle = .overFullScreen
             self.navigationController?.present(vc, animated: true, completion: nil)
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    
    @IBAction func btnUpdatePasswordClicked(_ sender: ThemeButton) {
        self.authVM.apiCreatePassword(newPassword: self.txtNewPassword.text!, confirmPassword: self.txtConfirmPassword.text!, phone: self.userData["phone"].stringValue)
//        UIApplication.popTo(viewController: SignInVC.self)
    }
    
    //------------------------------------------------------
    
}
