//
//  NoNetworkVC.swift
//  ShivYog
//
//  Created by 2022M30 on 19/12/22.
//

import UIKit

class NoNetworkVC: UIViewController {
    
    //MARK: - Outlet

    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var btnSettings: UIButton!
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var homeVM : HomeViewModel = HomeViewModel()
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.lblMsg.font(name: FontFamily.SFProDisplay.bold, size: 24.0).textColor(color: .app_000000)
        self.btnSettings.borderColor(color: .app_990000, borderWidth: 1.0).font(name: FontFamily.SFProDisplay.bold, size: 16.0).textColor(color: .app_990000, state: .normal)
        DispatchQueue.main.async {
            
            self.btnSettings.cornerRadius(cornerRadius: self.btnSettings.frame.height / 2)
        }
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })*/
    }
    
    //------------------------------------------------------
    
    //MARK: - Action methods -
    @IBAction func btnDownloadsClicked(_ sender: ThemeButton) {
        //        self.sheetViewController?.animateOut()
        self.sheetViewController?.animateOut(duration: 0.3, completion: {
            
            if let topVC = UIApplication.topViewController() as? ProductItemsVC , topVC.isFrom == .downloads {
                topVC.fetchData()
            } else {
                
                if let tabbar = self.tabBarController {
                    //                let vc = StoryboardScene.Home.productListingVC.instantiate()
                    //                vc.isFrom = .downloads
                    //                vc.screenTitle = "Downloads"
                    //                vc.hidesBottomBarWhenPushed = true
                    //                tabbar.navigationController?.pushViewController(vc, animated: true)
                    
                    let vc = StoryboardScene.Home.productItemsVC.instantiate()
                    //            vc.hidesBottomBarWhenPushed = true
                    vc.screenTitle = "Downloads"
                    vc.isFrom = .downloads
                    vc.downloadMainModel = UserDefaults.standard.decode(for: DownloadMainModel.self, using: UserDefaults.Keys.downloadContent)
                    vc.hidesBottomBarWhenPushed = true
                    //            vc.isFromPlaylist = true
                    //            vc.contentType = .normal
                    tabbar.navigationController?.pushViewController(vc, animated: true)
                } else {
                    //                let vc = StoryboardScene.Home.productListingVC.instantiate()
                    //                vc.isFrom = .downloads
                    //                vc.screenTitle = "Downloads"
                    //                vc.hidesBottomBarWhenPushed = true
                    //                UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
                    
                    let vc = StoryboardScene.Home.productItemsVC.instantiate()
                    //            vc.hidesBottomBarWhenPushed = true
                    vc.screenTitle = "Downloads"
                    vc.isFrom = .downloads
                    //                vc.downloadMainModel = UserDefaults.standard.decode(for: DownloadMainModel.self, using: UserDefaults.Keys.downloadContent)
                    vc.hidesBottomBarWhenPushed = true
                    //            vc.isFromPlaylist = true
                    //            vc.contentType = .normal
                    UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
            
            
        })
        
        //        self.sheetViewController?.animateOut()
        //            Alert.shared.showSnackBar("Under Development")
        //        let vc = StoryboardScene.MySpace.playlistVC.instantiate()
        //        vc.hidesBottomBarWhenPushed = true
        //            UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
        //        self.sheetViewController.ani
        //        let vc = StoryboardScene.Home.productListingVC.instantiate()
        //        vc.isFrom = .downloads
        //        vc.screenTitle = "Downloads"
        //        vc.hidesBottomBarWhenPushed = true
        //        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSettingsClicked(_ sender: UIButton) {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }
    
    
    //------------------------------------------------------
    
}
