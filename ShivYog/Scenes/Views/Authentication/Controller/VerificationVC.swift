//
//  OTPVC.swift
//  ShivYog
//
//  Created by 2022M30 on 25/11/22.
//

import UIKit

class VerificationVC: UIViewController {
    
    //MARK: - Outlet

    @IBOutlet weak var lblVerificationTitle: UILabel!
    @IBOutlet weak var lblVerificationSubTitle: UILabel!
    @IBOutlet weak var lblResendCode: UILabel!
    @IBOutlet weak var txtOne: OTPTextField!
    @IBOutlet weak var txtTwo: OTPTextField!
    @IBOutlet weak var txtThree: OTPTextField!
    @IBOutlet weak var txtFour: OTPTextField!
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var authVM : AuthViewModel = AuthViewModel()
    private var profileVM : ProfileViewModel = ProfileViewModel()
    var isFrom : IsFrom = .signIn
    var userData : JSON = JSON.null
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.lblVerificationTitle.font(name: FontFamily.SFProDisplay.medium, size: 28.0).textColor(color: .app_000000)
        self.lblVerificationSubTitle.font(name: FontFamily.SFProDisplay.regular, size: 15.0).textColor(color: .app_000000)
        self.lblResendCode.font(name: FontFamily.SFProDisplay.regular, size: 18.0).textColor(color: .app_222222)

        self.lblResendCode.attributedText = self.lblResendCode.text?.getAttributedText(defaultDic: [.font: self.lblResendCode.font!, .foregroundColor : self.lblResendCode.textColor!], attributeDic: [.font : FontFamily.SFProDisplay.regular.font(size: 18.0)!, .foregroundColor: UIColor.app_990000, .underlineStyle : 1.0, .underlineColor : UIColor.app_990000], attributedStrings: ["Resend Code"])

        let tapTermsCondition = UITapGestureRecognizer(target: self, action: #selector(lblResendCodeTapped(_:)))
        self.lblResendCode.isUserInteractionEnabled = true
        self.lblResendCode.addGestureRecognizer(tapTermsCondition)
        
//        self.lblVerificationSubTitle.text = self.isFrom == .forgotPassword ? "Please type the verification code sent to\n+91 95851 65428 and  cel******@gm*******" : "Please enter your mobile number. You will get a\nOTP on provided mobile number."
        
        if self.isFrom == .forgotPassword {
            var numberEmail = self.userData["country_code"].stringValue + " " + self.userData["phone"].stringValue
            if !self.userData["email"].stringValue.isEmpty{
                numberEmail += " and " + self.userData["email"].stringValue//.maskEmail
            }
            self.lblVerificationSubTitle.text = "Please type the verification code sent to\n%@".localized(numberEmail)
        } /*else if self.isFrom == .signIn {
            self.lblVerificationSubTitle.text = "Please enter your mobile number. You will get a\nOTP on provided mobile number."
        }*/ else/* if self.isFrom == .changeMobile */{
            self.lblVerificationSubTitle.text = "Please type the verification code sent to\nyour moblie number"
        }
        
    }
    
    
    @objc func lblResendCodeTapped(_ gesture: UITapGestureRecognizer) {

        if gesture.didTapAttributedTextInLabel(label: self.lblResendCode, inRange: (self.lblResendCode.attributedText!.string as NSString).range(of: "Resend Code")) {
            
            if isFrom == .changeMobile{
                self.profileVM.apiChangeMobile(code: self.userData["country_code"].stringValue, mobile: self.userData["phone_number"].stringValue)
            }
            else{
                self.authVM.resendVerificationCode(phone: self.userData["phone"].stringValue)
            }
            
          }
      }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        self.authVM.otpVerification.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
             if self.isFrom == .signIn {
                 let vc = StoryboardScene.Authentication.successVC.instantiate()
                 vc.strTitle = "Verification Done!"
                 vc.strSubTitle = "Congratulations! You have successfully verified."
                 vc.modalPresentationStyle = .overFullScreen
                 vc.completion = {
                     if UserModel.currentUser?.isReachable ?? false {
                         var cData = UserModel.currentUser
                         cData?.otp = self.txtOne.text! + self.txtTwo.text! + self.txtThree.text! + self.txtFour.text!
                         UserModel.currentUser = cData
                         let vc2 = StoryboardScene.Authentication.loggedInDevicesVC.instantiate()
                         vc2.modalPresentationStyle = .overFullScreen
                         let nav = UINavigationController(rootViewController: vc2)
                         nav.modalPresentationStyle = .overFullScreen
                         nav.awakeFromNib()
                         self.navigationController?.present(nav, animated: true, completion: nil)
                     }
                     else{
                         UserModel.accessToken = UserModel.currentUser?.token ?? nil
                         UIApplication.shared.manageLogin(true)
                     }
                 }
                 self.navigationController?.present(vc, animated: true, completion: nil)
             } else if self.isFrom == .forgotPassword {
                 let vc = StoryboardScene.Authentication.createPasswordVC.instantiate()
                 vc.userData = self.userData
                 self.navigationController?.pushViewController(vc, animated: true)
             } else if self.isFrom == .changeMobile {
                 Alert.shared.showSnackBar("Mobile number changed successfully")
                 
                 DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
                     self.navigationController?.popToRootViewController(animated: true)
                 }
             }
             
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })
        
        self.authVM.resendOtpVerification.bind { [unowned self] result in
            switch result{
            case .success(_):
                Alert.shared.showSnackBar("Verification code resent successfully")
                break
            case .failure(_):
                break
            case .none: break
            }
        }
        
        self.profileVM.changeMobileResult.bind { [unowned self] result in
            switch result{
            case .success(_):
                Alert.shared.showSnackBar("Verification code resent successfully")
                break
            case .failure(_):
                break
            case .none: break
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    
    @IBAction func btnSubmitClicked(_ sender: ThemeButton) {
        self.authVM.apiVerification(txtOne: self.txtOne.text!, txtTwo: self.txtTwo.text!, txtThree: self.txtThree.text!, txtFour: self.txtFour.text!, from : self.isFrom, phone: self.userData["phone"].stringValue)
    }
    
    //------------------------------------------------------
    
}
