//
//  SuccessVC.swift
//  ShivYog
//
//  Created by 2022M30 on 25/11/22.
//

import UIKit

class SuccessVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var lblDone: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnSignIn: ThemeButton!
    @IBOutlet weak var btnDone: ThemeButton!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
//    private var viewModel : AuthViewModel = AuthViewModel()
    var verificationType : VerificationType = .signIn
    var strTitle : String = ""
    var strSubTitle: String = ""
    var showSignInButton : Bool = false
    var showDoneButton : Bool = false
    var completion : (()-> Void)?
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        DispatchQueue.main.async {
            self.btnSignIn.setRound()
            self.btnDone.setRound()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.btnSignIn.superview?.isHidden = !self.showSignInButton//self.verificationType == .signIn
        self.btnDone.isHidden = !self.showDoneButton
        self.lblDone.font(name: FontFamily.SFProDisplay.medium, size: 28.0).textColor(color: .appFfffff)
        self.lblMessage.font(name: FontFamily.SFProDisplay.regular, size: 15.0).textColor(color: .appFfffff).lineSpacing()
//        self.lblMessage.text = self.verificationType == .signIn ? "Congratulations! You have successfully\nverified." : "Congratulations! You have successfully\nchanged your password."
        self.lblDone.text = self.strTitle
        self.lblMessage.text = self.strSubTitle
        
        if !self.showSignInButton && !self.showDoneButton {
            DispatchQueue.main.asyncAfter(deadline: .now()+5) {
                self.dismiss(animated: true) {
                    self.completion?()
                }
            }
        }
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })*/
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    @IBAction func btnBackSignInClicked(_ sender: ThemeButton) {
        self.dismiss(animated: true) {
            UIApplication.popTo(viewController: SignInVC.self)
        }
//        self.completion?()
    }
    
    @IBAction func btnDoneClicked(_ sender: ThemeButton) {
//        UIApplication.popTo(viewController: PlaylistVC.self)
        self.dismiss(animated: true) {
            
            self.completion?()
        }
//        self.dismiss(animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
}
