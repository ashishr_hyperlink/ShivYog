//
//  SignInVC.swift
//  ShivYog
//
//  Created by 2022M30 on 24/11/22.
//

import UIKit

class SignInVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var lblSignIn: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    @IBOutlet weak var txtPhone: GradientTextField!
    @IBOutlet weak var txtPassword: GradientTextField!
    @IBOutlet weak var lblContactUs: UILabel!
    @IBOutlet weak var lblContinueWithPassword: UILabel!
    

    @IBOutlet weak var btnMobile: UIButton!
    @IBOutlet weak var btnEye: UIButton!
    @IBOutlet weak var vwCode: UIView!
    @IBOutlet weak var lblCode: UILabel!
    
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var authVM : AuthViewModel = AuthViewModel()
    var isShowPassword : Bool = false
    var tapCount = 1
    var countryCode : String!
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isShowPassword = false
        self.lblContinueWithPassword.text = "Continue with Password?"
        self.txtPassword.isHidden = !self.isShowPassword
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        
        self.lblSignIn.font(name: FontFamily.SFProDisplay.medium, size: 36.0).textColor(color: .app_000000)
        self.lblSubTitle.font(name: FontFamily.SFProDisplay.regular, size: 14.0).textColor(color: .app_000000).lineSpacing()
        
        self.lblContinueWithPassword.font(name: FontFamily.SFProDisplay.regular, size: 14.0).textColor(color: .app_000000)
        self.lblContactUs.font(name: FontFamily.SFProDisplay.regular, size: 14.0).textColor(color: .app_000000)
        self.lblContactUs.attributedText = self.lblContactUs.text?.getAttributedText(defaultDic: [.font : self.lblContactUs.font!, .foregroundColor : self.lblContactUs.textColor!], attributeDic: [.font : FontFamily.SFProDisplay.bold.font(size: 14.0)!, .foregroundColor : UIColor.app_990000], attributedStrings: ["Contact Us"])
        
        let tapContactUs = UITapGestureRecognizer(target: self, action: #selector(lblTermsConditionTapped(_:)))
        self.lblContactUs.isUserInteractionEnabled = true
        self.lblContactUs.addGestureRecognizer(tapContactUs)
        
        self.lblCode.font(name: FontFamily.SFProDisplay.bold, size: 16.0).textColor(color: .app_000000)
        self.vwCode.borderColor(color: .app_990000, borderWidth: 1.0)
        
        self.txtPhone.rightView = self.btnMobile
        self.txtPhone.rightViewMode = .always
        self.txtPhone.delegate = self
        
        self.txtPassword.rightView = self.btnEye
        self.txtPassword.rightViewMode = .always
        self.txtPassword.isHidden = !self.isShowPassword
        self.txtPassword.isSecureTextEntry = true
        self.txtPassword.delegate = self
        self.btnEye.isSelected = true
        let currentCountryCode = GFunction.shared.getCurrentCountryCode()
        let bundle = Bundle.init(for: LocalePickerViewController.self)
        self.lblCode.text = currentCountryCode.0
        
        DispatchQueue.main.async {
            self.vwCode.setRound()
            self.vwCode.setGradient()
        }
        
        self.setTapGesture()
    }
    
    @objc func lblTermsConditionTapped(_ gesture: UITapGestureRecognizer) {
          
        if gesture.didTapAttributedTextInLabel(label: self.lblContactUs, inRange: (self.lblContactUs.attributedText!.string as NSString).range(of: "Contact Us")) {
              let vc = StoryboardScene.Profile.contactUsVC.instantiate()
              self.navigationController?.pushViewController(vc, animated: true)
          }
      }
    
    private func setTapGesture() {
        
        self.lblContinueWithPassword.addTapGestureRecognizer {
            
            self.isShowPassword.toggle()
            self.txtPassword.isHidden = !self.isShowPassword
            
            if self.lblContinueWithPassword.text! == "Forgot Password ?" {
                let vc = StoryboardScene.Authentication.forgotPasswordVC.instantiate()
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            self.lblContinueWithPassword.text = self.isShowPassword ? "Forgot Password ?" : "Continue with Password?"
            //            self.isShowPassword.toggle()
//            if /*self.isShowPassword && */self.tapCount == 2 {
//                let vc = StoryboardScene.Authentication.forgotPasswordVC.instantiate()
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
//            self.tapCount = self.tapCount + 1
            //            self.isShowPassword.toggle()
        }
        
        self.vwCode.addTapGestureRecognizer {
            let alert = UIAlertController(style: .actionSheet,  title: "Select country code", tintColor: .app_990000)
            alert.addLocalePicker(type: .phoneCode) { info in
                if let info = info {
                    self.lblCode.text = info.phoneCode
                    self.countryCode = info.phoneCode
                    
                    DispatchQueue.main.async {
                        guard let sublayers = self.vwCode.layer.sublayers else { return }
                        for sublayer in sublayers where sublayer.isKind(of: CAGradientLayer.self) {
                                sublayer.removeFromSuperlayer()
                        }
                        self.txtPhone.completion?()
                        self.vwCode.setGradient()
                        
//                        guard let txtSublayers = self.txtEmail.layer.sublayers else { return }
//                        for txtSublayer in txtSublayers where txtSublayer.isKind(of: CAGradientLayer.self) {
//                            txtSublayer.removeFromSuperlayer()
//                        }
//
//                        self.txtEmail.setGradient(isBorder: true)
                    }
                }
            }
            alert.addAction(title: "Cancel", style: .destructive)
            alert.show()
        }
        
        self.btnEye.addTapGestureRecognizer {
            self.btnEye.isSelected.toggle()
            self.txtPassword.isSecureTextEntry = self.btnEye.isSelected
        }
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        self.authVM.signInResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
             if self.isShowPassword {
                 if UserModel.currentUser?.isReachable ?? false {
                     var cData = UserModel.currentUser
                     cData?.password = self.txtPassword.text!
                     UserModel.currentUser = cData
                     let vc = StoryboardScene.Authentication.loggedInDevicesVC.instantiate()
                     vc.modalPresentationStyle = .overFullScreen
                     let nav = UINavigationController(rootViewController: vc)
                     nav.modalPresentationStyle = .overFullScreen
                     nav.awakeFromNib()
                     vc.completion = {

                         if UserDefaultsConfig.firstTimePreferences {
                             let vc = StoryboardScene.Profile.preferencesVC.instantiate()
                             vc.isFrom = .signIn
                             self.navigationController?.pushViewController(vc, animated: true)
                         } else {
                             UserDefaultsConfig.isShowTutorial = true
//                             UserDefaultsConfig.isAuthorization = true
                             UIApplication.shared.manageLogin(true)
                         }
                     }
                     self.navigationController?.present(nav, animated: true, completion: nil)
                 }
                 else{
                     UserModel.accessToken = UserModel.currentUser?.token ?? nil
                     UIApplication.shared.manageLogin(true)
                 }
             } else {
                 
                 let vc = StoryboardScene.Authentication.verificationVC.instantiate()
                 vc.userData = JSON(["phone" : self.lblCode.text! + self.txtPhone.text!])
                 self.navigationController?.pushViewController(vc, animated: true)
             }
             
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    
    
    @IBAction func btnSignInClicked(_ sender: ThemeButton) {
        self.view.endEditing(true)
        self.authVM.apiSignIn(isPassword: self.isShowPassword, code: self.lblCode.text!, phone: self.txtPhone.text!, password: self.txtPassword.text ?? "")
    }
    //------------------------------------------------------
    
}


//MARK: - UITextfield delegate -

extension SignInVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if GFunction.shared.isBackspace(string) {
            return true
        }
        
        
        if textField == self.txtPhone {
            if self.txtPhone.text!.count >=  ValidationEnum.PhoneNumber.Maximum.rawValue {
                return false
            } else if textField == txtPhone {
                return string.isValid(.number)
            }
        }
        
//        if textField == self.txtPassword {
//            return string.isValid(.password)
//        }
        
        return !string.contains("")
    }
}
