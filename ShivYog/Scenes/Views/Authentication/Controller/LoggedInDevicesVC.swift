//
//  LoggedInDevicesVC.swift
//  ShivYog
//
//  Created by 2022M30 on 29/11/22.
//

import UIKit

class LoggedInDevicesVC: UIViewController {
    
    //MARK: - Outlet -
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDevicesCount: UILabel!
    @IBOutlet weak var lblLogoutFromOne: UILabel!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var btnContinue: ThemeButton!
    @IBOutlet var tblList: IntrinsicTableView!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var viewModel : AuthViewModel = AuthViewModel()
    var isDeviceOneSelected : Bool = true
    var isFrom : IsFrom = .downloads
    var completion : (()-> Void)?
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.clearNavigation(textColor: .appFfffff, navigationColor: .app_990000)
        self.navigationItem.title = "Your Devices"
        AppDelegate.shared.addAnalyticsEvent(screenName: .LoggedinDevices, eventName: .Pageview, category: .LoggedinDevices)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        DispatchQueue.main.async {
            self.btnContinue.setRound()
            self.btnLogout.setRound()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        
        self.lblTitle.font(name: FontFamily.SFProDisplay.bold, size: 18.0).textColor(color: .appFfffff)
        self.lblDevicesCount.font(name: FontFamily.SFProDisplay.medium, size: 18.0).textColor(color: .app_990000)
        self.lblLogoutFromOne.font(name: FontFamily.SFProDisplay.medium, size: 18.0).textColor(color: .app_222222)
        
        self.btnLogout.font(name: FontFamily.SFProDisplay.bold, size: 16.0).textColor(color: .app_990000, state: .normal)
        self.btnLogout.borderColor(color: .app_990000, borderWidth: 1.0)

        self.tblList.registerNib(forCellWithClass: LoggedInDevicesCell.self)
        
        self.btnLogout.isHidden = self.isFrom == .profile
//        self.btnContinue.isHidden = self.isFrom == .profile
        
        self.lblDevicesCount.text = "You are currently logged into %@ other devices".localized(self.viewModel.numberOfRowsForDevice(!(self.isFrom == .profile)).description)
        
        if self.isFrom == .profile{
            let barButton = UIBarButtonItem(image: UIImage.cancelWhite.image, style: .plain, target: self, action: #selector(self.dismss))
            self.navigationItem.rightBarButtonItem = barButton
            self.viewModel.getDeviceList()
            self.tblList.isHidden = true
            self.lblDevicesCount.isHidden = true
            self.lblLogoutFromOne.isHidden = true
        }
        else{
            self.tblList.dataSource = self
            self.tblList.delegate = self
        }
    }
    
    @objc func dismss(){
        UIApplication.topViewController()?.navigationController?.clearNavigation(textColor: .app_222222, navigationColor: .clear)
        self.dismiss(animated: true) {
            UIApplication.topViewController()?.navigationController?.clearNavigation(textColor: .app_222222, navigationColor: .clear)
        }
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
        
        self.viewModel.signInResult.bind(observer: { [unowned self] (result) in
            switch result {
            case .success(_):
                // Redirect to next screen or home screen
                //                UIApplication.shared.setHome()
                
                if UserModel.currentUser?.isReachable ?? false {
                    let vc = StoryboardScene.Authentication.loggedInDevicesVC.instantiate()
                    vc.modalPresentationStyle = .overFullScreen
                    vc.completion = {
                        
                        if UserDefaultsConfig.firstTimePreferences {
                            let vc = StoryboardScene.Profile.preferencesVC.instantiate()
                            vc.isFrom = .signIn
                            self.navigationController?.pushViewController(vc, animated: true)
                        } else {
                            UserDefaultsConfig.isShowTutorial = true
                            UserDefaultsConfig.isAuthorization = true
                            UIApplication.shared.manageLogin(true)
                        }
                    }
                    self.navigationController?.present(vc, animated: true, completion: nil)
                }
                else{
                    UserModel.accessToken = UserModel.currentUser?.token ?? nil
                    UIApplication.shared.manageLogin(true)
                }
                
                break
                
            case .failure(let error):
                Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
                
            case .none: break
            }
        })
        
        viewModel.isDeviceFetchComplete.bind { [unowned self] result in
            if result ?? false{
                self.lblDevicesCount.text = "You are currently logged into %@ other devices".localized(self.viewModel.numberOfRowsForDevice(!(self.isFrom == .profile)).description)
                self.tblList.isHidden = false
                self.lblDevicesCount.isHidden = false
                self.lblLogoutFromOne.isHidden = false
                self.tblList.dataSource = self
                self.tblList.delegate = self
                self.tblList.reloadData()
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    
    @IBAction func btnLogoutClicked(_ sender: UIButton) {
//        if self.isFrom != .signIn {
//            self.dismiss(animated: true, completion: nil)
//        } else {

//            self.completion?()
        self.dismiss(animated: true) {
            UIApplication.popTo(viewController: SignInVC.self)
        }
        
//        }
        
        
    }
    
    @IBAction func btnContinueClicked(_ sender: ThemeButton) {
        if let data = self.viewModel.getSelectedDevice(){
            guard let userData = UserModel.currentUser else{return}
            var dictData = Dictionary<String, Any>()
            dictData["phone"] = userData.phone
            dictData["continues"] = "True"
            if let password = userData.password, !password.isEmpty{
                dictData["password"] = userData.password
                dictData["login_type"] = "simple"
            }
            else if let otp = userData.otp, !otp.isEmpty{
                dictData["otp"] = userData.otp
                dictData["login_type"] = "otp"
            }
            dictData["exist_device_id"] = data.id
            
            dictData["device_type"] = DeviceManager.shared.deviceType
            dictData["device_name"] = DeviceManager.shared.DeviceName
            dictData["model_name"] = DeviceManager.shared.modelName
            dictData["imei_number"] = DeviceManager.shared.uuid
            
            if self.isFrom == .profile{
                self.viewModel.logoutDevice(deviceData: data)
            }
            else{
                self.viewModel.apiLogin(parameter: dictData, completion: self.viewModel.signInResult)
            }
            
        }
        else{
            Alert.shared.showSnackBar("Please select the device to logout")
        }
//        self.dismiss(animated: true, completion: nil)
//        self.completion?()

    }
    
    //------------------------------------------------------
    
}

extension LoggedInDevicesVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRowsForDevice(!(self.isFrom == .profile))
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: LoggedInDevicesCell.self)
        let dataAtIndex = self.viewModel.dataAtIndexForDevice(!(self.isFrom == .profile), index: indexPath.row)
        cell.data = dataAtIndex
        cell.lblTitle.text = "Device %@".localized((indexPath.row + 1).description)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.updateDeviceDetailsAtIndex(!(self.isFrom == .profile), index: indexPath.row)
        self.tblList.reloadData()
    }
}
