//
//  AuthViewModel.swift
//  ShivYog
//
//  Created by 2022M30 on 24/11/22.
//

import Foundation
import UIKit

class WalkThroughModel {
    var image : UIImage!
    var title : String!
    var subTitle : String!
    
    init(image: UIImage, title: String, subTitle: String) {
        self.image = image
        self.title = title
        self.subTitle = subTitle
    }
}

class AuthViewModel {
    
    private var arrAuthModel : [WalkThroughModel] = [
        WalkThroughModel(image: .walkOne.image, title: "Workout List", subTitle: "Now is as good a time as any to focus on getting your\n body into the best shape possible. if you really want\n make something happen."),
        WalkThroughModel(image: .walkTwo.image, title: "Everyday Training", subTitle: "Now is as good a time as any to focus on getting your\n body into the best shape possible. if you really want\n make something happen."),
        WalkThroughModel(image: .walkThree.image, title: "Contemplation", subTitle: "Now is as good a time as any to focus on getting your\n body into the best shape possible. if you really want\n make something happen.")]
    
    private var arrDeviceList : [DeviceDetail] = []
    
    private(set) var signInResult = Bindable<Result<String?, AppError>>()
    private(set) var forgotPassResult = Bindable<Result<String?, AppError>>()
    private(set) var createPassResult = Bindable<Result<String?, AppError>>()
    private(set) var otpVerification = Bindable<Result<String?, AppError>>()
    private(set) var resendOtpVerification = Bindable<Result<String?, AppError>>()
    private(set) var isDeviceFetchComplete = Bindable<Bool>()
    
    init(){
        self.arrDeviceList = UserModel.currentUser?.deviceDetail ?? []
    }
}

extension AuthViewModel {
    func numberOfItemsInAuth() -> Int {
        return self.arrAuthModel.count
    }
    
    func dataAtIndexForAuth(index: Int) -> WalkThroughModel {
        return self.arrAuthModel[index]
    }
    
    func numberOfRowsForDevice(_ withOwnDevice : Bool = true) -> Int {
        if !withOwnDevice{
            let filterData = self.arrDeviceList.filter{$0.token != UserModel.accessToken ?? ""}
            return filterData.count
        }
        return self.arrDeviceList.count
    }
    
    func dataAtIndexForDevice(_ withOwnDevice : Bool = true, index: Int) -> DeviceDetail {
        var data = self.arrDeviceList
        if !withOwnDevice{
            data = self.arrDeviceList.filter{$0.token != UserModel.accessToken ?? ""}
        }
        return data[index]
    }
    
    func updateDeviceDetailsAtIndex(_ withOwnDevice : Bool = true, index : Int){
        var data = self.arrDeviceList
        if !withOwnDevice{
            data = self.arrDeviceList.filter{$0.token != UserModel.accessToken ?? ""}
        }
        data.map{$0.isSelected = false}
        let id = data[index].id
        self.arrDeviceList.map{
            if $0.id == id{
                $0.isSelected = true
            }
        }
//        self.arrDeviceList[index].isSelected = true
    }
    
    func getSelectedDevice() -> DeviceDetail?{
        return self.arrDeviceList.filter{$0.isSelected}.first
    }
}

extension AuthViewModel {
    func isValid(isPassword: Bool, code: String, phone: String, password: String = "") -> AppError? {
        guard !code.isEmpty else { return AppError.validation(type: .enterCountryCode)}
        guard !phone.isEmpty else { return AppError.validation(type: .enterPhone) }
        guard phone.isValid(.mobileNumber) else { return AppError.validation(type: .enterValidMobile) }
        
        if isPassword {
            guard !password.isEmpty else { return AppError.validation(type: .enterPassword) }
            
        }
        return nil
    }
    
    func apiSignIn(isPassword: Bool, code: String, phone: String, password: String = "") {
        if let err = isValid(isPassword: isPassword, code: code, phone: phone, password: password) {
            self.signInResult.value = .failure(err)
            return
        }
        var dictData = Dictionary<String, Any>()
        dictData["phone"] = code + phone
        
        if isPassword{
            dictData["password"] = password
            dictData["login_type"] = "simple"
            dictData["continues"] = "False"
            dictData["device_type"] = DeviceManager.shared.deviceType
            dictData["device_name"] = DeviceManager.shared.DeviceName
            dictData["model_name"] = DeviceManager.shared.modelName
            dictData["imei_number"] = DeviceManager.shared.uuid
            self.apiLogin(parameter: dictData, completion: self.signInResult)
        }
        else{
            self.sendOTP(parameter: dictData, completion: self.signInResult)
        }
    }
}

extension AuthViewModel {
    private func isValid(code: String = "", phone: String = "") -> AppError?{
        guard !code.isEmpty else { return AppError.validation(type: .enterCountryCode) }
        guard !phone.isEmpty else { return AppError.validation(type: .enterPhone) }
        guard phone.isValid(.mobileNumber) else { return AppError.validation(type: .enterValidMobile) }
        return nil
    }
    
    func apiForgotPassword(code: String = "", phone: String = "") {
        if let err = self.isValid(code: code, phone: phone) {
            self.forgotPassResult.value = .failure(err)
            return
        }
        var dictData = Dictionary<String, Any>()
        dictData["phone"] = code + phone
        self.apiResetPasswordWithPhone(parameter: dictData, completion: self.forgotPassResult)
    }
}


extension AuthViewModel {
    private func isValid(newPassword: String, confirmPassword: String) -> AppError? {
        guard !newPassword.isEmpty else { return AppError.validation(type: .enterNewPassword) }
        guard newPassword.isValid(.password) else { return AppError.validation(type: .enterValidPassword) }
        guard !confirmPassword.isEmpty else { return AppError.validation(type: .enterConfirmPassword) }
        guard newPassword == confirmPassword else { return AppError.validation(type: .passwordMismatch) }
        return nil
    }
    
    func apiCreatePassword(newPassword: String, confirmPassword: String, phone : String) {
        if let err = self.isValid(newPassword: newPassword, confirmPassword: confirmPassword) {
            self.createPassResult.value = .failure(err)
            return
        }
        var dictData = Dictionary<String,Any>()
        dictData["phone"] = phone
        dictData["new_password"] = newPassword
        self.apiChangePassword(parameter: dictData, completion: self.createPassResult)
//        self.createPassResult.value = .success(nil)
    }
}

extension AuthViewModel {
    private func isValidVerification(txtOne: String, txtTwo: String, txtThree: String, txtFour: String) -> AppError? {
        guard !txtOne.isEmpty else { return AppError.validation(type: .enterOTP)}
        guard !txtTwo.isEmpty else { return AppError.validation(type: .validOTP)}
        guard !txtThree.isEmpty else { return AppError.validation(type: .validOTP)}
        guard !txtFour.isEmpty else { return AppError.validation(type: .validOTP)}
        //        guard txtOne+txtTwo+txtThree+txtFour == "1234" else { return AppError.validation(type: .validOTP) }
        return nil
    }
    
    func apiVerification(txtOne: String, txtTwo: String, txtThree: String, txtFour: String, from : IsFrom, phone : String) {
        if let err = self.isValidVerification(txtOne: txtOne, txtTwo: txtTwo, txtThree: txtThree, txtFour: txtFour) {
            self.otpVerification.value = .failure(err)
            return
        }
        var dictData = Dictionary<String, Any>()
        dictData["phone"] =  phone
        
        if from == .signIn {
            dictData["otp"] = txtOne + txtTwo + txtThree + txtFour
            dictData["login_type"] = "otp"
            dictData["continues"] = "False"
            dictData["device_type"] = DeviceManager.shared.deviceType
            dictData["device_name"] = DeviceManager.shared.DeviceName
            dictData["model_name"] = DeviceManager.shared.modelName
            dictData["imei_number"] = DeviceManager.shared.uuid
            
            self.apiLogin(parameter: dictData, completion: self.otpVerification)
        }
        else if from == .forgotPassword{
            dictData["otp"] = txtOne + txtTwo + txtThree + txtFour
            self.apiCheckForgotPasswordOTP(parameter: dictData, completion: self.otpVerification)
        }
        else if from == .changeMobile{
            dictData["otp"] = txtOne + txtTwo + txtThree + txtFour
            self.apiUpdatePhone(parameter: dictData, completion: self.otpVerification)
        }
    }
    
    func resendVerificationCode(phone : String){
        var dictData = Dictionary<String, Any>()
        dictData["phone"] = phone
        self.sendOTP(parameter: dictData, completion: self.resendOtpVerification)
    }
}

extension AuthViewModel{
    func apiLogin(parameter : Dictionary<String,Any>, completion : Bindable<Result<String?, AppError>>){
        ApiManager.shared.makeRequestWithModel(endPoint: .auth(.login), modelType: UserModel.self, methodType: .post, responseModelType: .dictonary, parameter: parameter, queryParameters: [:], withErrorAlert: true, withLoader: true, withdebugLog: true) { [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                UserModel.currentUser = apiData.data
                completion.value = .success(nil)
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
    
    func sendOTP(parameter : Dictionary<String,Any>, completion : Bindable<Result<String?, AppError>>){
        ApiManager.shared.makeRequest(endPoint: .auth(.sendOTP), methodType: .post, parameter: parameter, withErrorAlert: true, withLoader: true, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                completion.value = .success(nil)
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
    
    func apiResetPasswordWithPhone(parameter : Dictionary<String,Any>, completion : Bindable<Result<String?, AppError>>){
        ApiManager.shared.makeRequest(endPoint: .user(.forgotPassword), methodType: .post, parameter: parameter, withErrorAlert: true, withLoader: true, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                completion.value = .success(apiData.data["email"].stringValue)
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
    
    func apiCheckForgotPasswordOTP(parameter : Dictionary<String,Any>, completion : Bindable<Result<String?, AppError>>){
        ApiManager.shared.makeRequest(endPoint: .user(.checkPasswordOtp), methodType: .post, parameter: parameter, withErrorAlert: true, withLoader: true, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                completion.value = .success(nil)
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
    
    func apiChangePassword(parameter : Dictionary<String,Any>, completion : Bindable<Result<String?, AppError>>){
        ApiManager.shared.makeRequest(endPoint: .user(.resetPassword), methodType: .post, parameter: parameter, withErrorAlert: true, withLoader: true, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                completion.value = .success(nil)
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
    
    func apiUpdatePhone(parameter : Dictionary<String,Any>, completion : Bindable<Result<String?, AppError>>){
        ApiManager.shared.makeRequest(endPoint: .user(.updatePhone), methodType: .post, parameter: parameter, withErrorAlert: true, withLoader: true, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                UserModel.currentUser = UserModel(fromJson: apiData.data)
                completion.value = .success(nil)
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
    
    func getDeviceList(withLoader isLoader : Bool = true){
        ApiManager.shared.makeRequest(endPoint: .user(.getDevice), methodType: .get, parameter: nil, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                self.arrDeviceList = DeviceDetail.listFromArray(data: apiData.data["results"].arrayValue)
                let data = UserModel.currentUser
                data?.deviceDetail = self.arrDeviceList
                UserModel.currentUser = data
                self.isDeviceFetchComplete.value = true
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
    
    func logoutDevice(deviceData : DeviceDetail){
        var dictData = Dictionary<String,Any>()
        dictData["exist_device_id"] = deviceData.id
        ApiManager.shared.makeRequest(endPoint: .user(.deviceLogout), methodType: .post, parameter: dictData, withErrorAlert: false, withLoader: true, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                self.arrDeviceList = self.arrDeviceList.filter{$0.id != deviceData.id}
                let data = UserModel.currentUser
                data?.deviceDetail = self.arrDeviceList
                UserModel.currentUser = data
                self.isDeviceFetchComplete.value = true
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
}

