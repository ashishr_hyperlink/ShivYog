//
//  WalkThroughVC.swift
//  ShivYog
//
//  Created by 2022M30 on 23/11/22.
//

import UIKit
import CHIPageControl

class WalkThroughVC: UIViewController {
    
    //MARK: - Outlet -

    @IBOutlet weak var colAuth: UICollectionView!
    @IBOutlet weak var svCount: UIStackView!
    @IBOutlet weak var btnSkip: UIBarButtonItem!
    @IBOutlet weak var lblNavigateApp: UILabel!
    @IBOutlet weak var btnBack: UIBarButtonItem!
    @IBOutlet weak var constColHeight: NSLayoutConstraint!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var authVM : AuthViewModel = AuthViewModel()
    var indexPath : IndexPath = [0,0]
    private var currentIndex : Int = 0 {
        didSet{
            self.setData()
            self.btnBack.plainView.isHidden = self.currentIndex == 0
//            self.indexPath.row = self.currentIndex - 1
        }
    }
    //------------------------------------------------------
    
    //MARK: - Memory Management Method -
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method -
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.colAuth.registerNib(forCellWithClass: WalkThroughCell.self)
        self.colAuth.delegate = self
        self.colAuth.dataSource = self
        
        self.lblNavigateApp.font(name: FontFamily.SFProDisplay.regular, size: 14.0).textColor(color: .appFfffff)
        self.lblNavigateApp.attributedText = self.lblNavigateApp.text?.getAttributedText(defaultDic: [.font : self.lblNavigateApp.font!, .foregroundColor : self.lblNavigateApp.textColor!], attributeDic: [.font : FontFamily.SFProDisplay.bold.font(size: 14.0)!, .foregroundColor : UIColor.app_990000], attributedStrings: ["Navigate through"])
        
        self.btnSkip.setTitleTextAttributes([.font : FontFamily.SFProDisplay.medium.font(size: 16.0)!,.foregroundColor : UIColor.white], for: .normal)
        self.btnSkip.setTitleTextAttributes([.font : FontFamily.SFProDisplay.medium.font(size: 16.0)!,.foregroundColor : UIColor.white], for: .highlighted)
        self.constColHeight.constant = ScreenSize.width * 1.2
        for _ in 0..<self.authVM.numberOfItemsInAuth(){
            let image = UIImageView(image: .unselectedPage.image)
            self.svCount.addArrangedSubview(image)
        }
        self.currentIndex = 0
        let tapTermsCondition = UITapGestureRecognizer(target: self, action: #selector(lblTermsConditionTapped(_:)))
        self.lblNavigateApp.isUserInteractionEnabled = true
        self.lblNavigateApp.addGestureRecognizer(tapTermsCondition)
//        UserDefaultsConfig.isShowTutorial = true
    }
    
    func setData(){
        if currentIndex < self.authVM.numberOfItemsInAuth(){
            for item in svCount.arrangedSubviews{
                (item as? UIImageView ?? UIImageView())?.image = .unselectedPage.image
            }
            (self.svCount.arrangedSubviews[currentIndex] as? UIImageView ?? UIImageView()).image = .selectedPage.image
        }
    }
    
    @objc func lblTermsConditionTapped(_ gesture: UITapGestureRecognizer) {
          
        if gesture.didTapAttributedTextInLabel(label: self.lblNavigateApp, inRange: (self.lblNavigateApp.attributedText!.string as NSString).range(of: "Navigate through")) {
            
            
              let vc = StoryboardScene.Profile.webContentVC.instantiate()
              vc.type = .navigateThroughApp
              self.navigationController?.pushViewController(vc, animated: true)
          }
      }
      
    

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })*/
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method -
    @IBAction func btnSkipTapped(_ sender: UIBarButtonItem) {
        self.currentIndex = 3
        UserDefaultsConfig.isShowTutorial = true
        UIApplication.shared.manageLogin()
        
    }
    
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        
        self.indexPath.row -= 1
        self.colAuth.scrollToItem(at: self.indexPath, at: .left, animated: true)
        self.currentIndex = self.indexPath.row
    }
    
    
    @IBAction func btnCreateAccountClicked(_ sender: ThemeButton) {
//        Alert.shared.showSnackBar("Under Development")
//        let vc = StoryboardScene.Profile.webContentVC.instantiate()
//        vc.type = .createAccount
//        self.navigationController?.pushViewController(vc, animated: true)
        self.openURL(WebViewLinks.createAccount.url!.description)
    }
    
    //------------------------------------------------------
    
}

//MARK: - UICollectionview delegate and datasource -

extension WalkThroughVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.authVM.numberOfItemsInAuth()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: WalkThroughCell.self, for: indexPath)
        let data = self.authVM.dataAtIndexForAuth(index: indexPath.row)
        cell.data = data
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ScreenSize.width
        let height = width * 1.1
        return CGSize(width: width, height: height)//collectionView.size
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let indexPath = self.colAuth.getVisibleCellIndexPath() {//self.colAuth.visibleCells.first, let indexPath = self.colAuth.indexPath(for: cell) {
            self.indexPath = indexPath
            self.currentIndex = indexPath.row
        }
    }
}


