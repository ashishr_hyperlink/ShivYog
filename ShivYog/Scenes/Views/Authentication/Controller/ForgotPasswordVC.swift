//
//  ForgotPasswordVC.swift
//  ShivYog
//
//  Created by 2022M30 on 25/11/22.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var lblResetPassword: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var txtPhone: GradientTextField!
    @IBOutlet weak var btnMobileRightView: UIButton!
    @IBOutlet weak var btnEmailRightView: UIButton!
    @IBOutlet weak var vwCode: UIView!
    @IBOutlet weak var lblCode: UILabel!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var authVM : AuthViewModel = AuthViewModel()
    var countryCode : String!
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        self.vwCode.setGradient(startColor: .appFfffff, endColor:   .appFfffff)
//        self.txtPhone.setGradient(startColor: .appFfffff, endColor: .appFfffff, isBorder: self.txtPhone.isBorder)
//        self.txtPhone.setGradient(isBorder : self.txtPhone.isBorder)
//        self.vwCode.setGradient()
//        guard let sublayers = self.txtEmail.layer.sublayers else { return }
//        for sublayer in sublayers where sublayer.isKind(of: CAGradientLayer.self) {
//                sublayer.removeFromSuperlayer()
//        }
//        self.txtEmail.setGradient(isBorder: true)
//    }
    
//    override func viewWillLayoutSubviews() {
//        super.viewWillLayoutSubviews()
//        guard let sublayers = self.txtEmail.layer.sublayers else { return }
//        for sublayer in sublayers where sublayer.isKind(of: CAGradientLayer.self) {
//                sublayer.removeFromSuperlayer()
//        }
//        self.txtEmail.setGradient(isBorder: true)
//    }
    
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.lblResetPassword.font(name: FontFamily.SFProDisplay.medium, size: 28.0).textColor(color: .app_000000)
        self.lblSubTitle.font(name: FontFamily.SFProDisplay.regular, size: 16.0).textColor(color: .app_000000.withAlphaComponent(0.5))
        
        self.lblCode.font(name: FontFamily.SFProDisplay.bold, size: 16.0).textColor(color: .app_222222)
        self.vwCode.borderColor(color: .app_990000, borderWidth: 1.0)
        
        self.txtPhone.rightView = self.btnMobileRightView
        self.txtPhone.rightViewMode = .always
        
        
        self.txtPhone.delegate = self
        let currentCountryCode = GFunction.shared.getCurrentCountryCode()
        let bundle = Bundle.init(for: LocalePickerViewController.self)
        self.lblCode.text = currentCountryCode.0
        
        self.setTapGesture()
        DispatchQueue.main.async {
            self.vwCode.setRound()
            self.vwCode.setGradient()
        }
        
    }
    
    private func setTapGesture() {
        self.vwCode.addTapGestureRecognizer {
            let alert = UIAlertController(style: .actionSheet,  title: "Select country code", tintColor: .app_990000)
            alert.addLocalePicker(type: .phoneCode) { info in
                if let info = info {
                    self.lblCode.text = info.phoneCode
                    self.countryCode = info.phoneCode
                    
                    DispatchQueue.main.async {
                        guard let sublayers = self.vwCode.layer.sublayers else { return }
                        for sublayer in sublayers where sublayer.isKind(of: CAGradientLayer.self) {
                                sublayer.removeFromSuperlayer()
                        }
                        self.txtPhone.completion?()
                        self.vwCode.setGradient()
                        
//                        guard let txtSublayers = self.txtEmail.layer.sublayers else { return }
//                        for txtSublayer in txtSublayers where txtSublayer.isKind(of: CAGradientLayer.self) {
//                            txtSublayer.removeFromSuperlayer()
//                        }
//
//                        self.txtEmail.setGradient(isBorder: true)
                    }
                    
                }
            }
            alert.addAction(title: "Cancel", style: .destructive)
            alert.show()
        }
    }
    
    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        self.authVM.forgotPassResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(let res):
             var userData = JSON(["phone" : self.lblCode.text! + self.txtPhone.text!])
             if let email = res{
                 userData["email"] = JSON(email)
             }
             let vc = StoryboardScene.Authentication.verificationVC.instantiate()
             vc.isFrom = .forgotPassword
             vc.userData = userData
             self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    
    @IBAction func btnNextClicked(_ sender: ThemeButton) {
        self.authVM.apiForgotPassword(code: self.lblCode.text!, phone: self.txtPhone.text!)
        //        let vc = StoryboardScene.Authentication.verificationVC.instantiate()
        //        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //------------------------------------------------------
    
}


//MARK: - UITextfield delegate -

extension ForgotPasswordVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if GFunction.shared.isBackspace(string) {
            return true
        }
        
        
        if textField == self.txtPhone {
            if self.txtPhone.text!.count >=  ValidationEnum.PhoneNumber.Maximum.rawValue {
                return false
            } else if textField == txtPhone {
                return string.isValid(.number)
            }
        }
        
        return !string.contains("")
    }
}
