//
//  SignInInitialVC.swift
//  ShivYog
//
//  Created by 2022M30 on 24/11/22.
//

import UIKit

class SignInInitialVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var btnFaqs: UIBarButtonItem!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblContactUs: UILabel!
    @IBOutlet weak var btnSignUp: UIButton!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
//    private var viewModel : AuthViewModel = AuthViewModel()
    
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.lblTitle.font(name: FontFamily.SFProDisplay.medium, size: 30.0).textColor(color:.appFfffff)
        self.lblSubTitle.font(name: FontFamily.SFProDisplay.regular, size: 14.0).textColor(color: .appFfffff).lineSpacing()
        self.lblContactUs.font(name: FontFamily.SFProDisplay.regular, size: 14.0).textColor(color: .appFfffff)
//        self.btnSignUp.cornerRadius(cornerRadius: 30)
        self.btnSignUp.clipsToBounds = true
        self.lblSubTitle.text = "A wonderful serenity has taken possession of my entire \nsweet mornings of spring."
        self.btnSignUp.font(name: FontFamily.SFProDisplay.medium, size: 16).textColor(color: .app_990000).backGroundColor(color: .clear)
        self.btnSignUp.borderColor(color: .app_990000, borderWidth: 1.0)
        
        DispatchQueue.main.async {
            self.btnSignUp.setRound()
        }
        
        self.lblContactUs.attributedText = self.lblContactUs.text?.getAttributedText(defaultDic: [.font : self.lblContactUs.font!, .foregroundColor : self.lblContactUs.textColor!], attributeDic: [.font : FontFamily.SFProDisplay.bold.font(size: 14.0)!, .foregroundColor : UIColor.app_990000], attributedStrings: ["Contact Us"])
        
        
        self.btnFaqs.setTitleTextAttributes([.font : FontFamily.SFProDisplay.medium.font(size: 16.0)!,.foregroundColor : UIColor.white], for: .normal)
        self.btnFaqs.setTitleTextAttributes([.font : FontFamily.SFProDisplay.medium.font(size: 16.0)!,.foregroundColor : UIColor.white], for: .selected)
        
        let tapContactUs = UITapGestureRecognizer(target: self, action: #selector(lblTermsConditionTapped(_:)))
        self.lblContactUs.isUserInteractionEnabled = true
        self.lblContactUs.addGestureRecognizer(tapContactUs)

    }
    
    @objc func lblTermsConditionTapped(_ gesture: UITapGestureRecognizer) {
          
        if gesture.didTapAttributedTextInLabel(label: self.lblContactUs, inRange: (self.lblContactUs.attributedText!.string as NSString).range(of: "Contact Us")) {
              let vc = StoryboardScene.Profile.contactUsVC.instantiate()
              self.navigationController?.pushViewController(vc, animated: true)
          }
      }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })*/
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    @IBAction func btnSignInClicked(_ sender: ThemeButton) {
        let vc = StoryboardScene.Authentication.signInVC.instantiate()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSignUpClicked(_ sender: UIButton) {
        if let url = WebViewLinks.createAccount.url{
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func btnFaqsClicked(_ sender: UIBarButtonItem) {
        let vc = StoryboardScene.Profile.webContentVC.instantiate()
        vc.type = .faq
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //------------------------------------------------------
    
}
