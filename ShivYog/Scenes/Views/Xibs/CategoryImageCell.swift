//
//  CategoryImageCell.swift
//  ShivYog
//
//  Created by 2022M30 on 02/12/22.
//

import UIKit

class CategoryImageCell: UICollectionViewCell {
    
    //MARK: - Outlet -
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblTitle: MarqueeLabel!
    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var btnFreeTag: UIButton!//UIImageView!
    @IBOutlet weak var imgPlay: UIImageView!
    
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet var btnRemove: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyStyle()
        // Initialization code
    }
    
    private func applyStyle() {
        self.imgProduct.cornerRadius(cornerRadius: 20.0)
        self.imgProduct.superview?.cornerRadius(cornerRadius: 20.0)
        self.lblTitle.font(name: FontFamily.CircularStd.bold, size: 12.0).textColor(color: .appFfffff)
    }
}
