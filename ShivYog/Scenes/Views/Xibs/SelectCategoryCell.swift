//
//  SelectCategoryCell.swift
//  ShivYog
//
//  Created by 2022M30 on 02/12/22.
//

import UIKit

class SelectCategoryCell: UITableViewCell {
    
    //MARK: - Outlet -
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgSelect: UIImageView!
    @IBOutlet weak var vwContentView: UIView!
    
    
    
    var data : CategoryModel! {
        didSet {
            self.configCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyStyle()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func applyStyle() {
        self.lblTitle.font(name: FontFamily.SFProDisplay.bold, size: 16.0).textColor(color: .app_222222)
        self.lblDescription.font(name: FontFamily.Inter.regular, size: 12.0).textColor(color: .app_222222.withAlphaComponent(0.6))
        self.vwContentView.cornerRadius(cornerRadius: 10.0)
        self.imgSelect.isHighlighted = true
    }
    
    private func configCell() {
        if let data = data {
            self.imgCategory.setImage(with: data.image)
            self.lblTitle.text = data.title
            self.lblDescription.text = data.content
            self.imgSelect.isHighlighted = data.isSelected
            
        }
    }
}
