//
//  YogaStoreCell.swift
//  ShivYog
//
//  Created by 2022M30 on 05/12/22.
//

import UIKit
import SwiftUI

class YogaStoreCell: UITableViewCell {
    
    //MARK: - outlet -
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var colList: UICollectionView!
    @IBOutlet weak var btnSeeAll: UIButton!
    @IBOutlet weak var constColHeight: NSLayoutConstraint!
    @IBOutlet weak var vwPageControl: ZPageControl!
    
    
    var completion : ((IndexPath)-> Void)?
    var centeredCollectionView : CenteredCollectionViewFlowLayout!
    var isFrom : IsFrom!
//    var index: Int = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.applyStyle()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func applyStyle() {
        self.lblTitle.font(name: FontFamily.SFProDisplay.semibold, size: 16.0).textColor(color: .app_222222)
        self.btnSeeAll.font(name: FontFamily.CircularStd.book, size: 12.0).textColor(color: .app_990000).imageRightSide()
        self.btnSeeAll.centerTextAndImage(spacing: 4.0)
        self.constColHeight.constant = ((ScreenSize.width - 32 ) / 1.11) * 0.62
        
        self.centeredCollectionView = self.colList.collectionViewLayout as? CenteredCollectionViewFlowLayout
        self.colList.decelerationRate = UIScrollView.DecelerationRate.fast
        self.colList.registerNib(forCellWithClass: StoreProductCell.self)
        self.colList.registerNib(forCellWithClass: HomeProductCell.self)
        self.colList.delegate = self
        self.colList.dataSource = self
        let width = (ScreenSize.width - 32) / 1.11
        let height = width * 0.6
        self.centeredCollectionView.itemSize = CGSize(width: width, height: height)
        self.centeredCollectionView.minimumLineSpacing = 15.0
        self.vwPageControl.currentPage = 0
        self.vwPageControl.numberOfPages = 3
        self.vwPageControl.currentDotSize = CGSize(width: 25.0, height: 8.0)
        self.vwPageControl.dotSize = CGSize(width: 8.0, height: 8.0)
        self.vwPageControl.currentDotImage = .selectedDot.image
        self.vwPageControl.dotImage = .unSelectedDot.image
        
//        self.vwPageControl.currentPage = self.currentIndex
        
    }
}

//MARK: - UIcollectionView delegate and datasource -

extension YogaStoreCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if isFrom == .yogaStore {
            let cell = collectionView.dequeueReusableCell(withClass: StoreProductCell.self, for: indexPath)
    //        self.currentIndex = indexPath.row
//            self.vwPageControl.currentPage = indexPath.row
            cell.btnBuyNow.addTapGestureRecognizer {
                self.completion?(indexPath)
            }
            return cell
        } else if isFrom == .home {
            let cell = collectionView.dequeueReusableCell(withClass: HomeProductCell.self, for: indexPath)
            cell.imgPremium.isHighlighted = true
            cell.imgFavourite.addTapGestureRecognizer {
                if cell.imgFavourite.isHighlighted {
                    Alert.shared.showAlert(message: AppMessages.removeFromFavourite, actionTitles: [AppMessages.no, AppMessages.yes], actions: [{ (no) in }, { [weak self] (yes) in
                        guard let _ = self else { return }
                        cell.imgFavourite.isHighlighted.toggle()
                    }])
                    return
                }
                cell.imgFavourite.isHighlighted.toggle()

            }
            cell.btnDownload.addTapGestureRecognizer {
                Alert.shared.showSnackBar("Under Development")
            }
            
//            if indexPath.row == 0 || indexPath.row == 2 {
//                cell.lblType.isHidden = false
//                cell.btnDownload.isHidden = false
//            } else{
//                cell.lblType.isHidden = false
            cell.btnDownload.isHidden = indexPath.row % 2 == 0
                
//            }
//            } else if self.index == 3 {
//                cell.lblType.isHidden = false
//                cell.btnDownload.isHidden = false
//            }
            return cell
        }
        return UICollectionViewCell()
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (ScreenSize.width - 32) / 1.11
        let height = width * 0.6
        return CGSize(width: width, height: height)
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        self.colList.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
//    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        if let cell = self.colList.getVisibleCellIndexPath(){
            self.vwPageControl.currentPage = cell.row
        }
//
//        self.colList.setContentOffset(CGPoint(x: 18.0, y: 0.0), animated: true)
    }
    
}
