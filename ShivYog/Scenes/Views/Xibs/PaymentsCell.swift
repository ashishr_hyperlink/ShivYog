//
//  PaymentsCell.swift
//  ShivYog
//
//  Created by 2022M30 on 29/11/22.
//

import UIKit

class PaymentsCell: UITableViewCell {
    //MARK: - Outlet -
    
    @IBOutlet weak var lblTransactionDescription: UILabel!
    @IBOutlet weak var lblTransactionID: UILabel!
    @IBOutlet weak var lblTransactionDate: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var vwAmount: UIView!
    
    var isFrom : IsFrom = .subscriptionPayment
    var data : SubScriptionPaymentModel? {
        didSet {
            self.configCell()
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyStyle()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func applyStyle() {
        self.lblTransactionDescription.font(name: FontFamily.SFProDisplay.medium, size: 16.0).textColor(color: .app_222222)
        self.lblTransactionID.font(name: FontFamily.SFProDisplay.regular, size: 12.0).textColor(color: .app_222222)
        self.lblTransactionDate.font(name: FontFamily.ProximaNova.regular, size: 12.0).textColor(color: .appAeaeae)
        self.lblAmount.font(name: FontFamily.CircularStd.medium, size: 16.0).textColor(color: .appFfffff)
        self.vwAmount.cornerRadius(cornerRadius: 10.0)
    }
    
    private func configCell() {
        if let data = data {
            
            let str = data.paymentCurrency == "Dollar" ? "$ " : "₹ "
            self.lblAmount.text = str + data.price
            self.lblTransactionID.text = "ID : " + data.paymentid
            if self.isFrom == .subscriptionPayment {
                self.lblTransactionDescription.text = data.message
                self.lblTransactionDate.text = (data.startdate.changeDateFormat(from: .default(format: .NodeUTCFormat), to: .default(format: .ddmm_yyyy_hhmm), type: .local) ?? "")
            } else {
                self.lblTransactionDescription.text = data.message + " " + data.storesubcontentDetails.title
                self.lblTransactionDate.text = (data.startdate.changeDateFormat(from: .default(format: .NodeUTCFormatWithMiliseconds), to: .default(format: .ddmm_yyyy_hhmm), type: .local) ?? "")
            }
        }
    }
    
}
