//
//  StatusCell.swift
//  ShivYog
//
//  Created by 2022M30 on 25/01/23.
//

import UIKit

class StatusCell: UICollectionViewCell {
    
    //MARK: - Outlet -
    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgStatusIcon: UIImageView!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyStyle()
        // Initialization code
    }
    
    private func applyStyle() {
        self.lblTime.font(name: FontFamily.SFProDisplay.medium, size: 10.0).textColor(color: .app_5E5E5E)
        
    }

}
