//
//  StoreProductCell.swift
//  ShivYog
//
//  Created by 2022M30 on 05/12/22.
//

import UIKit

class StoreProductCell: UICollectionViewCell {
    
    //MARK: - outlet -
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var btnBuyNow: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
//    @IBOutlet weak var constViewWidth: NSLayoutConstraint!
//    @IBOutlet weak var constViewHeight: NSLayoutConstraint!
    @IBOutlet weak var vwContainer: UIView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.applyStyle()
    }
    
    private func applyStyle() {
        self.imgProduct.cornerRadius(cornerRadius: 20.0)
        self.lblTitle.font(name: FontFamily.CircularStd.medium, size: 18.0).textColor(color: .appFfffff)
        self.lblType.font(name: FontFamily.CircularStd.medium, size: 10.0).textColor(color: .appFfffff)
        self.btnBuyNow.font(name: FontFamily.SFProDisplay.medium, size: 12.0).textColor(color: .app_222222).backGroundColor(color: .appFfffff).cornerRadius(cornerRadius: self.btnBuyNow.frame.height / 2.0)
//        self.constViewWidth.constant = ScreenSize.width - 73
//        self.constViewHeight.constant = (ScreenSize.width - 73) * 0.6
        self.vwContainer.cornerRadius(cornerRadius: 35.0).backGroundColor(color: .app_990000)
        self.lblPrice.font(name: FontFamily.SFProDisplay.medium, size: 14.0).textColor(color: .app_222222)
    }

}
