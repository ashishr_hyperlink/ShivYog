//
//  MySpaceCell.swift
//  ShivYog
//
//  Created by Ashish on 13/02/23.
//

import UIKit

class MySpaceCell: UICollectionViewCell {
    
    //MARK: - Outlet -
    
    @IBOutlet weak var imgSelection: UIImageView!
    @IBOutlet weak var imgOption: UIImageView!
    @IBOutlet weak var lblOptionType: UILabel!
    
    
    var isFrom : IsFrom = .preferences
    var data : PreferencesModel! {
        didSet {
            self.configCell()
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.applyStyle()
    }
    
    private func applyStyle() {
        self.lblOptionType.font(name: FontFamily.SFProDisplay.bold, size: 16.0).textColor(color: .app_222222)
        //self.lblOptionType.font(name: FontFamily.CircularStd.book, size: 18.0).textColor(color: .app_222222)
        self.imgSelection.superview?.cornerRadius(cornerRadius: 10.0)
    }
    
    private func configCell() {
        if let data = data {
            self.imgSelection.isHighlighted = data.isSelect
            self.imgOption.image = data.image
            self.lblOptionType.text = data.title
            
//            if self.isFrom == .mySpace {
//                self.lblOptionType.font(name: FontFamily.SFProDisplay.bold, size: 18.0)
//            } else {
//                self.lblOptionType.font(name: FontFamily.CircularStd.book, size: 18.0)
//            }
        }
    }
}
