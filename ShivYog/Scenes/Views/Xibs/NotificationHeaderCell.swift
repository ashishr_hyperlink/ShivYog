//
//  NotificationHeaderCell.swift
//  ShivYog
//
//  Created by 2022M30 on 05/12/22.
//

import UIKit

class NotificationHeaderCell: UITableViewCell {
    //MARK: - Outlet -
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.applyStyle()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func applyStyle() {
        self.lblHeaderTitle.font(name: FontFamily.PTSans.bold, size: 14.0).textColor(color: .app_222222)
    }
    
}
