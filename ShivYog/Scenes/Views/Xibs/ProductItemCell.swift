//
//  ProductItemCell.swift
//  ShivYog
//
//  Created by 2022M30 on 23/01/23.
//

import UIKit
import AVKit
import AVFoundation
import HGCircularSlider

class ProductItemCell: UICollectionViewCell {
    
    //MARK: - Outlets -
    
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var btnContentType: UIButton!
    @IBOutlet weak var btnFree: UIButton!//UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContentSubType: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var progressRing: CircularSlider!
    @IBOutlet weak var vwProgressContainer: UIView!
    @IBOutlet weak var lblPercentages: UILabel!
    var isFrom : IsFrom = .home
    var data : CategoryContentModel! {
        didSet {
            self.configCell()
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyStyle()
        // Initialization code
    }
    
    private func applyStyle() {
        self.imgProduct.cornerRadius(cornerRadius: 10.0)
        self.vwProgressContainer.cornerRadius(cornerRadius: 10.0)
        self.btnContentType.backGroundColor(color: .app_990000).font(name: FontFamily.SFProDisplay.medium, size: 12.0).textColor(color: .appFfffff).borderColor(color: .appFfffff, borderWidth: 1.0).imageRightSide()
        self.btnContentType.centerTextAndImage(spacing: 2.0)
        
        self.lblTitle.font(name: FontFamily.SFProDisplay.regular, size: 16.0).textColor(color: .app_222222)
        self.lblDescription.font(name: FontFamily.CircularStd.book, size: 12.0).textColor(color: .app_5E5E5E)
        self.lblContentSubType.font(name: FontFamily.DMSans.regular, size: 12.0).textColor(color: .app_990000)
        self.lblPercentages.font(name: FontFamily.CircularStd.bold, size: 12.0).textColor(color: .appFfffff)
        self.progressRing.trackColor = .appFfffff.withAlphaComponent(0.5)
        self.progressRing.diskFillColor = .clear
        self.progressRing.diskColor = .clear
        self.progressRing.trackFillColor = .app_990000
        self.progressRing.endThumbTintColor = .clear
        self.progressRing.isUserInteractionEnabled = false
        self.progressRing.minimumValue = 0.0
        self.progressRing.maximumValue = 100.0
        DispatchQueue.main.async {
            self.btnContentType.setRound()
        }
    }
    
    private func configCell() {
        if let data = data {
            self.lblTitle.text = data.title
            if let image = UIImage(named: data.image){
                self.imgProduct.image = image
            }
            else{
                self.imgProduct.setImage(with: data.image)
            }
            self.lblDescription.text = data.description
            var tags : [String] = self.data.tag.flatMap{$0.title}
            tags.append(self.data.contentType == .pdf ? self.data.contentType.rawValue.uppercased() : self.data.contentType.rawValue.capitalized)//self.data.contentType.rawValue.capitalized)
            self.lblContentSubType.text = tags.joined(separator: " • ")
            self.btnFree.isSelected = self.isFrom == .yogaStore ? false :  UserModel.isSubscribeUser
            self.btnFree.isHidden = self.isFrom == .yogaStore ? false : !data.isSubscribe
            self.btnMore.isHidden = self.isFrom == .yogaStore
            self.btnContentType.setImage(data.contentType == .pdf ? .pdfWhiteSmall.image : .playWhite.image, for: .normal)
            
            switch data.contentType{
            case .audio, .video:
                self.btnContentType.isHidden = false
                let formatter = DateComponentsFormatter()
                formatter.allowedUnits = [.hour, .minute]
                formatter.unitsStyle = .short
                let formattedString =
                formatter.string(from: TimeInterval(data.duration)) ?? " "
                self.btnContentType.setTitle(formattedString, for: .normal)
                break
            case .pdf:
                self.btnContentType.isHidden = false
                self.btnContentType.setTitle("Read"+" ", for: .normal)
                break
            default:
                self.btnContentType.isHidden = true
                self.btnContentType.setTitle(" ", for: .normal)
            }
        }
    }

}
