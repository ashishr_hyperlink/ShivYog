//
//  RewardCell.swift
//  ShivYog
//
//  Created by 2022M30 on 05/12/22.
//

import UIKit

class RewardCell: UITableViewCell {
    //MARK: - Outlet -
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblWonText: UILabel!
    @IBOutlet weak var btnRedeem: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyStyle()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func applyStyle() {
        self.lblTitle.font(name: FontFamily.SFProDisplay.regular, size: 14.0).textColor(color: .appFfffff)
        self.lblWonText.font(name: FontFamily.Inter.regular, size: 12.0).textColor(color: .app_929292)
        self.btnRedeem.font(name: FontFamily.Inter.medium, size: 14.0).textColor(color: .appFfffff).cornerRadius(cornerRadius: 10.0).backGroundColor(color: .app_990000)
    }
    
}
