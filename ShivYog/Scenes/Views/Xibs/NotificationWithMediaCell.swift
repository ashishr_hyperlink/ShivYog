//
//  NotificationWithMediaCell.swift
//  ShivYog
//
//  Created by 2022M30 on 05/12/22.
//

import UIKit

class NotificationWithMediaCell: UITableViewCell {
    //MARK: - Outlet -
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var vwContainer: UIView!
    
    var data : NotificationModel? {
        didSet {
            self.configCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyStyle()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func applyStyle() {
        self.lblTitle.font(name: FontFamily.SFProDisplay.regular, size: 14.0).textColor(color: .app_990000)
        self.lblTime.font(name: FontFamily.SFProDisplay.regular, size: 12.0).textColor(color: .app_222222.withAlphaComponent(0.70))
        self.imgIcon.cornerRadius(cornerRadius: 4.0)

    }
    
    private func configCell() {
        if let data = data {
            self.vwContainer.backgroundColor = data.isSelected ? .clear : .app_990000.withAlphaComponent(0.08)
        }
    }
    
}
