//
//  ProfileHeaderCell.swift
//  ShivYog
//
//  Created by 2022M30 on 28/11/22.
//

import UIKit

class ProfileHeaderCell: UITableViewCell {
    
    //MARK: - Outlet -
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    var data : ProfileMainModel! {
        didSet {
            self.configCell()
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.applyStyle()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    private func applyStyle() {
        self.lblHeaderTitle.font(name: FontFamily.SFProDisplay.medium, size: 18.0).textColor(color: .app_000000)
    }
    
    private func configCell() {
        if let data = data {
            self.imgIcon.image = data.profileOptions.image
            self.lblHeaderTitle.text = data.profileOptions.rawValue
        }
    }
    
}
