//
//  MiniPlayerView.swift
//  ShivYog
//
//  Created by 2022M30 on 27/12/22.
//

import UIKit

class MiniPlayerView: UIView {

    //MARK: - Outlet -
    
    @IBOutlet weak var imgPlayer: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: MarqueeLabel!
    @IBOutlet weak var btnPlayPause: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var vwContainer: UIView!
    
    
    //MARK: - Class Variable -
    
    var audioDuration = Double()
    var seconds : Float = 0.0
    var miniPlayer = "miniplayer"
    var isAudioPlaying : Bool = true
    var fileName = String()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyStyle()
        
    }
    
    //MARK: - Custom methods -
    
    private func applyStyle() {
        self.lblTitle.font(name: FontFamily.SFProDisplay.bold, size: 14.0).textColor(color: .appFfffff).text = "Playing Yoga Ashtanga"
        self.lblSubTitle.font(name: FontFamily.SFProDisplay.regular, size: 13.0).textColor(color: .appFfffff).text = "ShivYog"
        self.lblSubTitle.forceScrolling = true
        self.lblSubTitle.labelize = false
        self.lblSubTitle.leadingBuffer = 10.0
        self.lblSubTitle.trailingBuffer = 30.0
        self.vwContainer.isUserInteractionEnabled = true
        self.vwContainer.addTapGestureRecognizer {
            
            let vc = StoryboardScene.Home.audioPlayerVC.instantiate()
            vc.isFrom = .miniPlayer
            if (UIApplication.topViewController() is HomeVC) ||
                (UIApplication.topViewController() is CategoriesVC) ||
                (UIApplication.topViewController() is MySpaceVC) ||
                (UIApplication.topViewController() is MyStoreVC) ||
                (UIApplication.topViewController() is ProfileVC) {
                vc.hidesBottomBarWhenPushed = true
                
            }
            vc.hidesBottomBarWhenPushed = true
            vc.mediaData = GAudioPlayer.shared.mediaData
            UIApplication.push(viewController: vc)
        }
        self.setData()
    }
    
    func setData(){
        if let data = GAudioPlayer.shared.mediaData{
            self.imgPlayer.setImage(with: data.image)
            self.lblTitle.text = "Playing %@".localized(data.title)
        }
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    //MARK: - Button Action Methods -
    
    @IBAction func btnPlayPauseClicked(_ sender: UIButton) {
//        self.btnPlayPause.isSelected = !self.isAudioPlaying
        GAudioPlayer.shared.managedState(url: "123",state: self.btnPlayPause.isSelected ? .pause : .play, speed: "1")
    }
    
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        GAudioPlayer.shared.isRepeat = false
        GAudioPlayer.shared.managedState(url: "123", state: .off, speed: "1")
    }
}
