//
//  NetWorkErrorVC.swift
//  ShivYog
//
//  Created by 2022M30 on 02/12/22.
//

import UIKit

class NetWorkErrorVC: UIViewController {

    //MARK: - Outlet -
    
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var btnSettings: UIButton!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
        self.applyStyle()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    private func setUpView() {
        
    }
    
    private func applyStyle() {
        self.lblMsg.font(name: FontFamily.SFProDisplay.bold, size: 24.0).textColor(color: .app_000000)
        self.btnSettings.borderColor(color: .app_990000, borderWidth: 1.0).font(name: FontFamily.SFProDisplay.bold, size: 16.0).textColor(color: .app_990000, state: .normal)
        DispatchQueue.main.async {
            
            self.btnSettings.cornerRadius(cornerRadius: self.btnSettings.frame.height / 2)
        }
    }
    
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    //MARK: - Action methods -
    @IBAction func btnDownloadsClicked(_ sender: ThemeButton) {
        let vc = StoryboardScene.Home.productListingVC.instantiate()
        vc.isFrom = .downloads
        vc.screenTitle = "Downloads"
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSettingsClicked(_ sender: UIButton) {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }
    
}
