//
//  HomeBannerCell.swift
//  ShivYog
//
//  Created by 2022M30 on 05/12/22.
//

import UIKit

class HomeBannerCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgBannerCover: UIImageView!
    @IBOutlet weak var imgGradient: UIImageView!
    
//    @IBOutlet weak var constVwHeight: NSLayoutConstraint!
//    @IBOutlet weak var constVwWidth: NSLayoutConstraint!
    
    var data : BannerModel?{
        didSet{
            self.configCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyStyle()
        // Initialization code
    }
    
    private func applyStyle() {
        self.imgBanner.cornerRadius(cornerRadius: 10.0)
        self.imgBannerCover.cornerRadius(cornerRadius: 10.0)
        self.lblTitle.font(name: FontFamily.CircularStd.medium, size: 18.0).textColor(color: .appFfffff)
        self.lblDescription.font(name: FontFamily.CircularStd.book, size: 12.0).textColor(color: .appFfffff)
        DispatchQueue.main.async {
            self.imgBanner.setGradient(startColor: .app_000000.withAlphaComponent(0.0), endColor: .app_000000, isRadius: false)
        }
//        self.constVwWidth.constant = (ScreenSize.width - 36)
//        self.constVwHeight.constant = (ScreenSize.width - 36) * 0.52
    }
    
    func configCell(){
        if let data = self.data{
            self.imgBanner.setImage(with: data.appImage)
            self.lblTitle.text = data.title
            self.lblDescription.text = data.descriptionField
        }
    }
}
