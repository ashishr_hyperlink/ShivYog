//
//  SearchCell.swift
//  ShivYog
//
//  Created by 2022M30 on 30/11/22.
//

import UIKit

class SearchCell: UITableViewCell {
    
    //MARK: - Outlet -
    
    @IBOutlet weak var imgThumb: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContentType: UILabel!
    @IBOutlet weak var btnPlay: UIButton!
    
    
    var data : CategoryContentModel! {
        didSet {
            self.configCell()
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyStyle()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func applyStyle() {
        self.imgThumb.cornerRadius(cornerRadius: 4.0)
        self.lblTitle.font(name: FontFamily.SFProDisplay.bold, size: 14.0).textColor(color: .app_222222)
        self.lblContentType.font(name: FontFamily.SFProDisplay.regular, size: 12.0).textColor(color: .app_222222)
        
    }
    
    private func configCell() {
        if let data = data {
            self.imgThumb.setImage(with: data.image)
            self.lblTitle.text = data.title
            var tags : [String] = self.data.tag.flatMap{$0.title}
            tags.append(self.data.contentType.rawValue.capitalized)
            self.lblContentType.text = tags.joined(separator: " • ")
        }
    }
}
