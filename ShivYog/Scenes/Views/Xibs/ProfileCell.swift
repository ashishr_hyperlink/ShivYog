//
//  ProfileCell.swift
//  ShivYog
//
//  Created by 2022M30 on 28/11/22.
//

import UIKit

class ProfileCell: UITableViewCell {
    
    //MARK: - Outlet -
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var imgBottom: UIImageView!
    var isZeorIndex : Bool = false
    var isLastIndex : Bool = false
    var data : ProfileModel! {
        didSet {
            self.configCell()
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.applyStyle()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    private func applyStyle() {
        self.lblTitle.font(name: FontFamily.SFProDisplay.regular, size: 18.0).textColor(color: .app_000000)
        self.imgBottom.backGroundColor(color: .app_990000.withAlphaComponent(0.1))
    }
    
    private func configCell() {
        if let data = data {
            self.lblTitle.text = data.optionType.rawValue
        }
    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        
//        if self.isZeorIndex {
//            self.vwContainer.roundCorners([.topLeft, .topRight], radius: 24.0)
//            self.imgBottom.isHidden = false
//        } else if self.isLastIndex {
//            self.vwContainer.roundCorners([.bottomLeft, .bottomRight], radius: 24.0)
//            self.imgBottom.isHidden = true
//        }
//    }
    
}
