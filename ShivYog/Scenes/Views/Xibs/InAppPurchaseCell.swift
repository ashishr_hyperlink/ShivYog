//
//  InAppPurchaseCell.swift
//  ShivYog
//
//  Created by 2022M30 on 26/01/23.
//

import UIKit

class InAppPurchaseCell: UICollectionViewCell {
    
    //MARK: - Outlet -
    
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var btnContentType: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContentSubType: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
    
    var data: CategoryContentModel? {
        didSet {
            self.configCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyStyle()
        // Initialization code
    }
    
    private func applyStyle() {
        self.imgProduct.cornerRadius(cornerRadius: 10.0)
        
        self.btnContentType.backGroundColor(color: .app_990000).font(name: FontFamily.SFProDisplay.medium, size: 12.0).textColor(color: .appFfffff).borderColor(color: .appFfffff, borderWidth: 1.0).imageRightSide()
        self.btnContentType.centerTextAndImage(spacing: 2.0)
        
        self.lblTitle.font(name: FontFamily.SFProDisplay.bold, size: 14.0).textColor(color: .app_222222)
        self.lblDescription.font(name: FontFamily.CircularStd.book, size: 12.0).textColor(color: .app_5E5E5E)
        self.lblContentSubType.font(name: FontFamily.DMSans.regular, size: 12.0).textColor(color: .app_990000)
        self.lblPrice.font(name: FontFamily.SFProDisplay.bold, size: 14.0).textColor(color: .app_990000)
        self.btnSelect.font(name: FontFamily.SFProDisplay.bold, size: 12.0).textColor(color: .appFfffff).backGroundColor(color: .app_990000)
        
        DispatchQueue.main.async {
            self.btnContentType.setRound()
            self.btnSelect.setRound()
        }
    }
    
    private func configCell() {
        if let data = data {
            self.imgProduct.setImage(with: data.image)
            self.lblTitle.text = data.title
            self.lblDescription.text = data.description
            self.btnContentType.isHidden = true
            self.lblContentSubType.isHidden = true
            if let user = UserModel.currentUser {
                if user.countryCode == "+91" {
                    self.lblPrice.text = "₹ " + data.inrPrice
                } else {
                    self.lblPrice.text = "$ " + data.dollarPrice
                }
            }
        }
    }

}
