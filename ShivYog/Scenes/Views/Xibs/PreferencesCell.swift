//
//  PreferencesCell.swift
//  ShivYog
//
//  Created by 2022M30 on 29/11/22.
//

import UIKit

class PreferencesCell: UICollectionViewCell {
    
    //MARK: - Outlet -
    
    @IBOutlet weak var imgSelection: UIImageView!
    @IBOutlet weak var imgOption: UIImageView!
    @IBOutlet weak var lblOptionType: UILabel!
    
    var data : PreferenceModel? {
        didSet {
            self.configCell()
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.applyStyle()
    }
    
    private func applyStyle() {
        self.lblOptionType.font(name: FontFamily.CircularStd.book, size: 18.0).textColor(color: .app_222222)
        self.imgSelection.superview?.cornerRadius(cornerRadius: 10.0)
        self.imgOption.contentMode = .redraw
        if let cons = (self.imgOption.constraints.filter{$0.firstAttribute == .width}).first{
            cons.constant = (ScreenSize.width - 230) / 2
        }
    }
    
    private func configCell() {
        if let data = data {
            self.imgSelection.isHighlighted = data.isSelected
            self.imgOption.setImage(with: data.image)
            self.lblOptionType.text = data.title
        }
    }
}
