//
//  CategoriesCell.swift
//  ShivYog
//
//  Created by 2022M30 on 02/12/22.
//

import UIKit

class CategoriesCell: UITableViewCell {

    //MARK: - Outlet -
    @IBOutlet weak var lblCategoryTitle: UILabel!
    @IBOutlet weak var colCategory: UICollectionView!
    @IBOutlet weak var constColHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSeeAll: UIButton!
    
    
    var completion : ((String, CategoryContentModel) -> Void)?
    var openMediaScreensCompletion : ((ContentType, CategoryContentModel) -> Void)?
    var completionRemove : ((CategoryContentModel) -> Void)?
    var isFrom : IsFrom = .home
    var index : Int = 0
    var shouldShowRemove : Bool = false
    
    
    var data : SubCategoryModel = SubCategoryModel(fromJson: JSON()) {
        didSet {
            self.configCell()
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyStyle()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func applyStyle() {
        self.lblCategoryTitle.font(name: FontFamily.SFProDisplay.bold, size: 16.0).textColor(color: .app_222222)
        self.colCategory.registerNib(forCellWithClass: CategoryImageCell.self)
        self.colCategory.delegate = self
        self.colCategory.dataSource = self
        self.constColHeight.constant = ((ScreenSize.width - 48) / 2.45 ) * 1.3//142 * ScreenSize.heightAspectRatio//((ScreenSize.width - 48 ) / 2.33) * 1.3
        self.btnSeeAll.font(name: FontFamily.CircularStd.book, size: 12.0).textColor(color: .app_990000).imageRightSide()
        self.btnSeeAll.centerTextAndImage(spacing: 4.0)
    }
    
    private func configCell() {
        self.lblCategoryTitle.text = self.data.title
        if self.isFrom == .yogaStore {
            self.data.storecontent = self.data.storecontent.filter({ !($0.isPurchase) })
        }
        self.colCategory.reloadData()
    }
}


extension CategoriesCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.isFrom == .yogaStore ? data.storecontent.count : data.categoryContent.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: CategoryImageCell.self, for: indexPath)
        let datatAtIndex = self.isFrom == .yogaStore ? data.storecontent[indexPath.row] : data.categoryContent[indexPath.row]
        if let image = UIImage(named: datatAtIndex.image){
            cell.imgProduct.image = image
        }
        else{
            cell.imgProduct.setImage(with: datatAtIndex.image)
        }
        
        DispatchQueue.main.async {
            cell.lblTitle.superview?.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 20.0)
            cell.lblTitle.textAlignment = .center
            cell.lblTitle.forceScrolling = true
//            cell.lblTitle.type = .continuous
//            cell.lblTitle.animationCurve = .easeInOut
//            cell.lblTitle.fadeLength = 10.0
            cell.lblTitle.leadingBuffer = 5.0
            cell.lblTitle.trailingBuffer = 20.0
            cell.lblTitle.triggerScrollStart()
        }
        cell.imgPlay.isHidden = datatAtIndex.contentType == .folder
        
        cell.lblTitle.text = datatAtIndex.title
        cell.btnRemove.isHidden = !self.shouldShowRemove
        cell.btnMore.isHidden = true
        cell.btnFreeTag.isHidden = self.shouldShowRemove ? true : (!datatAtIndex.isSubscribe || self.shouldShowRemove)
        switch datatAtIndex.contentType{
        case .video, .audio:
            cell.imgPlay.image = UIImage.playRoundWhite.image
            break
        case .pdf:
            cell.imgPlay.image = UIImage.pdfWhite.image
            break
        default:
//            if self.data.title == HomePageTitle.FeaturedPlaylists.rawValue{
//                cell.imgPlay.image = UIImage.playRoundWhite.image
//                cell.imgPlay.isHidden = false
//            }
            
            break
        }
        cell.btnRemove.addTapGestureRecognizer {
            self.completionRemove?(datatAtIndex)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? CategoryImageCell
        if self.isFrom == .yogaStore {
            let data = self.data.storecontent[indexPath.row]
            self.completion?("", data)
        } else {
            let data = self.data.categoryContent[indexPath.row]
            if data.contentType == .folder {
                self.completion?(cell!.lblTitle.text!, self.data.categoryContent[indexPath.row])
            } else {
                self.openMediaScreensCompletion?(data.contentType, data)
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (ScreenSize.width - 48) / 2.45
        return CGSize(width: width, height: width * 1.27)
    }
}
