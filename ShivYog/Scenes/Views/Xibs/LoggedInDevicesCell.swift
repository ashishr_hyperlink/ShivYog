//
//  LoggedInDevicesCell.swift
//  ShivYog
//
//  Created by Ashish on 10/02/23.
//

import UIKit

class LoggedInDevicesCell: UITableViewCell {
    
    //MARK: - Outlets
    
    @IBOutlet var vwBase: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubTitle: UILabel!
    //-----------------------------------------------------------
    
    //MARK: - Class Variables
    var data : DeviceDetail = DeviceDetail(fromJson: JSON()){
        didSet{
            self.configCell()
        }
    }
    //-----------------------------------------------------------

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.applyStyle()
    }
    
    private func applyStyle() {
        self.lblTitle.font(name: FontFamily.SFProDisplay.bold, size: 20).textColor(color: .app_000000)
        self.lblSubTitle.font(name: FontFamily.SFProDisplay.medium, size: 16).textColor(color: .app_979797)
        self.vwBase.backGroundColor(color: .appF1F1F1.withAlphaComponent(0.5)).cornerRadius(cornerRadius: 10)
        self.vwBase.borderWidth = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(){
        self.lblSubTitle.text = "%@ %@".localized(self.data.deviceName, self.data.modelName)
        self.vwBase.borderColor = self.data.isSelected ? .app_990000 : self.vwBase.backgroundColor
    }
}
