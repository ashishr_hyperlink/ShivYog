//
//  HomeProductCell.swift
//  ShivYog
//
//  Created by 2022M30 on 05/12/22.
//

import UIKit

class HomeProductCell: UICollectionViewCell {
    //MARK: - Outlet -
    
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var imgPremium: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var imgFavourite: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
//    @IBOutlet weak var constVwWidth: NSLayoutConstraint!
//    @IBOutlet weak var constVwHeight: NSLayoutConstraint!
    @IBOutlet weak var vwContainer: UIView!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyStyle()
        // Initialization code
    }
    
    private func applyStyle() {
        self.imgProduct.cornerRadius(cornerRadius: 20.0)
        
        
        self.lblTitle.font(name: FontFamily.CircularStd.medium, size: 18.0).textColor(color: .appFfffff)
        self.lblType.font(name: FontFamily.CircularStd.medium, size: 10.0).textColor(color: .appFfffff)
        self.btnDownload.font(name: FontFamily.SFProDisplay.semibold, size: 8.0).textColor(color: .app_000000).backGroundColor(color: .appFfffff).imageRightSide()
        self.btnDownload.centerTextAndImage(spacing: 6.0)
        self.btnPlay.font(name: FontFamily.SFProDisplay.semibold, size: 8.0).textColor(color: .app_000000).backGroundColor(color: .appFfffff).imageRightSide()
        self.btnPlay.centerTextAndImage(spacing: 5.0)
        self.vwContainer.backGroundColor(color: .app_990000).cornerRadius(cornerRadius: 35.0)
//        self.constVwWidth.constant = ScreenSize.width - 73
//        self.constVwHeight.constant = (ScreenSize.width - 73) * 0.6
        
        DispatchQueue.main.async {
            self.btnDownload.cornerRadius(cornerRadius: self.btnDownload.frame.height / 2)
            self.btnPlay.cornerRadius(cornerRadius: self.btnDownload.frame.height / 2)
            self.imgPremium.applyViewShadow(shadowOffset: CGSize(width: 0.0, height: 10.0), shadowColor: .app_000000, shadowOpacity: 0.3, shdowRadious: 8.0)
        }
    }

}
