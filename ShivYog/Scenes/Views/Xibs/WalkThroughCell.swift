//
//  WalkThroughCell.swift
//  ShivYog
//
//  Created by 2022M30 on 24/11/22.
//

import UIKit

class WalkThroughCell: UICollectionViewCell {
    //MARK: - Outlets -
    @IBOutlet weak var imgWalk: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    var data : WalkThroughModel! {
        didSet {
            configCell()
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.applyStyle()
    }

    
    func applyStyle() {
        self.lblTitle.font(name: FontFamily.SFProDisplay.medium, size: 30.0).textColor(color:.appFfffff)
        self.lblSubTitle.font(name: FontFamily.SFProDisplay.regular, size: 14.0).textColor(color: .appFfffff).lineSpacing(lineSpacing: 10.0, alignment: .center)
    }
    
    func configCell() {
        guard data != nil else { return }
        
        self.imgWalk.image = data.image
        self.lblTitle.text = data.title.uppercased()
        self.lblSubTitle.text = data.subTitle
    }
}
