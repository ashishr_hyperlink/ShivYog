//
//  BadgesCell.swift
//  ShivYog
//
//  Created by 2022M30 on 25/01/23.
//

import UIKit

class BadgesCell: UITableViewCell {
    
    //MARK: - Outlet -
    @IBOutlet weak var imgBadge: UIImageView!
    @IBOutlet weak var lblBadgeTitle: UILabel!
    @IBOutlet weak var lblBadgeDescription: UILabel!
    
    var data : BadgesModel? {
        didSet {
            self.configCell()
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyStyle()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func applyStyle() {
        self.lblBadgeTitle.font(name: FontFamily.SFProDisplay.semibold, size: 17.0).textColor(color: .app_050505)
        self.lblBadgeDescription.font(name: FontFamily.SFProDisplay.regular, size: 12.0).textColor(color: .app_222222)
    }
    
    private func configCell() {
        if let data = data {
            self.lblBadgeTitle.text = data.title
            self.imgBadge.setImage(with: data.image)

            if !data.isEarned {
                self.imgBadge.alpha = 0.5
                self.lblBadgeTitle.textColor(color: .app_000000.withAlphaComponent(0.5))
                self.lblBadgeDescription.textColor(color: .app_000000.withAlphaComponent(0.5))
            } else {
                self.imgBadge.alpha = 1.0
                self.lblBadgeTitle.textColor(color: .app_050505)
                self.lblBadgeDescription.textColor(color: .app_222222)
            }
        }
    }
    
}
