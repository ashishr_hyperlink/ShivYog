//
//  ProductCell.swift
//  ShivYog
//
//  Created by 2022M30 on 30/11/22.
//

import UIKit

class ProductCell: UITableViewCell {
    
    //MARK: - Outlet -
    
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var btnContentType: UIButton!
    @IBOutlet weak var imgFree: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnMore: UIButton!
    
//    var data: ProductModel! {
//        didSet {
//            self.configCell()
//        }
//    }
    var isFrom : IsFrom = .home
    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyStyle()
        // Initialization code
    }

//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
    
    private func applyStyle() {
        self.imgProduct.cornerRadius(cornerRadius: 10.0)
        
        self.btnContentType.cornerRadius(cornerRadius: self.btnContentType.frame.height / 2).backGroundColor(color: .app_990000).font(name: FontFamily.SFProDisplay.medium, size: 12.0).textColor(color: .appFfffff).borderColor(color: .appFfffff, borderWidth: 1.0).imageRightSide()
        self.btnContentType.centerTextAndImage(spacing: 6.0)
        
        self.lblTitle.font(name: FontFamily.SFProDisplay.medium, size: 16.0).textColor(color: .app_222222)
        self.lblDescription.font(name: FontFamily.PTSans.regular, size: 12.0).textColor(color: .app_39321C)
    }
    
    private func configCell() {
//        if let data = data {
//            self.lblTitle.text = data.title
//            self.lblDescription.text = data.subTitle
//            
//            if data.isPlaylist && !(self.isFrom == .yogaStore){
//                
//                self.btnContentType.isHidden = true
//                self.btnMore.isHidden = true
//            } else if !(data.isPlaylist) && self.isFrom == .yogaStore {
//                self.btnContentType.isHidden = true
//                self.btnMore.isHidden = true
//            }else {
//                
//                self.btnContentType.setTitle( data.contentType == .pdf ? "Read" : "30 min", for: .normal)
//                self.btnContentType.setImage( data.contentType == .pdf ? .pdfWhite.image : .playWhite.image, for: .normal)
//            }
//            
//            
//        }
    }
    
}
