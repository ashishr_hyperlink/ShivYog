//
//  SelectTimeVC.swift
//  ShivYog
//
//  Created by 2022M30 on 26/01/23.
//

import UIKit

class SelectTimeVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var btnWeekly: UIButton!
    @IBOutlet weak var btnMonthly: UIButton!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
//    private var viewModel : AuthViewModel = AuthViewModel()
    var journeyTypeCompletion : ((MyJourneyTimeType) -> Void)?
    var timeRangeType : MyJourneyTimeType = .weekly
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.btnWeekly.font(name: FontFamily.SFProDisplay.medium, size: 16.0).textColor(color: .app_000000, state: .normal).textColor(color: .appFfffff, state: .selected).borderColor(color: .app_222222.withAlphaComponent(0.8), borderWidth: 1.0)
        self.btnMonthly.font(name: FontFamily.SFProDisplay.medium, size: 16.0).textColor(color: .app_000000, state: .normal).textColor(color: .appFfffff, state: .selected).borderColor(color: .app_222222.withAlphaComponent(0.8), borderWidth: 1.0)
        
        
        DispatchQueue.main.async {
            self.vwContainer.roundCorners([.topLeft, .topRight], radius: 6.0)
            self.btnWeekly.cornerRadius(cornerRadius: 6.0)
            self.btnMonthly.cornerRadius(cornerRadius: 6.0)
        }
        self.setUpButtons(sender: self.timeRangeType == .weekly ? self.btnWeekly : self.btnMonthly)
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })*/
    }
    
    private func setUpButtons(sender: UIButton) {
        self.btnWeekly.isSelected = false
        self.btnMonthly.isSelected = false
        
        self.btnWeekly.borderColor(color: .app_222222.withAlphaComponent(0.8), borderWidth: 1.0).backGroundColor(color: .clear)
        self.btnMonthly.borderColor(color: .app_222222.withAlphaComponent(0.8), borderWidth: 1.0).backGroundColor(color: .clear)
        sender.isSelected = true
        sender.borderColor(color: .appFfffff, borderWidth: 1.0).backGroundColor(color: .app_222222)
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    @IBAction func btnWeeklyClicked(_ sender: UIButton) {
        self.setUpButtons(sender: self.btnWeekly)
        self.sheetViewController?.animateOut(duration: 0.3, completion: {
            self.journeyTypeCompletion?(.weekly)
        })
//        self.dismiss(animated: true) {
//
//        }
    }
    
    
    
    @IBAction func btnMonthlyClicked(_ sender: UIButton) {
        self.setUpButtons(sender: self.btnMonthly)
        self.sheetViewController?.animateOut(duration: 0.3, completion: {
            self.journeyTypeCompletion?(.monthly)
        })
//        self.dismiss(animated: true) {
//            self.journeyTypeCompletion?(.monthly)
//        }
    }
    
    //------------------------------------------------------
    
}
