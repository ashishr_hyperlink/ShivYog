//
//  CreatePlaylistVC.swift
//  ShivYog
//
//  Created by 2022M30 on 01/12/22.
//

import UIKit

class CreatePlaylistVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var imgCancel: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtPlaylist: UITextField!
    @IBOutlet weak var btnCreate: ThemeButton!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var mySpaceVM : MySpaceViewModel = MySpaceViewModel()
    var completion : (()-> Void)?
    var mediaContent : CategoryContentModel?
    var contentTitle : String?
    var isFromEdit : Bool = false
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    override func viewWillLayoutSubviews() {
        DispatchQueue.main.async {
            self.btnCreate.setRound()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        
        self.lblTitle.font(name: FontFamily.SFProDisplay.medium, size: 36.0).textColor(color: .app_000000)
        
        self.txtPlaylist.attributedPlaceholder = NSAttributedString(string: "Enter playlist name", attributes: [.font: FontFamily.SFProDisplay.bold.font(size: 18.0)!, .foregroundColor: UIColor.app_000000.withAlphaComponent(0.5)])
        let _ = self.txtPlaylist.superview?.addBottomBorderWithColor(color: .app_000000, origin: CGPoint(x: 0.0, y: 0.0), width: self.txtPlaylist.frame.width, height: 1.0)
        
        self.txtPlaylist.font(name: FontFamily.SFProDisplay.bold, size: 18.0).textColor(color: .app_000000)
        self.txtPlaylist.delegate = self
        
        self.imgCancel.addTapGestureRecognizer {
            self.dismiss(animated: true, completion: nil)
        }
        
        if self.isFromEdit {
            self.btnCreate.setTitle("Edit", for: .normal)
            self.lblTitle.text = "Edit Your Playlist \nName"
            if let data = self.mediaContent {
                self.txtPlaylist.text = data.title
            }

        }
        
        if let mediaContent = mediaContent {
            if let detail = mediaContent.subcategorycontentDetails {
                self.contentTitle = detail.title
            }
            if let detail = mediaContent.storesubcontentDetails {
                self.contentTitle = detail.title
            }
        }
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
        
        self.mySpaceVM.createPlaylistResult.bind(observer: { [unowned self] (result) in
            switch result {
            case .success(_):
                AppDelegate.shared.addAnalyticsEvent(screenName: .VideoAudio, eventName: .Addtoplaylist, category: .Content, action: "Add to playlist", label: "\(self.contentTitle ?? "") : \(self.mediaContent?.title ?? "")")
                Alert.shared.showSnackBar("Content added to the playlist", isError: false)
                DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                    self.dismiss(animated: false) {
                        self.completion?()
                    }
                }
                
                
                break
                
            case .failure(let error):
                Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
                
            case .none: break
            }
        })
        
        self.mySpaceVM.deletePlaylist.bind { isChanged in
            if isChanged ?? false {
                Alert.shared.showSnackBar("Playlist title changed successfully")
                DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                    self.dismiss(animated: false) {
                        self.completion?()
                    }
                }
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    @IBAction func btnCreateClicked(_ sender: ThemeButton) {
        self.mySpaceVM.apiCreateName(name: self.txtPlaylist.text!, content: self.mediaContent, isFromEdit: self.isFromEdit)
    }
    
    
    //------------------------------------------------------
    
}

extension CreatePlaylistVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
