//
//  MySpaceVC.swift
//  ShivYog
//
//  Created by 2022M30 on 01/12/22.
//

import UIKit

class MySpaceVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var colList: UICollectionView!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var profileVM : ProfileViewModel = ProfileViewModel()
    private var homeVM : HomeViewModel = HomeViewModel()
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        debugPrint("load")
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        self.navigationController?.clearNavigation(textColor: .appFfffff, navigationColor: .app_990000)
        self.colList.reloadData()
        AppDelegate.shared.addAnalyticsEvent(screenName: .MySpace, eventName: .Pageview, category: .MySpace)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.clearNavigation(textColor: .appFfffff, navigationColor: .app_990000)
        self.colList.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.clearNavigation()
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.colList.registerNib(forCellWithClass: MySpaceCell.self)
        self.colList.delegate = self
        self.colList.dataSource = self
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })*/
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    @IBAction func btnSearchClicked(_ sender: UIBarButtonItem) {
        let vc = StoryboardScene.Home.searchVC.instantiate()
        vc.isFrom = .mySpace
        vc.modalPresentationStyle = .overFullScreen
        vc.hidesBottomBarWhenPushed = true
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
}

//MARK: - UICollectionView delegate and Datasource -

extension MySpaceVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.profileVM.numberOfPreferencesItem(isFromSpace: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: MySpaceCell.self, for: indexPath)
//        cell.isFrom = .mySpace
        cell.data = self.profileVM.dataAtIndexForPreferences(isFromSpace: true, index: indexPath.row)
        cell.imgSelection.isHidden = true
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ( ScreenSize.width - 50) / 2
        
        return CGSize(width: width, height:/* 150 * ScreenSize.fontAspectRatio*/ width * 1.058)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = self.profileVM.dataAtIndexForPreferences(isFromSpace: true, index: indexPath.row)
        switch data.isFrom {
        case .myProducts:
            let vc = StoryboardScene.Home.productListingVC.instantiate()
//            vc.hidesBottomBarWhenPushed = true
//            vc.arrData = self.homeVM.arrNewPlaylist
            vc.screenTitle = "My Products"
//            vc.arrSubData = self.homeVM.arrProductModel
            vc.isFrom = .mySpace
//            vc.isFromPlaylist = true
//            vc.contentType = .normal
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case .playlists:
//            let vc = StoryboardScene.MySpace.playlistVC.instantiate()
            let vc = StoryboardScene.Home.productListingVC.instantiate()
//            vc.hidesBottomBarWhenPushed = true
//            vc.arrData = self.homeVM.arrNewPlaylist
            vc.screenTitle = "My Playlist"
            vc.isFromPlaylist = true
            vc.isFrom = .playlists
//            vc.contentType = .folder
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case .downloads:
            let vc = StoryboardScene.Home.productItemsVC.instantiate()
//            vc.hidesBottomBarWhenPushed = true
            vc.screenTitle = "Downloads"
            vc.isFrom = .downloads
//            vc.contentType = .normal
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case .favourites:
            let vc = StoryboardScene.Home.productItemsVC.instantiate()
//            vc.hidesBottomBarWhenPushed = true
            vc.screenTitle = "My Favorites"
            vc.isFrom = .favourites
//            vc.contentType = .normal
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case .myJourney:
            let vc = StoryboardScene.MySpace.myJourneyVC.instantiate()
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
//            Alert.shared.showSnackBar("Under Development")
            break
        default:
            break
        }
//        self.colList.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        var edgeInset = UIEdgeInsets(top: 20, left: 18, bottom: 20, right: 18)
        if GAudioPlayer.shared.isPlaying{
            edgeInset.bottom = 20 + kMiniPlayerConstant
        }
        return edgeInset
    }
}

