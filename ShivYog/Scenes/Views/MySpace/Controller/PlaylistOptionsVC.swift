//
//  PlaylistOptionsVC.swift
//  ShivYog
//
//  Created by 2022M30 on 01/12/22.
//

import UIKit
import SwiftyJSON

class PlaylistOptionsVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var vwDownloadNow: UIView!
    @IBOutlet weak var vwShare: UIView!
    @IBOutlet weak var vwDelete: UIView!
    @IBOutlet var lblOptionsTitle: [UILabel]!
    @IBOutlet weak var lblCancel: UILabel!
    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var vwAddToPlaylist: UIView!
    @IBOutlet weak var vwChangePlaylistTitle: UIView!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
//    private var viewModel : AuthViewModel = AuthViewModel()
    var isFrom : IsFrom = .playlists
    var mediaData : CategoryContentModel?
    var completion : ((Bool, PlaylistOptions) -> Void)?
    var deletePlaylistCompletion : ((PlaylistOptions, CategoryContentModel) -> Void)?
    var arrDownloadedContent : [DownloadModel] = []
    var tempMediaData : CategoryContentModel?
    var isFromPlayer : Bool = false
    var contentTitle : String?
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.tabBarController?.tabBar.isHidden = true
//        TabVC.sharedTab.vwBottom.isHidden = true//tabBar.isHidden = true
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if /*self.isFrom != .videoPlayer*/ !self.isFromPlayer {
            self.tabBarController?.tabBar.isHidden = false
//            (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        }
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        UserDefaultsConfig.isFromVideoPlayer = false
        self.lblOptionsTitle.forEach {
            $0.font(name: FontFamily.SFProDisplay.regular, size: 20.0).textColor(color: .app_222222)
        }
        
        self.lblCancel.font(name: FontFamily.SFProDisplay.bold, size: 20.0).textColor(color: .app_222222)
        
        
        self.lblCancel.addTapGestureRecognizer {
            self.sheetViewController?.animateOut()
            self.completion?(true, .cancel)
        }
        
        DispatchQueue.main.async {
            self.vwContainer.roundCorners([.topLeft, .topRight], radius: 5.0)
        }
        if let mediaContent = mediaData {
            if let detail = mediaContent.subcategorycontentDetails {
                self.contentTitle = detail.title
            }
            if let detail = mediaContent.storesubcontentDetails {
                self.contentTitle = detail.title
            }
        }
        self.setupOptions()
        self.setTapGestures()
        self.getDownloadData()
    }
    
    private func setupOptions() {
        self.vwAddToPlaylist.isHidden = (self.isFrom == .playlists || self.mediaData?.contentType == .pdf || self.isFrom == .downloads || self.isFrom == .deletePlaylist)
        self.vwDelete.isHidden = !(self.isFrom == .playlists || self.isFrom == .downloads || self.isFrom == .favourites || self.isFrom == .deletePlaylist)
        self.vwDownloadNow.isHidden = self.isFrom == .downloads || self.isFrom == .deletePlaylist
        self.vwShare.isHidden = self.isFrom == .downloads || self.isFrom == .deletePlaylist || (self.isFrom == .mySpace && (self.mediaData?.isExclusive ?? false))
        if self.isFrom == .playlists || self.isFrom == .favourites{
            self.lblOptionsTitle[safe : 2]?.text = "Remove"
        }
        self.vwChangePlaylistTitle.isHidden = !(self.isFrom == .deletePlaylist)
//        if self.isFrom == .playlists {
//            self.vwAddToPlaylist.isHidden = true
//            self.vwDelete.isHidden = true
//        } else if self.isFrom == .downloads {
//            self.vwDownloadNow.isHidden = true
//        }
    }
    
    private func getDownloadData() {
        //        var jsonData : [JSON] = []
        //        let data = UserDefaults.standard.object(forKey: UserDefaults.Keys.downloadContent) as? Data//.data(forKey: kFruitsAndVeggies)
        //        if let downLoadData = data {
        //            do {
        //                let arrData = UserDefaultsConfig.downloadContent
        let dataDecode = UserDefaults.standard.decode(for: DownloadMainModel.self, using: UserDefaults.Keys.downloadContent)
        if let decode = dataDecode {
            let downLoadData = decode.data
            if let arrDownload = downLoadData {
                //                self.tempMediaData = arrDownload.filter { CategoryContentModel(fromJson: $0.fileContent).id! == self.mediaData?.id }
                self.arrDownloadedContent = arrDownload
                GlobalAPI.shared.arrGDownload = self.arrDownloadedContent
                if let mediaData = self.mediaData {
                    
                    arrDownload.forEach {
                        if CategoryContentModel(fromJson: $0.fileContent).id! == mediaData.id {
                            self.tempMediaData = CategoryContentModel(fromJson: $0.fileContent)
                            self.vwDownloadNow.isHidden = true
                        }
                    }
                }
            }
            
        }
    }
    
    private func setTapGestures() {
        self.vwDelete.addTapGestureRecognizer {
            if self.isFrom == .favourites{
                self.sheetViewController?.animateOut()
                Alert.shared.showAlert(message: AppMessages.removeFromFavourite, actionTitles: [AppMessages.no, AppMessages.yes], actions: [{ [weak self] (no) in
                    self?.completion?(true, .cancel)
//                    self?.sheetViewController?.animateOut()
                    return
                }, { [weak self] (yes) in
                    guard let _ = self else { return }
                    //                    if let data = self?.mediaData{
                    //                        GlobalAPI.shared.markAsFav(contentData: data)
                    //                        self?.mediaData?.favourite.toggle()
                    //                    }
                    if let mediaData = self?.mediaData{
                        GlobalAPI.shared.markAsFav(contentData: mediaData)
                        self?.completion?(true, .remove)
//                        self?.sheetViewController?.animateOut()
                        return
                    }
                }])
                
                //                if let mediaData = self.mediaData{
                //                    GlobalAPI.shared.markAsFav(contentData: mediaData)
                //                    self.completion?(true, .remove)
                //                    self.sheetViewController?.animateOut()
                //                    return
                //                }
                return
            } else if self.isFrom == .deletePlaylist {
                self.sheetViewController?.animateOut()
                Alert.shared.showAlert(message: AppMessages.removePlaylist, actionTitles: [AppMessages.no, AppMessages.yes], actions: [{ [weak self] (no) in
//                    self?.completion?(true, .cancel)
//                    self?.sheetViewController?.animateOut()
                    return
                }, { [weak self] (yes) in
                    guard let _ = self else { return }
                    //                    if let data = self?.mediaData{
                    //                        GlobalAPI.shared.markAsFav(contentData: data)
                    //                        self?.mediaData?.favourite.toggle()
                    //                    }
                    if let mediaData = self?.mediaData{
                        self?.deletePlaylistCompletion?(.remove, mediaData)
//                        self?.sheetViewController?.animateOut()
                        return
                    }
                }])
                return
            } else if self.isFrom == .playlists{
                self.sheetViewController?.animateOut()
                Alert.shared.showAlert(message: AppMessages.removeFromPlaylist, actionTitles: [AppMessages.no, AppMessages.yes], actions: [{ [weak self] (no) in
                    self?.completion?(true, .cancel)
//                    self?.sheetViewController?.animateOut()
                    return
                }, { [weak self] (yes) in
                    guard let _ = self else { return }
                    self?.completion?(true, .remove)
                }])
                
                
                return
            } else if self.isFrom == .downloads {
                self.sheetViewController?.animateOut()
                Alert.shared.showAlert(message: AppMessages.removeFromDownloads, actionTitles: [AppMessages.no, AppMessages.yes], actions: [{ [weak self] (no) in
                    self?.completion?(true, .cancel)
//                    self?.sheetViewController?.animateOut()
                    return
                }, { [weak self] (yes) in
                    guard let _ = self else { return }
                    
                    if let media = self?.mediaData {
                        
                        if media.contentType == .pdf {
                            URL.removItem(url: media.file)
                            URL.removItem(url: media.image)
                            
                            if media.attachAudioFile.count > 0 {
                                if let file = media.attachAudioFile.first?.file {
                                    URL.removItem(url: file)
                                }
                            }
                            
                            
                        } else if media.contentType == .audio {
                            URL.removItem(url: media.file)
                            URL.removItem(url: media.image)
                             
                        } else if media.contentType == .video {
                            URL.removItem(url: media.videoUrl)
                            URL.removItem(url: media.image)
                        }
                        if let arr = self?.arrDownloadedContent {
                            
                            for (idx,item) in arr.enumerated() {
                                if CategoryContentModel(fromJson: item.fileContent).id == media.id {
                                    self?.arrDownloadedContent.remove(at: idx)
                                    break
                                }
                            }
                        }
                        
                        
                        let data =  DownloadMainModel(modelData: self?.arrDownloadedContent ?? [])
                        UserDefaults.standard.encode(for: data, using: UserDefaults.Keys.downloadContent)
                        
                            self?.completion?(true, .remove)
                        
                    }
                }])
                return
            }
            Alert.shared.showSnackBar("Under Development")
            self.sheetViewController?.animateOut()
        }
        
        self.vwChangePlaylistTitle.addTapGestureRecognizer {
            self.sheetViewController?.animateOut()
//            self.completion?(true, .editPlaylist)
            if let mediaData = self.mediaData {
                self.deletePlaylistCompletion?(.editPlaylist, mediaData)
            }
        }
        
        self.vwDownloadNow.addTapGestureRecognizer {
            
            if self.mediaData?.contentType == .video{
                let vc = StoryboardScene.MySpace.downloadOptionsVC.instantiate()
                vc.arrData = (self.mediaData?.files ?? []).filter{$0.quality != "hls"}
                vc.completion = { downloadRes in
                    self.mediaData?.videoUrl = downloadRes.link
                    debugPrint("Download link:-",self.mediaData?.videoUrl)
                    self.checkData()
                }
                self.openBottomSheetAnimate(vc: vc, sizes: [.intrinsic])
                return
            }
            
            self.checkData()
            
        }
        
        self.vwAddToPlaylist.addTapGestureRecognizer {
            self.sheetViewController?.animateOut()
            
            let vc = StoryboardScene.MySpace.playlistVC.instantiate()
            vc.mediaData = self.mediaData
            vc.hidesBottomBarWhenPushed = true
            UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
        }
        
        self.vwShare.addTapGestureRecognizer {
            if let data = self.mediaData{
                UserDefaultsConfig.isFromVideoPlayer = self.isFromPlayer
                if data.contentType == .audio || data.contentType == .video {
                    AppDelegate.shared.addAnalyticsEvent(screenName: .ContentDetail, eventName: .Share, category: .Content, action: "Share Video / Audio", label: "\(self.contentTitle ?? "") : \(data.title ?? "")")
                }
                GFunction.shared.openContentSharning(contentData: data)
//                self.dismiss(animated: false)
                self.sheetViewController?.animateOut()
            }
            else{
                Alert.shared.showSnackBar("Under Development")
            }
            self.sheetViewController?.animateOut()
        }
    }
    
    
    private func checkData () {
        if let contentData = self.mediaData {
            AppLoader.shared.addLoader()
            
            let dict = self.convertMediaDataToDictionary(data: contentData)
            self.arrDownloadedContent.append(DownloadModel(fileContent: JSON(dict)))
            
            AppDelegate.shared.appCoordinator.downloadFile(contentData: contentData) { isTrue in
                if isTrue {
                    DispatchQueue.main.asyncAfter(deadline: .now()+2.0) {
                        if AppLoader.shared.isLoaderAnimating() {
                            AppLoader.shared.removeLoader()
                        }
                        Alert.shared.showSnackBar("Download Started")
                        let data =  DownloadMainModel(modelData: self.arrDownloadedContent)
                        UserDefaults.standard.encode(for: data, using: UserDefaults.Keys.downloadContent)
                        self.sheetViewController?.animateOut()
                    }
                }
            }
            //                Alert.shared.showSnackBar(DownloadManager.shared.arrTaskDataForDownload.count.description)
            
            //                if AppLoader.shared.isLoaderAnimating() {
            //                    AppLoader.shared.removeLoader()
            //                }
            
            //                AppDelegate.shared.appCoordinator.downloadFile(contentData: contentData) { _ , taskModel in
            //                    //                    let dict = self.convertMediaDataToDictionary(data: contentData)
            //                    //                    DownloadManager.shared.arrTaskDataForDownload.forEach { taskData in
            //                    //                    let tempTaskData = DownloadManager.shared.arrTaskDataForDownload.first {
            //                    //
            //                    //                        $0.downloadTask.taskIdentifier == taskModel.downloadTask.taskIdentifier && $0.status == .completed
            //                    //                    }
            //
            //
            //                }
            DownloadManager.shared.downloadTaskCompletion = { sessionTask in
                debugPrint("TASK Compelted task in progress completion:-", sessionTask)
//                var index: Int = -1
//                var id: Int = -1
                //                    completion?(true, DownloadManager.shared.arrTaskDataForDownload.first {
                //                        $0.downloadTask.taskIdentifier == task.taskIdentifier
                //                    }!)
                
                let taskModel = DownloadManager.shared.arrTaskDataForDownload.first {
                    $0.downloadTask.taskIdentifier == sessionTask.taskIdentifier && $0.status == .completed
                }
                
                if let task = taskModel {
                    
                    for (idx, arrDefault) in self.arrDownloadedContent.enumerated() {
                        let modelData = CategoryContentModel(fromJson: arrDefault.fileContent)
                            var contentTitle : String = ""
                            if let detail = modelData.subcategorycontentDetails {
                                contentTitle = detail.title
                            }
                            if let detail = modelData.storesubcontentDetails {
                                contentTitle = detail.title
                        }
                        let dwnFileType = task.downloadingFileType
                        
                        if modelData.id == task.contentModel.id {
                            if modelData.contentType == .pdf {
                                if modelData.attachAudioFile.count == 0 {
                                    arrDefault.isAttachedFileDownload = true
                                }
                                
                                if dwnFileType == .attachedFile {
                                    modelData.attachAudioFile[0].file = task.localFileLocation
                                    arrDefault.isAttachedFileDownload = true
                                    
                                } else if dwnFileType == .pdf {
                                    modelData.file = task.localFileLocation
                                    arrDefault.isFileDownload = true
                                    
                                } else if dwnFileType == .thumbImage {
                                    modelData.image = task.localFileLocation
                                    arrDefault.isThumbDownload = true
                                }
                            } else {
                                if dwnFileType == .audio {
                                    modelData.file = task.localFileLocation
                                    arrDefault.isFileDownload = true
                                    
                                } else if dwnFileType == .video  {
                                    
                                    modelData.videoUrl = task.localFileLocation
                                    arrDefault.isFileDownload = true
                                    debugPrint("Main Data :-", modelData.videoUrl as Any)
                                    
                                } else if dwnFileType == .thumbImage {
                                    modelData.image = task.localFileLocation
                                    arrDefault.isThumbDownload = true
                                }
                            }
                            
                            if modelData.contentType == .pdf {
                                arrDefault.isDownloaded = arrDefault.isThumbDownload && arrDefault.isFileDownload && arrDefault.isAttachedFileDownload
                            } else {
                                arrDefault.isDownloaded = arrDefault.isThumbDownload && arrDefault.isFileDownload
                            }
                            
                            //                                if arrDefault.isDownloaded {
                            let dict = self.convertMediaDataToDictionary(data: modelData)
                            var jsonData = JSON(dict)
                            jsonData["video_url"] = jsonData["videoUrl"]
//                            jsonData["subcategorycontent_details"] = jsonData["subcategorycontentDetails"]
                            self.arrDownloadedContent[idx].fileContent = jsonData//JSON(dict)
                            
                            if arrDefault.isDownloaded {
                                DispatchQueue.main.async {
//                                    id = modelData.id
                                    DownloadManager.shared.arrTaskDataForDownload = DownloadManager.shared.arrTaskDataForDownload.filter{ $0.contentModel.id != modelData.id }
                                    Alert.shared.showSnackBar("\(modelData.title!) is Downloaded")
                                    
                                    if modelData.contentType == .video {
                                        AppDelegate.shared.addAnalyticsEvent(screenName: .ContentDetail, eventName: .Videodownload, category: .Content, action: "Video download",label: "\(contentTitle ) : \(modelData.title ?? "")")
                                    } else if modelData.contentType == .audio {
                                        AppDelegate.shared.addAnalyticsEvent(screenName: .ContentDetail, eventName: .Audiodownload, category: .Content, action: "Audio download",label: "\(contentTitle) : \(modelData.title ?? "")")
                                    }
                                }
                            }
                            break
                            //                                }
                        }
                    }
                    
//                    if id != -1 {
                        
//                    }
//                    for (idx,item) in DownloadManager.shared.arrTaskDataForDownload.enumerated() {
//                        if task.downloadTask.taskIdentifier == item.downloadTask.taskIdentifier && item.status == .completed {
//                            DownloadManager.shared.arrTaskDataForDownload.remove(at: idx)
//                            break
//                        }
//                    }
                }
                
                DispatchQueue.main.async {
                    let data =  DownloadMainModel(modelData: self.arrDownloadedContent)
                    UserDefaults.standard.encode(for: data, using: UserDefaults.Keys.downloadContent)
                    if let topVC = UIApplication.topViewController() as? ProductItemsVC, topVC.isFrom == .downloads {
                        if let timer = topVC.downloadTimer {
                            timer.invalidate()
                        }
                        topVC.fetchData()
                    }
                }
                
            }
            
            
        }
        else{
            Alert.shared.showSnackBar("Under Development")
        }
//        DispatchQueue.main.asyncAfter(deadline: .now()+2.0) {
//            Alert.shared.showSnackBar("Download Started")
//            if AppLoader.shared.isLoaderAnimating() {
//                AppLoader.shared.removeLoader()
//            }
//            self.sheetViewController?.animateOut()
//        }
    }
    
    private func convertMediaDataToDictionary (data: CategoryContentModel) -> [String: Any] {
        var dictData = data.asDictionary
        
        if data.tag.count > 0 {
            let preferenceDict = data.tag.map {
                $0.asDictionary
            }
            dictData["tag"] = preferenceDict
        }
        
        if let _ = data.subcategorycontentDetails {
            dictData["subcategorycontent_details"] = data.subcategorycontentDetails.asDictionary
        }
        
        if let _ = data.storesubcontentDetails {
            dictData["storesubcontent_details"] = data.storesubcontentDetails.asDictionary
        }
        
        if data.files.count > 0 {
            let filesDict = data.files.map{
                $0.asDictionary
            }
            dictData["files"] = filesDict
        }
        
        if data.attachAudioFile.count > 0 {
            let attachAudioFile = data.attachAudioFile.map {
                $0.asDictionary
            }
            dictData["attach_audio_file"] = attachAudioFile
        }
        dictData["content_type"] = data.contentType.rawValue
        if let _ = dictData["video_url"] {
            dictData["video_url"] = dictData["videoUrl"]
        } else {
            dictData["video_url"] = ""
        }
        return dictData
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })*/
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    
    
    //------------------------------------------------------
    
}
