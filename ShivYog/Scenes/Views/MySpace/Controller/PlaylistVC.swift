//
//  PlaylistVC.swift
//  ShivYog
//
//  Created by 2022M30 on 01/12/22.
//

import UIKit

class PlaylistVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var vwEmpty: UIView!
    @IBOutlet weak var lblCreateList: UILabel!
    @IBOutlet weak var colList: UICollectionView!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var homeVM : HomeViewModel = HomeViewModel()
    private var mySpaceVM : MySpaceViewModel = MySpaceViewModel()
    var showData = false
    var mediaData : CategoryContentModel?
    private var arrData : [CategoryContentModel] = []
    var contentTitle : String?
    var currentPage = 1
    var isCallingApi = true
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.clearNavigation(textColor: .appFfffff, navigationColor: .app_990000)
        self.navigationController?.isNavigationBarHidden = false
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.clearNavigation()
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.setupViewModelObserver()
        self.applyStyle()
    }

    private func applyStyle(){
        self.lblCreateList.font(name: FontFamily.SFProDisplay.bold, size: 20.0).textColor(color: .app_222222)
        
//        self.tblList.registerNib(forCellWithClass: ProductCell.self)
//        self.tblList.delegate = self
//        self.tblList.dataSource = self
//        self.tblList.isHidden = !showData
        
        self.colList.registerNib(forCellWithClass: CategoryImageCell.self)
        self.colList.delegate = self
        self.colList.dataSource = self
//        self.colList.isHidden = !showData
        
//        self.vwEmpty.superview?.isHidden = showData
//        self.setData()
        self.vwEmpty.superview?.isHidden = true
        self.callAPI(withLoader: true)
        if let mediaContent = mediaData {
            if let detail = mediaContent.subcategorycontentDetails {
                self.contentTitle = detail.title
            }
            if let detail = mediaContent.storesubcontentDetails {
                self.contentTitle = detail.title
            }
        }
    }
    
    private func callAPI(withLoader: Bool) {
        self.homeVM.getPlaylistContent(withLoader: true,  page: self.currentPage)
    }
    
    private func setData() {
        self.vwEmpty.superview?.isHidden = !(self.arrData.count == 0)//!self.arrData.isEmpty
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        self.homeVM.isAPIFetchComplete.bind(observer: { [unowned self] (result) in
            self.colList.endRefreshing()
            if result ?? false {
                self.arrData = self.homeVM.arrCategoryData
                self.isCallingApi = true
                self.currentPage += 1
//                self.arrData = self.arrData.filter{!$0.mediaContent.isEmpty}
                self.colList.reloadData()
                self.setData()
            } else {
                self.isCallingApi = false
                self.setData()
            }
//            if let result = result {
//                self.arrData.append(result)
//                self.isCallingApi = true
//                self.currentPage += 1
//                self.arrData = self.arrData.filter{!$0.mediaContent.isEmpty}
//                self.colList.reloadData()
//                self.setData()
//            } else {
//                self.isCallingApi = false
//                self.setData()
//            }
////            if (result ?? []).count == 0 {
////                self.isCallingApi = false
////                self.setData()
////                return
////            }
////
////            self.arrData = result ?? []
////            self.isCallingApi = true
////            self.currentPage += 1
////            self.arrData = self.arrData.filter{!$0.mediaContent.isEmpty}
////            self.colList.endRefreshing()
////            self.colList.reloadData()
////            self.setData()
        })
        
        self.mySpaceVM.createPlaylistResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
             AppDelegate.shared.addAnalyticsEvent(screenName: .VideoAudio, eventName: .Addtoplaylist, category: .Content, action: "Add to playlist", label: "\(self.contentTitle ?? "") : \(self.mediaData?.title ?? "")")
             self.navigationController?.popViewController(animated: false)
             Alert.shared.showSnackBar("Content added to the playlist", isError: false)
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    @IBAction func btnCancelClicked(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCreatePlaylistClicked(_ sender: ThemeButton) {
        let vc = StoryboardScene.MySpace.createPlaylistVC.instantiate()
        vc.mediaContent = self.mediaData
        vc.modalPresentationStyle = .overFullScreen
        vc.completion = {
            let vc = StoryboardScene.Authentication.successVC.instantiate()
            vc.strTitle = "Playlist Created!"
            vc.strSubTitle = "Congratulations! You have successfully\ncreated your playlist."
            vc.showDoneButton = true
            vc.completion = {
//                self.showData = true
//                self.setData()
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    if let vc = self.navigationController?.viewControllers.filter{ $0.isKind(of: VideoPlayerVC.self)}.first {
                        self.navigationController?.popToViewController(vc, animated: false)
                    } else {
                        
                        self.navigationController?.popViewController(animated: false)
                    }
                }
            }
            vc.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    //------------------------------------------------------
    
}

//MARK: - UItableView delegate and Datasource -

//extension PlaylistVC: UITableViewDelegate, UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return self.homeVM.numberOfProducts(isPlaylist: true)
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withClass: ProductCell.self, for: indexPath)
//        cell.data = self.homeVM.dataAtIndexForProduct(isPlaylist: true, index: indexPath.row)
//        return cell
//    }
//}


//MARK: - UICollectionview Delegate and Datasource -

extension PlaylistVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: CategoryImageCell.self, for: indexPath)
        
        cell.btnFreeTag.isHidden = true
        cell.imgPlay.isHidden = true
        
        let data = self.arrData[indexPath.row]
        if let image = UIImage(named: data.image){
            cell.imgProduct.image = image
        }
        else{
            cell.imgProduct.setImage(with: data.image)
        }
        
        cell.lblTitle.text = data.title
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (ScreenSize.width - 55) / 2
        let height = width * 1.27
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dataAtIndex = self.arrData[indexPath.row]
        if let mediaData = mediaData{
            let isStoreContent : Bool = mediaData.content == "store media content"
            self.mySpaceVM.apiAddMediaToPlaylist(playlistId: dataAtIndex.id.description, mediaId: mediaData.id.description, isStoreContent: isStoreContent)
        }
    }
}


extension PlaylistVC {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.isAtBottom {
            if self.isCallingApi {
//                self.callAPI(strSearch: self.txtSearch.text ?? "")
                self.isCallingApi = false
                self.callAPI(withLoader: false)
            }
        }
    }
}
