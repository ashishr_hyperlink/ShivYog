//
//  GPlayer.swift
//  ShivYog
//
//  Created by 2022M30 on 27/12/22.
//

import Foundation
import UIKit
import AVKit

class GAudioPlayer: NSObject, AVAudioPlayerDelegate {
    static let shared : GAudioPlayer = GAudioPlayer()
    
    var isPlaying: Bool = false
    var player: AVPlayer!//AVAudioPlayer!
    var newPlayer : AVPlayer?
    //    var sliderTimer: Timer!
    var audioDuration = Double()
    var currentTime = Double()
//    var isRepeatSingle : Bool = false
//    var isRepeatPlaylist: Bool = false
    var isRepeat: Bool = false
    var isRepeatAll : Bool = false
    let vc = UIApplication.topViewController()?.tabBarController as? TabVC
    var currentVC = UIViewController()
    var newState : PlayerStateEnum = .stop
    var timer: Timer!
    var currentStateOfPlayer : PlayerStateEnum?
    var updateAudioPlayerCompletion : (()-> Void)?
    var tabItemsHidden : Bool = true {
        didSet {
            //            (UIApplication.topViewController()?.tabBarController as? TabVC)?.addview(frame: false)
        }
    }
    var mediaData : CategoryContentModel?
    var continueWatchingTimer : Timer!
    var lastUpdatedDuration : Double = 0
    var continueWatchingUpdateInterval : Int = 60
    var globalArrMediaData : [CategoryContentModel]?
    var globalCurrentMediaIndex : Int = -1
    
    override func awakeFromNib() {
        let name = NSNotification.Name.AVPlayerItemDidPlayToEndTime
        NotificationCenter.default.removeObserver(self, name: name, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endAudio), name: name, object: nil)
    }
    
    @objc func endAudio(){
        debugPrint("endAudio")
        if self.isRepeat{
            player?.seek(to: .zero)
            player?.play()
        }
        else{
//            self.audioStop()
            if !self.setNextItemToPlay() {
                self.audioStop()
            }
        }
        
//        self.audioStop()
    }
    
    
    func managedState(url: String? = nil, state : PlayerStateEnum? = nil, speed: String? = nil, showPlayer: Bool = true) {
        
        if state == .stop {
            self.newState = .play
            self.audioPlay(filename: url)
            return
        } else if state == .play {
            self.newState = .pause
            self.audioPause(showPlayer: showPlayer)
        } else if state == .pause {
            self.audioResume()
            self.newState = .resume
            
            //            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            //                self.setMiniplayerData()
            //            }
        } else if state == .off {
            self.audioStop()
            self.newState = .stop
        } else if state == .resume {
            self.audioPause()
            self.newState = .pause
        }
        self.currentStateOfPlayer = state ?? .off
        //        self.addMiniPlayer()
        
    }
    
    private func addMiniPlayer(frame: Bool = true) {
        self.vc?.miniPlayer.removeFromSuperview()
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [self] in
        
        self.vc?.miniPlayer = MiniPlayerView.instanceFromNib() as! MiniPlayerView
        UserDefaults.standard.set(true, forKey: UserDefaults.Keys.miniplayer)
        
        if (UIApplication.topViewController() is HomeVC) ||
            (UIApplication.topViewController() is CategoriesVC) ||
            (UIApplication.topViewController() is MySpaceVC) ||
            (UIApplication.topViewController() is MyStoreVC) ||
            (UIApplication.topViewController() is ProfileVC) {
            self.vc?.miniPlayer.frame = CGRect(x: 0, y: ScreenSize.height - (kMiniPlayerConstant + (self.vc?.tabBar.frame.height ?? 83.0) + 5.0), width: ScreenSize.width, height: kMiniPlayerConstant)
            
        } else {
            self.vc?.miniPlayer.frame = CGRect(x: 0, y: ScreenSize.height - kMiniPlayerConstant , width: ScreenSize.width, height: kMiniPlayerConstant)
        }
        
        self.vc?.view.addSubview(self.vc!.miniPlayer)
        
        //            self.miniPlayer.backgroundColor = .clear
        self.vc?.miniPlayer.isHidden = frame
    }
    
    private func setNextItemToPlay() -> Bool{
        self.globalCurrentMediaIndex += 1
        if let data = self.globalArrMediaData {
            if data.count == self.globalCurrentMediaIndex, self.isRepeatAll {
                self.globalCurrentMediaIndex = 0
            }
            
            if self.isRepeatAll, data.count - 1 >= self.globalCurrentMediaIndex {
                self.continueWatchingUpdate()
                self.continueWatchingTimer?.invalidate()
                self.player = nil
                self.mediaData = data[self.globalCurrentMediaIndex]
                self.managedState(url: self.mediaData?.file, state: .stop, speed: "1")
                if let audioVC = UIApplication.topViewController() as? AudioPlayerVC {
                    AppLoader.shared.addLoader()
                    audioVC.mediaData = self.mediaData
                    audioVC.vwSlider.selectedMaxValue = 0.0
                    audioVC.vwSlider.refresh()
                    audioVC.lblStartTime.text = "00:00"
                    audioVC.lblEndTime.text = "00:00"
                    audioVC.setPlayerData()
                }
                if !((UIApplication.topViewController()?.parent?.parent as? TabVC)?.miniPlayer.vwContainer.isHidden ?? false) {
                    self.vc?.miniPlayer.setData()
                }
                
                
               return true
            }
            return false
        }
        return false
    }
    
    
    //MARK: audioPlayFunction
    private func audioPlay(filename : String?){
        if let fileUrl = filename {
            //"https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3"
            let name = NSNotification.Name.AVPlayerItemDidPlayToEndTime
            NotificationCenter.default.removeObserver(self, name: name, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.endAudio), name: name, object: nil)
            if let urlString =  URL(string:  fileUrl) {
                do {
                    
                    try AVAudioSession.sharedInstance().setActive(true)
                    try AVAudioSession.sharedInstance().setCategory(.playback)
                    //                    try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
                    
                    let asset : AVURLAsset = AVURLAsset(url: urlString, options: nil)
                    let statusKey = "tracks"
                    
                    asset.loadValuesAsynchronously(forKeys: [statusKey]) {
                        var error: NSError? = nil
                        
                        DispatchQueue.main.async {
                            let status : AVKeyValueStatus = asset.statusOfValue(forKey: statusKey, error: &error)
                            debugPrint("main async error-", error)
                            if status == AVKeyValueStatus.loaded {
                                
                                let playerItem = AVPlayerItem(asset: asset)
                                self.player = AVPlayer(playerItem: playerItem)
                                self.player?.play()
                                let newTime = self.mediaData?.watchedDuration ?? 0
                                if newTime > 0{
                                    let time: CMTime = CMTimeMake(value: Int64(newTime * 1000), timescale: 1000)
                                    self.player?.seek(to: time)
                                }
                                self.audioDuration = CMTimeGetSeconds((self.player?.currentItem!.asset.duration)!)
                                self.isPlaying = true
                                //                                NotificationCenter.default.addObserver(self, selector: #selector(self.videoDidEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
                                NotificationCenter.default.addObserver(self, selector: #selector(self.videoDidEnd), name: NSNotification.Name.AVPlayerItemPlaybackStalled, object: nil)
                                //                                (UIApplication.topViewController()?.parent?.parent as? TabVC)?.miniPlayer.isHidden = false
                                if (UIApplication.topViewController() is HomeVC) ||
                                    (UIApplication.topViewController() is SearchVC) ||
                                    (UIApplication.topViewController() is MySpaceVC) ||
                                    (UIApplication.topViewController() is MyStoreVC) ||
                                    (UIApplication.topViewController() is ProfileVC) ||
                                    (UIApplication.topViewController() is ChangeEmailNameNumberVC) ||
                                    (UIApplication.topViewController() is ContactUsVC) ||
                                    (UIApplication.topViewController() is WebContentVC) ||
                                    (UIApplication.topViewController() is ProductListingVC) ||
                                    (UIApplication.topViewController() is PaymentsVC) ||
                                    (UIApplication.topViewController() is PlaylistVC) ||
                                    (UIApplication.topViewController() is CategoriesVC) {
                                    (UIApplication.topViewController()?.parent?.parent as? TabVC)?.miniPlayer.vwContainer.isHidden = true
                                }
                                self.startTimer()
                                if (UIApplication.topViewController()?.parent?.parent as? TabVC)?.miniPlayer.btnPlayPause != nil{
                                    (UIApplication.topViewController()?.parent?.parent as? TabVC)?.miniPlayer.btnPlayPause.isSelected = false
                                    self.vc?.miniPlayer.btnPlayPause.isSelected = false
                                }
                                if (UIApplication.topViewController() is AudioPlayerVC) {
                                    AppLoader.shared.removeLoader()
                                }
                            } else if status == AVKeyValueStatus.loading {
                                
                                debugPrint("Loading")
                            } else if status == AVKeyValueStatus.failed {
                                
                                debugPrint("failed")
                            } else {
                                debugPrint(status, "Status error")
                            }
                            
                        }
                    }
                    
                } catch {
                    debugPrint(error.localizedDescription, "error for here")
                }
                
            } else {
                debugPrint("Error")
            }
        }
    }
    
    
    @objc func videoDidEnd(){
        if (UIApplication.topViewController() is AudioPlayerVC) {
            AppLoader.shared.addLoader()
        }
        return
//        debugPrint("videoDidEnd")
//        if self.isRepeat{
//            player?.seek(to: .zero)
//            player?.play()
//        }
//        else{
//            self.audioStop()
//        }
        
    }
    
    func startTimer() {
        self.timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
        if self.continueWatchingTimer == nil{
//            self.continueWatchingTimer = Timer(fireAt: Date(), interval: 60, target: self, selector: #selector(self.continueWatchingUpdate), userInfo: nil, repeats: true)
            self.continueWatchingTimer = Timer.scheduledTimer(timeInterval: TimeInterval(self.continueWatchingUpdateInterval), target: self, selector: #selector(self.continueWatchingUpdate), userInfo: nil, repeats: true)
            self.continueWatchingTimer.fire()
        }
        //        let a = self.newPlayer?.addPeriodicTimeObserver(forInterval: CMTime(seconds: 1, preferredTimescale: CMTimeScale(NSEC_PER_SEC)), queue: .main, using: { time in
        //            debugPrint("PeriodeicTimeObserver :-", time)
        //        })
        //        debugPrint(a)
    }
    
    @objc func continueWatchingUpdate(){
        let currentTime = CMTimeGetSeconds(self.player?.currentTime() ?? CMTime())
        var interval = self.continueWatchingUpdateInterval - Int(Date().timeIntervalSince(self.continueWatchingTimer.fireDate).truncatingRemainder(dividingBy: Double(self.continueWatchingUpdateInterval)))
        debugPrint("PPP Blah Blah Blah:-",Int(Date().timeIntervalSince(self.continueWatchingTimer.fireDate).truncatingRemainder(dividingBy: Double(self.continueWatchingUpdateInterval))))
        debugPrint("PPP player interval", interval)
        if interval > 0{
            guard let mediaData = self.mediaData else { return }
            GlobalAPI.shared.addUpdateContinueWatching(contentData: mediaData, watchedDuration: Int(currentTime), interval: Int(interval))
            self.lastUpdatedDuration = currentTime
        }
    }
    
    
    @objc func updateTimer() {
        guard let player = player else{return}
        self.currentTime = CMTimeGetSeconds((player.currentTime()))
        //When audio will buffer

        if let audioVC = UIApplication.topViewController() as? AudioPlayerVC {

//                (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()

            
            self.updateAudioPlayerCompletion?()
            
        }
        else {
            
            if (UIApplication.topViewController() is SelectCategoryVC) ||
                (UIApplication.topViewController() is PlaylistOptionsVC) ||
                (UIApplication.topViewController() is CreatePlaylistVC) ||
                (UIApplication.topViewController() is PlaylistVC) ||
                (UIApplication.topViewController() is MyJourneyVC) ||
                (UIApplication.topViewController() is ChangeEmailNameNumberVC) ||
                (UIApplication.topViewController() is SelectTimeVC) ||
                (UIApplication.topViewController() is PaymentResponseVC) ||
                (UIApplication.topViewController() is ProductDescriptionVC) ||
                (UIApplication.topViewController() is WebContentVC) ||
                (UIApplication.topViewController() is ChangePasswordVC) ||
                (UIApplication.topViewController() is ContactUsVC) ||
                (UIApplication.topViewController() is PaymentsVC) ||
                (UIApplication.topViewController() is PreferencesVC) ||
                (UIApplication.topViewController() is RewardVC) ||
                (UIApplication.topViewController() is RedeemGiftVC) ||
                (UIApplication.topViewController() is SendGiftVC) ||
                (UIApplication.topViewController() is InAppPurchaseVC) ||
                (UIApplication.topViewController() is SearchVC) ||
                (UIApplication.topViewController() is NotificationsVC) ||
                (UIApplication.topViewController() is SignInInitialVC) ||
                (UIApplication.topViewController() is SignInVC) ||
                (UIApplication.topViewController() is VerificationVC) ||
                (UIApplication.topViewController() is ForgotPasswordVC) ||
                (UIApplication.topViewController() is SuccessVC) ||
                (UIApplication.topViewController() is CreatePasswordVC) ||
                (UIApplication.topViewController() is LoggedInDevicesVC) ||
                (UIApplication.topViewController() is NoNetworkVC) {
                (UIApplication.topViewController()?.parent?.parent as? TabVC)?.miniPlayer.isHidden = true
            } else {
                
                if (UIApplication.topViewController()?.parent?.parent as? TabVC)?.miniPlayer == nil{
                    (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
                }
                (UIApplication.topViewController()?.parent?.parent as? TabVC)?.miniPlayer.isHidden = false
                (UIApplication.topViewController()?.parent?.parent as? TabVC)?.miniPlayer.btnPlayPause.isSelected = !self.isPlaying
                if self.player.timeControlStatus == .waitingToPlayAtSpecifiedRate {
                    
                    (UIApplication.topViewController()?.parent?.parent as? TabVC)?.miniPlayer.btnPlayPause.isUserInteractionEnabled = false
                } else {
                    (UIApplication.topViewController()?.parent?.parent as? TabVC)?.miniPlayer.btnPlayPause.isUserInteractionEnabled = true
                }
            }
        }
    }
    
    //MARK: - audioPauseFunction -
    
    private func audioPause(showPlayer: Bool = true){
        if let player = self.player {
            if self.isPlaying {//player.isPlaying {
                self.isPlaying = false
                self.player?.pause()
                //                self.timer.invalidate()
                if self.vc?.miniPlayer.btnPlayPause != nil{
                    self.vc?.miniPlayer.btnPlayPause.isSelected = !self.isPlaying//true
                    (UIApplication.topViewController()?.parent?.parent as? TabVC)?.miniPlayer.isHidden = !showPlayer
                }
            }
        }
    }
    
    //MARK: - audioResumeFunction -
    
    private func audioResume(){
        self.isPlaying = true
        self.player?.play()
        self.startTimer()
        if self.vc?.miniPlayer.btnPlayPause != nil{
            self.vc?.miniPlayer.btnPlayPause.isSelected = false
        }
    }
    
    //MARK: - audioSeekFunction -
    
    func audioSeek(seekToTime: Double) {
//        self.audioPause()
        let time = CMTimeMake(value: Int64(seekToTime * 1000), timescale: 1000)
        self.player?.seek(to: time)//currentTime = seekToTime
        self.currentTime = CMTimeGetSeconds((time))
//        self.startTimer()
    }
    
    //MARK: - audioStopFunction -
    
    func audioStop(){
//        if self.isRepeat{
//            let seconds : Int64 = 0
//            let preferredTimeScale : Int32 = 1
//            let seekTime : CMTime = CMTimeMake(value: seconds, timescale: preferredTimeScale)
//
//            self.player.seek(to: seekTime)
//            self.player.play()
//            return
//        }
        if self.player != nil {
            self.continueWatchingUpdate()
            self.isPlaying = false
            self.isRepeat = false
            //            self.sliderTimer?.invalidate()
            self.timer?.invalidate()
            
            self.continueWatchingTimer?.invalidate()
            
            
            if let topVc = self.currentVC as? AudioPlayerVC {
                topVc.btnPlayPause.isSelected = self.isPlaying
                //                topVc.sliderTimer.invalidate()
                topVc.vwSlider.selectedMaxValue = 0.0
                topVc.vwSlider.refresh()
                topVc.lblStartTime.text = "00:00"
            }
            vc?.miniPlayer.isHidden = true
            
            self.player?.pause()
            self.player = nil
            
            (UIApplication.topViewController()?.parent?.parent as? TabVC)?.miniPlayer.btnPlayPause.isSelected = false
            (UIApplication.topViewController()?.parent?.parent as? TabVC)?.miniPlayer.isHidden = true
        }
    }
    
    
    //MARK: - setMiniplayerData -
    
    private func setMiniplayerData() {
        if let player = self.player {
            self.audioDuration = CMTimeGetSeconds((player.currentItem!.duration))//player.duration
            vc?.miniPlayer.audioDuration = self.audioDuration//player.duration
            
            
            (UIApplication.topViewController()?.parent?.parent as? TabVC)?.miniPlayer.audioDuration = self.audioDuration//player.duration
            vc?.miniPlayer.btnPlayPause.isSelected = !self.isPlaying//player.isPlaying
        }
    }
}
