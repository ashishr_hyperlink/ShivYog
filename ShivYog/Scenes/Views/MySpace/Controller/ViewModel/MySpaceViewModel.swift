//
//  MySpaceViewModel.swift
//  ShivYog
//
//  Created by 2022M30 on 01/12/22.
//

import Foundation
import UIKit
import SwiftyJSON

//class BadgesModel {
//    var title : String!
//    var image: UIImage!
//    var isEarned: Bool!
//
//    init(title: String, image: UIImage, isEarned: Bool = false) {
//        self.title = title
//        self.image = image
//        self.isEarned = isEarned
//    }
//
//}


class MySpaceViewModel {
    
    private(set) var createPlaylistResult = Bindable<Result<String?,AppError>>()
    private(set) var deleteMediaFromPlaylistResult = Bindable<Bool>()
    private(set) var appUsageResult = Bindable<[AppUsageModel]>()
    private(set) var badgesResult = Bindable<Bool>()//Bindable<[BadgesModel]>()
    private(set) var journeyStatusResult = Bindable<JSON>()//Bindable<[AppUsageModel]>()
    private(set) var deletePlaylist = Bindable<Bool>()
    private var arrBadges : [BadgesModel] = []/*[BadgesModel(title: "Star", image: .starBadge.image, isEarned: true),
                                             BadgesModel(title: "Bronze", image: .bronzeBadge.image, isEarned: true),
                                             BadgesModel(title: "Silver", image: .silverBadge.image, isEarned: true),
                                             BadgesModel(title: "Gold", image: .goldBadge.image, isEarned: false),
                                             BadgesModel(title: "Premium", image: .platinumBadge.image, isEarned: false)]*/
}

extension MySpaceViewModel {
    private func isValid(name: String) -> AppError? {
        guard !name.isEmpty else { return AppError.validation(type: .enterPlaylistName) }
        guard !name.trim().isEmpty else { return AppError.validation(type: .enterValidPlaylistName) }
        return nil
    }
    
    func apiCreateName(name: String, content : CategoryContentModel?, isFromEdit: Bool = false) {
        if let err = self.isValid(name: name) {
            self.createPlaylistResult.value = .failure(err)
            return
        }
        if isFromEdit {
            self.apiDeleteToPlaylist(playlistId: content?.id.description ?? "", isEditTitle: true, title: name)
        } else {
            self.apiCreatePlaylist(name: name, content: content)
        }
    }
}


//extension MySpaceViewModel {
//    func numberOfBadges() -> Int {
//        return self.arrBadges.count
//    }
//
//    func getDataAtIndexForBadges(index: Int) -> BadgesModel {
//        return self.arrBadges[index]
//    }
//}

extension MySpaceViewModel{
    func apiCreatePlaylist(name: String, content : CategoryContentModel?){
        var param = Dictionary<String,Any>()
        param["title"] = name
        ApiManager.shared.makeRequest(endPoint: .playlist(.playlist), methodType: .post, parameter: param, withErrorAlert: true, withLoader: true, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                let isStoreContent : Bool = content?.content == "store media content"
                self.apiAddMediaToPlaylist(playlistId: apiData.response["id"].stringValue, mediaId: content?.id.description ?? "", isStoreContent: isStoreContent)
                break
            case .failure(let error):
                self.createPlaylistResult.value = .failure(AppError.custom(errorDescription: error.localizedDescription))
                print("the error \(error)")
            }
        }
    }
    
    func apiAddMediaToPlaylist(playlistId : String, mediaId : String, isStoreContent: Bool = false){
        var param = Dictionary<String,Any>()
        if isStoreContent {
            param["store_media_details_id"] = mediaId
        } else {
            param["media_details_id"] = mediaId
        }
        ApiManager.shared.makeRequest(endPoint: .playlistMedia(.addMedia(playlistId)), methodType: .post, parameter: param, withErrorAlert: true, withLoader: true, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                self.createPlaylistResult.value = .success(nil)
                break
            case .failure(let error):
                self.createPlaylistResult.value = .failure(AppError.custom(errorDescription: error.localizedDescription))
                print("the error \(error)")
            }
        }
    }
    
    func apiDeleteMediaToPlaylist(playlistId : String, mediaId : String, isStoreMedia: Bool = false){
        var param = Dictionary<String,Any>()
        if isStoreMedia {
            param["store_media_details_id"] = mediaId
        } else {
            param["media_details_id"] = mediaId
        }
        ApiManager.shared.makeRequest(endPoint: .playlistMedia(.deleteMedia(playlistId)), methodType: .delete, parameter: param, withErrorAlert: true, withLoader: true, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                self.deleteMediaFromPlaylistResult.value = true
                break
            case .failure(let error):
                self.deleteMediaFromPlaylistResult.value = false
                print("the error \(error)")
            }
        }
    }
    
    func apiDeleteToPlaylist(playlistId : String, isEditTitle: Bool = false, title: String = ""){
        var dictData = Dictionary<String, String>()
        dictData["title"] = title
        
        ApiManager.shared.makeRequest(endPoint: .playlistMedia(.deletePlaylist(playlistId)), methodType: isEditTitle ? .patch : .delete, parameter: isEditTitle ? dictData : [:], withErrorAlert: false, withLoader: true, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                self.deletePlaylist.value = true
                break
            case .failure(let error):
                self.deletePlaylist.value = false
                print("the error \(error)")
            }
        }
    }
}


//MARK: - My Journey -

extension MySpaceViewModel {
    func apiGetAppUsage(usageType: MyJourneyTimeType,startDate: String, endDate: String, year: String) {
        var dictData = Dictionary<String, String>()
        
        if usageType == .weekly {
            dictData["start_date"] = startDate
            dictData["end_date"] = endDate
        } else {
            dictData["year"] = year
        }
        
        ApiManager.shared.makeRequest(endPoint: .content(.myJourney), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: true, withdebugLog: true) { [weak self] (result) in
            
            guard let self = self else { return }
            
            switch result {
            case .success(let apiData):
//                let arrData = apiData.data.arrayValue
//                if let arr = arrData.first {
                    
                    self.appUsageResult.value = AppUsageModel.listFromArray(data: apiData.data["results"].arrayValue)
                    
//                }
                
                
                
            case .failure(let error):
                print("the error \(error)")
                break
            }
        }
        
        
    }
    
    func apiGetBadges(withLoader: Bool, page: Int) {
        
        var dictData = Dictionary<String, String>()
        dictData["page"] = page.description
        
        ApiManager.shared.makeRequest(endPoint: .content(.badges), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: withLoader, withdebugLog: true) { [weak self] (result) in
            
            guard let self = self else { return }
            
            switch result {
            case .success(let apiData):
                if page == 1 {
                    self.arrBadges = []
                    self.arrBadges = BadgesModel.listFromArray(data: apiData.data["results"].arrayValue)
                } else {
                    self.arrBadges.append(BadgesModel.listFromArray(data: apiData.data["results"].arrayValue))
                }
                self.badgesResult.value = true//BadgesModel.listFromArray(data: apiData.data["results"].arrayValue)
            case .failure(let error):
                self.badgesResult.value = false
                print("the error \(error)")
                break
            }
        }
    }
    
    func numberOfBadges() -> Int {
        return self.arrBadges.count
    }
    
    func getDataAtIndexForBadges(index: Int) -> BadgesModel {
        return self.arrBadges[index]
    }
    
    func apiJourneyStatus() {
        ApiManager.shared.makeRequest(endPoint: .content(.myJourneyStatus), methodType: .get, parameter: nil, queryParameters: [:], withErrorAlert: false, withLoader: true, withdebugLog: true) { [weak self] (result) in
            
            guard let self = self else { return }
            
            switch result {
            case .success(let apiData):
                let arrData = apiData.data["results"].arrayValue
                
                if let data = arrData.first {
                    self.journeyStatusResult.value = data//AppUsageModel.listFromArray(data: data["weekly_status"].arrayValue)
                }
                //                self.arrBadgesResult.value = BadgesModel.listFromArray(data: apiData.data["results"].arrayValue)
                break
            case .failure(let error):
                print("the error \(error)")
                break
            }
        }
    }
}
