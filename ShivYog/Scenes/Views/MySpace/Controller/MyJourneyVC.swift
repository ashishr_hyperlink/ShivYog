//
//  MyJourneyVC.swift
//  ShivYog
//
//  Created by 2022M30 on 25/01/23.
//

import UIKit
import Foundation
import SwiftyJSON

class MyJourneyVC: UIViewController {
    
    //MARK: - Outlet -
    @IBOutlet weak var imgCurrentStatus: UIImageView!
    @IBOutlet weak var lblCurrentStatusTitle: UILabel!
    @IBOutlet weak var lblCurrentStatus: UILabel!
    @IBOutlet weak var lblTimeToNextStatus: UILabel!
    @IBOutlet weak var prgBarStatus: UIProgressView!
    @IBOutlet weak var imgUpcomingStatus: UIImageView!
    @IBOutlet weak var lblFiveWeekTitle: UILabel!
    @IBOutlet weak var colFiveStatus: UICollectionView!
    @IBOutlet weak var lblAppUsageTitle: UILabel!
    @IBOutlet weak var lblTimeRange: UILabel!
    @IBOutlet weak var lblRangeType: UILabel!
    
    @IBOutlet weak var vwRangeType: UIView!
    @IBOutlet weak var lblBadgesTitle: UILabel!
    @IBOutlet weak var tblBadges: SelfSizedTableView!
    @IBOutlet weak var vwStatusContainer: UIView!
    @IBOutlet weak var vwUpcomingStatusContainer: UIView!
    @IBOutlet weak var constColHeight: NSLayoutConstraint!
    @IBOutlet weak var vwBarChart: BarChartView!
    

    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var mySpaceVM : MySpaceViewModel = MySpaceViewModel()
    var journeyRangeType : MyJourneyTimeType = .weekly
    var barChartWeeklyData : [Double] = []//[200, 500, 450, 450, 350, 420, 100]
    var barChartMonthlyData : [Double] = []//[500, 50, 200, 100, 250, 300, 400, 250, 300, 400, 50, 150]
    var weekData = ["12 Dec 22 - 18 Dec 22", "19 Dec 22 - 25 Dec 22", "26 Dec 22 - 01 Jan 23", "02 Jan 23 - 08 Jan 23","09 Jan 23 - 15 Jan 23" ]
    var weekEndDate = Date()
    var weekStartDate = Date()//Calendar.current.date(byAdding: .day, value: -6, to: Date()) ?? Date()
    var strStartDate : String = ""
    var strEndDate : String = ""
    var strYear : String = ""
    var fiveWeekData = [AppUsageModel]()//["12 Dec - 18 Dec", "19 Dec - 25 Dec", "26 Dec - 01 Jan", "02 Jan - 08 Jan","09 Jan - 15 Jan" ]
    var yearData = [2018,2019,2020,2021,2022]
    var currentYear = Date()
    var currentIndex : Int = 0
    var arrJourneyData : [AppUsageModel] = []
    var currentPage : Int = 1
    var isApiCalling : Bool = true
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        AppDelegate.shared.addAnalyticsEvent(screenName: .MyJourney, eventName: .Pageview, category: .MyJourney)
        self.fetchBadgesData(withLoader: true)
        self.mySpaceVM.apiJourneyStatus()
        self.fetchUsageData()
//        self.mySpaceVM.apiGetAppUsage(usageType: self.journeyRangeType, startDate: self.strStartDate, endDate: self.strEndDate, year: self.strYear)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.setupViewModelObserver()
        self.applyStyle()
    }

    private func applyStyle(){
//        self.endDayOfWeek()
        self.weekEndDate = self.endDayOfWeek()
        self.weekStartDate = Calendar.current.date(byAdding: .day, value: -6, to: self.weekEndDate) ?? Date()
        self.convertAndPrintSelectionDateYear()
        self.lblCurrentStatusTitle.font(name: FontFamily.SFProDisplay.regular, size: 12.0).textColor(color: .appFfffff)
        self.lblCurrentStatus.font(name: FontFamily.SFProDisplay.bold, size: 16.0).textColor(color: .appFfffff)
        self.lblTimeToNextStatus.font(name: FontFamily.SFProDisplay.medium, size: 13.0).textColor(color: .app_5E5E5E)
        self.lblFiveWeekTitle.font(name: FontFamily.SFProDisplay.bold, size: 18.0).textColor(color: .app_4F4F4F)
        self.lblAppUsageTitle.font(name: FontFamily.SFProDisplay.bold, size: 18.0).textColor(color: .app_4F4F4F)
        self.lblTimeRange.font(name: FontFamily.SFProDisplay.medium, size: 16.0).textColor(color: .app_222222)
        self.lblRangeType.font(name: FontFamily.SFProDisplay.regular, size: 13.0).textColor(color: .app_990000)
        self.lblBadgesTitle.font(name: FontFamily.SFProDisplay.bold, size: 18.0).textColor(color: .app_4F4F4F)
        
        self.vwRangeType.borderColor(color: .appD9D9D9, borderWidth: 1.0)
        
        self.colFiveStatus.registerNib(forCellWithClass: StatusCell.self)
        self.colFiveStatus.delegate = self
        self.colFiveStatus.dataSource = self
        
        self.tblBadges.registerNib(forCellWithClass: BadgesCell.self)
        self.tblBadges.dataSource = self
        self.tblBadges.delegate = self
        
        self.vwUpcomingStatusContainer.borderColor(color: .app_990000, borderWidth: 3.0)
        self.constColHeight.constant = 70 * ScreenSize.fontAspectRatio * 0.8
        DispatchQueue.main.async {
            self.vwStatusContainer.cornerRadius(cornerRadius: 8.0)
            self.vwUpcomingStatusContainer.cornerRadius(cornerRadius: 8.0)
            self.vwRangeType.cornerRadius(cornerRadius: 8.0)
        }
        
        self.vwRangeType.addTapGestureRecognizer {
            let vc = StoryboardScene.MySpace.selectTimeVC.instantiate()
            vc.timeRangeType = self.journeyRangeType
            vc.modalPresentationStyle = .overFullScreen
            vc.journeyTypeCompletion = { timeRangeType in
                self.lblRangeType.text = timeRangeType.rawValue
                self.journeyRangeType = timeRangeType
                self.currentIndex = 0
//                self.lblTimeRange.text = timeRangeType == .weekly ? self.weekData[self.currentIndex] : self.yearData[self.currentIndex].description
//                self.chartSetUP()
//                self.chartDataSetup()
                self.countDateAndYear(rangeType: self.journeyRangeType, nextOrPrevious: .none)
                self.fetchUsageData()
            }
            self.openBottomSheetAnimate(vc: vc, sizes: [.percent(0.25)])//.navigationController?.present(vc, animated: true, completion: nil)
        }
//        self.lblTimeRange.text = self.journeyRangeType == .weekly ? self.weekData[self.currentIndex] : self.yearData[self.currentIndex].description
//        self.chartSetUP()
//        self.chartDataSetup()
        
//        self.countDateAndYear(rangeType: self.journeyRangeType, nextOrPrevious: .none)
        self.tblBadges.addRefreshControl {
            self.isApiCalling = true
            self.currentPage = 1
            self.fetchBadgesData(withLoader: true)
        }
    }
    
    private func fetchUsageData() {
        let sDate = self.strStartDate.changeDateFormat(from: .default(format: .dd_MMM_yy), to: .default(format: .yyyymmdd), type: .noconversion)!
        let eDate = self.strEndDate.changeDateFormat(from: .default(format: .dd_MMM_yy), to: .default(format: .yyyymmdd), type: .noconversion)!
        self.mySpaceVM.apiGetAppUsage(usageType: self.journeyRangeType, startDate: sDate, endDate: eDate, year: self.strYear)
        
    }
    
    private func setCurrentWeekStatusData(data: JSON) {
        debugPrint(data)
        let totalUsage : Double = data["total_app_usage"].doubleValue
        let bronze = data[JourneyStatus.Bronze.rawValue.lowercased()].doubleValue * Double(3600) //to get seconds from the hors
        let silver = data[JourneyStatus.Silver.rawValue.lowercased()].doubleValue * Double(3600) //to get seconds from the hors
        let gold = data[JourneyStatus.Gold.rawValue.lowercased()].doubleValue * Double(3600) //to get seconds from the hors
        
        
        if totalUsage < bronze {
            self.lblCurrentStatus.text = "-"
            self.prgBarStatus.setProgress(Float(totalUsage / bronze), animated: true)
            let remainingSeconds = bronze - totalUsage
            let remainingHours = remainingSeconds.asString(style: .positional, allowUnits: [.hour])
            let remainingMinutes = remainingSeconds.asString(style: .positional, allowUnits: [.minute])
            if let hours = Int(remainingHours), hours > 0{
                self.lblTimeToNextStatus.text = "%@ left to get %@".localized(remainingSeconds.asString(style: .full, allowUnits: [.hour, .minute]), JourneyStatus.Bronze.rawValue)
            }
            else if let minutes = Int(remainingMinutes), minutes > 0{
                self.lblTimeToNextStatus.text = "%@ left to get %@".localized(remainingSeconds.asString(style: .full, allowUnits: [.minute]), JourneyStatus.Bronze.rawValue)
            }
            else{
                self.lblTimeToNextStatus.text = "%@ left to get %@".localized(remainingSeconds.asString(style: .full, allowUnits: [.second]), JourneyStatus.Bronze.rawValue)
            }
            
            self.imgUpcomingStatus.image = .bronzeStatus.image
            self.imgCurrentStatus.image = .noStatus.image
        } else if totalUsage >= bronze && totalUsage < silver {
            self.lblCurrentStatus.text = JourneyStatus.Bronze.rawValue.uppercased()
            self.prgBarStatus.setProgress(Float(totalUsage / silver), animated: true)
            let remainingSeconds = silver - totalUsage
            let remainingHours = remainingSeconds.asString(style: .positional, allowUnits: [.hour])
            let remainingMinutes = remainingSeconds.asString(style: .positional, allowUnits: [.minute])
            if let hours = Int(remainingHours), hours > 0{
                self.lblTimeToNextStatus.text = "%@ left to get %@".localized(remainingSeconds.asString(style: .full, allowUnits: [.hour, .minute]), JourneyStatus.Silver.rawValue)
            }
            else if let minutes = Int(remainingMinutes), minutes > 0{
                self.lblTimeToNextStatus.text = "%@ left to get %@".localized(remainingSeconds.asString(style: .full, allowUnits: [.minute]), JourneyStatus.Silver.rawValue)
            }
            else{
                self.lblTimeToNextStatus.text = "%@ left to get %@".localized(remainingSeconds.asString(style: .full, allowUnits: [.second]), JourneyStatus.Silver.rawValue)
            }
            self.imgUpcomingStatus.image = .silverStatus.image
            self.imgCurrentStatus.image = .bronzeStatus.image
        } else if totalUsage >= silver && totalUsage < gold {
            self.lblCurrentStatus.text = JourneyStatus.Silver.rawValue.uppercased()
            let remainingSeconds = gold - totalUsage
            let remainingHours = remainingSeconds.asString(style: .positional, allowUnits: [.hour])
            let remainingMinutes = remainingSeconds.asString(style: .positional, allowUnits: [.minute])
            if let hours = Int(remainingHours), hours > 0{
                self.lblTimeToNextStatus.text = "%@ left to get %@".localized(remainingSeconds.asString(style: .full, allowUnits: [.hour, .minute]), JourneyStatus.Gold.rawValue)
            }
            else if let minutes = Int(remainingMinutes), minutes > 0{
                self.lblTimeToNextStatus.text = "%@ left to get %@".localized(remainingSeconds.asString(style: .full, allowUnits: [.minute]), JourneyStatus.Gold.rawValue)
            }
            else{
                self.lblTimeToNextStatus.text = "%@ left to get %@".localized(remainingSeconds.asString(style: .full, allowUnits: [.second]), JourneyStatus.Gold.rawValue)
            }
            self.prgBarStatus.setProgress(Float(totalUsage / gold), animated: true)
            self.imgUpcomingStatus.image = .goldStatus.image
            self.imgCurrentStatus.image = .silverStatus.image
        } else {
            self.lblCurrentStatus.text = JourneyStatus.Gold.rawValue.uppercased()
            self.lblTimeToNextStatus.isHidden = true
            self.prgBarStatus.progress = 1.0
            self.imgUpcomingStatus.image = .noStatus.image
            self.imgCurrentStatus.image = .goldStatus.image
        }
    }
    
    private func fetchBadgesData(withLoader: Bool) {
        self.mySpaceVM.apiGetBadges(withLoader: true, page: self.currentPage)
    }
    
    private func chartSetUP() {
      //  viewPop.isHidden = true
        vwBarChart.clear()
        
        //TODO: Chart Setup
        vwBarChart.zoomToCenter(scaleX: 0.0, scaleY: 0.0)
//        vwBarChart.delegate = self
        vwBarChart.chartDescription?.enabled = false
        vwBarChart.legend.enabled = false
        vwBarChart.fitScreen()
        vwBarChart.drawGridBackgroundEnabled = false
        vwBarChart.drawBordersEnabled = false
        vwBarChart.dragEnabled = true
        vwBarChart.setScaleEnabled(true)
        vwBarChart.pinchZoomEnabled = true //  x and y axis can be scaled simultaneously
        vwBarChart.doubleTapToZoomEnabled = true
        
        //TODO: Left - Right Axies
        vwBarChart.leftAxis.enabled = true
        vwBarChart.rightAxis.enabled = false
        vwBarChart.rightAxis.drawAxisLineEnabled = false
        vwBarChart.leftAxis.axisMinimum = 00
//        vwBarChart.leftAxis.axisMaxLabels = 5
//        vwBarChart.leftAxis.axisMaximum = 1000
        vwBarChart.leftAxis.granularityEnabled = true
        vwBarChart.leftAxis.drawTopYLabelEntryEnabled = true
        vwBarChart.leftAxis.labelTextColor = .app_5E5E5E
        vwBarChart.leftAxis.drawGridLinesEnabled = false
        vwBarChart.leftAxis.drawAxisLineEnabled = false
        vwBarChart.leftAxis.labelFont = UIFont.cFont(ofType: FontFamily.SFProDisplay.regular, withSize: 13)
        vwBarChart.leftAxis.gridLineDashLengths = [2]
        vwBarChart.leftAxis.spaceBottom = 0.0
        vwBarChart.rightAxis.spaceBottom = 0.0
        vwBarChart.leftAxis.valueFormatter = self.journeyRangeType == .weekly ? MyLeftAxisFormatterForMin() : MyLeftAxisFormatterForHour()
        
        let leftAxisFormatter = NumberFormatter()
        leftAxisFormatter.minimumFractionDigits = 0
        leftAxisFormatter.maximumFractionDigits = 1
     
//        vwBarChart.leftAxis.valueFormatter = DefaultAxisValueFormatter(formatter: leftAxisFormatter)
        
        //TODO: X - Axis
        vwBarChart.xAxis.enabled = true
        vwBarChart.xAxis.granularityEnabled = false
        vwBarChart.xAxis.granularity = 1.0
        
        let xAxis = vwBarChart.xAxis
        xAxis.labelPosition = .bottom
        xAxis.drawGridLinesEnabled = false
        xAxis.drawLimitLinesBehindDataEnabled = true
        
//        viewBarChart.rightAxis.drawGridLinesEnabled = false
//        viewBarChart.rightAxis.drawAxisLineEnabled = false
//        viewBarChart.rightAxis.drawLabelsEnabled = false
//        viewBarChart.xAxis.drawAxisLineEnabled = false
//        vwBarChart.xAxis.axisMinimum = -0.5
        //        barChart.xAxis.gridColor = UIColor.ColorGrey.withAlphaComponent(0.5)
        //        barChart.xAxis.axisLineColor = UIColor.ColorGrey
        //        viewChart.xAxis.axisMaximum = Double(arrWeakly.count)
        vwBarChart.xAxis.centerAxisLabelsEnabled = false
        vwBarChart.xAxis.labelFont = UIFont(font: FontFamily.SFProDisplay.regular, size: 13)!
        vwBarChart.xAxis.labelTextColor = .app_5E5E5E
        vwBarChart.xAxis.drawLabelsEnabled = true
        vwBarChart.animate(yAxisDuration: 2.0)
        
       /// viewBarChart.maskToBounds = true
        
        
        if self.journeyRangeType == .weekly {
            var temparr : [String] = []
            self.arrJourneyData.compactMap { $0.date }.forEach { str in
                var tempStr = str
                tempStr.insert("\n", at: tempStr.index(tempStr.startIndex, offsetBy: 4))
                temparr.append(tempStr + " ")
            }

            vwBarChart.xAxis.valueFormatter = BarChartValueFormatter(labels: temparr )
        } else if self.journeyRangeType == .monthly {
            vwBarChart.xAxis.valueFormatter = BarChartValueFormatter(labels: self.arrJourneyData.compactMap{ $0.date })
        }
        
        DispatchQueue.main.async {
                self.vwBarChart.pinchZoomEnabled = true
                self.vwBarChart.doubleTapToZoomEnabled = true
                self.vwBarChart.setScaleEnabled(false)
            }
        
        let marker = XYMarkerView(color: .app_000000,
                                  font: UIFont(font: FontFamily.SFProDisplay.regular, size: 14)!,
                                  textColor: .appFfffff,
                                  insets: UIEdgeInsets(top: 6, left: 6, bottom: 16, right: 6),
                                  xAxisValueFormatter:  /*vwBarChart.xAxis.valueFormatter!*/self.journeyRangeType == .weekly ? MyLeftAxisFormatterForMin() : MyLeftAxisFormatterForHour(),
                                    type: self.journeyRangeType == .weekly ? "Min" : "Hr")
        marker.chartView = vwBarChart
        marker.minimumSize = CGSize(width: 60, height: 30)
        vwBarChart.marker = marker
        
        
    }
    
    func chartDataSetup() {
        self.barChartWeeklyData = self.arrJourneyData.compactMap{ Double($0.appUsageTime) }
        self.barChartMonthlyData = self.arrJourneyData.compactMap{ Double($0.appUsageTime) }
//        let start = 0
//        let yVals = (start..<start+barChartWeeklyData.count).map { (i) -> BarChartDataEntry in
//            return BarChartDataEntry(x: Double(i), y: JSON(barChartWeeklyData[i]).doubleValue)
//        }
        
        
        var yVal = [BarChartDataEntry]()
        
        if self.journeyRangeType == .weekly {
            self.barChartWeeklyData = self.barChartWeeklyData.map{ round($0 / 60) }
//            vwBarChart.leftAxis.axisMaximum = Double(self.barChartWeeklyData.count)  - 0.5
//            vwBarChart.leftAxis._customAxisMax = true
//            vwBarChart.leftAxis.axisMaximum = ceil(self.barChartWeeklyData.max() ?? 0.0)
            vwBarChart.leftAxis.spaceTop = 0.5//ceil(self.barChartWeeklyData.max() ?? 0.0)
            for (index,i) in barChartWeeklyData.enumerated() {
                print("Week Data:-",i)
                yVal.append(BarChartDataEntry(x: Double(index), y: Double(i)))
                
            }
        } else if self.journeyRangeType == .monthly {
            self.barChartMonthlyData = self.barChartMonthlyData.map{ round($0 / 3600) }
//            vwBarChart.leftAxis._customAxisMax = true
//            vwBarChart.leftAxis.axisMaximum = ceil(self.barChartMonthlyData.max() ?? 0.0)
//            vwBarChart.leftAxis.axisMaxLabels = 5
            vwBarChart.leftAxis.spaceTop = 1.0 //* ceil(self.barChartMonthlyData.max() ?? 0.0)
            for (index,i) in barChartMonthlyData.enumerated() {
                print("Month Data:-",i)
                yVal.append(BarChartDataEntry(x: Double(index), y: Double(i)))
            }
        }
        
//        let set2 = BarChartDataSet(entries: yVals, label: "hr")
        
        
        
        var set1: BarChartDataSet! = nil
        set1 = BarChartDataSet(entries: yVal, label: "")
        set1.colors.removeAll()
        
        if self.journeyRangeType == .weekly {
            for _ in 0...barChartWeeklyData.count - 1 {
                set1.colors.append(.appE1E1E1)
            }
        } else if self.journeyRangeType == .monthly {
            for _ in 0...barChartMonthlyData.count - 1 {
                set1.colors.append(.appE1E1E1)
            }
        }
        
        set1.axisDependency = .left
        set1.drawValuesEnabled = false
        set1.drawIconsEnabled = false
        set1.valueTextColor = .app_5E5E5E
        
        set1.highlightEnabled = true
        set1.highlightAlpha = 1
        set1.highlightColor = .app_990000
        set1.barBorderColor = .clear
        set1.barBorderWidth = 0.0
      //  set1.highlightLineWidth = 1
        
        let data = BarChartData(dataSet: set1 )
        data.setValueFont(UIFont(font: FontFamily.SFProDisplay.regular, size: 13)!)
        data.barWidth = 0.4
        
        vwBarChart.data = data
        
        
        //FIXME: - Development -
        
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })*/
        self.mySpaceVM.journeyStatusResult.bind { jsonData in
            if let data = jsonData {
                self.fiveWeekData = AppUsageModel.listFromArray(data: data["weekly_status"].arrayValue)
                let usageData = data["total_app_usage"]
                self.setCurrentWeekStatusData(data: usageData)
                self.colFiveStatus.reloadData()
            }
        }
        
        
        self.mySpaceVM.badgesResult.bind { result in
            if result ?? false {
                self.currentPage += 1
                self.isApiCalling = true
                self.tblBadges.reloadData()
            } else {
                self.isApiCalling = false
            }
        }
        
        
        self.mySpaceVM.appUsageResult.bind { result in
            
            self.arrJourneyData = result ?? []
            self.chartSetUP()
            self.chartDataSetup()
        }
    }
    
    private func endDayOfWeek() -> Date {
        var count = Calendar.current.component(.weekday, from: Date())
        count = 8 - count
        let endDate = Calendar.current.date(byAdding: .day, value: count, to: Date()) ?? Date()
        return endDate
    }
    
    private func countDateAndYear(rangeType: MyJourneyTimeType, nextOrPrevious: MyJourneyTimeChangeType) {
        let yearCount: Int = nextOrPrevious == .next ? 1 : -1
        let weekCount : Int = nextOrPrevious == .next ? 7 : -7
        var strJoinDate : String = ""
        if let user = UserModel.currentUser {
            if rangeType == .weekly {
                strJoinDate = (user.dateJoined.changeDateFormat(from: .default(format: .NodeUTCFormatWithMiliseconds), to: .default(format: .dd_MMM_yy), type: .noconversion) ?? "")
            } else {
                strJoinDate = (user.dateJoined.changeDateFormat(from: .default(format: .NodeUTCFormatWithMiliseconds), to: .default(format: .yyyy), type: .noconversion) ?? "")
            }
        }
        
        if nextOrPrevious == .none {
            self.convertAndPrintSelectionDateYear()
            return
        }
        let endDate = self.endDayOfWeek().convertDateFormat(output: .default(format: .dd_MMM_yy), type: .noconversion).0
        let year = Date().convertDateFormat(output: .default(format: .yyyy), type: .noconversion).0
        
       
        
        if nextOrPrevious == .next && ((rangeType == .weekly && self.strEndDate == endDate) || (rangeType == .monthly && self.strYear == year)) {
            return
        } else if nextOrPrevious == .previous && ((rangeType == .monthly && self.strYear == strJoinDate) || (rangeType == .weekly && (self.strStartDate.toDate(from: .default(format: .dd_MMM_yy)) ?? Date()) <= (strJoinDate.toDate(from: .default(format: .dd_MMM_yy)) ?? Date()))) {
            return
        } else {
            if rangeType == .weekly {
                self.weekStartDate = Calendar.current.date(byAdding: .day, value: weekCount, to: self.weekStartDate) ?? Date()
                self.weekEndDate = Calendar.current.date(byAdding: .day, value: weekCount, to: self.weekEndDate) ?? Date()
            } else {
                self.currentYear = Calendar.current.date(byAdding: .year, value: yearCount, to: self.currentYear) ?? Date()
            }
            self.convertAndPrintSelectionDateYear()
        }
        
    }
    
    private func convertAndPrintSelectionDateYear() {
        self.strStartDate = self.weekStartDate.convertDateFormat(output: .default(format: .dd_MMM_yy), type: .noconversion).0
        self.strEndDate = self.weekEndDate.convertDateFormat(output: .default(format: .dd_MMM_yy), type: .noconversion).0
        self.strYear = self.currentYear.convertDateFormat(output: .default(format: .yyyy), type: .noconversion).0
        
        if self.journeyRangeType == .weekly {
            self.lblTimeRange.text = strStartDate + " - " + strEndDate
        } else {
            self.lblTimeRange.text = strYear
        }
    }
    
    
    
    //------------------------------------------------------
    
    //MARK: - Action Method -
    
    @IBAction func btnPreviousClicked(_ sender: UIButton) {

        self.countDateAndYear(rangeType: self.journeyRangeType, nextOrPrevious: .previous)
        self.fetchUsageData()
        
    }
    
    @IBAction func btnNextClicked(_ sender: UIButton) {

        self.countDateAndYear(rangeType: self.journeyRangeType, nextOrPrevious: .next)
        self.fetchUsageData()
    }
    
    //------------------------------------------------------
    
}

//MARK: - UICollectionview Delegate and Datasource -

extension MyJourneyVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.fiveWeekData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: StatusCell.self, for: indexPath)
        let data = self.fiveWeekData[indexPath.row]
        let time = data.startDate + " - " + data.endDate
        if data.status == JourneyStatus.Bronze.rawValue {
            cell.imgStatusIcon.image = .bronzeStatus.image
        } else if data.status == JourneyStatus.Silver.rawValue {
            cell.imgStatusIcon.image = .silverStatus.image
        } else if data.status == JourneyStatus.Gold.rawValue {
            cell.imgStatusIcon.image = .goldStatus.image
        } else {
            cell.imgStatusIcon.image = .noStatus.image
        }
        cell.lblTime.text = time
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let cell = collectionView.cellForItem(at: indexPath) as? StatusCell
//        let wd = cell?.lblTime.text!.widthOfString(usingFont: FontFamily.SFProDisplay.medium.font(size: 10.0)) ?? 0.0
        let width = 73 * ScreenSize.fontAspectRatio
        let height = width * 0.75
        return CGSize(width: width, height: height)
    }
}

//MARK: - UITableView Delegate and Datasource -

extension MyJourneyVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mySpaceVM.numberOfBadges()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: BadgesCell.self, for: indexPath)
        let data = self.mySpaceVM.getDataAtIndexForBadges(index: indexPath.row)
        cell.data = data
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tblBadges.isAtBottom {
            if self.isApiCalling {
                self.fetchBadgesData(withLoader: false)
            }
        }
    }
    
}


//MARK: - IAxisValueFormatter -
public class BarChartValueFormatter: NSObject, IAxisValueFormatter {
    
    var labels: [String] = []
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        if Int(value) >= labels.count {
            return ""
        }
        return labels[Int(value)]
    }
    
    init(labels: [String]) {
        super.init()
        self.labels = labels
    }
    
}

class MyLeftAxisFormatterForHour: NSObject, IAxisValueFormatter {

//    var labels: [Int] = []

    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
//        if Int(value) >= labels.count {
//            return ""
//        }
        return String(Int(value)) + " hr"
    }
    
//    init(labels: [Int], type: String = "hr") {
//        super.init()
//        self.labels = labels
//    }
}

class MyLeftAxisFormatterForMin: NSObject, IAxisValueFormatter {

//    var labels: [Int] = []

    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
//        if Int(value) >= labels.count {
//            return ""
//        }
        return String(Int(value)) + " Min"
    }
    
//    init(labels: [Int], type: String = "hr") {
//        super.init()
//        self.labels = labels
//    }
}
