//
//  DownloadOptionsVC.swift
//  ShivYog
//
//  Created by Ashish on 31/03/23.
//

import UIKit

class DownloadOptionCell : UITableViewCell{
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        self.applyStyle()
    }
    
    func applyStyle(){
        self.lblTitle.font(name: FontFamily.SFProDisplay.regular, size: 16).textColor(color: .app_000000)
    }
}

class DownloadOptionsVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblList: IntrinsicTableView!
    var completion : ((Files) -> Void)?
    //------------------------------------------------------
    
    //MARK: - Class Variable
    
    var arrData : [Files] = []
    
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.lblTitle.font(name: FontFamily.SFProDisplay.medium, size: 24).textColor(color: .app_000000)
        self.tblList.delegate = self
        self.tblList.dataSource = self
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    
    //------------------------------------------------------
    
}


extension DownloadOptionsVC  : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: DownloadOptionCell.self)
        let dataAtIndex = self.arrData[indexPath.row]
        cell.lblTitle.text = "%@ x %@".localized(dataAtIndex.height.description, dataAtIndex.width.description)
//        if indexPath.row == 0{
//            cell.lblTitle.text = "360 x 640"
//        }
//        else if indexPath.row == 1{
//            cell.lblTitle.text = "720 x 1280"
//        }
//        else if indexPath.row == 2{
//            cell.lblTitle.text = "540 x 960"
//        }
//        else if indexPath.row == 3{
//            cell.lblTitle.text = "240 x 426"
//        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dataAtIndex = self.arrData[indexPath.row]
        self.completion?(dataAtIndex)
        self.animateOut(sender: UIButton())
    }
}
