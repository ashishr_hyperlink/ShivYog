//
//  CategoryViewModel.swift
//  ShivYog
//
//  Created by 2022M30 on 02/12/22.
//

import Foundation
import UIKit


class CategoryViewModel {
    private var arrCategoryContent : [SubCategoryModel] = []
    private(set) var isSubDataFetchComplete = Bindable<Bool>()
    private var arrProductData : [CategoryContentModel] = []
    private(set) var isProductFetchComplete = Bindable<Bool>()
    
    func getSubCatData(catID : Int, withLoader isLoader : Bool = false, page : Int = 1){
        var dictData = Dictionary<String,String>()
        dictData["category__id"] = catID.description
        if !UserModel.selectedPref.isEmpty{
            dictData["tag__id__in"] = UserModel.selectedPref
        }
        dictData["page"] = page.description
        ApiManager.shared.makeRequest(endPoint: .content(.categorycontent), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                if page == 1 {
                    self.arrCategoryContent.removeAll()
                    self.arrCategoryContent = SubCategoryModel.listFromArray(data: apiData.data["results"].arrayValue).filter{!$0.categoryContent.isEmpty}
                } else {
                    self.arrCategoryContent.append(SubCategoryModel.listFromArray(data: apiData.data["results"].arrayValue).filter{!$0.categoryContent.isEmpty})
                }
                
                self.isSubDataFetchComplete.value = true
                break
            case .failure(let error):
                self.isSubDataFetchComplete.value = false
                print("the error \(error)")
            }
        }
    }
    
    func getSubCatCount() -> Int{
        return self.arrCategoryContent.count
    }
    
    func getSubCatAtIndex(index : Int) -> SubCategoryModel{
        return self.arrCategoryContent[index]
    }
}

extension CategoryViewModel{
    func getContentData(catID : Int, isFrom : IsFrom, withLoader isLoader : Bool = false, searchText : String = "",isFromSearch : Bool = false, page: Int = 1){
        if isFrom == .home || isFrom == .categories{
            self.apiGetMediaContent(catID: catID, withLoader: isLoader, page: page)
        }
        else if isFrom == .yogaStore {
            self.apiGetStoreMediaContent(withLoader: isLoader, storeSubContentId: catID, search: searchText, isFromSearch: isFromSearch, page: page)
        }
        else if isFrom == .favourites{
            self.apiGetFavouriteContent(withLoader: isLoader, searchText: searchText, isFromSearch: isFromSearch, page: page)
        }
        else{
            self.arrProductData = []//Array(repeating:CategoryContentModel(fromJson: JSON(["image" : UIImage.temp8.name, "title" : "Kathopanishad Vol 1", "contentType" : ContentType.video.rawValue])), count: 2)
            self.isProductFetchComplete.value = true
        }
    }
    
    func setContentData(data : [CategoryContentModel]){
        self.arrProductData = data
        self.isProductFetchComplete.value = true
    }
    
    private func apiGetMediaContent(catID : Int, withLoader isLoader : Bool = false, page: Int = 1){
        var dictData = Dictionary<String,String>()
        dictData["subcategorycontent__id"] = catID.description
        if !UserModel.selectedPref.isEmpty{
            dictData["tag__id__in"] = UserModel.selectedPref
        }
        dictData["page"] = page.description
        ApiManager.shared.makeRequest(endPoint: .content(.mediacontent), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                
                if page == 1 {
                    self.arrProductData.removeAll()
                    self.arrProductData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)
                } else {
                    self.arrProductData.append(CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue))
                }
                
                self.isProductFetchComplete.value = true
                break
            case .failure(let error):
                self.isProductFetchComplete.value = false
                print("the error \(error)")
            }
        }
    }
    
    private func apiGetSubCatContent(catID : Int, withLoader isLoader : Bool = false){
        var dictData = Dictionary<String,String>()
        dictData["categorycontent__id"] = catID.description
        if !UserModel.selectedPref.isEmpty{
            dictData["tag__id__in"] = UserModel.selectedPref
        }
        ApiManager.shared.makeRequest(endPoint: .content(.subcategorycontent), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                self.arrProductData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)
                
                self.isProductFetchComplete.value = true
                break
            case .failure(let error):
                print("the error \(error)")
            }
        }
    }
    
    private func apiGetStoreMediaContent(withLoader isLoader : Bool = false, storeSubContentId: Int, search: String = "", isFromSearch: Bool = false, page: Int = 1) {
        var dictData = Dictionary<String,String>()
        
        if isFromSearch {
            dictData["search"] = search
        }
        
        dictData["storesubcontent__id"] = storeSubContentId.description
        
        if !UserModel.selectedPref.isEmpty {
            dictData["tag__id__in"] = UserModel.selectedPref
        }
        dictData["page"] = page.description
        ApiManager.shared.makeRequest(endPoint: .store(.storeMediacontent), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true) { [weak self] (result) in
            
            guard let self = self else { return }
            switch result {
            case .success(let apiData):
                if page == 1 {
                    self.arrProductData.removeAll()
                    self.arrProductData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue)
                } else {
                    self.arrProductData.append(CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue))
                }
                self.isProductFetchComplete.value = true
            case .failure(let error):
                self.isProductFetchComplete.value = false
                print("the error \(error)")
            }
            
        }
        
    }
    
    private func apiGetFavouriteContent(withLoader isLoader : Bool = false, searchText : String = "", isFromSearch : Bool, page: Int = 1){
        ApiManager.shared.cancelRequest(endpoint: .content(.favourite))
        var dictData = Dictionary<String,String>()
//        self.arrProductData.removeAll()

        if isFromSearch {
            if searchText.trim().isEmpty{
                self.arrProductData.removeAll()
                self.isProductFetchComplete.value = true
                return
            }
            dictData["search"] = searchText
        }
        dictData["page"] = page.description
        ApiManager.shared.makeRequest(endPoint: .content(.favourite), methodType: .get, parameter: nil, queryParameters: dictData, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                if page == 1 {
                    self.arrProductData.removeAll()
                    self.arrProductData = CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue/*.flatMap{$0["media_content_details"]}*/)
                } else {
                    self.arrProductData.append(CategoryContentModel.listFromArray(data: apiData.data["results"].arrayValue))
                }
                
                
                self.isProductFetchComplete.value = true
                break
            case .failure(let error):
                self.isProductFetchComplete.value = false
                print("the error \(error)")
            }
        }
    }
    
    func getPlaylistContent(withLoader isLoader : Bool = false, playListID : Int){
        var dictData = Dictionary<String,String>()
        dictData["id"] = playListID.description
        ApiManager.shared.makeRequest(endPoint: .playlist(.playlist), methodType: .get, parameter: nil, withErrorAlert: false, withLoader: isLoader, withdebugLog: true){ [weak self] (result) in
            
            guard let self = self else { return }
            switch result{
            case .success(let apiData):
                
                self.isProductFetchComplete.value = true
                break
            case .failure(let error):
                self.isProductFetchComplete.value = true
                print("the error \(error)")
            }
        }
    }
    
    func getMediaCount() -> Int{
        return self.arrProductData.count
    }
    
    func getMediaAtIndex(index : Int) -> CategoryContentModel{
        return self.arrProductData[index]
    }
    
    func deleteItemAtIndex(index : Int){
        self.arrProductData.remove(at: index)
    }
    
    func getFilterdData(type: ContentType) -> [CategoryContentModel] {
        return self.arrProductData.filter { $0.contentType == type && !$0.isSubscribe}
    }
}



