//
//  CategoryListVC.swift
//  ShivYog
//
//  Created by 2022M30 on 02/12/22.
//

import UIKit

class SelectCategoryVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var lblSelectCategory: UILabel!
    @IBOutlet weak var tblList: SelfSizedTableView!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var categoryVM : CategoryViewModel = CategoryViewModel()
    var completion : ((String?, Int?) -> Void)?
    var strTitle : String?
    var index: Int = 0
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.navigationController?.clearNavigation(textColor: .appFfffff, navigationColor: .app_990000)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.clearNavigation()
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.applyStyle()
        self.setupViewModelObserver()
    }

    private func applyStyle(){
        self.lblSelectCategory.font(name: FontFamily.SFProDisplay.bold, size: 18.0).textColor(color: .app_222222)
        self.tblList.registerNib(forCellWithClass: SelectCategoryCell.self)
        self.tblList.delegate = self
        self.tblList.dataSource = self
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
    
        /*self.viewModel.loginResult.bind(observer: { [unowned self] (result) in
         switch result {
         case .success(_):
            // Redirect to next screen or home screen
            //                UIApplication.shared.setHome()
            break
            
        case .failure(let error):
            Alert.shared.showSnackBar(error.errorDescription ?? "", isError: true)
            
        case .none: break
        }
         })*/
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        self.sheetViewController?.animateOut()
//        self.completion?(self.strTitle, index)
    }
    
    
    //------------------------------------------------------
    
}

//MARK: - UITableView delegate and datasource -

extension SelectCategoryVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return GlobalAPI.shared.getNumberOfCategories()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: SelectCategoryCell.self, for: indexPath)
        let dataAtIndex = GlobalAPI.shared.getDataAtIndexForCategories(index: indexPath.row)
        cell.data = dataAtIndex
        if dataAtIndex.isSelected {
            self.strTitle = dataAtIndex.title
        }
//        self.strTitle = dataAtIndex.isSelect ? dataAtIndex.title : ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        GlobalAPI.shared.updateDataForCategories(index: indexPath.row)
        let dataAtIndex = GlobalAPI.shared.getDataAtIndexForCategories(index: indexPath.row)
        if dataAtIndex.isSelected {
            self.strTitle = dataAtIndex.title
            self.index = indexPath.row
        }
        self.tblList.reloadData()
        self.sheetViewController?.animateOut()
        self.completion?(self.strTitle, self.index)
    }
}
