//
//  CategoriesVC.swift
//  ShivYog
//
//  Created by 2022M30 on 02/12/22.
//

import UIKit

class CategoriesVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet var vwTitle: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnTitle: UIButton!
    
    //------------------------------------------------------
    
    //MARK: - Class Variable
    private var catVM : CategoryViewModel = CategoryViewModel()
    var index : Int = -1
    var currentPage = 1
    var isCallingApi = true
    //------------------------------------------------------
    
    //MARK: - Memory Management Method
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        debugPrint("‼️‼️‼️ deinit : \(self.classForCoder) ‼️‼️‼️")
    }
    
    //------------------------------------------------------
    
    //MARK: - Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        debugPrint("load")
        setUpView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
        self.navigationController?.clearNavigation(textColor: .appFfffff, navigationColor: .app_990000)
        AppDelegate.shared.addAnalyticsEvent(screenName: .Category, eventName: .Pageview, category: .Category)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.clearNavigation(textColor: .appFfffff, navigationColor: .app_990000)
        if ((self.tabBarController as? TabVC)?.miniPlayer.isHidden ?? true){
            self.tblList.addFooterView(bottomSpace: 0)
        }
        else{
            self.tblList.addFooterView(bottomSpace: kMiniPlayerConstant)
        }
        self.tblList.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.clearNavigation()
//        (UIApplication.topViewController()?.parent?.parent as? TabVC)?.addview()
    }
    
    //------------------------------------------------------
    
    //MARK: - Custom Method
    
    private func setUpView() {
        self.setupViewModelObserver()
        self.applyStyle()
    }

    private func applyStyle(){
//        self.navigationItem.titleView = self.vwTitle
        self.tblList.registerNib(forCellWithClass: CategoriesCell.self)
        self.tblList.delegate = self
        self.tblList.dataSource = self
        self.btnTitle.font(name: FontFamily.SFProDisplay.bold, size: 18.0).textColor(color: .appFfffff).imageRightSide()
        self.btnTitle.centerTextAndImage(spacing: 15.0)
        self.lblTitle.font(name: FontFamily.SFProDisplay.bold, size: 18.0).textColor(color: .appFfffff)
        var title = ""
        if let selectedCat = GlobalAPI.shared.getSelectedCategories(){
            title = selectedCat.title
            
        }
        self.btnTitle.setTitle(title, for: .normal)
        self.tblList.addRefreshControl {
            self.currentPage = 1
            self.isCallingApi = true
            self.fetCatData()
        }
        self.fetCatData(true)
    }
    
    func fetCatData(_ withLoader : Bool = false){
        if let selectedCat = GlobalAPI.shared.getSelectedCategories(){
            self.catVM.getSubCatData(catID: selectedCat.id, withLoader: withLoader, page: self.currentPage)
        }
        else{
            self.tblList.endRefreshing()
        }
    }

    /**
     Setup all view model observer and handel data and erros.
     */
    private func setupViewModelObserver() {
        // Result binding observer
        
        self.catVM.isSubDataFetchComplete.bind(observer: { [unowned self] (result) in
            self.tblList.emptyDataSetSource = self
            self.tblList.emptyDataSetDelegate = self
            if result ?? false{
                self.isCallingApi = true
                self.currentPage += 1
                self.tblList.endRefreshing()
                self.tblList.reloadData()
            }
            else{
                self.isCallingApi = false
                self.tblList.endRefreshing()
            }
        })
        
        GlobalAPI.shared.isCategoriesFetchComplete.bind { [unowned self] (result) in
            if result ?? false{
                self.fetCatData()
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: - Action Method
    @IBAction func btnSearchClicked(_ sender: UIBarButtonItem) {
        let vc = StoryboardScene.Home.searchVC.instantiate()
        vc.isFrom = .categories
        if let selectedCat = GlobalAPI.shared.getSelectedCategories(){
            vc.categoryId = selectedCat.id
        }
        vc.modalPresentationStyle = .overFullScreen
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func btnFilterClicked(_ sender: UIBarButtonItem) {
        let vc = StoryboardScene.Profile.preferencesVC.instantiate()
        vc.completion = { isSuccess in
            if isSuccess{
                self.currentPage = 1
                self.isCallingApi = true
                self.fetCatData()
                self.tblList.startRefreshing()
            }
        }
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnTitleClicked(_ sender: UIButton) {
        let vc = StoryboardScene.Categories.selectCategoryVC.instantiate()
        vc.modalPresentationStyle = .overFullScreen
        vc.hidesBottomBarWhenPushed = true
        vc.index = self.index
        vc.completion = { str, id in
            self.index = id ?? 0
            self.btnTitle.setTitle(str, for: .normal)
            self.btnTitle.setTitle(str, for: .selected)
            self.currentPage = 1
            self.isCallingApi = true
            self.fetCatData()
            self.tblList.startRefreshing()
            AppDelegate.shared.addAnalyticsEvent(screenName: .Category, eventName: .ChangeCategory, category: .Category, label: str ?? "")
        }

        self.tabBarController?.openBottomSheetAnimate(vc: vc, sizes: [.percent(0.7)])
    }
    
    //------------------------------------------------------
    
}

//MARK: - UITableView delegate and Datasource -

extension CategoriesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.catVM.getSubCatCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: CategoriesCell.self, for: indexPath)
        let dataAtIndex = self.catVM.getSubCatAtIndex(index: indexPath.row)
        cell.index = indexPath.row
        cell.btnSeeAll.isHidden = true
        cell.data = dataAtIndex
        cell.completion = { str, selCat in
            let vc = StoryboardScene.Home.productItemsVC.instantiate()
            vc.screenTitle = str
            vc.selCat = selCat
            vc.contentId = selCat.id
            vc.isFrom = .categories
            self.navigationController?.pushViewController(vc, animated: true)
        }
        return cell
    }
}

extension CategoriesVC {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.isAtBottom {
            if self.isCallingApi {
                self.isCallingApi = false
                self.fetCatData()
            }
        }
    }
}
