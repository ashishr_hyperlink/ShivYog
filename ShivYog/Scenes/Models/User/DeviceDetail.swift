//
//	DeviceDetail.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class DeviceDetail{
    
    var deviceName : String!
    var deviceToken : String!
    var deviceType : String!
    var id : Int!
    var imeiNumber : String!
    var ipAddress : String!
    var modelName : String!
    var osVersion : String!
    var user : Int!
    var uuidNumber : String!
    var token : String!
    var isSelected : Bool = false
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        deviceName = json["device_name"].stringValue
        deviceToken = json["device_token"].stringValue
        deviceType = json["device_type"].stringValue
        id = json["id"].intValue
        imeiNumber = json["imei_number"].stringValue
        ipAddress = json["ip_address"].stringValue
        modelName = json["model_name"].stringValue
        osVersion = json["os_version"].stringValue
        user = json["user"].intValue
        uuidNumber = json["uuid_number"].stringValue
        token = json["token"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if deviceName != nil{
            dictionary["device_name"] = deviceName
        }
        if deviceToken != nil{
            dictionary["device_token"] = deviceToken
        }
        if deviceType != nil{
            dictionary["device_type"] = deviceType
        }
        if id != nil{
            dictionary["id"] = id
        }
        if imeiNumber != nil{
            dictionary["imei_number"] = imeiNumber
        }
        if ipAddress != nil{
            dictionary["ip_address"] = ipAddress
        }
        if modelName != nil{
            dictionary["model_name"] = modelName
        }
        if osVersion != nil{
            dictionary["os_version"] = osVersion
        }
        if user != nil{
            dictionary["user"] = user
        }
        if uuidNumber != nil{
            dictionary["uuid_number"] = uuidNumber
        }
        if token != nil{
            dictionary["token"] = token
        }
        return dictionary
    }
    
    static func listFromArray(data : [JSON]) -> [DeviceDetail]{
        var arrData :  [DeviceDetail] = []
        for item in data{
            arrData.append(DeviceDetail(fromJson: item))
        }
        return arrData
    }
}
