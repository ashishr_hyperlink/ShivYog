//
//	UserModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON

class UserModel : Mappable{
    
    var token : String!
    var age : Int!
    var country : String!
    var dateOfBirth : String!
    var deviceDetail : [DeviceDetail]!
    var district : String!
    var email : String!
    var fullname : String!
    var gender : String!
    var id : Int!
    var countryCode : String!
    var phone : String!
    var phoneNumber : String!
    var state : String!
    var uuid : String!
    var isReachable : Bool!
    var otp : String!
    var password : String!
    var tag : [Int]!
    var subscriptionPaymentDetails : SubscriptionDetails!
    var dateJoined : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    required init(fromJson json: JSON){
        if json.isEmpty{
            return
        }
        token = json["token"].stringValue
        age = json["age"].intValue
        country = json["country"].stringValue
        dateOfBirth = json["date_of_birth"].stringValue
        deviceDetail = [DeviceDetail]()
        let deviceDetailArray = json["device_detail"].arrayValue
        for deviceDetailJson in deviceDetailArray{
            let value = DeviceDetail(fromJson: deviceDetailJson)
            deviceDetail.append(value)
        }
        district = json["district"].stringValue
        email = json["email"].stringValue
        fullname = json["fullname"].stringValue
        gender = json["gender"].stringValue
        id = json["id"].intValue
        phone = json["phone"].stringValue
        state = json["state"].stringValue
        uuid = json["uuid"].stringValue
        isReachable = json["is_reachable"].boolValue
        countryCode = json["country_code"].stringValue
        phoneNumber = json["phone_number"].stringValue
        otp = json["otp"].stringValue
        password = json["password"].stringValue
        tag = json["tag"].arrayValue.flatMap{$0.intValue}
        subscriptionPaymentDetails = SubscriptionDetails(fromJson: json["subscription_payment_details"])
        dateJoined = json["date_joined"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if token != nil{
            dictionary["token"] = token
        }
        if age != nil{
            dictionary["age"] = age
        }
        if country != nil{
            dictionary["country"] = country
        }
        if dateOfBirth != nil{
            dictionary["date_of_birth"] = dateOfBirth
        }
        if deviceDetail != nil{
            var dictionaryElements = [[String:Any]]()
            for deviceDetailElement in deviceDetail {
                dictionaryElements.append(deviceDetailElement.toDictionary())
            }
            dictionary["device_detail"] = dictionaryElements
        }
        if district != nil{
            dictionary["district"] = district
        }
        if email != nil{
            dictionary["email"] = email
        }
        if fullname != nil{
            dictionary["fullname"] = fullname
        }
        if gender != nil{
            dictionary["gender"] = gender
        }
        if id != nil{
            dictionary["id"] = id
        }
        if phone != nil{
            dictionary["phone"] = phone
        }
        if state != nil{
            dictionary["state"] = state
        }
        if uuid != nil{
            dictionary["uuid"] = uuid
        }
        if isReachable != nil {
            dictionary["is_reachable"] = isReachable
        }
        if countryCode != nil{
            dictionary["country_code"] = countryCode
        }
        if phoneNumber != nil{
            dictionary["phone_number"] = phoneNumber
        }
        if otp != nil{
            dictionary["otp"] = otp
        }
        if password != nil{
            dictionary["password"] = password
        }
        if tag != nil{
            dictionary["tag"] = tag
        }
        if subscriptionPaymentDetails != nil{
            dictionary["subscription_payment_details"] = subscriptionPaymentDetails.toDictionary()
        }
        if dateJoined != nil{
            dictionary["date_joined"] = dateJoined
        }
        return dictionary
    }
}

extension UserModel {
    /**
     Return true if user is gurst user
     */
    static var isGuestUser : Bool {
        return !(UserModel.isUserLoggedIn && UserModel.isVerifiedUser)
    }
    
    /**
     Return true if user loggedin
     */
    static var isUserLoggedIn : Bool { 
        return ((UserDefaults.standard.value(forKey: UserDefaults.Keys.currentUser) != nil) && (UserModel.accessToken != nil))
    }
    
    /**
     Return true if verified by OTP
     */
    static var isVerifiedUser : Bool {
        return UserDefaults.standard.value(forKey: UserDefaults.Keys.authorization) != nil
    }
    
    /**
     Return current loggedin user data and set data
     */
    static var currentUser : UserModel? {
        set{
            guard let unwarrapedKey = newValue else {
                return
            }
            UserDefaults.standard.set(unwarrapedKey.toDictionary(), forKey: UserDefaults.Keys.currentUser)
            UserDefaults.standard.synchronize()
            
        } get {
            guard let userDict =  UserDefaults.standard.object(forKey: UserDefaults.Keys.currentUser) else {
                return nil
            }
            return UserModel(fromJson: JSON(userDict))
        }
    }
    
    static var selectedPref : String{
        if let userData = UserModel.currentUser, userData.tag.count > 0{
            return ((userData.tag.flatMap{String($0)})).joined(separator: ",") ?? ""
            
        }
        return ""
    }
    
    /**
     Return and sets login user token for session management
     */
    static var accessToken: String? {
        set{
            guard let unwrappedKey = newValue else{
                return
            }
            UserDefaults.standard.set(unwrappedKey, forKey: UserDefaults.Keys.accessToken)
            UserDefaults.standard.synchronize()
            
        } get {
            return UserDefaults.standard.value(forKey: UserDefaults.Keys.accessToken) as? String
        }
    }
    
    /**
     Remove all current user login data
     */
    class func removeCurrentUser(){
        if UserDefaults.standard.value(forKey: UserDefaults.Keys.currentUser) != nil {
            UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.currentUser)
            UserDefaults.standard.synchronize()
        }
    }
    
    static var isSubscribeUser : Bool{
        if let currentUser = self.currentUser, let subscribeData = currentUser.subscriptionPaymentDetails{
            return subscribeData.isSubscribe
        }
        return false
    }
        
    class func canOpenMediaWithoutSubcription(content : CategoryContentModel) -> Bool{
            if content.isSubscribe{
                if !UserModel.isSubscribeUser{
                    Alert.shared.showAlert("Please subscribe to watch this content", actionOkTitle : "Subscribe", actionCancelTitle : "Later", message: "") { isSuccess in
                        if isSuccess{
                            print("UserModel:-",UserModel.currentUser?.uuid ?? "")
                            print("DeviceManager:-",DeviceManager.shared.uuid)
//                            if let url = URL(string: WebViewLinks.subscription.title){
//                                  print("Subscribe URL:-",url)
//  //                                url = url.description + "?token=\(accessToken ?? "")"
//                                  UIApplication.shared.open(url)
//                              }
                            if let url = URL(string: WebViewLinks.subscription.title + "?token=\(self.accessToken ?? "")&uuid=\(UserModel.currentUser?.uuid ?? "")"){
                                print("Subscribe URL:-",url)
//                                url = url.description + "?token=\(accessToken ?? "")"
                                UIApplication.shared.open(url)
                            }
                        }
                    }
                    return false
                }
            }
            return true
        }
    
    class func isIndianCountryCode() -> Bool {
        if let user = UserModel.currentUser {
            if user.countryCode == "+91" {
                return true
            } else {
                return false
            }
        }
        return false
    }
}
