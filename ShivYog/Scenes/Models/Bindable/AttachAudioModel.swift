//
//	AttachAudioModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class AttachAudioModel : Mappable{

	var addedAt : String!
	var file : String!
	var id : Int!
	var title : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required init(fromJson json: JSON){
		if json.isEmpty{
			return
		}
		addedAt = json["added_at"].stringValue
		file = json["file"].stringValue
		id = json["id"].intValue
		title = json["title"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if addedAt != nil{
			dictionary["added_at"] = addedAt
		}
		if file != nil{
			dictionary["file"] = file
		}
		if id != nil{
			dictionary["id"] = id
		}
		if title != nil{
			dictionary["title"] = title
		}
		return dictionary
	}
    
    var asDictionary : [String:Any] {
        let mirror = Mirror(reflecting: self)
        let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String!, value:Any) -> (String, Any)? in
            guard let label = label else { return nil }
            return (label, value)
        }).compactMap { $0 })
        return dict
    }
    

    static func listFromArray(data : [JSON]) -> [AttachAudioModel]{
        var arrData :  [AttachAudioModel] = []
        for item in data{
            arrData.append(AttachAudioModel(fromJson: item))
        }
        return arrData
    }
}
