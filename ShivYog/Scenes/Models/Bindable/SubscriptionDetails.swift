//
//	SubscriptionDetails.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SubscriptionDetails {

	var enddate : String!
	var interval : String!
	var isCancel : Bool!
	var isSubscribe : Bool!
	var paymentid : String!
	var paymenttag : String!
	var planid : String!
	var price : String!
	var startdate : String!
	var subscriptionid : String!
    var id : Int!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
		enddate = json["enddate"].stringValue
		interval = json["interval"].stringValue
		isCancel = json["is_cancel"].boolValue
		isSubscribe = json["is_subscribe"].boolValue
		paymentid = json["paymentid"].stringValue
		paymenttag = json["paymenttag"].stringValue
		planid = json["planid"].stringValue
		price = json["price"].stringValue
		startdate = json["startdate"].stringValue
		subscriptionid = json["subscriptionid"].stringValue
        id = json["id"].intValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if enddate != nil{
			dictionary["enddate"] = enddate
		}
		if interval != nil{
			dictionary["interval"] = interval
		}
		if isCancel != nil{
			dictionary["is_cancel"] = isCancel
		}
		if isSubscribe != nil{
			dictionary["is_subscribe"] = isSubscribe
		}
		if paymentid != nil{
			dictionary["paymentid"] = paymentid
		}
		if paymenttag != nil{
			dictionary["paymenttag"] = paymenttag
		}
		if planid != nil{
			dictionary["planid"] = planid
		}
		if price != nil{
			dictionary["price"] = price
		}
		if startdate != nil{
			dictionary["startdate"] = startdate
		}
		if subscriptionid != nil{
			dictionary["subscriptionid"] = subscriptionid
		}
        if id != nil{
            dictionary["id"] = id
        }
		return dictionary
	}

}
