//
//	Result.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class BadgesModel : Mappable{

	var category : [Int]!
	var descriptionField : String!
	var id : Int!
	var image : String!
	var isEarned : Bool!
	var storecategory : [Int]!
	var time : Int!
	var title : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required init(fromJson json: JSON){
		if json.isEmpty{
			return
		}
		category = [Int]()
		let categoryArray = json["category"].arrayValue
		for categoryJson in categoryArray{
			category.append(categoryJson.intValue)
		}
		descriptionField = json["description"].stringValue
		id = json["id"].intValue
		image = json["image"].stringValue
		isEarned = json["is_earned"].boolValue
		storecategory = [Int]()
		let storecategoryArray = json["storecategory"].arrayValue
		for storecategoryJson in storecategoryArray{
			storecategory.append(storecategoryJson.intValue)
		}
		time = json["time"].intValue
		title = json["title"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if category != nil{
			dictionary["category"] = category
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if isEarned != nil{
			dictionary["is_earned"] = isEarned
		}
		if storecategory != nil{
			dictionary["storecategory"] = storecategory
		}
		if time != nil{
			dictionary["time"] = time
		}
		if title != nil{
			dictionary["title"] = title
		}
		return dictionary
	}

    static func listFromArray(data : [JSON]) -> [BadgesModel]{
        var arrData :  [BadgesModel] = []
        for item in data{
            arrData.append(BadgesModel(fromJson: item))
        }
        return arrData
    }
    
}
