//
//	Files.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class Files : Mappable{

	var codec : String!
	var createdTime : String!
	var fps : Int!
	var height : Int!
	var link : String!
	var linkExpirationTime : String!
	var md5 : String!
	var size : Int!
	var type : String!
	var width : Int!
    var quality : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
    required init(fromJson json: JSON){
		if json.isEmpty{
			return
		}
		codec = json["codec"].stringValue
		createdTime = json["created_time"].stringValue
		fps = json["fps"].intValue
		height = json["height"].intValue
		link = json["link"].stringValue
		linkExpirationTime = json["link_expiration_time"].stringValue
		md5 = json["md5"].stringValue
		size = json["size"].intValue
		type = json["type"].stringValue
		width = json["width"].intValue
        quality = json["quality"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if codec != nil{
			dictionary["codec"] = codec
		}
		if createdTime != nil{
			dictionary["created_time"] = createdTime
		}
		if fps != nil{
			dictionary["fps"] = fps
		}
		if height != nil{
			dictionary["height"] = height
		}
		if link != nil{
			dictionary["link"] = link
		}
		if linkExpirationTime != nil{
			dictionary["link_expiration_time"] = linkExpirationTime
		}
		if md5 != nil{
			dictionary["md5"] = md5
		}
		if size != nil{
			dictionary["size"] = size
		}
		if type != nil{
			dictionary["type"] = type
		}
		if width != nil{
			dictionary["width"] = width
		}
        if quality != nil {
            dictionary["quality"] = quality
        }
		return dictionary
	}
    
    var asDictionary : [String:Any] {
        let mirror = Mirror(reflecting: self)
        let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String!, value:Any) -> (String, Any)? in
            guard let label = label else { return nil }
            return (label, value)
        }).compactMap { $0 })
        return dict
    }

    static func listFromArray(data : [JSON]) -> [Files]{
        var arrData :  [Files] = []
        for item in data{
            arrData.append(Files(fromJson: item))
        }
        return arrData
    }
}
