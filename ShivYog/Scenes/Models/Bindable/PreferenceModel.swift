//
//	PreferenceModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class PreferenceModel : Mappable{

	var id : Int!
	var image : String!
	var slug : String!
	var title : String!
    var isSelected : Bool = false

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required init(fromJson json: JSON){
		if json.isEmpty{
			return
		}
		id = json["id"].intValue
		image = json["image"].stringValue
		slug = json["slug"].stringValue
		title = json["title"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if slug != nil{
			dictionary["slug"] = slug
		}
		if title != nil{
			dictionary["title"] = title
		}
        if isSelected != nil{
            dictionary["isSelected"] = isSelected
        }
		return dictionary
	}
    
    var asDictionary : [String:Any] {
        let mirror = Mirror(reflecting: self)
        let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String!, value:Any) -> (String, Any)? in
            guard let label = label else { return nil }
            return (label, value)
        }).compactMap { $0 })
        return dict
    }
    
    static func listFromArray(data : [JSON]) -> [PreferenceModel]{
        var arrData :  [PreferenceModel] = []
        for item in data{
            arrData.append(PreferenceModel(fromJson: item))
        }
        return arrData
    }
}
