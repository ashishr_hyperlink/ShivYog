//
//	Result.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class MyProductsModel : Mappable{

	var id : Int!
	var mediaContent : [CategoryContentModel]!
	var redeemCode : String!
	var storeMediaContent : [CategoryContentModel]!
	var storeMedias : [CategoryContentModel]!
	var storeSubcontentDetails : CategoryContentModel!
	var storesubcontent : Int!
	var subscriptionid : String!
	var user : UserModel!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required init(fromJson json: JSON){
		if json.isEmpty{
			return
		}
		id = json["id"].intValue
		mediaContent = [CategoryContentModel]()
		let mediaContentArray = json["media_content"].arrayValue
		for mediaContentJson in mediaContentArray{
            let value = CategoryContentModel(fromJson: mediaContentJson)
			mediaContent.append(value)
		}
		redeemCode = json["redeem_code"].stringValue
		storeMediaContent = [CategoryContentModel]()
		let storeMediaContentArray = json["store_media_content"].arrayValue
		for storeMediaContentJson in storeMediaContentArray{
            let value = CategoryContentModel(fromJson: storeMediaContentJson)
			storeMediaContent.append(value)
		}
		storeMedias = [CategoryContentModel]()
		let storeMediasArray = json["store_medias"].arrayValue
		for storeMediasJson in storeMediasArray{
			let value = CategoryContentModel(fromJson: storeMediasJson)
			storeMedias.append(value)
		}
		let storeSubcontentDetailsJson = json["store_subcontent_details"]
		if !storeSubcontentDetailsJson.isEmpty{
			storeSubcontentDetails = CategoryContentModel(fromJson: storeSubcontentDetailsJson)
		}
		storesubcontent = json["storesubcontent"].intValue
		subscriptionid = json["subscriptionid"].stringValue
		let userJson = json["user"]
		if !userJson.isEmpty{
			user = UserModel(fromJson: userJson)
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if id != nil{
			dictionary["id"] = id
		}
		if mediaContent != nil{
            var dictionaryElements = [[String:Any]]()
            for mediaContentElement in mediaContent {
                dictionaryElements.append(mediaContentElement.toDictionary())
            }
			dictionary["media_content"] = dictionaryElements
		}
		if redeemCode != nil{
			dictionary["redeem_code"] = redeemCode
		}
		if storeMediaContent != nil{
            var dictionaryElements = [[String:Any]]()
            for storeMediaContentElement in storeMediaContent {
                dictionaryElements.append(storeMediaContentElement.toDictionary())
            }
			dictionary["store_media_content"] = dictionaryElements
		}
		if storeMedias != nil{
			var dictionaryElements = [[String:Any]]()
			for storeMediasElement in storeMedias {
				dictionaryElements.append(storeMediasElement.toDictionary())
			}
			dictionary["store_medias"] = dictionaryElements
		}
		if storeSubcontentDetails != nil{
			dictionary["store_subcontent_details"] = storeSubcontentDetails.toDictionary()
		}
		if storesubcontent != nil{
			dictionary["storesubcontent"] = storesubcontent
		}
		if subscriptionid != nil{
			dictionary["subscriptionid"] = subscriptionid
		}
		if user != nil{
			dictionary["user"] = user.toDictionary()
		}
		return dictionary
	}

    static func listFromArray(data : [JSON]) -> [MyProductsModel]{
        var arrData :  [MyProductsModel] = []
        for item in data{
            arrData.append(MyProductsModel(fromJson: item))
        }
        return arrData
    }

}
