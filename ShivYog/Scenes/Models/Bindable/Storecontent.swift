//
//	Storecontent.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class Storecontent : Mappable{

	var androidBundleId : String!
	var descriptionField : String!
	var id : Int!
	var image : String!
	var iosBundleId : String!
	var price : String!
	var slug : String!
	var storecontent : Int!
	var title : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required init(fromJson json: JSON){
		if json.isEmpty{
			return
		}
		androidBundleId = json["android_bundle_id"].stringValue
		descriptionField = json["description"].stringValue
		id = json["id"].intValue
		image = json["image"].stringValue
		iosBundleId = json["ios_bundle_id"].stringValue
		price = json["price"].stringValue
		slug = json["slug"].stringValue
		storecontent = json["storecontent"].intValue
		title = json["title"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if androidBundleId != nil{
			dictionary["android_bundle_id"] = androidBundleId
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if iosBundleId != nil{
			dictionary["ios_bundle_id"] = iosBundleId
		}
		if price != nil{
			dictionary["price"] = price
		}
		if slug != nil{
			dictionary["slug"] = slug
		}
		if storecontent != nil{
			dictionary["storecontent"] = storecontent
		}
		if title != nil{
			dictionary["title"] = title
		}
		return dictionary
	}

    static func listFromArray(data : [JSON]) -> [Storecontent]{
        var arrData :  [Storecontent] = []
        for item in data{
            arrData.append(Storecontent(fromJson: item))
        }
        return arrData
    }
    
}
