//
//	SubCategoryModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SubCategoryModel : Mappable{

	var category : Int!
	var categoryContent : [CategoryContentModel]!
	var id : Int!
	var slug : String!
	var title : String!
    var storecontent : [CategoryContentModel]!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required init(fromJson json: JSON){
		if json.isEmpty{
			return
		}
		category = json["category"].intValue
		categoryContent = [CategoryContentModel]()
        if !json["category_content"].arrayValue.isEmpty {
            let categoryContentArray = json["category_content"].arrayValue
            for categoryContentJson in categoryContentArray{
                let value = CategoryContentModel(fromJson: categoryContentJson)
                categoryContent.append(value)
            }
        }
//        if !json["subcategory_content"].arrayValue.isEmpty {
//            let subCategoryContentArray = json["subcategory_content"].arrayValue
//            for categoryContentJson in subCategoryContentArray{
//                let value = CategoryContentModel(fromJson: categoryContentJson)
//                categoryContent.append(value)
//            }
//        }
		id = json["id"].intValue
		slug = json["slug"].stringValue
		title = json["title"].stringValue
        storecontent = [CategoryContentModel]()
        let storecontentArray = json["storecontent"].arrayValue
        for storecontentJson in storecontentArray{
            let value = CategoryContentModel(fromJson: storecontentJson)
            storecontent.append(value)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if category != nil{
			dictionary["category"] = category
		}
		if categoryContent != nil{
			var dictionaryElements = [[String:Any]]()
			for categoryContentElement in categoryContent {
				dictionaryElements.append(categoryContentElement.toDictionary())
			}
			dictionary["category_content"] = dictionaryElements
		}
		if id != nil{
			dictionary["id"] = id
		}
		if slug != nil{
			dictionary["slug"] = slug
		}
		if title != nil{
			dictionary["title"] = title
		}
        if storecontent != nil{
            var dictionaryElements = [[String:Any]]()
            for storecontentElement in storecontent {
                dictionaryElements.append(storecontentElement.toDictionary())
            }
            dictionary["storecontent"] = dictionaryElements
        }
		return dictionary
	}
    
    static func listFromArray(data : [JSON]) -> [SubCategoryModel]{
        var arrData :  [SubCategoryModel] = []
        for item in data{
            arrData.append(SubCategoryModel(fromJson: item))
        }
        return arrData
    }
}
