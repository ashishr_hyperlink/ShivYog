//
//	CategoryModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class CategoryModel : Mappable{

	var content : String!
	var id : Int!
	var image : String!
	var isSelected : Bool!
	var slug : String!
	var title : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required init(fromJson json: JSON){
		if json.isEmpty{
			return
		}
		content = json["content"].stringValue
		id = json["id"].intValue
		image = json["image"].stringValue
		isSelected = json["isSelected"].boolValue
		slug = json["slug"].stringValue
		title = json["title"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if content != nil{
			dictionary["content"] = content
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if isSelected != nil{
			dictionary["isSelected"] = isSelected
		}
		if slug != nil{
			dictionary["slug"] = slug
		}
		if title != nil{
			dictionary["title"] = title
		}
		return dictionary
	}
    
    static func listFromArray(data : [JSON]) -> [CategoryModel]{
        var arrData :  [CategoryModel] = []
        for item in data{
            arrData.append(CategoryModel(fromJson: item))
        }
        return arrData
    }
}
