//
//  DownloadModel.swift
//  ShivYog
//
//  Created by 2022M30 on 21/03/23.
//

import Foundation
import UIKit
import SwiftyJSON

class DownloadModel : NSObject, NSCoding, Codable{
    var fileContent : JSON!
    var isDownloaded : Bool!
    var isThumbDownload : Bool!
    var isFileDownload : Bool!
    var isAttachedFileDownload : Bool!
    var downloadProgress : Double!
//    var tasks : [URLSessionDownloadTask]!
    
    enum Keys : String {
        case content = "content"
        case isDownloadComplete = "isDownloadComplete"
        case isThumbDownload = "isThumbDownload"
        case isFileDownload = "isFileDownload"
        case isAttachedFileDownload = "isAttachedFileDownload"
        case downloadProgress = "downloadProgress"
//        case tasks = "tasks"
    }
    
    init(fileContent: JSON, isThumbDownloaded: Bool = false, isFileDownloaded: Bool = false, isAttachedFileDownload : Bool = false, isDownloaded: Bool = false, downloadProgress : Double = 0.0/*, tasks: [URLSessionDownloadTask] = []*/) {
        self.fileContent = fileContent
        self.isDownloaded = isDownloaded
        self.isThumbDownload = isThumbDownloaded
        self.isFileDownload = isFileDownloaded
        self.isAttachedFileDownload = isAttachedFileDownload
        self.downloadProgress = downloadProgress
//        self.tasks = tasks
    }

    required convenience init(coder decoder: NSCoder) {
        let content = decoder.decodeObject(forKey: Keys.content.rawValue) as! JSON
        let isDownloaded = decoder.decodeBool(forKey: Keys.isDownloadComplete.rawValue)
        let isThumbDownloaded = decoder.decodeBool(forKey: Keys.isThumbDownload.rawValue)
        let isFileDownloaded = decoder.decodeBool(forKey: Keys.isFileDownload.rawValue)
        let isAttachedFileDownload = decoder.decodeBool(forKey: Keys.isAttachedFileDownload.rawValue)
        let downloadProgress = decoder.decodeDouble(forKey: Keys.downloadProgress.rawValue)
//        let tasks = decoder.decodeObject(forKey: Keys.tasks.rawValue) as! [URLSessionDownloadTask]
        
        self.init(fileContent: content, isThumbDownloaded: isThumbDownloaded, isFileDownloaded: isFileDownloaded, isAttachedFileDownload : isAttachedFileDownload, isDownloaded: isDownloaded, downloadProgress: downloadProgress/*,tasks: tasks*/)
    }
    
    
    func encode(with coder: NSCoder) {
        coder.encode(fileContent, forKey: Keys.content.rawValue)
        coder.encode(isDownloaded, forKey: Keys.isDownloadComplete.rawValue)
        coder.encode(isThumbDownload, forKey: Keys.isThumbDownload.rawValue)
        coder.encode(isFileDownload, forKey: Keys.isFileDownload.rawValue)
        coder.encode(isAttachedFileDownload, forKey: Keys.isAttachedFileDownload.rawValue)
        coder.encode(downloadProgress, forKey: Keys.downloadProgress.rawValue)
//        coder.encode(tasks, forKey: Keys.tasks.rawValue)
    }
    
}

class DownloadMainModel : NSObject, NSCoding, Codable {
    var data : [DownloadModel]!
    
    init(modelData: [DownloadModel]) {
        self.data = modelData
    }
    
    required convenience init(coder decoder: NSCoder) {
        let data = decoder.decodeObject(forKey: "modelData") as! [DownloadModel]
        self.init(modelData: data)
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(data, forKey: "modelData")
    }
    
}

enum DownloadFileType : String {
    case pdf = "pdf"
    case video = "video"
    case audio = "audio"
    case attachedFile = "attachedFile"
    case thumbImage = "thumbImage"
    case none = "none"
}

enum DownloadStatus {
    case downloading, completed
}


class DownloadTaskModel {
    var downloadTask : URLSessionDownloadTask!
    var contentModel : CategoryContentModel!
    var fileType : ContentType!
    var downloadingFileType : DownloadFileType!
    var status : DownloadStatus!
    var localFileLocation : String!
    
    init( downloadTask : URLSessionDownloadTask, contentModel : CategoryContentModel, fileType : ContentType, downloadingFileType : DownloadFileType, status : DownloadStatus, localFileLocation : String) {
        self.downloadTask = downloadTask
        self.contentModel = contentModel
        self.fileType = fileType
        self.downloadingFileType = downloadingFileType
        self.status = status
        self.localFileLocation = localFileLocation
    }
}
