//
//	AppUsage.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class AppUsageModel : Mappable{

	var appUsageTime : Int!
	var date : String!
    var endDate : String!
    var startDate : String!
    var status : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required init(fromJson json: JSON){
		if json.isEmpty{
			return
		}
		appUsageTime = json["app_usage_time"].intValue
		date = json["date"].stringValue
        endDate = json["end_date"].stringValue
        startDate = json["start_date"].stringValue
        status = json["status"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if appUsageTime != nil{
			dictionary["app_usage_time"] = appUsageTime
		}
		if date != nil{
			dictionary["date"] = date
		}
        if endDate != nil{
            dictionary["end_date"] = endDate
        }
        if startDate != nil{
            dictionary["start_date"] = startDate
        }
        if status != nil{
            dictionary["status"] = status
        }
		return dictionary
	}
    
    static func listFromArray(data : [JSON]) -> [AppUsageModel]{
        var arrData :  [AppUsageModel] = []
        for item in data{
            arrData.append(AppUsageModel(fromJson: item))
        }
        return arrData
    }
}
