//
//	SubcategorycontentDetail.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SubcategorycontentDetailModel : Mappable{

	var categorycontent : Int!
	var featuredContent : Bool!
	var id : Int!
	var image : String!
	var slug : String!
	var title : String!
	var whatsnew : Bool!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required init(fromJson json: JSON){
		if json.isEmpty{
			return
		}
		categorycontent = json["categorycontent"].intValue
		featuredContent = json["featured_content"].boolValue
		id = json["id"].intValue
		image = json["image"].stringValue
		slug = json["slug"].stringValue
		title = json["title"].stringValue
		whatsnew = json["whatsnew"].boolValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if categorycontent != nil{
			dictionary["categorycontent"] = categorycontent
		}
		if featuredContent != nil{
			dictionary["featured_content"] = featuredContent
		}
		if id != nil{
			dictionary["id"] = id
		}
		if image != nil{
			dictionary["image"] = image
		}
		if slug != nil{
			dictionary["slug"] = slug
		}
		if title != nil{
			dictionary["title"] = title
		}
		if whatsnew != nil{
			dictionary["whatsnew"] = whatsnew
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         categorycontent = aDecoder.decodeObject(forKey: "categorycontent") as? Int
         featuredContent = aDecoder.decodeObject(forKey: "featured_content") as? Bool
         id = aDecoder.decodeObject(forKey: "id") as? Int
         image = aDecoder.decodeObject(forKey: "image") as? String
         slug = aDecoder.decodeObject(forKey: "slug") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String
         whatsnew = aDecoder.decodeObject(forKey: "whatsnew") as? Bool

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if categorycontent != nil{
			aCoder.encode(categorycontent, forKey: "categorycontent")
		}
		if featuredContent != nil{
			aCoder.encode(featuredContent, forKey: "featured_content")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if slug != nil{
			aCoder.encode(slug, forKey: "slug")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}
		if whatsnew != nil{
			aCoder.encode(whatsnew, forKey: "whatsnew")
		}

	}
    
    
    var asDictionary : [String:Any] {
        let mirror = Mirror(reflecting: self)
        let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String!, value:Any) -> (String, Any)? in
            guard let label = label else { return nil }
            return (label, value)
        }).compactMap { $0 })
        return dict
    }

}
