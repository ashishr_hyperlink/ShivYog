//
//	CategoryContent.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class CategoryContentModel : Mappable{
    
    var categorycontent : Int!
    var featuredContent : Bool!
    var id : Int!
    var image : String!
    var slug : String!
    var title : String!
    var contentType : ContentType!
    var attachAudioFile : [AttachAudioModel]!
    var tag : [PreferenceModel]!
    var file : String!
    var videoUrl : String!
    var description : String!
    var mediaContent : [CategoryContentModel] = []
    var favourite : Bool!
    var duration : Double!
    var playlistMediaId : Int!
    var watchedDuration : Int!
    var interval : Int!
    var isSubscribe : Bool!
    var files : [Files]!
    var subcategorycontentDetails : SubcategorycontentDetailModel!
    var androidBundleId : String!
    var iosBundleId : String!
    var storecontent : Int!
    var content : String!
    var storesubcontent : Int!
    var storesubcontentDetails : CategoryContentModel!
    var redeemCode : String!
    var subscriptionid : String!
    var user : UserModel!
    var storecontentDetails : CategoryContentModel!
    var dollarPrice : String!
    var inrPrice : String!
    var isExclusive : Bool!
    var isPurchase : Bool!
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    required init(fromJson json: JSON){
        if json.isEmpty{
            return
        }
        var json = json
        watchedDuration = json["watched_duration"].intValue
        interval = json["interval"].intValue
        if !json["media_content_details"].isEmpty{
            json = json["media_content_details"]
        }
        if !json["store_media_content_details"].isEmpty {
             json = json["store_media_content_details"]
        }
        categorycontent = json["categorycontent"].intValue
        featuredContent = json["featured_content"].boolValue
        id = json["id"].intValue
        image = json["image"].stringValue
        slug = json["slug"].stringValue
        title = json["title"].stringValue
        contentType = ContentType(rawValue: json["content_type"].stringValue) ?? ContentType.folder
        tag = PreferenceModel.listFromArray(data: json["tag"].arrayValue)
        file = json["file"].stringValue
        videoUrl = json["video_url"].stringValue
        description = json["description"].stringValue
        attachAudioFile = AttachAudioModel.listFromArray(data: json["attach_audio_file"].arrayValue)
        let mediaContentArray = json["media_content"].arrayValue
        for mediaContentJson in mediaContentArray{
            let value = CategoryContentModel(fromJson: mediaContentJson)
            mediaContent.append(value)
        }
        if !json["store_media_content"].arrayValue.isEmpty {
            let storeMediaContentArray = json["store_media_content"].arrayValue
            for storeMediaContentJson in storeMediaContentArray{
                let value = CategoryContentModel(fromJson: storeMediaContentJson)
                mediaContent.append(value)
            }
        }
        if !json["store_medias"].arrayValue.isEmpty {
            let storeMediasArray = json["store_medias"].arrayValue
            for storeMediasJson in storeMediasArray{
                let value = CategoryContentModel(fromJson: storeMediasJson)
                mediaContent.append(value)
            }
        }
        if !json["store_exclusive_medias"].arrayValue.isEmpty {
            let storeMediasArray = json["store_exclusive_medias"].arrayValue
            for storeMediasJson in storeMediasArray{
                let value = CategoryContentModel(fromJson: storeMediasJson)
                mediaContent.append(value)
            }
        }
        if !json["content_exclusive_medias"].arrayValue.isEmpty {
            let storeMediasArray = json["content_exclusive_medias"].arrayValue
            for storeMediasJson in storeMediasArray{
                let value = CategoryContentModel(fromJson: storeMediasJson)
                mediaContent.append(value)
            }
        }
        if !json["playlists_details"].arrayValue.isEmpty{
            let playlistContentArray = json["playlists_details"].arrayValue
            for playlistContentJson in playlistContentArray{
                if !playlistContentJson["media"].isEmpty {
                    let value = CategoryContentModel(fromJson: playlistContentJson["media"])
                    value.playlistMediaId = playlistContentJson["id"].intValue
                    mediaContent.append(value)
                }
                if !playlistContentJson["store_media"].isEmpty {
                    let value = CategoryContentModel(fromJson: playlistContentJson["store_media"])
                    value.playlistMediaId = playlistContentJson["id"].intValue
                    mediaContent.append(value)
                }
            }
        }
        
        
        if image.isEmpty, let mediaData = mediaContent.first, !mediaData.image.isEmpty{
            image = mediaData.image
        }
        favourite = json["favourite"].boolValue
        duration = json["duration"].doubleValue
        playlistMediaId = json["playlist_media_id"].intValue
        isSubscribe = json["is_subscribe"].boolValue
        
        files = Files.listFromArray(data: json["files"].arrayValue)
        let subcategorycontentDetailsJson = json["subcategorycontent_details"]
        if !subcategorycontentDetailsJson.isEmpty{
            subcategorycontentDetails = SubcategorycontentDetailModel(fromJson: subcategorycontentDetailsJson)
        }
        androidBundleId = json["android_bundle_id"].stringValue
        iosBundleId = json["ios_bundle_id"].stringValue
        storecontent = json["storecontent"].intValue
        content = json["content"].stringValue
        storesubcontent = json["storesubcontent"].intValue
        let storesubcontentDetailsJson = json["storesubcontent_details"]
        if !storesubcontentDetailsJson.isEmpty{
            storesubcontentDetails = CategoryContentModel(fromJson: storesubcontentDetailsJson)
        }
        let storeSubcontentDetailsJson = json["store_subcontent_details"]
        if !storeSubcontentDetailsJson.isEmpty{
            storecontentDetails = CategoryContentModel(fromJson: storeSubcontentDetailsJson)
            title = storecontentDetails.title
            image = storecontentDetails.image
        }
        let subCategoryContentDetailsJson = json["subcategory_content_details"]
        if !subCategoryContentDetailsJson.isEmpty{
            storecontentDetails = CategoryContentModel(fromJson: subCategoryContentDetailsJson)
            title = storecontentDetails.title
            image = storecontentDetails.image
        }
        redeemCode = json["redeem_code"].stringValue
        subscriptionid = json["subscriptionid"].stringValue
        let userJson = json["user"]
        if !userJson.isEmpty{
            user = UserModel(fromJson: userJson)
        }
        dollarPrice = json["dollar_price"].stringValue
        inrPrice = json["inr_price"].stringValue
        isExclusive = json["is_exclusive"].boolValue
        isPurchase = json["is_purchase"].boolValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if categorycontent != nil{
            dictionary["categorycontent"] = categorycontent
        }
        if featuredContent != nil{
            dictionary["featured_content"] = featuredContent
        }
        if id != nil{
            dictionary["id"] = id
        }
        if image != nil{
            dictionary["image"] = image
        }
        if slug != nil{
            dictionary["slug"] = slug
        }
        if title != nil{
            dictionary["title"] = title
        }
        if attachAudioFile != nil{
            dictionary["attach_audio_file"] = attachAudioFile
        }
        if favourite != nil{
            dictionary["favourite"] = favourite
        }
        if duration != nil{
            dictionary["duration"] = duration
        }
        if playlistMediaId != nil{
            dictionary["playlist_media_id"] = playlistMediaId
        }
        if watchedDuration != nil{
            dictionary["watched_duration"] = watchedDuration
        }
        if interval != nil{
            dictionary["interval"] = interval
        }
        if isSubscribe != nil{
            dictionary["is_subscribe"] = isSubscribe
        }
        if subcategorycontentDetails != nil{
            dictionary["subcategorycontent_details"] = subcategorycontentDetails.toDictionary()
        }
        if androidBundleId != nil{
            dictionary["android_bundle_id"] = androidBundleId
        }
        if iosBundleId != nil{
            dictionary["ios_bundle_id"] = iosBundleId
        }
        if storecontent != nil{
            dictionary["storecontent"] = storecontent
        }
        if content != nil{
            dictionary["content"] = content
        }
        if storesubcontent != nil{
            dictionary["storesubcontent"] = storesubcontent
        }
        if storesubcontentDetails != nil{
            dictionary["storesubcontent_details"] = storesubcontentDetails.toDictionary()
        }
        if storecontentDetails != nil{
            dictionary["store_subcontent_details"] = storecontentDetails.toDictionary()
        }
        if redeemCode != nil{
            dictionary["redeem_code"] = redeemCode
        }
        if subscriptionid != nil{
            dictionary["subscriptionid"] = subscriptionid
        }
        if user != nil{
            dictionary["user"] = user.toDictionary()
        }
        if dollarPrice != nil{
            dictionary["dollar_price"] = dollarPrice
        }
        if inrPrice != nil{
            dictionary["inr_price"] = inrPrice
        }
        if isExclusive != nil{
            dictionary["is_exclusive"] = isExclusive
        }
        if isPurchase != nil{
            dictionary["is_purchase"] = isPurchase
        }
        return dictionary
    }
    
    var asDictionary : [String:Any] {
        let mirror = Mirror(reflecting: self)
        let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String!, value:Any) -> (String, Any)? in
            guard let label = label else { return nil }
            return (label, value)
        }).compactMap { $0 })
        return dict
    }
    
    static func listFromArray(data : [JSON]) -> [CategoryContentModel]{
        var arrData :  [CategoryContentModel] = []
        for item in data{
            arrData.append(CategoryContentModel(fromJson: item))
        }
        return arrData
    }
}
