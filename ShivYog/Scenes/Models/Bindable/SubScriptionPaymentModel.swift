//
//	Result.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class SubScriptionPaymentModel : Mappable{

	var browserName : String!
	var browserVersion : String!
	var enddate : String!
	var id : Int!
	var interval : String!
	var isCancel : Bool!
	var message : String!
	var osName : String!
	var osVersion : String!
	var paymentCurrency : String!
	var paymentid : String!
	var paymenttag : String!
	var planid : String!
	var price : String!
	var startdate : String!
	var subscriptionid : String!
	var user : UserModel!
    var created : String!
    var storesubcontent : Int!
    var storesubcontentDetails : CategoryContentModel!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required init(fromJson json: JSON){
		if json.isEmpty{
			return
		}
		browserName = json["browser_name"].stringValue
		browserVersion = json["browser_version"].stringValue
		enddate = json["enddate"].stringValue
		id = json["id"].intValue
		interval = json["interval"].stringValue
		isCancel = json["is_cancel"].boolValue
		message = json["message"].stringValue
		osName = json["os_name"].stringValue
		osVersion = json["os_version"].stringValue
		paymentCurrency = json["payment_currency"].stringValue
		paymentid = json["paymentid"].stringValue
		paymenttag = json["paymenttag"].stringValue
		planid = json["planid"].stringValue
		price = json["price"].stringValue
		startdate = json["startdate"].stringValue
		subscriptionid = json["subscriptionid"].stringValue
		let userJson = json["user"]
		if !userJson.isEmpty{
			user = UserModel(fromJson: userJson)
		}
        if !json["created"].stringValue.isEmpty {
            startdate = json["created"].stringValue
        }
        storesubcontent = json["storesubcontent"].intValue
        let storesubcontentDetailsJson = json["storesubcontent_details"]
        if !storesubcontentDetailsJson.isEmpty{
            storesubcontentDetails = CategoryContentModel(fromJson: storesubcontentDetailsJson)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if browserName != nil{
			dictionary["browser_name"] = browserName
		}
		if browserVersion != nil{
			dictionary["browser_version"] = browserVersion
		}
		if enddate != nil{
			dictionary["enddate"] = enddate
		}
		if id != nil{
			dictionary["id"] = id
		}
		if interval != nil{
			dictionary["interval"] = interval
		}
		if isCancel != nil{
			dictionary["is_cancel"] = isCancel
		}
		if message != nil{
			dictionary["message"] = message
		}
		if osName != nil{
			dictionary["os_name"] = osName
		}
		if osVersion != nil{
			dictionary["os_version"] = osVersion
		}
		if paymentCurrency != nil{
			dictionary["payment_currency"] = paymentCurrency
		}
		if paymentid != nil{
			dictionary["paymentid"] = paymentid
		}
		if paymenttag != nil{
			dictionary["paymenttag"] = paymenttag
		}
		if planid != nil{
			dictionary["planid"] = planid
		}
		if price != nil{
			dictionary["price"] = price
		}
		if startdate != nil{
			dictionary["startdate"] = startdate
		}
		if subscriptionid != nil{
			dictionary["subscriptionid"] = subscriptionid
		}
		if user != nil{
			dictionary["user"] = user.toDictionary()
		}
//        if created != nil{
//            dictionary["created"] = created
//        }
        if storesubcontent != nil{
            dictionary["storesubcontent"] = storesubcontent
        }
        if storesubcontentDetails != nil{
            dictionary["storesubcontent_details"] = storesubcontentDetails.toDictionary()
        }
		return dictionary
	}
    
    static func listFromArray(data : [JSON]) -> [SubScriptionPaymentModel]{
        var arrData :  [SubScriptionPaymentModel] = []
        for item in data{
            arrData.append(SubScriptionPaymentModel(fromJson: item))
        }
        return arrData
    }
}
