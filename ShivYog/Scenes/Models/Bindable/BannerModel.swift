//
//	BannerModel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import SwiftyJSON


class BannerModel : Mappable{

	var appImage : String!
	var descriptionField : String!
	var id : Int!
	var link : String!
	var slug : String!
	var title : String!
	var webImage : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	required init(fromJson json: JSON){
		if json.isEmpty{
			return
		}
		appImage = json["app_image"].stringValue
		descriptionField = json["description"].stringValue
		id = json["id"].intValue
		link = json["link"].stringValue
		slug = json["slug"].stringValue
		title = json["title"].stringValue
		webImage = json["web_image"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if appImage != nil{
			dictionary["app_image"] = appImage
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if id != nil{
			dictionary["id"] = id
		}
		if link != nil{
			dictionary["link"] = link
		}
		if slug != nil{
			dictionary["slug"] = slug
		}
		if title != nil{
			dictionary["title"] = title
		}
		if webImage != nil{
			dictionary["web_image"] = webImage
		}
		return dictionary
	}
    
    static func listFromArray(data : [JSON]) -> [BannerModel]{
        var arrData :  [BannerModel] = []
        for item in data{
            arrData.append(BannerModel(fromJson: item))
        }
        return arrData
    }

}
