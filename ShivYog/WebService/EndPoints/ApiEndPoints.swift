//
//  ApiEndPoints.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 18/12/20.
//

import UIKit

/// ApiEndPoints - This will be main api points
enum ApiEndPoints {
    case user(User)
    case auth(Auth)
    case editProfile(EditProfile)
    case category(Category)
    case cms(CMS)
    case content(Content)
    case banner(Banner)
    case playlist(Playlist)
    case playlistMedia(PlaylistMedia)
    case store(Store)
    case payment(Payment)
    
    /// API methods name.
    var methodName : String {
        switch self {
        case .user(let user):
            return "custom-auth/users/" +  user.rawValue
            
        case .auth(let auth):
            return "custom-auth/auth/" + auth.rawValue
            
        case .editProfile(let edit):
            return "custom-auth/users/" + edit.methodName
            
        case .category(let category):
            return "category/" + category.rawValue
            
        case .cms(let cms):
            return "cms/" + cms.rawValue
            
        case .content(let content):
            return "content/" + content.rawValue
            
        case .banner(let banner):
            return "banner/" + banner.rawValue
            
        case .playlist(let playlist):
            return "playlist/" + playlist.rawValue
            
        case .playlistMedia(let media):
            return "playlist/" + media.methodName
            
        case .store(let store):
            return "store/" + store.rawValue
            
        case .payment(let payment):
            return "payment/" + payment.rawValue
        }
        
    }
}

/// User Endpoints
enum User: String {
    case forgotPassword                 = "reset-password-email/"
    case checkPasswordOtp               = "check-password-otp/"
    case resetPassword                  = "change-reset-password/"
    case logout                         = "logout/"
    case changePassword                 = "change-password/"
    case sendOtpForUpdatePhone          = "send-otp-for-update-phone/"
    case updatePhone                    = "update-phone/"
    case getDevice                      = "get-device/"
    case deviceLogout                   = "device-logout/"
}

enum EditProfile{
    case editProfile(String)
    
    var methodName : String{
        switch self {
        case .editProfile(let id):
            return id + "/"
        }
    }
}

/// Registration Endpoints
enum Auth: String {
    case sendOTP                        = "send_otp/"
    case login                          = "login/"
    case logout                         = "logout/"
    
}

enum Category : String{
    case tag                        =   "tag/"
    case category                   =   "category/"
}

enum CMS : String{
    case contactus                  = "contactus/"
}

enum Content : String{
    case catecontentSubcatecontent  =   "catecontent-subcatecontent/"
    case mediacontent               =   "mediacontent/"
    case categorycontent            =   "categorycontent/"
    case home                       =   "home/"
    case subcategorycontent         =   "subcategorycontent/"
    case favourite                  =   "favourite/"
    case feturecontent              =   "feturecontent/"
    case whatsnew                   =   "whatsnew/"
    case featuredplaylist           =   "featuredplaylist/"
    case continueWatching           =   "continue-watching/create_or_update/"
    case getContinueWatching        =   "continue-watching/"
    case featuredcontentSearch      =   "featuredcontent-search/"
    case whatsnewSearch             =   "whatsnew-search/"
    case myJourney                  =   "my-journey/"
    case myJourneyStatus            =   "my-journey-status/"
    case badges                     =   "badges/"
}

enum Banner : String{
    case Banner                     =   "banner/"
}

enum Playlist : String{
    case playlist                   =   "playlist/"
    case mediaplaylist              =   "mediaplaylist/"
    case featuredPlaylist           =   "featuredplaylist/"
    case featuredplaylistSearch     =   "featuredplaylist-search/"
    case mediaplaylistSearch        =   "mediaplaylist-search/"
}

enum PlaylistMedia{
    case addMedia(String)
    case deleteMedia(String)
    case deletePlaylist(String)
    
    var methodName : String{
        switch self {
        case .addMedia(let id):
            return "playlist/add_media/" + id + "/"
            
        case .deleteMedia(let id):
            return "playlist/delete_media/" + id + "/"
            
        case .deletePlaylist(let id):
            return "playlist/" + id + "/"
        }
    }
}

enum Store : String{
    case storeContent       =   "store-content/"
    case storeSubcontent    =   "store-subcontent/"
    case storeMediacontent  =   "store-mediacontent/"
    case storeProduct       =   "store-product/"
    case storeProductSearch =   "store-product-search/"
    case sendGift           =   "send-gift/"
    case sendGiftCheck      =   "send-gift/check/"
    case storeProductShare  =   "store-product-share/"
    case storeProductList   =   "store-product-list/"
}

enum Payment : String {
    case subscriptionPayment    =   "subscription-payment/"
    case storePayment           =   "store-payment/"
}
