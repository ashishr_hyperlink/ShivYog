//
//  NewApiManager.swift
//  MVVMBasicStructure
//
//  Created by KISHAN_RAJA on 18/12/20.
//

import Foundation
import UIKit
import Moya
import Alamofire

//------------------------------------------------------

//MARK:- API Manager

///Manage and create all type API reqeust.
class ApiManager : TargetType {
    
    var path: String = ""
    
    static var shared = ApiManager()
    
    let provider = MoyaProvider<ApiManager>()
    
    var sampleData: Data {
        return "Half measures are as bad as nothing at all.".utf8Encoded
    }
    
    var requests : [(endPoint : ApiEndPoints , cancellable : Moya.Cancellable)] = []
    
    var headers: [String : String]?
    
    ///Return base url according environment
    var environmentBaseURL : String {
        return APIEnvironment.getUrl(state: NetworkManager.environment).baseurl
    }
    
    // MARK: - baseURL
    var baseURL: URL {
        guard let url  = URL(string: environmentBaseURL) else{
            fatalError("base url could not be configured")
        }
        return url
    }
    
    var method: Moya.Method = .post
    
    // MARK: - parameterEncoding
    
    var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    
    // MARK: - task
    var task: Task {
        if method == .get {
            if !urlQueryParameters.isEmpty {
                return .requestParameters(parameters: urlQueryParameters, encoding: URLEncoding.default)
            }
            return .requestPlain
        }
        if method == .delete {
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
//            return .requestPlain
        }
        if method == .patch {
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        }
        return .requestParameters(parameters: parameters, encoding: self.method == . post ? JSONEncoding.default : URLEncoding.default)
    }
    
    ///Request parameters
    var parameters: [String: Any] = [:]
    var urlQueryParameters: [String: String] = [:]
    
//    let debugCryptoLib = CryptLib()
    
    /// Set API headers
    func setHeaders()  {
        var headersToSend : [String : String] = [:]
        headersToSend[ApiKeys.header(.apiKey).value] = ApiKeys.header(.apiKeyValue).value
        //        headersToSend[ApiKeys.header(.KHeaderLanguage).value] = Bundle.main.kCurrentAppLanguage.headerValue
        // Pass user login token
        if let token = UserModel.accessToken {
            debugPrint("USER LOGIN TOKEN : \(token)")
            headersToSend[ApiKeys.header(.tokenKey).value] = "Token " + token
        }
        //
        if self.method == .put {
            headersToSend[ApiKeys.header(.contentTypeKey).value] = ApiKeys.header(.contentTypeApplicationForm).value
        }
        else{
            headersToSend[ApiKeys.header(.contentTypeKey).value] = ApiKeys.header(.contentTypeApplicationJson).value
        }
        
        self.headers = headersToSend
    }
    
    /// Make HTTP request with json responce.
    /// - Parameters:
    ///   - endPoint: API URL end point
    ///   - methodType: HTTP method type
    ///   - parameter: HTTP request parameter
    ///   - isErrorAlert: Flag for showing error alert. Default true.
    ///   - isLoader: Flag for showing loader. Default true.
    ///   - isDebug: Falg for print debug info. Default true.
    ///   - completion: Get response of the request with data result.
    func makeRequest(endPoint: ApiEndPoints,
                     methodType: HTTPMethod = .post,
                     parameter: Dictionary<String,Any>?,
                     queryParameters : [String:String] = [:],
                     withErrorAlert isErrorAlert : Bool = true,
                     withLoader isLoader: Bool = true,
                     withdebugLog isDebug: Bool = true,
                     withBlock completion: ((Swift.Result<DataResult,Error>) -> Void)?) {
        
        
        //Assign Value to Moya Parameters
        self.path = endPoint.methodName
        self.parameters = parameter ?? [:]
        self.method = methodType
        self.urlQueryParameters = queryParameters
        
        setHeaders()
        
        if isLoader {
            self.addLoader()
        }
        
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        if isDebug {
            self.manageDebugRequest(parameters: self.parameters)
        }
        
        ///Create request
        let request = provider.request(self) { [weak self] (result) in
            guard let self = self else { return }
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if isLoader {
                self.removeLoader()
            }
            
            switch result {
            case .success(let response):
                
                let res = self.jsonData(response: response, endPoint: endPoint, isDebug: isDebug) ?? [:]
                let resDic  = JSON(res as Any)
                switch response.statusCode {
                case 200...299:
                    let responseData = DataResult(data: resDic,
                                                  httpCode: response.statusCode,
                                                  message: resDic[ApiKeys.respsone(.errors).value].stringValue,
                                                  response: resDic)
                    
                    completion?(.success(responseData))
                case 401:
                    if isErrorAlert {
                        Alert.shared.showSnackBar(resDic[ApiKeys.respsone(.errors).value].stringValue)
                    }
                    self.logout()
                default:
                    completion?(.failure(APIError.custom(message: resDic[ApiKeys.respsone(.errors).value].stringValue)))
                    if isErrorAlert {
                        Alert.shared.showSnackBar(resDic[ApiKeys.respsone(.errors).value].stringValue)
                    }
                }
                break
                
            case .failure(let error):
                
                if (error as NSError).code == NSURLErrorCancelled {
                    // Manage cancellation here
                    self.manageErrors(apiName: endPoint.methodName, error: error, isShowAlert: false)
                    completion?(.failure(error))
                    return
                }
                
                self.manageErrors(apiName: endPoint.methodName, error: error, isShowAlert: isErrorAlert)
                completion?(.failure(error))
                break
                
            }
            
        }
        
        requests.append((endPoint,request))
    }
    
    /// Make HTTP request with given model responce.
    /// - Parameters:
    ///   - endPoint: API URL end point
    ///   - modelType: Data model
    ///   - methodType: HTTP method type
    ///   - parameter: HTTP request parameter
    ///   - isErrorAlert: Flag for showing error alert. Default true.
    ///   - isLoader: Flag for showing loader. Default true.
    ///   - isDebug: Falg for print debug info. Default true.
    ///   - completion: Get response of the request with data result.
    func makeRequestWithModel<T: Mappable>(endPoint: ApiEndPoints,
                                           modelType: T.Type,
                                           methodType: HTTPMethod = .post,
                                           responseModelType: ResponseModelType = .dictonary,
                                           parameter: Dictionary<String,Any>?,
                                           queryParameters : [String:String] = [:],
                                           withErrorAlert isErrorAlert: Bool = true,
                                           withLoader isLoader: Bool = true,
                                           withdebugLog isDebug: Bool = true,
                                           withBlock completion: ((Swift.Result<DataResultModel<T>,Error>) -> Void)?)  {
        
        
        //Assign Value to Moya Parameters
        self.path = endPoint.methodName
        self.parameters = parameter ?? [:]
        self.method = methodType
        self.urlQueryParameters = queryParameters
        
        setHeaders()
        
        if isLoader {
            self.addLoader()
        }
        
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        if isDebug {
            self.manageDebugRequest(parameters: self.parameters)
        }
        
        ///Create request
        let request = provider.request(self) { [weak self] (result) in
            
            guard let self = self else { return }
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if isLoader {
                self.removeLoader()
            }
            
            switch result {
            case .success(let response):
                
                let res = self.jsonData(response: response, endPoint: endPoint, isDebug: isDebug) ?? [:]
                let resDic  = JSON(res as Any)
                switch response.statusCode {
                case 200...299:
                    let responseData = DataResultModel(data: T(fromJson: resDic), httpCode: response.statusCode, message: resDic[ApiKeys.respsone(.errors).value].stringValue, response: resDic)
                    completion?(.success(responseData))
                case 401:
                    if isErrorAlert {
                        Alert.shared.showSnackBar(resDic[ApiKeys.respsone(.errors).value].stringValue)
                    }
                    self.logout()
                default:
                    completion?(.failure(APIError.custom(message: resDic[ApiKeys.respsone(.errors).value].stringValue)))
                    if isErrorAlert {
                        Alert.shared.showSnackBar(resDic[ApiKeys.respsone(.errors).value].stringValue)
                    }
                }
                break
                
            case .failure(let error):
                
                if isLoader {
                    self.removeLoader()
                }
                
                if (error as NSError).code == NSURLErrorCancelled || (error as MoyaError)._code == 6{
                    // Manage cancellation here
                    self.manageErrors(apiName: endPoint.methodName, error: error, isShowAlert: false)
                    completion?(.failure(error))
                    return
                }
                
                self.manageErrors(apiName: endPoint.methodName, error: error, isShowAlert: isErrorAlert)
                completion?(.failure(error))
                break
            }
        }
        requests.append((endPoint,request))
    }
    
    /// Cancel all HTTP request task
    /// - Parameter url: API end point
    func cancelAllTask(url: ApiEndPoints) {
        provider.session.session.getAllTasks { (tasks) in
            let task = tasks.filter{ $0.currentRequest?.url?.absoluteString == self.baseURL.absoluteString + url.methodName}
            _ =  task.map ({(taskToCancel : URLSessionTask) in
                print("Cancel API")
                taskToCancel.cancel()
            })
        }
    }
    
    private func jsonData(response : Response, endPoint : ApiEndPoints, isDebug : Bool) -> Parameters? {
        do {
            guard let res = try response.mapJSON() as? Parameters else {
                if let resString = try response.mapString() as? String{
                    QLPlusLine()
                    QL1("\n==============================Decrypted Response==============================\n")
                    QL1(resString)
                } else{
                    return nil
                }
                
                return nil
            }
            if isDebug {
                self.manageDebugResponse(encryptedString: "", responseDic: JSON(res))
            }
            return res
        } catch let error {
            self.manageErrors(apiName: endPoint.methodName, error: error, isShowAlert: false)
        }
        return nil
    }
}
